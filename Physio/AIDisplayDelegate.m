classdef AIDisplayDelegate < handle
    properties  (SetAccess=private)
        hDisplay;
        channelList;
        h1;
        h2;
        h3;
        buffer1;
        buffer2;
        buffer3;
        acq_freq;
        save_buffer = [];
        save_data = 0;
        ecg_index = 0;
        resp_index = 0;
    end
    methods
        function obj = AIDisplayDelegate(hDisplay,acq_freq)
            obj.hDisplay = hDisplay;
            obj.acq_freq=acq_freq;
            % Create buffers, fix size to avoid memory creation
            % Hard coded time = 5 sec
            obj.buffer1=zeros(5*hDisplay.ai.sampRate,1);
            obj.buffer2=zeros(5*hDisplay.ai.sampRate,1);
            obj.buffer3=zeros(5*hDisplay.ai.sampRate,1);
            t=(1:5*hDisplay.ai.sampRate)/hDisplay.ai.sampRate;
            % Create empty plots just to get the handles
            obj.h1=plot(obj.hDisplay.axes1,t,obj.buffer1);
            obj.h2=plot(obj.hDisplay.axes2,t,obj.buffer2);
            obj.h3=plot(obj.hDisplay.axes3,t,obj.buffer3);
            xlabel('time (sec)')
        end
        
        function setDisplayChannels(obj,channelList)
            obj.channelList = channelList;
        end
        function setECG(obj,channel_num)
            obj.ecg_index = channel_num;
        end
        function setRESP(obj,channel_num)
            obj.resp_index = channel_num;
        end
        function setSaveData(obj,save_data)
            obj.save_data = save_data;
        end
        function reset(obj,filename)
            if(obj.save_data)
                aux=obj.save_buffer;
                save(fullfile(obj.hDisplay.save_dir,[filename,'.mat']),'aux')
            end
            obj.save_buffer =[];
        end
        function updategraph(obj,src,evt)
            nSampPts = src.hTask.everyNSamples;
            inData = readAnalogData(src.hTask,nSampPts,'native');
            obj.buffer1(1:end-nSampPts) = obj.buffer1(nSampPts+1:end);
            obj.buffer1(end-nSampPts+1:end)=inData(:,obj.channelList(1));
            obj.buffer2(1:end-nSampPts) = obj.buffer2(nSampPts+1:end);
            obj.buffer2(end-nSampPts+1:end)=inData(:,obj.channelList(2));
            obj.buffer3(1:end-nSampPts) = obj.buffer3(nSampPts+1:end);
            obj.buffer3(end-nSampPts+1:end)=inData(:,obj.channelList(3));
            
            % This is a faster plot, does not update the axes
            set(obj.h1,'YData',obj.buffer1);
            set(obj.h2,'YData',obj.buffer2);
            set(obj.h3,'YData',obj.buffer3);
            
            % Update ECG and RESP rate
            warning('off','signal:findpeaks:noPeaks')
            
            if(obj.ecg_index>0)
                ecg_data=double(inData(:,obj.ecg_index));
                max_value = max(ecg_data);
                median_value = median(ecg_data);
                threshold = median_value + 0.5 * (max_value-median_value); % arbitrary, but most p and q peaks should lie below this
                [~,locs] = findpeaks(ecg_data,'minpeakheight',threshold);
                ecg_rate=60/(mean(diff(locs))*obj.acq_freq);
                set(obj.hDisplay.bpm_text,'String',['BPM: ',num2str(ecg_rate)]);
            end
            
            if(obj.resp_index>0)
                resp_data=double(inData(:,obj.resp_index));
                max_value = max(resp_data);
                median_value = median(resp_data);
                threshold = median_value + 0.5 * (max_value-median_value); % arbitrary, but most p and q peaks should lie below this
                [~,locs] = findpeaks(resp_data,'minpeakheight',threshold);
                locs
                resp_rate=60/(mean(diff(locs))/obj.acq_freq)
                set(obj.hDisplay.resp_rate_text,'String',['RESP RATE: ',num2str(resp_rate)]);
                drawnow;
            end
            
            if( obj.save_data )
                obj.save_buffer=[obj.save_buffer;inData];
            end
        end
    end
end