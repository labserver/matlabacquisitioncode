function varargout = ai_config_ui(varargin)
% AI_CONFIG_UI MATLAB code for ai_config_ui.fig
%      AI_CONFIG_UI, by itself, creates a new AI_CONFIG_UI or raises the existing
%      singleton*.
%
%      H = AI_CONFIG_UI returns the handle to a new AI_CONFIG_UI or the handle to
%      the existing singleton*.
%
%      AI_CONFIG_UI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in AI_CONFIG_UI.M with the given input arguments.
%
%      AI_CONFIG_UI('Property','Value',...) creates a new AI_CONFIG_UI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ai_config_ui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ai_config_ui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ai_config_ui

% Last Modified by GUIDE v2.5 02-Oct-2013 17:04:31

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @ai_config_ui_OpeningFcn, ...
    'gui_OutputFcn',  @ai_config_ui_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before ai_config_ui is made visible.
function ai_config_ui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ai_config_ui (see VARARGIN)

% Choose default command line output for ai_config_ui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ai_config_ui wait for user response (see UIRESUME)
uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ai_config_ui_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = get(handles.uitable1,'Data');
varargout{2} = get(handles.device_popupmenu,'Value');
delete(handles.figure1);

% --- Executes on selection change in ai_popupmenu.
function ai_popupmenu_Callback(hObject, eventdata, handles)
% hObject    handle to ai_popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns ai_popupmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from ai_popupmenu


% --- Executes during object creation, after setting all properties.
function ai_popupmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ai_popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in signal_popupmenu.
function signal_popupmenu_Callback(hObject, eventdata, handles)
% hObject    handle to signal_popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns signal_popupmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from signal_popupmenu


% --- Executes during object creation, after setting all properties.
function signal_popupmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to signal_popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in add_pushbutton.
function add_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to add_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

ai=get(handles.ai_popupmenu,'Value')-1;
freq=str2double(get(handles.freq_edit,'String'));
signal= get(handles.signal_popupmenu,'Value');
signal_names= get(handles.signal_popupmenu,'String');
current_data = get(handles.uitable1,'Data');
current_data=[current_data;{ai,signal_names{signal},freq}];
set(handles.uitable1,'Data',current_data)

function freq_edit_Callback(hObject, eventdata, handles)
% hObject    handle to freq_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of freq_edit as text
%        str2double(get(hObject,'String')) returns contents of freq_edit as a double


% --- Executes during object creation, after setting all properties.
function freq_edit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to freq_edit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in save_config_pushbutton.
function save_config_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to save_config_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName,PathName] = uiputfile('*.cfg');
ai_config = get(handles.uitable1,'Data');
device = get(handles.device_popupmenu,'Value');
save(fullfile(PathName,FileName),'ai_config','device');


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isequal(get(hObject, 'waitstatus'), 'waiting')
    % The GUI is still in UIWAIT, us UIRESUME
    uiresume(hObject);
else
    % The GUI is no longer waiting, just close it
    delete(hObject);
end


% --- Executes during object creation, after setting all properties.
function uitable1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uitable1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
set(hObject,'Data',[]);


% --- Executes on button press in load_pushbutton.
function load_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to load_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName,PathName] = uigetfile('*.cfg');
load(fullfile(PathName,FileName),'-mat');
if ~isequal(FileName,0)
    set(handles.uitable1,'Data',ai_config)
end
set(handles.device_popupmenu,'Value', device);


% --- Executes on selection change in device_popupmenu.
function device_popupmenu_Callback(hObject, eventdata, handles)
% hObject    handle to device_popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns device_popupmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from device_popupmenu


% --- Executes during object creation, after setting all properties.
function device_popupmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to device_popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
