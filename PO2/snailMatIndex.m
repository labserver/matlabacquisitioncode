function iHelix=snailMatIndex(inMatDims, rotDir, growDir)
%% snailMatIndex
% The function calculates the snail/helix indexing of a matrix.
%
%% Syntax
%  iHelix=snailMatIndex(inMatDims);
%  iHelix=snailMatIndex(inMatDims, rotationDir);
%  iHelix=snailMatIndex(inMatDims, rotationDir, growthDir);
%
%% Description
% The iHelix returns indexes, allowing to treat matrix elemnt in a clock-wise
% (snail/escargot/helix) order.
%
%% Input arguments (defaults exist):
%	inMatDims- the indexed matrix dimentions.
% The remainign parameters are a optional values, with exisitng defaults
%   rotDir- a string specifying the helix rotation direction- clocwise 'CW' or ,
%       counter-clocwise 'CCW'. Default value is 'CW'.
%   growDirr- a string specifying the helix growth direction- inside (from top left corner
%       iniside) 'in' or , outside (from center- out) 'out'. Default value is 'in'.
%
%% Output arguments
%  iHelix-  a row vector including helix/snail ordered elements.
%
%% Issues & Comments
%
%% Example
% iMat=reshape(1:15, 3, 5)
% iHelix=snailMatIndex(size(iMat))
%
%% See also
%
%% Revision history
% First version: Nikolay S. 2012-08-27.
% Last update:   Nikolay S. 2012-09-01.
% 
% *List of Changes:*
% 2012-09-01, by  Nikolay S.: optional parameters "rotDir" and "growDir", specifiying
%   vertex rotation direction, and vertex growth direction added.

if nargin<2
    rotDir='cw';
end
if nargin<3
    growDir='in';
end

if ~strcmpi(growDir, 'in') % If growDir point was set to "in"- flip rotDir
    if strcmpi(rotDir, 'CW')
        rotDir='CCW';
    else
        rotDir='CW';
    end
end

if length(inMatDims)==1
    inMatDims=repmat(inMatDims, 1, 2);
end

nElemems=inMatDims(1)*inMatDims(2);
indMat =reshape(1:nElemems, inMatDims);
iHelix=[];

if strcmpi(rotDir, 'CCW')
    indMat=rot90CCW(indMat);
end

while ~isempty(indMat)
    if strcmpi(rotDir, 'CW')
        iHelix=cat( 2, iHelix, indMat(1, :) );
        indMat(1, :)=[];        % remove the current top row
        indMat=rot90CW(indMat);
    else
        iHelix=cat( 2, iHelix, indMat(1, end:-1:1) );
        indMat(1, :)=[];        % remove the current top row
        indMat=rot90CCW(indMat);
    end
end

if ~strcmpi(growDir, 'in') % If start point was set to center- flip vector
    iHelix =iHelix(end:-1:1);
end

%% Servise functions- rotation clocl-wies e and counter clock wise
function outMat=rot90CW(imMat)
outMat = imMat(:, end:-1:1).';

function outMat=rot90CCW(imMat)
outMat = imMat(end:-1:1, :).';