
% This function performs calibration between the camera space and galvo
% space. 
% Input: handles to the gui to enable control of galvo and camera
% Output: Matrix which translates galvo voltage space to camera pixels

function CalMat = calibration_transfer_matrix(handles)

% Turn on laser, with intensity at 80%
handles.laser_power.write([4]);
handles.hLaser.writeDigitalData(1);

points = 4;
num = 0;
axes(handles.camera) 
for i=-3.5:(7.0/points):3.5         % x axis
    for j = -3.0:(6.0/points):3.0   % y axis
        % Move laser
        handles.galvos.write([i,j]);
        pause(0.05);
        num = num+1;
        % Get image
        imdata = handles.im_camera.get_image();
        imagesc(imdata);
        pause(0.1);
        colormap(gray);
        axis off
        % Find laser position
        max_data = max(imdata(:));
        [max_x, max_y] = find(imdata==max_data);
        if length(max_x)>1
            x(num) = mean(max_x,1);
            y(num) = mean(max_y,1);
        else
            x(num) = max_x;
            y(num) = max_y;
        end
        galvo_volts(num,1) = i;
        galvo_volts(num,2) = j;
    end
end

% Turn off laser
handles.hLaser.writeDigitalData(0);

% Allow rotation and translation
image_pixels = [x',y'];
constant = ones(size(image_pixels,1),1);
image_pixels = [image_pixels,constant];

% Construct calibration matrix by pseudoinverse
CalMat = pinv(image_pixels)*galvo_volts;

% Recenter galvos
handles.galvos.write([0 0]);


end

