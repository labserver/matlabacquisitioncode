function varargout = interface(varargin)
% INTERFACE MATLAB code for interface.fig
%      INTERFACE, by itself, creates a new INTERFACE or raises the existing
%      singleton*.
%
%      H = INTERFACE returns the handle to a new INTERFACE or the handle to
%      the existing singleton*.
%
%      INTERFACE('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in INTERFACE.M with the given input arguments.
%
%      INTERFACE('Property','Value',...) creates a new INTERFACE or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before interface_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to interface_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help interface

% Last Modified by GUIDE v2.5 18-Dec-2013 13:32:34

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @interface_OpeningFcn, ...
    'gui_OutputFcn',  @interface_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before interface is made visible.
function interface_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to interface (see VARARGIN)

% Choose default command line output for interface
handles.output = hObject;

addpath('../');
import liom.*;

% Create Galvos and Laser control (power and on/off) as well as LED
handles.galvos = OnDemandAOClass('galvo','Dev1',2:3);
handles.laser_power = OnDemandAOClass('power','Dev1',1);
handles.hLaser = dabs.ni.daqmx.Task('pulse');
handles.hLaser.createDOChan('Dev1','port2/line4');
handles.LED = OnDemandAOClass('LED','Dev1',0);
handles.intensity=0;

% Create camera with default values and display start image
handles.im_camera=liom.CameraClass();
handles.exposure_time = 5;
set_exposure (handles.im_camera,handles.exposure_time);
axes(handles.camera);
imagesc(handles.im_camera.get_image(),'Parent',handles.camera);
colormap(gray);
axis off

handles.Syn_time = dabs.ni.daqmx.Task('Syn_time');%times for synchronizationn with electrophysiology
handles.Syn_time.createDOChan('Dev1','port0/line6');
handles.Syn_point = dabs.ni.daqmx.Task('Syn_point');%points for synchronization with electrophysiology
handles.Syn_point.createDOChan('Dev1','port0/line7');



% Default values, take them from interface so that they can be changed from 
% the gui without modifying this code
handles.line_num_points = str2double(get(handles.num_points_line,'String'));    %number of points on the line
handles.file_name = get(handles.file_name,'String');
handles.Pulse_Period = str2double(get(handles.pulse_period,'String'));
handles.Pulse_High_Time = str2double(get(handles.pulse_high_time,'String'));
handles.recording_time = 0;
handles.Points_Circle = str2double(get(handles.points_circles,'String'));       %number of points on spiral
handles.Num_Circles = str2double(get(handles.num_circles,'String'));            %number of circles on spiral
handles.n_avg = str2double(get(handles.num_average,'String'));                  %number of average times
handles.n_hist_points = str2double(get(handles.num_hist_points,'String'));      %number of points for histogram and recording
handles.point_num = 0;
handles.line_num = 0;

%***********directory for saving data
handles.path_name = uigetdir; %select file path
if ~exist([handles.path_name '\' date])
mkdir([handles.path_name '\'],date);
end
handles.exp_name = [handles.path_name '\' date];
fid_resumeExp = fopen([handles.exp_name '\resumeExp.txt'], 'a+');
fprintf(fid_resumeExp,'%s','Date: ');
fprintf(fid_resumeExp,'%s\n',date);
fprintf(fid_resumeExp,'%s\n%s\n','Animal no:','Beginning surgery :');
fprintf(fid_resumeExp,'%s\n%s\n','End of surgery : ','Beginning of recording :');
fprintf(fid_resumeExp,'%s\n%s\n%s\n','Stimulation area :', 'Recording Area :','Birth date :');
fprintf(fid_resumeExp,'%s\n%s\n%s\n','Weight :','Sex :','Anesthesia (induction) :');
fprintf(fid_resumeExp,'%s\n%s\n','Anesthesia (continuous) :','Initial hearth rythm :');
fprintf(fid_resumeExp,'%s\n%s\n','Comment on health of the animal :','Threshold : ');
fclose(fid_resumeExp);

% Timer functions, to revise
handles.timer_recording = timer('TimerFcn',{@recording_data,handles},'ExecutionMode', ...
    'fixedDelay');
handles.timer_aline = timer('TimerFcn',{@alignment,handles},'ExecutionMode', ...
    'fixedDelay');
handles.timer_camera = timer('TimerFcn',{@camera,handles},'ExecutionMode', ...
    'fixedDelay');

guidata(hObject, handles);

%  http://www.matlabsky.com/thread-646-1-1.html

% UIWAIT makes interface wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = interface_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes during object creation, after setting all properties.
function camera_CreateFcn(hObject, eventdata, handles)
% hObject    handle to camera (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in check_cal_pushbutton.
function check_cal_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to check_cal_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isfield(handles,'CalMat')
    handles.hLaser.writeDigitalData(1);
    axes(handles.camera);
    handles.im_camera.get_image();   
    pos=ginput(1);
    volts = [pos(1,2) pos(1,1) 1]*handles.CalMat;
    handles.galvos.write(volts);
    pause(0.03);
    imagesc(handles.im_camera.get_image(),'Parent',handles.camera);
    colormap(gray);  
    hold on;
    plot(pos(1,1),pos(1,2),'b*');
    hold off
    axis off;
else
    warndlg('Need to perform calibration first!');
end
handles.hLaser.writeDigitalData(0);
guidata(hObject, handles);

% --- Executes on slider movement.
function intensity_slider_Callback(hObject, eventdata, handles)
% hObject    handle to intensity_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.intensity = get(hObject,'Value');
set(handles.pulse_intensity,'string',num2str(get(hObject,'value'))); 
handles.laser_power.write(handles.intensity);
axes(handles.camera);
imagesc(handles.im_camera.get_image(),'Parent',handles.camera);
colormap(gray);
axis off;
guidata(hObject, handles);
 
% --- Executes during object creation, after setting all properties.
function intensity_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to intensity_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in delete_pushbutton.
function delete_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to delete_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% handles.LED.writeDigitalData(0);
handles.laser_power.write([0]);
handles.galvos.write([0 0]);
handles.LED.write([0]);
delete(handles.im_camera);
delete(handles.galvos);
delete(handles.LED);
delete(handles.hLaser);
delete(handles.laser_power);
delete(handles.Syn_time);
delete(handles.Syn_point);


% --- Executes on button press in calibration_pushbutton.
function calibration_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to calibration_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(hObject,'BackgroundColor','r');
set(hObject,'String', 'Ongoing!');
pause(0.03);
handles.CalMat = calibration_transfer_matrix(handles);
set(hObject,'BackgroundColor','g');
set(hObject,'String', 'Calibration is done!');
guidata(hObject, handles);



% --- Executes on button press in points_button.
function points_button_Callback(hObject, eventdata, handles)
% hObject    handle to points_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles,'points')
 handles = rmfield(handles, 'points');
 handles.point_num=0;
end
if ~isfield(handles,'LED_volts') || (~handles.LED_volts)
    warndlg('Turn on LED first!');
else
    good_point =1;
    axes(handles.camera);
    imagesc(handles.im_camera.get_image(),'Parent',handles.camera);
    colormap(gray);
    axis off;
    while (good_point)
        choice = questdlg('Would you like to select one point?',...
            'Choices',...
            'Yes','No','No');
        switch choice
            case 'Yes'
                good_point = 1;
            case 'No'
                good_point = 0;
        end
        if good_point
            h = impoint(gca,[]);
            pos = getPosition(h);
            handles.point_num=handles.point_num+1;
            handles.points(handles.point_num).pos = [pos(1,2) pos(1,1)];
            handles.points(handles.point_num).volts = [handles.points(handles.point_num).pos 1]*handles.CalMat;
        end
    end
end

guidata(hObject, handles);


% --- Executes on button press in line_button.
function line_button_Callback(hObject, eventdata, handles)
% hObject    handle to line_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles,'lines')
 handles = rmfield(handles, 'lines');
 handles.line_num =0;
end
if ~isfield(handles,'LED_volts')|| (~handles.LED_volts)
    warndlg('Turn on LED first!');
else
    
    good_line = 1;
    show_scan_points(handles);
    
    while (good_line)      
        choice = questdlg('Would you like to select one line?',...
            'Choices',...
            'Yes','No','No');       
        switch choice
            case 'Yes'
                good_line = 1;
            case 'No'
                good_line = 0;
        end
        if good_line
            h = imline(gca,[]);
            pos = wait(h);
            handles.line_num = handles.line_num+1;
            handles.lines(handles.line_num).pos = [pos(:,2) pos(:,1)];
            handles.lines(handles.line_num).pos1(:,1)=linspace(handles.lines(handles.line_num).pos(1,1),handles.lines(handles.line_num).pos(2,1),handles.line_num_points);
            handles.lines(handles.line_num).pos1(:,2)=linspace(handles.lines(handles.line_num).pos(1,2),handles.lines(handles.line_num).pos(2,2),handles.line_num_points);
            
            if (exist('handles.CalMat','var'))
                warndlg('Calibration first!');
            else
                handles.lines(handles.line_num).volts = [handles.lines(handles.line_num).pos1 ones(size(handles.lines(handles.line_num).pos1,1),1)]*handles.CalMat;
            end
        end
    end
end
guidata(hObject, handles);


function num_points_line_Callback(hObject, eventdata, handles)
% hObject    handle to num_points_line (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.line_num_points = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function num_points_line_CreateFcn(hObject, eventdata, handles)
% hObject    handle to num_points_line (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in recording_button. TODO
function recording_button_Callback(hObject, eventdata, handles)
% hObject    handle to recording_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.recording = get(hObject,'Value');
if handles.LED_volts
    warndlg('Turn off LED!');
else
    mkdir([handles.exp_name '\'],handles.file_name);
    handles.path_name1 = [handles.exp_name '\' handles.file_name];
    mkdir([handles.path_name1 '\'],'images');
    handles.path_nameImages = [handles.path_name1 '\' 'images'];
    mkdir([handles.path_name1 '\'],'Data');
    handles.path_nameData = [handles.path_name1 '\' 'Data'];
    mkdir([handles.path_name1 '\'],'Time and Position');
    handles.path_nameMatlab = [handles.path_name1 '\' 'Time and Position'];
    if isfield(handles,'timer_recording')
        if handles.recording
            handles.start_time = clock;
            Start_time = handles.start_time;
            save([handles.path_nameMatlab '\' 'Start_time.mat' ],'Start_time');
            set(hObject,'BackgroundColor','r');
            set(hObject,'string','Stop Recording');
            fid_resumeExp = fopen([handles.exp_name '\resumeExp.txt'], 'a');
            fprintf(fid_resumeExp,'%s','Exp name: ');
            fprintf(fid_resumeExp,'%s\n',handles.file_name);
            fprintf(fid_resumeExp,'%s','Start time : ');
            fprintf(fid_resumeExp,'%s\n',datestr(now,15));
            fclose(fid_resumeExp);
            
            recording_data(handles);
            set(handles.timer_recording, 'TimerFcn', {@recording_data, handles})
            start(handles.timer_recording);
        else
            if exist([handles.path_nameMatlab '\Start_time.mat'])
                load([handles.path_nameMatlab '\' 'Start_time.mat' ]);
                endtime = clock;
                handles.recording_time = etime(endtime,Start_time);
                fid_resumeExp = fopen([handles.exp_name '\resumeExp.txt'], 'a');
                fprintf(fid_resumeExp,'%s','Duration : ');
                fprintf(fid_resumeExp,'%d\n',handles.recording_time);
                fclose(fid_resumeExp);
                %save parameters on the interface
                parameters = struct();
                parameters.pulse_intensity = handles.intensity;
                parameters.pulse_period = handles.Pulse_Period;
                parameters.pulse_hight_time = handles.Pulse_High_Time;
                parameters.experiment_duraiton = handles.recording_time;
                parameters.num_points_on_line = handles.line_num_points;
                parameters.experiment_name = handles.file_name;
                parameters.camera_exposure_time = handles.exposure_time;
                parameters.num_average = handles.n_avg;
                parameters.num_hist_points = handles.n_hist_points;
                parameters.num_circles_on_spiral = handles.num_circles;
                parameters.points_on_spiral = handles.points_circle;
                save([handles.path_nameMatlab '\' 'Parameters on Interface.mat' ],'parameters');
                set(hObject,'BackGroundColor','g');
                set(hObject,'string','Start Recoring ');
                delete([handles.path_nameMatlab '\' 'Start_time.mat' ]);
                stop(handles.timer_recording);
            else
                warndlg('Restart the recording button again!');
            end
        end
    else  set(hObject,'string','Run','BackGroundColor','g', ...
            'value',0)
    end
end
guidata(hObject, handles);


function recording_data(obj,event,handles)
persistent flag_time
if(nargin == 1)
    flag_time = 0;
else
    try
        handles.Syn_time.writeDigitalData(1);
        flag_time = flag_time + 1;
        handles.Syn_time.writeDigitalData(0);
        num_all_points =0 ;
        laser_pulse=PulseClass(handles.Pulse_Period,handles.Pulse_High_Time,'Dev1',0);% laser pulse
        handles.laser_power.write([handles.intensity]); %Laser intensity will be changed during the experiment
        counter = CounterClass(handles.n_avg,handles.n_hist_points,handles.Pulse_Period);
        if isfield(handles,'points')
            for j = 1:size(handles.points,2)
                if ~isempty(handles.points(j).pos)
                    handles.galvos.write(handles.points(j).volts);
                    pause(0.03);
                    counter.start();
                    laser_pulse.start();
                    handles.Syn_point.writeDigitalData(1);
                    data=counter.read( );
                    handles.Syn_point.writeDigitalData(0);
                    laser_pulse.stop();
                    counter.stop();
                    num_all_points = num_all_points + 1;
                    if handles.n_avg>1
                        data = reshape(data',handles.n_hist_points,handles.n_avg);
                        data=(data((2:end),1:end)-data((1:end-1),1:end));
                        All_data(num_all_points,:) = sum(data,2)';
                    else data = data(2:end)-data(1:end-1);
                        All_data(num_all_points,:) = data(2:end)';
                    end
                    point_endtime = clock;
                    handles.recording_time1 = etime(point_endtime,handles.start_time );
                    set(handles.exp_time,'string',num2str(handles.recording_time1));
                end
            end
            All_points{1} = handles.points;
        end
        if isfield(handles,'lines')
            for i = 1:size(handles.lines,2)
                handles.lines(i).num_poins_on_line = handles.line_num_points;
                for j = 1 : handles.line_num_points
                    handles.galvos.write( [handles.lines(i).volts(j,1) handles.lines(i).volts(j,2)]);
                    pause(0.03);
                    counter.start();
                    laser_pulse.start();
                    handles.Syn_point.writeDigitalData(1);
                    data=counter.read( );
                    handles.Syn_point.writeDigitalData(0);
                    laser_pulse.stop();
                    counter.stop();
                    num_all_points =num_all_points + 1;
                    if handles.n_avg>1
                        data = reshape(data',handles.n_hist_points,handles.n_avg);
                        data=(data((2:end),1:end)-data((1:end-1),1:end));
                        All_data(num_all_points,:) = sum(data,2)';
                    else data = data(2:end)-data(1:end-1);
                        All_data(num_all_points,:) = data(2:end)';
                    end
                    endtime = clock;
                    handles.recording_time1  = etime(endtime,handles.start_time);
                    set(handles.exp_time,'string',num2str(handles.recording_time1 ));
                end
            end
            if exist('All_points')
                All_points{size(All_points,2)+1} = handles.lines;
            else All_points{1} = handles.lines;
            end
        end
        %****recording points on spiral
        if isfield(handles,'spirals')
            for i = 1:size(handles.spirals,2)
                handles.spirals(i).num_circles = handles.num_circles;
                handles.spirals(i).num_points = handles.points_circle;
                for j = 1 : handles.points_circle
                    handles.galvos.write( [handles.spirals(i).volts(j,1) handles.spirals(i).volts(j,2)]);
                    pause(0.03);
                    counter.start();
                    laser_pulse.start();
                    handles.Syn_point.writeDigitalData(1);
                    data=counter.read( );
                    handles.Syn_point.writeDigitalData(0);
                    laser_pulse.stop();
                    counter.stop();
                    num_all_points = num_all_points + 1;
                    if handles.n_avg>1
                        data = reshape(data',handles.n_hist_points,handles.n_avg);
                        data=(data((2:end),1:end)-data((1:end-1),1:end));
                        All_data(num_all_points,:) = sum(data,2)';
                    else data = data(2:end)-data(1:end-1);
                        All_data(num_all_points,:) = data(2:end)';
                    end
                    endtime = clock;
                    handles.recording_time1  = etime(endtime,handles.start_time);
                    set(handles.exp_time,'string',num2str(handles.recording_time1 ));
                end
            end
            if exist('All_points')
                All_points{size(All_points,2)+1} = handles.spirals;
            else
                All_points{1} = handles.spirals;
            end
        end
        handles.save_name = ['time' num2str(flag_time) 'All_data.mat'];
        save([handles.path_nameData '\' handles.save_name],'All_data');
        save([handles.path_nameMatlab '\' 'All_points.mat'],'All_points');
        fid_times = fopen([handles.path_nameMatlab  '\times.txt'], 'w');
        fwrite(fid_times,int2str(flag_time));
        fclose(fid_times);
        delete(laser_pulse);
        delete(counter);
    catch
        delete(laser_pulse);
        delete(counter);
    end
end


function pulse_intensity_Callback(hObject, eventdata, handles)
% hObject    handle to pulse_intensity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.intensity = str2double(get(hObject,'String'));
set(handles.intensity_slider,'value',handles.intensity);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function pulse_intensity_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pulse_intensity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function file_name_Callback(hObject, eventdata, handles)
% hObject    handle to file_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.file_name = get(hObject,'String');
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function file_name_CreateFcn(hObject, eventdata, handles)
% hObject    handle to file_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function pulse_period_Callback(hObject, eventdata, handles)
% hObject    handle to pulse_period (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Pulse_Period = str2double(get(hObject,'String'));
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function pulse_period_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pulse_period (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


function pulse_high_time_Callback(hObject, eventdata, handles)
% hObject    handle to pulse_high_time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Pulse_High_Time = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function pulse_high_time_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pulse_high_time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in histogram_test. TODO
function histogram_test_Callback(hObject, eventdata, handles)
% hObject    handle to histogram_test (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if handles.LED_volts
    warndlg('Turn off LED!');
else
    if handles.point_num > 0
        try
            laser_pulse=PulseClass(handles.Pulse_Period,handles.Pulse_High_Time,'Dev1',0);  % laser pulse
            handles.laser_power.write([handles.intensity]);                                 %Laser intensity will be changed during the experiment
            counter = CounterClass(handles.n_avg,handles.n_hist_points,handles.Pulse_Period);
            handles.galvos.write(handles.points(handles.point_num).volts);
            counter.start();
            laser_pulse.start();
            data=counter.read( );
            imagesc(handles.im_camera.get_image(),'Parent',handles.camera);
            colormap(gray);
            laser_pulse.stop();
            counter.stop();
            hold on;
            plot(handles.points(handles.point_num).pos(1,2),handles.points(handles.point_num).pos(1,1),'b*');
            hold off
            data = reshape(data',handles.n_hist_points,handles.n_avg);
            data=(data((2:end),1:end)-data((1:end-1),1:end));
            handles.hist_data = sum(data,2);
            data = imresize(handles.hist_data,[handles.Pulse_Period*1.0e6-1 1]);
            axes(handles.histogram);
            plot(handles.histogram,data,'b');
            xlabel(handles.histogram,'time (us)');
            ylabel(handles.histogram,'counts/bin');
            xlim(handles.histogram,[1 length(data)]);
            ylim(handles.histogram,[0 max(data(1:end-1))+0.1*max(data(1:end-1))]);
            delete(laser_pulse);
            delete(counter);
        catch
            delete(laser_pulse);
            delete(counter);
        end
    else
        warndlg('Select points first!');
    end
end

guidata(hObject, handles);

% --- Executes on button press in alignment.
function alignment_Callback(hObject, eventdata, handles)
% hObject    handle to alignment (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

try
    if get(hObject,'Value')
        set(hObject,'BackgroundColor','r');
        set(hObject,'string','Stop Alignment');
        
        handles.n_avg = 100;
        handles.n_hist_points = 2;
        handles.Pulse_Period = 0.001;
        handles.Pulse_High_Time = 100;
        set(handles.num_average,'string',num2str(handles.n_avg));
        set(handles.num_hist_points,'string',num2str(handles.n_hist_points));
        set(handles.pulse_period,'string',num2str(handles.Pulse_Period));
        set(handles.pulse_high_time,'string',num2str(handles.Pulse_High_Time));
        
        handles.galvos.write([0 0]);
        pause(0.03);
        handles.laser_pulse=PulseClass(handles.Pulse_Period,handles.Pulse_High_Time,'Dev1',0);% laser pulse
        handles.laser_power.write([handles.intensity]);
        handles.counter = Counter_Alignment_Class(handles.n_avg,handles.n_hist_points,handles.Pulse_Period);
        Callback1(handles);
        handles.counter.hTask_counter.registerEveryNSamplesEvent(@Callback1,handles.n_avg*handles.n_hist_points);
        handles.counter.start();
        handles.laser_pulse.start();
    else
        handles.laser_pulse.stop();
        handles.counter.stop();
        set(hObject,'BackGroundColor','g')
        set(hObject,'string','Start Alignment')
        delete(handles.laser_pulse);
        delete(handles.counter);
    end
catch
    delete(handles.laser_pulse);
    delete(handles.counter);
end

guidata(hObject, handles);

function Callback1(src,eventdata)
persistent handles
persistent plot_data
persistent i
if(nargin == 1)
    handles = src;
else
    if(isempty(plot_data))
        plot_data=zeros(200,1);
        i=1;
    end
    data=src.readCounterData(handles.n_avg*handles.n_hist_points);
    data = reshape(data',handles.n_hist_points,handles.n_avg);
    data=(data((2:end),1:end)-data((1:end-1),1:end));
    data = sum(sum(data));
    plot_data(i)=data;
    i=mod(i,200)+1;
    plot(handles.histogram, plot_data);
    xlabel(handles.histogram,'time');
    ylabel(handles.histogram,'Countrate');
end
    
% --- Executes on button press in select_central_point.
function select_central_point_Callback(hObject, eventdata, handles)
% hObject    handle to select_central_point (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.galvos.write([0 0]);
handles.hLaser.writeDigitalData(1);
axes(handles.camera);
imagesc(handles.im_camera.get_image(),'Parent',handles.camera);
colormap(gray);
axis off;
handles.hLaser.writeDigitalData(0);

guidata(hObject, handles);

function camera_exposure_time_Callback(hObject, eventdata, handles)
% hObject    handle to camera_exposure_time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.exposure_time = str2double(get(hObject,'String'));
set_exposure(handles.im_camera,handles.exposure_time);
axes(handles.camera);
handles.hLaser.writeDigitalData(1);
imagesc(handles.im_camera.get_image(),'Parent',handles.camera);
handles.hLaser.writeDigitalData(0);
colormap(gray);
axis off;
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function camera_exposure_time_CreateFcn(hObject, eventdata, handles)
% hObject    handle to camera_exposure_time (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in focus.
function focus_Callback(hObject, eventdata, handles)
% hObject    handle to exposure_time_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

if get(hObject,'Value')
    set(hObject,'BackgroundColor','r');
    set(hObject,'string','Stop Camera');
    set(handles.timer_camera, 'TimerFcn', {@camera, handles});
    if isfield(handles,'LED_volts')
        if handles.LED_volts>1
            handles.hLaser.writeDigitalData(0);
        else
            handles.hLaser.writeDigitalData(1);
        end
    end
    start(handles.timer_camera);
else
    set(hObject,'BackGroundColor','g')
    set(hObject,'string','Run Camera')
    stop(handles.timer_camera)
    handles.hLaser.writeDigitalData(0);
end

guidata(hObject, handles);
                
function camera(obj,event,handles)
imagesc(handles.im_camera.get_image(),'Parent',handles.camera);

% --- Executes on button press in fit_histogram.
function fit_histogram_Callback(hObject, eventdata, handles)
% hObject    handle to fit_histogram (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isfield(handles,'hist_data')
    data = imresize(handles.hist_data,[handles.Pulse_Period*1.0e6-1 1]);
    temps = 1:1:length(data);
    kq=190; % /mmHg/s %parameters of G4 (Esipova et al., 2011)
    tau0=242E-6; %s
    [p f1 gof pO2] = FitHistogram(temps, handles.Pulse_High_Time, data', kq, tau0);
    axes(handles.histogram);
    plot(handles.histogram,data);
    hold on;
    plot(f1,'r');
    hold off
    xlabel(handles.histogram,'time (us)');
    ylabel(handles.histogram,'counts/bin');
    xlim(handles.histogram,[temps(1) temps(end)]);
    ylim(handles.histogram,[0 max(data(1:end-1))+0.1*max(data(1:end-1))]);
    legend(handles.histogram,'data',sprintf('model, \\tau=%3.2f\\mus, pO_2=%3.2fmmHg',p(2),pO2))
    handles.fit_histogram = 0;
end
guidata(hObject, handles);


% --- Executes on button press in save_point_position.
function save_point_position_Callback(hObject, eventdata, handles)
% hObject    handle to save_point_position (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if exist([handles.exp_name filesep handles.file_name],'dir')
    handles.path_nameMatlab = [handles.exp_name filesep handles.file_name filesep 'Time and Position'];
else
    mkdir([handles.exp_name '\'],handles.file_name);
    handles.path_name1 = [handles.exp_name '\' handles.file_name];
    mkdir([handles.path_name1 '\'],'Data');
    handles.path_nameData = [handles.path_name1 '\' 'Data'];
end
if isfield(handles,'points')
    point = handles.points;
    save(fullfile(handles.path_nameMatlab,'point.mat'),'point');
else
    warndlg('There are no points!');
end
guidata(hObject, handles);

% --- Executes on button press in extract_position.
function extract_position_Callback(hObject, eventdata, handles)
% hObject    handle to extract_position (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.path_nameMatlab = [handles.exp_name '\' handles.file_name '\Time and Position'];

if  exist(fullfile(handles.path_nameMatlab,'point.mat'),'file')
    load (fullfile(handles.path_nameMatlab,'point.mat'));
    handles.points = point;
end
if exist(fullfile(handles.path_nameMatlab,'line.mat'),'file')
    line=load(fullfile(handles.path_nameMatlab,'line.mat'));
    handles.lines = line.line;
end
if exist(fullfile(handles.path_nameMatlab,'spiral.mat'),'file')
    spiral = load(fullfile(handles.path_nameMatlab,'spiral.mat'));
    handles.spirals = spiral.spiral;
end

guidata(hObject, handles);


% --- Executes on button press in delete_point.
function delete_point_Callback(hObject, eventdata, handles)
% hObject    handle to delete_point (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isfield(handles,'points')
    handles.points(handles.point_num) = [];
else
    warndlg('There are no points!');
end

guidata(hObject, handles);


% --- Executes on button press in plot_radius. (TODO)
function plot_radius_Callback(hObject, eventdata, handles)
% hObject    handle to plot_radius (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles,'spirals')
    handles = rmfield(handles, 'spirals');
end
if  ~isfield(handles,'LED_volts')|| (~handles.LED_volts)
    warndlg('Turn on LED first!');
else
%     warndlg('Input the number points on the spiral first!');
    handles.spiral_num =1;
    axes(handles.camera);
    imagesc(handles.im_camera.get_image(),'Parent',handles.camera);
    colormap(gray);
    axis off;
    if isfield(handles,'points')
        if ~isempty(handles.points)
            for j = 1:size(handles.points,2)
                hold on
                plot(handles.points(j).pos(1,2),handles.points(j).pos(1,1),'b*');
                hold off
            end
        end
    end
    if isfield(handles,'lines')
        if ~isempty(handles.lines)
            for i=1:size(handles.lines,2)
                hold on
                plot([handles.lines(i).pos(1,2),handles.lines(i).pos(2,2)],[handles.lines(i).pos(1,1) handles.lines(i).pos(2,1)]);
                hold off
            end
        end
    end
    while (handles.spiral_num)
        choice = questdlg('Would you like to select one radius?',...
            'Choices',...
            'Yes','No','No');
        switch choice
            case 'Yes'
                handles.spiral_num = handles.spiral_num+1;
            case 'No'
                handles.spiral_num = 0;
        end
        if handles.spiral_num
            h_spiral(handles.spiral_num-1) = imline(gca,[]);
            handles.spirals(handles.spiral_num-1).pos = wait(h_spiral(handles.spiral_num-1));
            handles.spirals(handles.spiral_num-1).pos = [handles.spirals(handles.spiral_num-1).pos(:,2) handles.spirals(handles.spiral_num-1).pos(:,1)];
            radius(handles.spiral_num-1) = ((handles.spirals(handles.spiral_num-1).pos(2,1)-handles.spirals(handles.spiral_num-1).pos(1,1))^2+(handles.spirals(handles.spiral_num-1).pos(2,2)-handles.spirals(handles.spiral_num-1).pos(1,2))^2)^(1/2);
            slope(handles.spiral_num-1) = ( handles.spirals(handles.spiral_num-1).pos(2,2)-handles.spirals(handles.spiral_num-1).pos(1,2))/(handles.spirals(handles.spiral_num-1).pos(2,1)-handles.spirals(handles.spiral_num-1).pos(1,1));
            if (handles.spirals(handles.spiral_num-1).pos(2,2)==handles.spirals(handles.spiral_num-1).pos(1,2))&&(handles.spirals(handles.spiral_num-1).pos(2,1)>handles.spirals(handles.spiral_num-1).pos(1,1))
                angle = 0;
            elseif (handles.spirals(handles.spiral_num-1).pos(2,2)==handles.spirals(handles.spiral_num-1).pos(1,2))&&(handles.spirals(handles.spiral_num-1).pos(2,1)<handles.spirals(handles.spiral_num-1).pos(1,1))
                angle = pi;
            elseif (handles.spirals(handles.spiral_num-1).pos(2,1)==handles.spirals(handles.spiral_num-1).pos(1,1))&&(handles.spirals(handles.spiral_num-1).pos(2,2)>handles.spirals(handles.spiral_num-1).pos(1,2))
                angle = pi/2;
            elseif(handles.spirals(handles.spiral_num-1).pos(2,1)==handles.spirals(handles.spiral_num-1).pos(1,1))&&(handles.spirals(handles.spiral_num-1).pos(2,2)<handles.spirals(handles.spiral_num-1).pos(1,2))
                angle = pi*3/2;
            elseif (handles.spirals(handles.spiral_num-1).pos(2,1)-handles.spirals(handles.spiral_num-1).pos(1,1))>0
                angle = atan(slope(handles.spiral_num-1));
            elseif (handles.spirals(handles.spiral_num-1).pos(2,1)-handles.spirals(handles.spiral_num-1).pos(1,1))<0
                angle = atan(slope(handles.spiral_num-1))+pi;
            end
            theta =0:(2*pi*handles.num_circles+ angle)/(handles.points_circle-1) :(2*pi*handles.num_circles+ angle);
            k = radius(handles.spiral_num-1)/(2*pi*handles.num_circles+angle);
            r = theta*k;
            [x,y]= pol2cart(theta,r);
            handles.spirals(handles.spiral_num-1).pos1(:,1) = x + handles.spirals(handles.spiral_num-1).pos(1,1);
            handles.spirals(handles.spiral_num-1).pos1(:,2) = y + handles.spirals(handles.spiral_num-1).pos(1,2);
            if (exist('handles.A','var'))
                warndlg('Calibration first!');
            else
                handles.spirals(handles.spiral_num-1).volts = [handles.spirals(handles.spiral_num-1).pos1 ones(size(handles.spirals(handles.spiral_num-1).pos1,1),1)]*handles.A;
            end
        end
    end
end
guidata(hObject, handles);

function points_circle_Callback(hObject, eventdata, handles)
% hObject    handle to points_circle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.points_circle = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function points_circle_CreateFcn(hObject, eventdata, handles)
% hObject    handle to points_circle (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function num_circles_Callback(hObject, eventdata, handles)
% hObject    handle to num_circles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.num_circles = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function num_circles_CreateFcn(hObject, eventdata, handles)
% hObject    handle to num_circles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in save_images.
function save_images_Callback(hObject, eventdata, handles)
% hObject    handle to save_images (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if  exist([handles.exp_name '\' handles.file_name '\images'],'dir')
    handles.path_nameImages = [handles.exp_name '\' handles.file_name '\images'];
else
    mkdir([handles.exp_name '\'],handles.file_name);
    handles.path_name1 = [handles.exp_name '\' handles.file_name];
    mkdir([handles.path_name1 '\'],'images');
    handles.path_nameImages = [handles.path_name1 '\' 'images'];
end
axes(handles.camera);
image_data = handles.im_camera.get_image();
imagesc(image_data,'Parent',handles.camera);
save([handles.path_nameImages  '\' 'AnatomicalImage.mat'],'image_data');
show_scan_points(handle)
image_with_points = getframe(handles.camera);
imwrite(image_with_points.cdata, [handles.path_nameImages  '\' 'image_points.tiff'],'tiff');

guidata(hObject, handles);

function num_average_Callback(hObject, eventdata, handles)
% hObject    handle to num_average (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.n_avg = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function num_average_CreateFcn(hObject, eventdata, handles)
% hObject    handle to num_average (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function num_hist_points_Callback(hObject, eventdata, handles)
% hObject    handle to num_hist_points (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.n_hist_points = str2double(get(hObject,'String'));
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function num_hist_points_CreateFcn(hObject, eventdata, handles)
% hObject    handle to num_hist_points (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on slider movement.
function LED_slider_Callback(hObject, eventdata, handles)
% hObject    handle to LED_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.LED_volts = get(hObject,'Value');
handles.LED.write([handles.LED_volts]);
imagesc(handles.im_camera.get_image(),'Parent',handles.camera);
colormap(gray);
axis off;
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function LED_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LED_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in laser_button.
function laser_button_Callback(hObject, eventdata, handles)
% hObject    handle to laser_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Continuous_laser_on = get(hObject,'Value');
if (handles.Continuous_laser_on)
    set(hObject,'BackgroundColor','r');
    set(hObject,'String', 'Laser On');
    handles.hLaser.writeDigitalData(1);
else
    set(hObject,'BackgroundColor','g');
    set(hObject,'String', 'Laser Off');
    handles.hLaser.writeDigitalData(0);
end
imagesc(handles.im_camera.get_image(),'Parent',handles.camera);
colormap(gray);
axis off;
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function recording_button_CreateFcn(hObject, eventdata, handles)
% hObject    handle to recording_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in plot_intensity.
function plot_intensity_Callback(hObject, eventdata, handles)
% hObject    handle to plot_intensity (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

min_intensity=min(handles.survey_data);
max_intensity=max(handles.survey_data);
color_map = jet(256);

axes(handles.camera);
imagesc(image_data);
colormap(gray);
hold on
if isfield(handles,'lines')
    i_pt=0;
    for i = 1:size(handles.lines,2)
        for j = 1:size(handles.lines(i).pos1,1)   
             i_pt=i_pt+1;
             value=round(255*(handles.survey_data(i_pt)-min_intensity)/(max_intensity-min_intensity))+1;
             plot(handles.lines(i).pos1(j,2),handles.lines(i).pos1(j,1),'Marker','o','MarkerFaceColor',[color_map(value,1),color_map(value,2),color_map(value,3)]);
        end
    end
end
hold off
axis off;
image_intensity = getframe(handles.camera);
imwrite(image_intensity.cdata, [handles.path_nameImages  '\' 'IntensityImage' '.tiff'],'tiff');
guidata(hObject, handles);


% --- Executes on button press in plot_po2_image.
function plot_po2_image_Callback(hObject, eventdata, handles)
% hObject    handle to plot_po2_image (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
load([handles.path_nameImages  '\' 'AnatomicalImage.mat']);
all_points = 0;
zero_image = zeros(size(image_data));
load([handles.path_nameData '\time1All_data.mat']);
if isfield(handles,'points')
    for i = 1:size(handles.points,2)
        all_points =all_points+1;
        temps = 1:1:size(All_data,2);
        kq=190; % /mmHg/s %parameters of G4 (Esipova et al., 2011)
        tau0=242E-6; %s
        [p f1 gof pO2] = FitHistogram(temps, 100,All_data(all_points,:), kq, tau0);        
        zero_image(round(handles.points(i).pos(1,2)),round(handles.points(i).pos(1,1))) =pO2;
    end
end
if isfield(handles,'lines')
    for i = 1:size(handles.lines,2)
        for j = 1:size(handles.lines(i).pos1,1)
            all_points =all_points+1;
            temps = 1:1:size(All_data,2);
            kq=190; % /mmHg/s %parameters of G4 (Esipova et al., 2011)
            tau0=242E-6; %s
            [p f1 gof pO2] = FitHistogram(temps, 100, All_data(all_points,:), kq, tau0);
            zero_image(round(handles.lines(i).pos1(j,2)),round(handles.lines(i).pos1(j,1))) = pO2;
        end
    end
end
if isfield(handles,'spirals')
    for i = 1 : size(handles.spirals,2)
        for j = 1:size(handles.spirals(i).pos1,1)  
            all_points =all_points+1;
            temps = 1:1:size(All_data,2);
            kq=190; % /mmHg/s %parameters of G4 (Esipova et al., 2011)
            tau0=242E-6; %s
            [p f1 gof pO2] = FitHistogram(temps, 100, All_data(all_points,:), kq, tau0);
            zero_image(round(handles.spirals(i).pos1(j,2)),round(handles.spirals(i).pos1(j,1))) =pO2;
        end
    end
end
color_value = jet(all_points);
[rows,cols,vals] = find(zero_image);
[int_values index] = sort(vals);
axes(handles.camera);
imagesc(image_data);
colormap(gray);
hold on
for  i =1:all_points
    plot(rows(index(i)),cols(index(i)),'Marker','.','MarkerFaceColor',[color_value(i,1),color_value(i,2),color_value(i,3)]);
    text(rows(index(i)),cols(index(i)),num2str(eval(vpa(vals(index(i)),3))),'color',[color_value(i,1),color_value(i,2),color_value(i,3)]);
end
hold off 
axis off;
image_intensity = getframe(handles.camera);
imwrite(image_intensity.cdata, [handles.path_nameImages  '\' 'images_intensity' '.tiff'],'tiff');
set(hObject,'BackgroundColor','g');
set(hObject,'String','Ploting is done!')
guidata(hObject, handles);


% --- Executes on button press in survey_scan.
function survey_scan_Callback(hObject, eventdata, handles)
% hObject    handle to survey_scan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(hObject,'BackgroundColor','r');
set(hObject,'String','Ongoing!')
start_time = clock;
handles.n_avg = 50;
handles.n_hist_points = 2;
handles.Pulse_Period = 0.0001;
handles.Pulse_High_Time = 50;
set(handles.pulse_period,'string',num2str(handles.Pulse_Period));
set(handles.pulse_high_time,'string',num2str(handles.Pulse_High_Time));
set(handles.num_average,'string',num2str(handles.n_avg));
set(handles.num_hist_points,'string',num2str(handles.n_hist_points));

laser_pulse=PulseClass(handles.Pulse_Period,handles.Pulse_High_Time,'Dev1',0);% laser pulse
handles.laser_power.write([handles.intensity]); %Laser intensity will be changed during the experiment
counter = CounterClass(handles.n_avg,handles.n_hist_points,handles.Pulse_Period);

num_all_points = 0;
for i = 1:size(handles.lines,2)
    for j = 1 : handles.line_num_points
        handles.galvos.write( [handles.lines(i).volts(j,1) handles.lines(i).volts(j,2)]);
        counter.start();
        laser_pulse.start();
        data=counter.read( );
        laser_pulse.stop();
        counter.stop();
        num_all_points =num_all_points + 1;
        data = reshape(data',handles.n_hist_points,handles.n_avg);
        data=(data((2:end),1:end)-data((1:end-1),1:end));
        handles.survey_data(num_all_points) = squeeze(sum(data,2));
    end
end
endtime = clock;
handles.recording_time1  = etime(endtime,start_time);
set(handles.exp_time,'string',num2str(handles.recording_time1 ));
delete(laser_pulse);
delete(counter);
set(hObject,'BackgroundColor','g');
set(hObject,'String','Scanning is done!')
guidata(hObject, handles);

function show_scan_points(handles)
axes(handles.camera);
imagesc(handles.im_camera.get_image(),'Parent',handles.camera);
colormap(gray);
axis off;

if isfield(handles,'points')
    for m = 1:size(handles.points,2)
        hold on
        plot(handles.points(m).pos(1,2),handles.points(m).pos(1,1),'b*');
        hold off
    end
end
if isfield(handles,'lines')
    for i = 1:size(handles.lines,2)
        for j = 1:size(handles.lines(i).pos1,1)
            hold on
            plot(handles.lines(i).pos1(j,2),handles.lines(i).pos1(j,1),'r*');
            hold off
        end
    end
end
if isfield(handles,'spirals')
    for i = 1 : size(handles.spirals,2)
        for j = 1:size(handles.spirals(i).pos1,1)
            hold on
            plot(handles.spirals(i).pos1(j,2),handles.spirals(i).pos1(j,1),'g*');
            hold off
        end
    end
end
