
function [p f1 gof pO2] = FitHistogram(temps, HighTime, counts, kq, tau0)
%MYFIT 
% For exponential model
% Fit exponential
% the first point for fit
for j=1:length(temps)
    if temps(j)<=HighTime
        k=j;
    end
end

% options of fit
startpoint=[1E2 242 0]; % the value of coefficient start the iretation
x=temps(k:end-1)';
y=counts(k:end-1)';
ffun=fittype(@(a,b,c,x)(a*(exp(-x/b)))+c,'coefficients',{'a','b','c'},'independent','x');
[f1 gof]=fit(x,y,ffun,'Startpoint',startpoint,'Lower',[0 0 0],'Upper',[1e7 500 500]);
p=coeffvalues(f1);

% Calcul  pO2
tau=p(2)*10^-6;
pO2=(1/tau-1/tau0)/kq;      
end

