function varargout = test_shutter2(varargin)
% TEST_SHUTTER2 M-file for test_shutter2.fig

%      TEST_SHUTTER2, by itself, creates a new TEST_SHUTTER2 or raises the
%      existing singleton*.
%
%      H = TEST_SHUTTER2 returns the handle to a new TEST_SHUTTER2 or the
%      handle to the existing singleton*.
%
%      TEST_SHUTTER2('CALLBACK',hObject,eventData,handles,...) calls the
%      local function named CALLBACK in TEST_SHUTTER2.M with the given input
%      arguments.
%
%      TEST_SHUTTER2('Property','Value',...) creates a new TEST_SHUTTER2 or
%      raises the existing singleton*.  Starting from the left, property
%      value pairs are applied to the GUI before test_shutter2_OpeningFcn
%      gets called.  An unrecognized property name or invalid value makes
%      property application stop.  All inputs are passed to
%      test_shutter2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help test_shutter2

% Last Modified by GUIDE v2.5 10-Mar-2015 14:55:53

% Begin initialization code - DO NOT EDIT

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @test_shutter2_OpeningFcn, ...
    'gui_OutputFcn',  @test_shutter2_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before test_shutter2 is made visible.
function test_shutter2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn. hObject    handle to
% figure eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA) varargin
% command line arguments to test_shutter2 (see VARARGIN)


clc;
%% Import library
addpath('..')
addpath('../SCANIMAGE_r3.8')

handles.save = 0;
handles.vol_scan_num  = 0;
handles.line_scan_num = 0;
handles.live_scan_num = 0;
handles.scan.binning = 1;
handles.scan.experiment_type = 'Rest-ISO';

set(handles.loop_scan,'Enable','off');

% System setup: Connect AO start trigger to scope trigger...
% May need to change for continuous mode

system = dabs.ni.daqmx.System();
system.connectTerms('/Dev1/ao/StartTrigger','/Dev1/PFI1');

%% SHOW IMAGE
bkg_image=imread('fun.png');
axes(handles.pmt_image);
axis off;
imshow(bkg_image);
colormap gray;
handles.channel = get(handles.PMT_image_popup,'Value');

set(handles.galvo, 'XTick', []);
set(handles.galvo, 'YTick', []);

set(handles.pmt_image, 'YTick', []);
set(handles.pmt_image, 'XTick', []);

set(handles.image, 'YTick', []);
set(handles.image, 'XTick', []);

set(handles.lifetime_image, 'YTick', []);
set(handles.lifetime_image, 'XTick', []);
xlabel(handles.lifetime_image,'ns','fontsize',7.5);
ylabel(handles.lifetime_image,'count','fontsize',7.5);
set(handles.lifetime_image,'LooseInset',get(handles.lifetime_image,'TightInset'))


set(handles.line_image, 'YTick', []);
set(handles.line_image, 'XTick', []);

 handles.output = hObject;

%% Choose default command line output for test_shutter2
 handles.shutter = liom.ShutterClass('Dev1','port0/line28');
handles.shutter_open = 0;

%% IMAGE SCAN
%sample points
handles.scan.nx= str2double(get(handles.nx,'String'));
handles.scan.ny= str2double(get(handles.ny,'String'));
handles.polynomial_drop = [];
handles.scan.shift = str2double(get(handles.shift,'String'));
%image size in micro meters
handles.scan.scan_height= str2double(get(handles.height,'String'));
handles.scan.scan_width= str2double(get(handles.width,'String'));
handles.scan3D=0;
handles.interupt = 0;% flag for interupting 3D scan
handles.scan.zpos = 0;

%% Voltage
handles.scan.volt_x = double(handles.scan.scan_width/(58*2)); %unit = volts (for 1v = 58 um) ~ 2.79 v
handles.scan.volt_y = double(handles.scan.scan_height/(58*2)); %unit = volts (for 1v = 58 um) ~ 2.79 v
handles.scan.ramp_type = 1;

%% MOTOR
handles.z_motor_steps = str2double(get(handles.z_motor_step,'String'))/1000;
handles.xy_motor_steps = str2double(get(handles.xy_motor_step,'String'))/1000;
 handles.motors = liom.MotorsClass('COM3');
 [x,y,z]=handles.motors.get_pos();
 set(handles.live_x,'String',num2str(x));
 set(handles.live_y,'String',num2str(y));
 set(handles.live_z,'String',num2str(z));
handles.select_brainsurface = 0;

%% AOM vs Z PLOT
xlabel(handles.interp_plot,'Z(um)','fontsize',7.5);
ylabel(handles.interp_plot,'AOM(% volt)','fontsize',7.5);
set(handles.interp_plot,'LooseInset',get(handles.interp_plot,'TightInset'))


handles.scan.slices_3D = str2double(get(handles.z_slices,'String'));
handles.scan.z_steps_3D = str2double(get(handles.z_step_size,'String'))/1000; % this is for loop scan, the system reads mm, so this is equivalent to 2 um
%set(handles.minZ,'String',(handles.scan.top_z - handles.scan.slices_3D*handles.scan.z_steps_3D)); %you can not go lower than number of slices*z_steps from where you start your interpolation!

%% AOM
handles.scan.aom_value = 0;
 handles.aom_task=liom.OnDemandAOClass('Dev1',2,'aom_task');
 handles.aom_task.write(0);

%% DATA
handles.data =[];

%% LINES POINTS ON IMAGE
handles.n_segments = 0;

handles.scan.LineRamp = 1;
handles.line_handles = []; % this handle is made so that we could delete the lines from screen with reset button
handles.point_handles =[]; % this handle is made so that we could delete the lines from screen with reset button

handles.Exist_points = 0;
handles.scan.segment_handles=[];
handles.scan.number_points_per_segment = [];
handles.histogSave =[];

handles.line_pos_handles=[];
handles.scan.repeat_line_scan = str2double(get(handles.repeat_scan,'String'));
handles.lineScan = 0; %M: it means line_scan is OFF
handles.scan.sum_lines_points = 0;
%% RAMPS
handles.scan.ramp_x = [];
handles.scan.ramp_y = [];

%% TABLE
set(handles.interp_table,'Data',[]);

%% LINE FREQ, put 100Hz, this will lead to 4Hz image rate in a 400x400 acquisition
handles.scan.line_frequency_live = str2double(get(handles.line_freq_live,'String'));
handles.scan.line_frequency_3D = str2double(get(handles.line_freq_3D,'String'));
handles.scan.line_frequency_line = str2double(get(handles.line_freq_line,'String'));

set(handles.line_per_sec_live,'String',num2str( handles.scan.line_frequency_live));% line (nx) per frequency (Hz)
set(handles.frame_per_sec_live,'String',num2str( handles.scan.line_frequency_live / handles.scan.ny ));%frame (nx*ny) per frequency(Hz)

set(handles.line_per_sec_3D,'String',num2str( handles.scan.line_frequency_3D));% line (nx) per frequency (Hz)
set(handles.frame_per_sec_3D,'String',num2str( handles.scan.line_frequency_3D / handles.scan.ny));%frame (nx*ny) per frequency(Hz)

set(handles.line_per_sec_line,'String',num2str(handles.scan.line_frequency_line));
set(handles.frame_per_sec_line,'String',num2str(handles.scan.line_frequency_line/handles.scan.repeat_line_scan));

handles.scan.shift_linescan = str2double(get(handles.shift_lineScan,'String'));
%% lifetime parameters
handles.scan.tacq_parameter = 0;
handles.scan.cfdLevel1=0; %these parameters can change
handles.scan.cfdZeroX1=0;  %these parameters can change
guidata(hObject, handles);


% UIWAIT makes test_shutter2 wait for user response (see UIRESUME)
% uiwait(handles.figure1); --- Outputs from this function are returned to
% the command line.
%%
function varargout = test_shutter2_OutputFcn(hObject, eventdata, handles)
varargout{1} = handles.output;

%% Scan Dimentions
function ny_Callback(hObject, eventdata, handles)
handles.scan.ny=str2double(get(hObject,'String'));
guidata(hObject, handles);
function nx_Callback(hObject, eventdata, handles)
handles.scan.nx=str2double(get(hObject,'String'));
set(handles.frame_per_sec_live,'String',num2str( handles.scan.line_frequency_live / handles.scan.nx ));%frame (nx*ny) per frequency(Hz)
set(handles.frame_per_sec_3D,'String',num2str( handles.scan.line_frequency_3D / handles.scan.nx));%frame (nx*ny) per frequency(Hz)
guidata(hObject, handles);
function height_Callback(hObject, eventdata, handles)
handles.scan.scan_height=str2double(get(hObject,'String'));
handles.scan.volt_y = (handles.scan.scan_height)/(58*2);
guidata(hObject, handles);
function width_Callback(hObject, eventdata, handles)
handles.scan.scan_width=str2double(get(hObject,'String'));
handles.scan.volt_x = (handles.scan.scan_width)/(58*2);
guidata(hObject, handles);

%% SHUTTER
function Shutter_checkbox_Callback(hObject, eventdata, handles)

if( handles.shutter_open )
    handles.shutter.close();
    handles.shutter_open = 0;
    set(handles.current,'String','Shutter is closed');
else
    handles.shutter.open();
    handles.shutter_open = 1;
    set(handles.current,'String','Shutter is opened');
end
guidata(hObject, handles);

%% MOTOR CONTROL
function home_motor_Callback(hObject, eventdata, handles)

handles.motor_home = get(handles.home_motor,'Value');
if (handles.motor_home)
    set(handles.current,'String','Motors are ON');
    handles.motors.home();
    [x,y,z]=handles.motors.get_pos();
    set(handles.live_x,'String',num2str(x));
    set(handles.live_y,'String',num2str(y));
    set(handles.live_z,'String',num2str(z));
    
end
set(handles.current,'String','Motors are HOME');
guidata(hObject, handles);
function xy_motor_step_Callback(hObject, eventdata, handles)
handles.xy_motor_steps = str2double(get(hObject,'String'))/1000;
guidata(hObject, handles);
function up_Callback(hObject, eventdata, handles)
handles.motors.move_dx(-handles.xy_motor_steps);
[x,~,~]=handles.motors.get_pos();
set(handles.live_x,'String',num2str(x));
guidata(hObject, handles);
function down_Callback(hObject, eventdata, handles)
handles.motors.move_dx(handles.xy_motor_steps)
[x,~,~]=handles.motors.get_pos();
set(handles.live_x,'String',num2str(x));
guidata(hObject, handles);
function right_Callback(hObject, eventdata, handles)
handles.motors.move_dy(-handles.xy_motor_steps)
[~,y,~]=handles.motors.get_pos();
set(handles.live_y,'String',num2str(y));
guidata(hObject, handles);
function left_Callback(hObject, eventdata, handles)
handles.motors.move_dy(handles.xy_motor_steps)
[~,y,~]=handles.motors.get_pos();
set(handles.live_y,'String',num2str(y));
guidata(hObject, handles);

function select_brainSurface_Callback(hObject, eventdata, handles)
handles.select_brainsurface = get(hObject,'Value');
if (get(hObject,'Value'))
[handles.scan.xpos_surface,handles.scan.ypos_surface,handles.scan.zpos_surface]=handles.motors.get_pos();
set(handles.current,'String','Brain Surface Coordinates are recorded');
end

guidata(hObject, handles);
function brainSurface_Callback(hObject, eventdata, handles)%should be fixed
if(handles.select_brainsurface)
handles.motors.move_az(handles.scan.zpos_surface)
handles.motors.move_ax(handles.scan.xpos_surface)
handles.motors.move_ay(handles.scan.ypos_surface)
[x,y,z]=handles.motors.get_pos();
    set(handles.live_x,'String',num2str(x));
    set(handles.live_y,'String',num2str(y));
    set(handles.live_z,'String',num2str(z));
    set(handles.current,'String','Motors Are at Brain Surface Coordinates');

else
    set(handles.current,'String','Record Brain Surface Coordinates First');
end
guidata(hObject, handles);

function z_motor_step_Callback(hObject, eventdata, handles)

handles.z_motor_steps = str2double(get(hObject,'String'))/1000; % user inputs 200 um, which is 0.2 mm
guidata(hObject, handles);
function z_raise_Callback(hObject, eventdata, handles)

% We should never go over 2mm, need to recheck since it changes
z_top_limit = 5;
[~,~,z]=handles.motors.get_pos();
if( z + handles.z_motor_steps < z_top_limit )
    handles.motors.move_dz(handles.z_motor_steps);
    [~,~,z]=handles.motors.get_pos();
    set(handles.live_z,'String',num2str(z));
    guidata(hObject, handles);
else
    msgbox('Trying to move z-stage over limit!','Careful','warn')
end
function z_desend_Callback(hObject, eventdata, handles)

handles.motors.move_dz(-handles.z_motor_steps)
[~,~,z]=handles.motors.get_pos();
set(handles.live_z,'String',num2str(z));
guidata(hObject, handles);

%% AOM
function aom_slider_Callback(hObject, eventdata, handles)

% AOM Slider is between 0-100%, need to translate to 0-2Volts
handles.scan.aom_value = get(hObject,'Value');
set(handles.aom_text,'String',handles.scan.aom_value);
handles.aom_task.write(handles.scan.aom_value/50);
guidata(hObject, handles);
function add_aom_Callback(hObject, eventdata, handles)
current_aom_value = handles.scan.aom_value; %$
[~,~,z]=handles.motors.get_pos();

% Add row to Table
table_data = get(handles.interp_table,'Data');
% Which row number does the new row get
next_row = size(table_data,1)+1;
if next_row == 1
    % Data is converted the first time, just empty
    table_data = num2cell([]);
end
table_data{next_row,1} = current_aom_value;
table_data{next_row,2} = z;
set(handles.interp_table,'Data',table_data)

guidata(hObject, handles);
function interpolate_Callback(hObject, eventdata, handles)
table_data = get(handles.interp_table, 'Data');
z_interp_points = cell2mat(table_data(:,2));
aom_interp_points=cell2mat(table_data(:,1));
% Assuming all scans start from highest value
handles.scan.top_z = max(z_interp_points); %%%%%%%%%check this value may have to be negative.
bottom_z = handles.scan.top_z - handles.scan.slices_3D*handles.scan.z_steps_3D;%$
% watch out for the step size and number of slice. you don't want to go
% over the minimum z
z_interp = linspace(handles.scan.top_z,bottom_z,handles.scan.slices_3D);

handles.aom_interp = interp1(z_interp_points,aom_interp_points,z_interp,'linear',max(aom_interp_points));%$aom_interp didn't need it
depth=linspace(0,handles.scan.slices_3D*handles.scan.z_steps_3D,handles.scan.slices_3D);
plot(handles.interp_plot,depth,handles.aom_interp);
xlabel(handles.interp_plot,'Z(um)','fontsize',7);
ylabel(handles.interp_plot,'AOM(% volt)','fontsize',7.5);
set(handles.interp_plot,'LooseInset',get(handles.interp_plot,'TightInset'))
set(handles.loop_scan,'Enable','on');

guidata(hObject, handles);
function reset_aom_Callback(hObject, eventdata, handles)
set(handles.interp_table,'Data',[]);
cla(handles.interp_plot,'reset');
xlabel(handles.interp_plot,'Z(um)','fontsize',7.5);
ylabel(handles.interp_plot,'AOM(% volt)','fontsize',7.5);
set(handles.interp_plot,'LooseInset',get(handles.interp_plot,'TightInset'))
hold off
set(handles.loop_scan,'Enable','off');
guidata(hObject, handles);

%% IMAGE DISPLAY
function PMT_image_popup_Callback(hObject, eventdata, handles)

switch get(handles.PMT_image_popup,'Value')
    case 1
        handles.channel = 1;
    case 2
        handles.channel = 2;
    case 3
        handles.channel = 3;
end

guidata(hObject, handles);
function ramp_type_popupmenu_Callback(hObject, eventdata, handles)
handles.scan.ramp_type = get(hObject,'Value');
guidata(hObject, handles);

%% LIVE SCAN
function line_freq_live_Callback(hObject, eventdata, handles)
handles.scan.line_frequency_live = str2double(get(handles.line_freq_live,'String'))

% Assuming fast axis is always in x direction
set(handles.line_per_sec_live,'String',num2str(handles.scan.line_frequency_live));
set(handles.frame_per_sec_live,'String',num2str( handles.scan.line_frequency_live / handles.scan.ny));

guidata(hObject, handles);
function shift_Callback(hObject, eventdata, handles)
handles.scan.shift = str2double(get(hObject,'String'));
guidata(hObject, handles);
function extra_points_live_Callback(hObject, eventdata, handles)
function live_scan_start_Callback(hObject, eventdata, handles)
import liom.*;
set(handles.line_scan_start,'Enable','off');
set(handles.line_scan_stop,'Enable','off');

handles.scan.volt_x = double(handles.scan.scan_height/(58*2)); %unit = volts (for 1v = 58 um) ~ 2.79 v
handles.scan.volt_y = double(handles.scan.scan_width/(58*2)); %unit = volts (for 1v = 58 um) ~ 2.79 v

if(handles.save)
    
    handles.live_scan_num = handles.live_scan_num+1;
    scan = handles.scan;
    mkdir(fullfile(handles.folder_name,['Live_',num2str(handles.live_scan_num)]));
    save(fullfile(handles.folder_name,['Live_',num2str(handles.live_scan_num)],[['LiveScanInfo_',num2str(handles.live_scan_num)],'.mat']),'scan');
    
end

 handles.shutter.open();
%reset_lines_Callback(hObject, eventdata, handles);
n_extra_points =str2double(get(handles.extra_points_live,'String'));

switch(handles.scan.ramp_type)
    % First type of ramp is the sawtooth, standard ramp but slow since the
    % galvos take a long time to get down to zero
    case 1
        
        poly_ramp = liom.get_poly_return2(-handles.scan.volt_x, handles.scan.volt_x, handles.scan.nx, ...
            -handles.scan.volt_x, handles.scan.volt_x, handles.scan.nx, n_extra_points );
        ramp_x = [linspace(-handles.scan.volt_x,handles.scan.volt_x,handles.scan.nx),poly_ramp];
        handles.pos_x = repmat(ramp_x,[1 handles.scan.ny]);
        handles.pos_y = repmat(linspace(-handles.scan.volt_y,handles.scan.volt_y,handles.scan.ny),[length(ramp_x),1]);
        handles.data =[handles.pos_y(:),handles.pos_x(:)];
        total_size=(handles.scan.nx+n_extra_points)*handles.scan.ny;
        handles.freq_galvos=(handles.scan.nx+n_extra_points)* handles.scan.line_frequency_live;
        
        % Second type of ramp is the triangle, faster but need to reform the
        % image for every line
    case 2
        poly_ramp_top = liom.get_poly_return2(-handles.scan.volt_x, handles.scan.volt_x, handles.scan.nx, ...
            handles.scan.volt_x, -handles.scan.volt_x, handles.scan.nx, n_extra_points );
        poly_ramp_bot = liom.get_poly_return2(handles.scan.volt_x, -handles.scan.volt_x, handles.scan.nx, ...
            -handles.scan.volt_x, handles.scan.volt_x, handles.scan.nx, n_extra_points );
        
        handles.pos_x = repmat([poly_ramp_bot,linspace(-handles.scan.volt_x,handles.scan.volt_x,handles.scan.nx),...
            poly_ramp_top,linspace(handles.scan.volt_x,-handles.scan.volt_x,handles.scan.nx)],[1 handles.scan.ny/2]);
        
        handles.pos_y = repmat(linspace(-handles.scan.volt_y,handles.scan.volt_y,handles.scan.ny),[(handles.scan.nx+n_extra_points),1]);
        handles.data =[handles.pos_y(:),handles.pos_x(:)];
        total_size=(handles.scan.nx+n_extra_points)*handles.scan.ny;
        handles.freq_galvos=(handles.scan.nx+n_extra_points)*handles.scan.line_frequency_live;
        
    case 3 %UNDER CONSTRUCTION
  
end

% Ramp generation and image display setup

handles.scope=liom.ScopeClass(handles.freq_galvos,total_size,handles.scan.binning);
handles.scope.initiate();
handles.ao=liom.TrigAOClass('Dev1',0:1,'galvoAO');
handles.ao.config(handles.freq_galvos);
display_delegate = LiveDisplayClassCa(handles,total_size,n_extra_points);
handles.ao.addlistener('AODone',@display_delegate.acq_done);

%Write signals and move galvos
handles.ao.write_signals(handles.data);
handles.ao.start();
set(handles.current,'String','Live scan is ON');
guidata(hObject, handles);
function live_scan_stop_Callback(hObject, eventdata, handles)
set(handles.line_scan_start,'Enable','ON');
set(handles.line_scan_stop, 'Enable','ON');

handles.ao.stop();
delete(handles.ao);
handles.shutter.close();
if (handles.save)
    answer = questdlg('Do you want to save this scan?','Save','yes','no','yes');
    if strcmp(answer,'no')
        fullfile(handles.folder_name,['Live_',num2str(handles.live_scan_num)])
        rmdir(fullfile(handles.folder_name,['Live_',num2str(handles.live_scan_num)]),'s');
        handles.live_scan_num = handles.live_scan_num -1;
    end
end
set(handles.current,'String','Live scan/Shutter is OFF');
guidata(hObject, handles);

%% SAVE and BINNING
function save_Callback(hObject, eventdata, handles)
button_state = get(hObject,'Value');
if button_state == get(hObject,'Max')
    handles.save = 1;
     [handles.folder_name,handles.live_scan_num,handles.line_scan_num,handles.vol_scan_num]...
     = FindDirectory();%checks the current file numbers

%  handles.chosen_folder =0;
elseif button_state == get(hObject,'Min')
    handles.save = 0;
end
guidata(hObject, handles);
function bin_popupmenu_Callback(hObject, eventdata, handles)
contents = cellstr(get(hObject,'String'));
handles.scan.binning= str2double(contents{get(hObject,'Value')});
guidata(hObject, handles);

%% 3D SCAN
function line_freq_3D_Callback(hObject, eventdata, handles)
handles.scan.line_frequency_3D = str2double(get(handles.line_freq_3D,'String'));
set(handles.line_per_sec_3D,'String',num2str(handles.scan.line_frequency_3D ));
set(handles.frame_per_sec_3D,'String',num2str(handles.scan.line_frequency_3D/handles.scan.ny ));
guidata(hObject, handles);
function z_slices_Callback(hObject, eventdata, handles) %number of slices in 3D scan
handles.scan.slices_3D = str2double(get(handles.z_slices,'String'));%$
guidata(hObject, handles);
function z_step_size_Callback(hObject, eventdata, handles)
handles.scan.z_steps_3D = str2double(get(handles.z_step_size,'String'))/1000;
guidata(hObject, handles); % loop scan
function extra_points_3D_Callback(hObject, eventdata, handles) %the value is read in loop scan directly
function loop_scan_Callback(hObject, eventdata, handles)
import liom.*;

  handles.motors.move_az(handles.scan.top_z);
  [~,~,z]=handles.motors.get_pos();
  set(handles.live_z,'String',num2str(z));

set(handles.ramp_type_popupmenu,'Enable','off');

if(handles.save)
    
    handles.vol_scan_num = handles.vol_scan_num+1;
    % Force update of handles so that saving works
    guidata(hObject, handles);
    scan = handles.scan;
    mkdir(fullfile(handles.folder_name,['Volume_',num2str(handles.vol_scan_num)]));
    save(fullfile(handles.folder_name,['Volume_',num2str(handles.vol_scan_num)],[['VolumeScanInfo_',num2str(handles.vol_scan_num)],'.mat']),'scan');
    
end
handles.scan3D=1;
% Configure galvo
handles.shutter.open();%should check for if shutter is already open or not
%handles.ao=liom.TrigAOClass('Dev1',0:1,'galvoAO');
n_extra_points= str2double(get(handles.extra_points_3D,'String'));

switch(handles.scan.ramp_type)
    % First type of ramp is the sawtooth, standard ramp but slow since the
    % galvos take a long time to get down to zero
    case 1
        
        poly_ramp = liom.get_poly_return2(-handles.scan.volt_x, handles.scan.volt_x, handles.scan.nx, ...
            -handles.scan.volt_x, handles.scan.volt_x, handles.scan.nx, n_extra_points );
        ramp_x = [linspace(-handles.scan.volt_x,handles.scan.volt_x,handles.scan.nx),poly_ramp];
        handles.pos_x = repmat(ramp_x,[1 handles.scan.ny]);
        handles.pos_y = repmat(linspace(-handles.scan.volt_y,handles.scan.volt_y,handles.scan.ny),[length(ramp_x),1]);
        handles.data =[handles.pos_y(:),handles.pos_x(:)];
        total_size=(handles.scan.nx+n_extra_points)*handles.scan.ny;
        handles.freq_galvos=(handles.scan.nx+n_extra_points)*handles.scan.line_frequency_3D;
        
        % Second type of ramp is the triangle, faster but need to reform the
        % image for every line
    case 2
        poly_ramp_top = liom.get_poly_return2(-handles.scan.volt_x, handles.scan.volt_x, handles.scan.nx, ...
            handles.scan.volt_x, -handles.scan.volt_x, handles.scan.nx, n_extra_points );
        poly_ramp_bot = liom.get_poly_return2(handles.scan.volt_x, -handles.scan.volt_x, handles.scan.nx, ...
            -handles.scan.volt_x, handles.scan.volt_x, handles.scan.nx, n_extra_points );
        
        handles.pos_x = repmat([poly_ramp_bot,linspace(-handles.scan.volt_x,handles.scan.volt_x,handles.scan.nx),...
            poly_ramp_top,linspace(handles.scan.volt_x,-handles.scan.volt_x,handles.scan.nx)],[1 handles.scan.ny/2]);
        
        handles.pos_y = repmat(linspace(-handles.scan.volt_y,handles.scan.volt_y,handles.scan.ny),[(handles.scan.nx+n_extra_points),1]);
        handles.data =[handles.pos_y(:),handles.pos_x(:)];
        total_size=(handles.scan.nx+n_extra_points)*handles.scan.ny;
        handles.freq_galvos=(handles.scan.nx+n_extra_points)*handles.scan.line_frequency_3D;
        
        % Third type is spiral scans, for future analyses
    case 3
        disp('Not implemented yet')
end

handles.scope=liom.ScopeClass(handles.freq_galvos,total_size,handles.scan.binning);
handles.scope.initiate();
handles.ao=liom.TrigAOClass('Dev1',0:1,'galvoAO');
handles.ao.config(handles.freq_galvos);
display_delegate = LiveDisplayClassCa(handles,total_size,n_extra_points);
handles.ao.addlistener('AODone',@display_delegate.acq_done);

%**** This is how much time it takes to take one image with the current
% parameters
image_acq_time = total_size / handles.freq_galvos;

for i=1:handles.scan.slices_3D
    handles.interupt = get(handles.interupt_3D,'Value');
    
    if(handles.interupt)
        handles.interupt =0;
        set(handles.ramp_type_popupmenu,'Enable','on');

        break;
    else
        % Adjust power with AOM, handles.aom_interp is slider value between
        % 0-100%
        set(handles.current,'String',i);
        handles.aom_task.write(handles.aom_interp(i)/50);
        
        %Write signals and move galvos
        handles.ao.write_signals(handles.data);
        handles.ao.start();
        
        %Wait approx time to generate image + 0.1 sec
        pause(image_acq_time + 0.1);
        
        %move motor to lower position and repeat
        handles.motors.move_dz(-str2double(get(handles.z_step_size,'String'))/1000); % in mm %$
        handles.scope.initiate();
    end
end

set(handles.current,'String','Loop Scan Is Done or Interupted');
handles.ao.stop();
delete(handles.ao);
handles.shutter.close();
handles.scan3D=0;
if (handles.save)
    answer = questdlg('Do you want to save this scan?','Save','yes','no','yes');
    if strcmp(answer,'no')
        fullfile(handles.folder_name,['Volume_',num2str(handles.vol_scan_num)])
        rmdir(fullfile(handles.folder_name,['Volume_',num2str(handles.vol_scan_num)]),'s');
        handles.vol_scan_num = handles.vol_scan_num -1;
    end
end
set(handles.ramp_type_popupmenu,'Enable','on');

guidata(hObject, handles);
function interupt_3D_Callback(hObject, eventdata, handles)

button_state = get(hObject,'Value');
if button_state == get(hObject,'Max')
    handles.interupt = 1;
elseif button_state == get(hObject,'Min')
    handles.interupt = 0;
end
guidata(hObject, handles);

%% LINE/POINT SCAN
function exp_type_popupmenu_Callback(hObject, eventdata, handles)
contents = cellstr(get(hObject,'String'));
handles.scan.experiment_type = contents{get(hObject,'Value')};
guidata(hObject, handles);
function line_freq_line_Callback(hObject, eventdata, handles)
handles.scan.line_frequency_line = str2double(get(handles.line_freq_line,'String'));
set(handles.line_per_sec_line,'String',num2str( handles.scan.line_frequency_line ));
set(handles.frame_per_sec_line,'String',num2str( handles.scan.line_frequency_line / handles.scan.repeat_line_scan ));
guidata(hObject, handles);
function linescan_type_Callback(hObject, eventdata, handles)
handles.scan.LineRamp = get(hObject,'Value');
guidata(hObject, handles);
function Lines_button_Callback(hObject, eventdata, handles)
set(handles.current,'String','Draw your line');
axes(handles.pmt_image);
current_line_handle = imline();
set(current_line_handle, 'UserData',1);
% Here we only keep the handles to the line, we will get the positions at
% the end so that the user can move the line

handles.scan.segment_handles = [handles.scan.segment_handles, {current_line_handle}];
handles.line_handles = [handles.line_handles, current_line_handle];
prompt = 'Number of points per line?';
set(handles.current,'String','Enter NUMBER of points per line');
handles.scan.number_points_per_segment = [handles.scan.number_points_per_segment, str2double((cell2mat(inputdlg(prompt))))];%bug:user input should be limited to int only
set(handles.current,'String','Click "Lines" to draw another line or click "Start Line Scan" when ready');
guidata(hObject, handles);
function reset_lines_Callback(hObject, eventdata, handles)
set(handles.current,'String','Draw Lines Again');
handles.scan.number_points_per_segment = [];
delete(handles.line_handles);
handles.Exist_points = 0;
delete(handles.point_handles);
handles.scan.segment_handles= [];
handles.scan.sum_lines_points = [];
handles.histogSave = [];
guidata(hObject, handles);
function repeat_scan_Callback(hObject, eventdata, handles)
handles.scan.repeat_line_scan = str2double(get(handles.repeat_scan,'String'));
guidata(hObject, handles);
function extra_points_line_Callback(hObject, eventdata, handles) % the value is read in line_scan directly.
function point_Callback(hObject, eventdata, handles)
% hObject    handle to point (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.current,'String','select points');
axes(handles.pmt_image);
handles.Exist_points = 1;
current_point_handle = impoint();
set(current_point_handle,'UserData',0);
handles.scan.segment_handles = [handles.scan.segment_handles, {current_point_handle}];
handles.point_handles = [handles.point_handles, current_point_handle];

prompt = 'time spent scanning the point and counting photons?'; %the input should be converted to exta points
set(handles.current,'String','Enter the time at which the galvo will stay on the point');
handles.scan.number_points_per_segment = [handles.scan.number_points_per_segment, str2double((cell2mat(inputdlg(prompt))))];
set(handles.current,'String','Click "Points" to select another line or click "Start Line Scan" when ready');

guidata(hObject, handles);
function shift_lineScan_Callback(hObject, eventdata, handles)
handles.scan.shift_linescan = str2double(get(hObject,'String'));
guidata(hObject, handles);
function line_scan_start_Callback(hObject, eventdata, handles)
import liom.*;
handles.td = liom.TDClass();

set(handles.live_scan_stop,'Enable','off');
set(handles.live_scan_start,'Enable','off');
% %*************by cong
% handles.Syn_time = dabs.ni.daqmx.Task('Syn_time');%times for synchronizationn with electrophysiology
% handles.Syn_time.createDOChan('Dev1','port0/line6');
handles.Syn_point = dabs.ni.daqmx.Task('Syn_point');%points for synchronization with electrophysiology
handles.Syn_point.createDOChan('Dev1','port0/line7');
% %********

 [~,~,z]=handles.motors.get_pos();
 handles.scan.zpos = z ;

positions=[];
handles.laserPulse_per_frame = 0;
handles.lineScan = 1;
n_extra_points = str2double(get(handles.extra_points_line,'String'));

 handles.ao=liom.TrigAOClass('Dev1',0:1,'galvoAO');
if(handles.Exist_points)
    handles.gate = dabs.ni.daqmx.Task('Syn_gate');
    handles.gate.createDOChan('Dev1','port0/line3','','DAQmx_Val_ChanPerLine');
end

% Count segments (either a point or a line)
n_segments = length(handles.scan.segment_handles);
handles.scan.sum_lines_points = sum(handles.scan.number_points_per_segment)+n_segments*n_extra_points;
total_size = handles.scan.sum_lines_points * handles.scan.repeat_line_scan;

% output number of points per second that galvo moves and takes data in.
freq_galvos= handles.scan.sum_lines_points*handles.scan.line_frequency_line; %e.g. for a single point, 100+11extra * 800 Hz

ramp_x=[];
ramp_y=[];
gate_signal=[];

% Do the ramp for a single line, the line moves in both dimensions
switch(handles.scan.LineRamp)
%     %**********by Cong
%     handles.Syn_time.writeDigitalData(1);
%     handles.Syn_time.writeDigitalData(0);
%     %*******end
    case 1 %triangle
        
        for i=1:length(handles.scan.segment_handles)
            
            if (get(handles.scan.segment_handles{i},'UserData'))
                
                % This is a line
                line_positions = getPosition(handles.scan.segment_handles{i});

                line_positions(:,1)-30; %the offset of -30 is added
                line_positions(:,2)+40;

                line_positions(:,1)-30; 
                line_positions(:,2)+40;

                positions = [positions,line_positions];
                gate_signal = [gate_signal, repmat(zeros(1,n_extra_points+handles.scan.number_points_per_segment(i)),[1,round(handles.scan.repeat_line_scan)])];
                
            else
                
                % This is a point
                point_positions = getPosition(handles.scan.segment_handles{i});
                double_the_point = [point_positions;point_positions];
                double_the_point(:,1)-30;
                double_the_point(:,2)+40;
                positions = [positions,double_the_point];
                gate_signal = [gate_signal, repmat(ones(1,(n_extra_points+handles.scan.number_points_per_segment(i))),[1,round(handles.scan.repeat_line_scan)])];
             
            end
            
        end
        
        handles.pos_x = [];
        handles.pos_y = [];
        
        for i = 1:n_segments
           
               curr_line = positions(1:2,(2*i)-1:(2*i)); 
            % x
            poly_ramp_top = liom.get_poly_return2(curr_line(1,1), curr_line(2,1), handles.scan.number_points_per_segment(i), ...
                curr_line(2,1), curr_line(1,1), handles.scan.number_points_per_segment(i), ...
                n_extra_points );
            poly_ramp_bot = liom.get_poly_return2(curr_line(2,1), curr_line(1,1), handles.scan.number_points_per_segment(i), ...
                curr_line(1,1), curr_line(2,1), handles.scan.number_points_per_segment(i), ...
                n_extra_points );
            ramp_x=[poly_ramp_bot,linspace(curr_line(1,1),curr_line(2,1),handles.scan.number_points_per_segment(i)),...
                poly_ramp_top,linspace(curr_line(2,1),curr_line(1,1),handles.scan.number_points_per_segment(i)),];
            handles.pos_x = [handles.pos_x, repmat(ramp_x,[1 handles.scan.repeat_line_scan/(2)])];
        
           % y
            poly_ramp_top = liom.get_poly_return2(curr_line(1,2), curr_line(2,2), handles.scan.number_points_per_segment(i), ...
                curr_line(2,2), curr_line(1,2), handles.scan.number_points_per_segment(i), ...
                n_extra_points );
            
            poly_ramp_bot = liom.get_poly_return2(curr_line(2,2), curr_line(1,2),  handles.scan.number_points_per_segment(i), ...
                curr_line(1,2), curr_line(2,2), handles.scan.number_points_per_segment(i), ...
                n_extra_points );
            
            ramp_y = [poly_ramp_bot,linspace(curr_line(1,2),curr_line(2,2),handles.scan.number_points_per_segment(i)),...
                poly_ramp_top,linspace(curr_line(2,2),curr_line(1,2),handles.scan.number_points_per_segment(i)) ];
            handles.pos_y = [handles.pos_y, repmat(ramp_y,[1 handles.scan.repeat_line_scan/(2)])];

                    
        end
        gate_signal = gate_signal';
        
        frame_time = freq_galvos/length(gate_signal); % frame/seconds. time it takes to create one frame
        laser_pulse = 80*10^6/frame_time; %pulses per frame. how many laser pulses are in a single frame. 80e6 is in pulse/sec.
        handles.laserPulse_per_frame = laser_pulse *(numel(find(gate_signal))/length(gate_signal)); %how many pulses we count when the gate_signal is 1
        
%            segment_time = length(gate_signal)/(freq_galvos*0.001) %ms
%            interval = segment_time/length(gate_signal)
%            t = (1:interval:segment_time)
% figure;
%             plot( t, handles.pos_x,'.r');
%             hold on
%            plot(handles.pos_y,'.b');
%            hold on 
%            plot( gate_signal*50,'.g');
%             hold off
%             legend('X Galvo','Y Galvo','Gate Signal*50');

%            xlabel('time(ms)')
        
    case 2 %sawtooth % has to be modified like triangle mode.
        
        for i=1:length(handles.scan.segment_handles)
            if (get(handles.scan.segment_handles{i},'UserData'))
                % This is a line
                line_positions = getPosition(handles.scan.segment_handles{i});
                positions = [positions,line_positions];
                gate_signal = [gate_signal, zeros(1,n_extra_points),zeros(1,handles.scan.number_points_per_segment(i))];
                
            else
                % This is a point
                point_positions = getPosition(handles.scan.segment_handles{i});
                double_the_point = [point_positions;point_positions];
                positions = [positions,double_the_point];
                gate_signal = [gate_signal,zeros(1,n_extra_points),ones(1,handles.scan.number_points_per_segment(i))];
            end
        end
                
        prev_line = positions(end-1:end,end-1:end);%get the positoin of the last line/point coordinates (2x2 matrix)
        prev_number_of_points = handles.scan.number_points_per_segment(n_segments); %get the number points per line/point of last line
        
        for i=1:n_segments
            curr_line = positions(1:2,(2*i)-1:(2*i));
            % Build polynomial ramp to start of line with equal end points and
            % slopes
            poly_ramp = liom.get_poly_return2(prev_line(1,1), prev_line(2,1), prev_number_of_points, ...
                curr_line(1,1), curr_line(2,1), handles.scan.number_points_per_segment(i), ...
                n_extra_points );
            
            ramp_x=[ramp_x,poly_ramp];
            ramp_x=[ramp_x,linspace(curr_line(1,1),curr_line(2,1),handles.scan.number_points_per_segment(i))];
            
            % Do the actual ramp for imaging of the i-th line, x1,y1
            % Move from previous line to this one using the extra ramp points set
            % Build polynomial ramp to start of line with equal end points and
            % slopes
            poly_ramp = liom.get_poly_return2(prev_line(1,2), prev_line(2,2), prev_number_of_points, ...
                curr_line(1,2), curr_line(2,2), handles.scan.number_points_per_segment(i), ...
                n_extra_points );
            
            ramp_y=[ramp_y,poly_ramp];
            % Do the actual ramp for imaging of the i-th line, x1,y1
            ramp_y=[ramp_y,linspace(curr_line(1,2),curr_line(2,2),handles.scan.number_points_per_segment(i))];
            
            prev_line = curr_line;
            prev_number_of_points=handles.scan.number_points_per_segment(i);
        end
        % Now repmat this line to generate the image
        handles.pos_x = repmat(ramp_x,[1 handles.scan.repeat_line_scan]);
        handles.pos_y = repmat(ramp_y,[1 handles.scan.repeat_line_scan]);
        gate_signal=repmat(gate_signal,[1 handles.scan.repeat_line_scan])';
                
        frame_time =length(gate_signal)/freq_galvos; %time it takes to create one frame
        laser_pulse = 80*10^6 /frame_time; %how many laser pulses are in a single frame
        handles.laserPulse_per_frame = laser_pulse *(numel(find(gate_signal))/length(gate_signal)); %how many pulses we count when we gate_signal is 1
        
end

handles.n_segments = n_segments;
handles.scan.n_extra_points = n_extra_points;
handles.scan.ramp_x = ramp_x;
handles.scan.ramp_y = ramp_y;

handles.scope=liom.ScopeClass(freq_galvos,total_size,handles.scan.binning);
handles.scope.initiate();
handles.ao.config(freq_galvos);

% calculating photon count rate during lifetime measurement

% Now need to scale from the pixel values returned by imline to voltage
% values: We assume that we are drawing on the latest image, so nx and ny
% as well as voltx and volty are valid.
handles.pos_x = (handles.pos_x-(handles.scan.nx/2))*(handles.scan.volt_x*2)/(handles.scan.nx);
handles.pos_y = (handles.pos_y -(handles.scan.ny/2))*(handles.scan.volt_y*2)/(handles.scan.ny);
handles.data  = [handles.pos_x(:),handles.pos_y(:)];

handles.shutter.open();

if (handles.Exist_points)
           
    handles.gate.cfgSampClkTiming(freq_galvos,'DAQmx_Val_FiniteSamps',size(gate_signal,1));
    handles.gate.cfgDigEdgeStartTrig('/Dev1/ao/StartTrigger');
    handles.gate.set('startTrigRetriggerable',1)
    handles.gate.writeDigitalData(gate_signal, inf,false,size(gate_signal,1));
    handles.gate.start();
    if  (handles.scan.tacq_parameter == 0)
        display('Tacq is not in direct use');
        handles.td.Tacq=length(gate_signal)/(freq_galvos*0.001) + 200; %ms
    else
        handles.td.Tacq = handles.scan.tacq_parameter;
    end
  
    if  (handles.scan.cfdLevel1 == 0)
          display('CFDLevel1 is not in direct use');
    else
        handles.td.CFDLevel1 = handles.scan.cfdLevel1;
    end
    
    if  (handles.scan.cfdZeroX1== 0)
    display('CFDZeroX1 is not in direct use');
    else
        handles.td.CFDLevel1 = handles.scan.cfdZeroX1;
    end
    
    
   handles.td.initialize();
   
end

if(handles.save)
    handles.line_scan_num = handles.line_scan_num+1;
    scan =handles.scan;
    mkdir(fullfile(handles.folder_name,['Line_',num2str(handles.line_scan_num)]));
    save(fullfile(handles.folder_name,['Line_',num2str(handles.line_scan_num)],[['LineScanInfo_',num2str(handles.line_scan_num)],'.mat']),'scan');
    image_ref_pmt = getimage(handles.pmt_image);
    image_ref_adp = getimage(handles.image);
    save(fullfile(handles.folder_name,['Line_',num2str(handles.line_scan_num)],'image_pmt.mat'),'image_ref_pmt');
    save(fullfile(handles.folder_name,['Line_',num2str(handles.line_scan_num)],'image_adp.mat'),'image_ref_adp');
end

display_delegate = LiveDisplayClassCa(handles,total_size,n_extra_points);
handles.ao.addlistener('AODone',@display_delegate.acq_done);
set(handles.current,'String','Scanning segments');

handles.ao.write_signals(handles.data);
if (handles.Exist_points)
 
    handles.td.start_meas();
 
end

handles.ao.start();

guidata(hObject, handles);
function line_scan_stop_Callback(hObject, eventdata, handles)
set(handles.live_scan_start,'Enable','ON');
set(handles.live_scan_stop, 'Enable','ON');
delete(handles.td);
delete(handles.Syn_point);



handles.lineScan = 0; % it means line_scan is OFF
handles.ao.stop();
if (handles.Exist_points)
    handles.gate.abort();
    delete(handles.gate);
end
delete(handles.ao);
handles.shutter.close();
set(handles.ramp_type_popupmenu,'Enable','on');
if (handles.save)
    answer = questdlg('Do you want to save this scan?','Save','yes','no','yes');
    if strcmp(answer,'no')
        fullfile(handles.folder_name,['Line_',num2str(handles.line_scan_num)])
        rmdir(fullfile(handles.folder_name,['Line_',num2str(handles.line_scan_num)]),'s');
        handles.line_scan_num = handles.line_scan_num -1;
    end
end
set(handles.current,'String','Line scan/Shutter is OFF');
guidata(hObject, handles);

function flim_Callback(hObject, eventdata, handles) % has to define how it works because it is not clear
import liom.*;
if(handles.save)
    handles.flim_scan_num = handles.flim_scan_num+1;
    scan =handles.scan;
    mkdir(fullfile(handles.folder_name,['FLIM_',num2str(handles.flim_scan_num)]));
end

handles.scan.volt_x = double(handles.scan.scan_height/(58*2)); %unit = volts (for 1v = 58 um) ~ 2.79 v
handles.scan.volt_y = double(handles.scan.scan_width/(58*2)); %unit = volts (for 1v = 58 um) ~ 2.79 v

points_x=linspace(-handles.scan.volt_x,handles.scan.volt_x,100);
points_y=zeros(1,100);
handles.td.Tacq=1000;
handles.td.initialize();

handles.shutter.open();
handles.aod = liom.OnDemandAOClass('Dev1',0:1,'flim_task');

handles.gated = dabs.ni.daqmx.Task('Syn_gate');
handles.gated.createDOChan('Dev1','port0/line3','','DAQmx_Val_ChanPerLine');
handles.gated.writeDigitalData(1);

set(handles.current,'String','Live scan is ON');

scan.profile=[];
for i=1:100
    
    handles.aod.write([points_x(i) points_y(i)]);
    handles.td.start_meas();
    pause(1.1);
    figure(99);
    histogRead = handles.td.read_meas();
    scan.profile=[scan.profile,sum(histogRead)];
    plot(scan.profile,'s');
    
    percent_count = (sum(histogRead)/(80*10^6))*100 ;
    set(handles.ph_count,'String',num2str(percent_count));
    
    if (percent_count > 5)
        set(handles.current,'String','Lower AOM to get less than 5% counts');
    else
        set(handles.current,'String','Scanning segments');
    end
    
    ns = 12.4/0.004 ; %this is for plotting up to 12.4 ns
    t = (1:ns)*0.004; %ns
    % histogRead = histogRead - histogRead(1,2)  ;
    semilogy(t,histogRead(1:ns),'Parent',handles.lifetime_image);
    if(handles.save)

    save(fullfile(handles.folder_name,['FLIM_',num2str(handles.flim_scan_num)],[['FLIMScanInfo_',num2str(handles.flim_scan_num)],'.mat']),'scan');
    image_ref_pmt = getimage(handles.pmt_image);
    image_ref_adp = getimage(handles.image);
    save(fullfile(handles.folder_name,['FLIM_',num2str(handles.flim_scan_num)],'image_pmt.mat'),'image_ref_pmt');
    save(fullfile(handles.folder_name,['FLIM_',num2str(handles.flim_scan_num)],'image_adp.mat'),'image_ref_adp');
               
     handles.histogSave = [handles.histogSave;histogRead]; %I wonder if it saves handles.scan.histog
     histogSave = handles.histogSave;
     save(fullfile(handles.folder_name,['FLIM_',num2str(handles.flim_scan_num)],[['FLIMScanHistogram_',num2str(handles.flim_scan_num)],'.mat']),'histogSave');
   
    end                  
   
end

handles.shutter.close();
delete(handles.aod)
handles.gated.writeDigitalData(0);
delete(handles.gated);

guidata(hObject, handles);

%% LIFETIME PARAMETERS
function tacq_parameter_Callback(hObject, eventdata, handles)
handles.scan.tacq_parameter=str2double(get(hObject,'String'));
guidata(hObject, handles);

function cfdLevel1_parameter_Callback(hObject, eventdata, handles)
handles.scan.cfdLevel1=str2double(get(hObject,'String'));
guidata(hObject, handles);

function cfdzeroX1_parameter_Callback(hObject, eventdata, handles)
handles.scan.cfdZeroX1=str2double(get(hObject,'String'));

guidata(hObject, handles);


%% **********************************CreatFcn ****************************************** %%
function ny_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function nx_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


guidata(hObject, handles);
function height_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function width_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function xy_motor_step_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function z_motor_step_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function aom_slider_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
function interp_table_CreateFcn(hObject, eventdata, handles)
function PMT_image_popup_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function ramp_type_popupmenu_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function line_freq_live_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function shift_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function extra_points_live_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function line_freq_3D_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function extra_points_3D_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function z_slices_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function z_step_size_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function line_freq_line_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function repeat_scan_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function extra_points_line_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function bin_popupmenu_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function linescan_type_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function shift_lineScan_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function exp_type_popupmenu_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function cfdzeroX1_parameter_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function tacq_parameter_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function cfdLevel1_parameter_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% Bugs

%25-08-2014
%check the line that saves the z position in interpulation section
% check if move.motors.az is actually going to the position you give.

%7-11-2013
%--- When lines disappear, they should be reseted or deleted. Example: After
%we do a line scan, if we click on Live scan Start button, the lines
%disappear, meaning they shoud be deleted. Then, after choosing another
%site, we should click on Lines button to start over again. This doesn't
%happen at the moment...
%---Double check if all the extra points correspond to the correct scan type
%---Changing pmt popup is not simultanious. Meaning that the scan has to be
%stopped, pmt channel changed, then re-start the scan to see the change in
%channel.
%---The line scan image axes corresponds to the channel chosen on pmt
%popup. Could this be a potential bug?
%---Save directory should be fixed to save a folder EXP1 etc...
%---SaveFor function should be checked, I think line axes slows down when I
%capture the frame image...
%---Motor position status does not change simultaniously
%---Truncation should be added-Should each scan type have it's own trunc
%value? should one be enough for all channels(except galvos of course)?






