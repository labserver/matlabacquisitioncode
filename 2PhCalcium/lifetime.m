%%%%%%%%%%%%%%%%%%%%%%%2014-01-16%%%%%%%%%%%%%%%%%%%%%%%%%%%
%this function will compute the lifetimes of the data obtained in an "*.m" file
%lifetime(data,plotIt,start_pt,end_pt)
%%%%%%%%%%%%%%%%%%%%%%Maryam Tabatabaei%%%%%%%%%%%%%%%%%%%%%

%plotIt takes 0 or 1 value

function [tau]= lifetime(data,plotIt,start_pt,end_pt)
if(nargin > 2)
    start_fit=start_pt;
    end_fit=end_pt;
    plotIt = plotIt;
else
    [~, index] = max(data);
    start_fit=index+100;
    end_fit=(36.8/100)*start_fit+start_fit;
    plotIt =  plotIt;
end
%plot the lifetime's exp decay

t=(1:length(data))*0.004; %ns %intiger operand error
cutout = data(start_fit:end_fit);
tt = t(start_fit:end_fit);
P = polyfit(tt,log(double(cutout)),1);
yfit = P(1)*(tt)+P(2);
tau= -(1/P(1)); %ns
if (plotIt)
    
    figure;
    subplot(3,1,1)
    plot(t,data);
    xlabel('ns','FontSize',13);
    ylabel('count','FontSize',13);
%     ylim([50 max(data)]);
    subplot(3,1,2)
    semilogy(t,data);
%    ylim([50 max(data)]);
    xlabel('ns','FontSize',13);
    ylabel('log count','FontSize',13);
    %fit to exp decays
    subplot(3,1,3)
    semilogy(tt,cutout,'-.')
    hold on
    semilogy(tt,exp(yfit),'r')
    xlabel('ns','FontSize',13);
    ylabel('log count','FontSize',13);
    hold off
    
    
end

if nargout > 1
    tau = tau;
end
end