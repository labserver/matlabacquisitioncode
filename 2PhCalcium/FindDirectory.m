%%%%% Maryam Tabatabaei mstaba68@gmail.com %%%%%
%%%%% This m file is checking saving status, whether we want to continue
%%%%% with what we have saved so far or we want to begin from begining.
%%%%% TAKE CAUTION: IF YOU CHOOSE NO AS AN ANSWER, MAKE SURE THE DIRECTORY
%%%%% YOU HAVE CHOSEN DOES NOT CONTAIN ANY SIMILAR FILES WITH OTHER NUMBERS
%%%%% IN IT %%%%%

function  [filename,live_num,line_num,vol_num] = FindDirectory()

button = questdlg('Choose one of the following:','Find Directory','Continue','NEW directory','Continue');

switch (button)
    
    case 'NEW directory'
        
        myDir = uigetdir; %('C:\Users\Liom\Desktop');
        num1=0;
        num2=0;
        num3=0;
        num4=0;
        
    case 'Continue'
        
        myDir = uigetdir;%('C:\Users\Liom\Desktop');
        live_dir =dir(fullfile(myDir,'Live_*'));
        line_dir =dir(fullfile(myDir,'Line_*'));
        vol_dir =dir(fullfile(myDir,'Volume_*'));
        
        %maybe num1 hast to +1
        %double check if there's another way to check for ifexist a file
        %with whatever name
        
        if(size(live_dir,1)>= 1)
            
            gg = [];
            bb = struct2cell(live_dir);
            for i =1:size(live_dir,1)
                gg = [gg, bb(1,i)];
            end
            gg= sort_nat(gg);
            lastFile = gg(end);
            num1 = cell2mat(lastFile);
            num1 = str2num(num1(6:end));
        else
            num1 = 0;
        end
        
        if (size(line_dir,1) >= 1)
            gg = [];
            bb = struct2cell(line_dir);
            for i =1:size(line_dir,1)
                gg = [gg, bb(1,i)];
            end
            gg= sort_nat(gg);
            lastFile = gg(end);
            num2 = cell2mat(lastFile);
            num2 = str2num(num2(6:end));
            
        else
            num2 = 0;
        end
        
        if (size(vol_dir,1) >= 1)
            gg = [];
            bb = struct2cell(vol_dir);
            for i =1:size(vol_dir,1)
                gg = [gg, bb(1,i)];
            end
            gg= sort_nat(gg);
            lastFile = gg(end)
            num3 = cell2mat(lastFile)
            num3 = str2num(num3(8:end))
            
        else
            num3 = 0;
        end
        
end
        filename = myDir;
        live_num = num1;
        line_num = num2;
        vol_num = num3;
        
        
        
        
        %myString = 'Hello, world!';
        %mySubstring = myString(3:end)
