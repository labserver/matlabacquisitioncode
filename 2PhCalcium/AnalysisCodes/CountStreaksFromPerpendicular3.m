function [t, NumberOfStreaks] = CountStreaksFromPerpendicular3(scan, image)
% scan: scan information
% image: longitudinal scan

LineScanRepeat = scan.repeat_line_scan; % Line Scan Repeat
LineScanFrequency = scan.line_frequency_line; % Line Scan Frequency (lines per sec)

sigma = 2.0;
KernelSize = [2.*round((10*sigma+1)/2)-1];
h = fspecial('gaussian', KernelSize, sigma); % creates a two-dimensional Gaussian lowpass filter. 

%Finding the number of streaks at each scan segment (one segment is one TIFF page corresponding to 200 repeats)
for i=1:size(image,3)
    
     image(:,:,i) = medfilt2(image(:,:,i), [3 3]) ;
%      image(:,:,i) = imadjust(image(:,:,i));%     image(:,:,i) = imadjust(image(:,:,i));
     image(:,:,i)=imfilter(image(:,:,i),h,'same','replicate'); %applying the Gaussian filter to 2D image (gaussian blurring)

    % Subtracting the spatial mean over the scan from image to remove light inhomogeneities
    column_mean=mean(image(:,:,i),2);
    for j=1:size(image,1)
        image(j,:,i)=image(j,:,i)-column_mean(j);
    end

    image_rot = imrotate(image(:,:,i),90,'loose'); % Rotates the image so the streaks are vertical 
    
    AvgWindow = 5; % smaller amounts will not be able to detect some peaks
    across = mean(image_rot); % means of each column in a row vector (converts the 2D graph to 1D curve)
    smooth_across = moving_avg(across,AvgWindow); %Moving average of the mean curve

    [maxtab, mintab] = peakdet(smooth_across, 25);
    
    NumberOfStreaks(i) = size(mintab,1);
    

    t(i) = ( (i-1)*LineScanRepeat + LineScanRepeat/2 ) /LineScanFrequency; % time corresponding to the center of the scan segment i (in sec)
    
end



end

