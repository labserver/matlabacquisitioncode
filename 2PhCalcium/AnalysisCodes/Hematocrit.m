function Hct = Hematocrit(flux,speed,diameter)
% Inputs : 
% flux: RBC Flux (cells /s)
% speed: RBC speed (mm/s)
% diameter: vessel inner diameter (average value) (um) 
% Ouput : hematocrit (%)


% Typical values : 
% flux = 190 RBCs/s ;
% speed = 1-5 mm/s (capillaries, venules), 5-10 m/s (arterioles) 
% radius = 2-7 um (cap.), 20 um (arteriole), 60 um (venule)
% Hct : 22% (large variance, +/- 19% over 100 capillaries).
% (Santisakultarm)


RBCVolume = 55; % Volume of a RBC, um^3 (the value found for C57Bl/6 WT mice in the literature)

Hct = 100.* flux .* RBCVolume ./ ((abs(speed)*1000) .* pi .* (diameter.^2) ./4 ); % hematocrit (%)

