function [t, RadonAngle] = StreakAngle(scan, image)

LineScanRepeat = scan.repeat_line_scan; % Line Scan Repeat
LineScanFrequency = scan.line_frequency_line; % Line Scan Frequency (lines per sec)

sigma = 1.0;
KernelSize = [2.*round((6*sigma+1)/2)-1];
h = fspecial('gaussian', KernelSize, sigma); % creates a two-dimensional Gaussian lowpass filter. 

%Finding the slope of the streaks at each scan segment (one segment is one TIFF page corresponding to 200 repeats)
for i=1:size(image,3)

    image(:,:,i)=imfilter(image(:,:,i),h,'same','replicate'); %applying the Gaussian filter to 2D image (gaussian blurring)
    
    % Subtracting the spatial mean over the scan from image to remove light inhomogeneities
    column_mean=mean(image(:,:,i),1);
    for j=1:size(image,2)
        image(:,j,i)=image(:,j,i)-column_mean(j);
    end
    ImageSection = image(:,:,i);
    ImageSection=ImageSection-mean(ImageSection(:));
    ImageSection = ImageSection(round(0.3*size(ImageSection,1)):round(0.7*size(ImageSection,1)),round(0.25*size(ImageSection,2)):round(0.75*size(ImageSection,2)));
    ImageRadon=radon(ImageSection); % Radon transform of the image segment i from 0o to 179o 
    ImageRadonVariance=var(ImageRadon);
    [maxx the_angle]=max(ImageRadonVariance); %deterines the angle at which the Radon transform has the greatest variation
    the_angle = the_angle -1;
    if the_angle > 83 && the_angle < 97 % Then we need more precision
        plage_angles = [83:0.1:97];
        ImageRadon2 = radon(ImageSection,plage_angles);
        ImageRadonVariance2 = var(ImageRadon2);
        [maxx AngleIndex] = max(ImageRadonVariance2);
        the_angle = plage_angles(AngleIndex);
    end
    
    RadonAngle(i) = the_angle; % Radon Angle defined as the angle from the x-axis (t-axis in our case) counterclockwise (in degree)
    t(i) = ( (i-1)*LineScanRepeat + LineScanRepeat/2 ) /LineScanFrequency; % time corresponding to the center of the scan segment i (in sec) 
    
end

    

end
