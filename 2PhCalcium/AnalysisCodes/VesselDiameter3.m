function [t, VesselD2] = VesselDiameter3(scan, Image)             % scan: linescan information
                                                                % Image: Linescans

%% Scan Parameters
ScanHeight = scan.scan_height; % Height of live view where linescan was drawn (in um)
ScanWidth = scan.scan_width;  % Width of live view where linescan was drawn (in um)
XPixels = scan.nx; % Number of pixels of live view where linescan was drawn in X direction 
YPixels = scan.ny; % Number of pixels of live view where linescan was drawn in Y direction

XPixelSize = ScanHeight / XPixels;  % Pixel size in X direction (in um) 
YPixelSize = ScanWidth / YPixels;   % Pixel size in Y direction (in um)

LinePosition = getPosition(scan.segment_handles{1});  % [X1 Y1; X2 Y2] first row: X and Y of the start of the line, second row: X and Y of the end of the line (in pixels)
LineLength = sqrt(((LinePosition(2,2)-LinePosition(1,2))*YPixelSize)^2. + ((LinePosition(2,1)-LinePosition(1,1))*XPixelSize)^2.);  % Line length (in um)

LineScanRepeat = scan.repeat_line_scan; % Line Scan Repeat
LineScanFrequency = scan.line_frequency_line; % Line Scan Frequency (lines per sec)

%%Defining the gaussian function for curve fitting (in order to determine the vessel width)
    fgauss = @(x,xdata)(  exp(-(xdata-x(2)).^2/2/x(1).^2) ./ (max(exp(-(xdata-x(2)).^2/2/x(1).^2))) ); %xdata: x, x(1): standard deviation, x(2): mean
                                                                                                      % normalized in a way that the maximum is 1
nLineScan = 15;
Threshhold = 0.15;

%% Obtaining vessel diameter at each scan segment (one segment is one TIFF page corresponding to 200 repeats)
for i=1:size(Image,3)
    nDiameter = 0;
    LineDiameter = 0;
    
    for j=1:floor(size(Image,1)/nLineScan)
        LineScan=mean(Image((j-1)*nLineScan+1:j*nLineScan,:,i),1); % Scan segment i, Line number j
        xdata = linspace(0,LineLength,size(Image,2)); % x in um ( generates a row vector y of n points linearly spaced between and including a and b)
        LineScan = lindetrend(LineScan')'; % removing the trends
        LineScan = LineScan./max(LineScan); % normalizign to make the maximum 1
        [maxx indices] = max(LineScan);  % Maximum element and the index of the maximum element
        
        try
            Xparameters = lsqcurvefit(fgauss,[1 xdata(indices)],xdata,LineScan); % Nonlinear least-squares curev fitting
        catch exception
            disp(exception.identifier);
            disp(exception.stack(1));
            Xparameters = [NaN NaN];
        end
        
        VesselWidth = Xparameters(1)*2*sqrt(2*log(1.0/Threshhold));% vessel diameter defined as "full width at Threshhold * maximum" of the gaussian fit (in um)
                
        if VesselWidth > 0.1
            nDiameter = nDiameter + 1;
            LineDiameter(nDiameter) = VesselWidth;
        end
    end
    
    VesselD2(i) = mean2(LineDiameter); % Average vessel diameter of scan segment i (in um)
    VesselD_SD(i) = std2(LineDiameter); %SD of the vessl diameter of scan segment i
    t(i) = ( (i-1)*LineScanRepeat + LineScanRepeat/2 ) /LineScanFrequency; % time corresponding to the center of the scan segment i (in sec)
    
end

end
