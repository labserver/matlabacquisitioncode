function [t, NumberOfStreaks] = CountStreaks3(scan, image, angle)
% scan: scan information
% image: longitudinal scan
% angle: angle of streaks calculated before

LineScanRepeat = scan.repeat_line_scan; % Line Scan Repeat
LineScanFrequency = scan.line_frequency_line; % Line Scan Frequency (lines per sec)

sigma = 2.0;
KernelSize = [2.*round((10*sigma+1)/2)-1];
h = fspecial('gaussian', KernelSize, sigma); % creates a two-dimensional Gaussian lowpass filter. 

%Finding the number of streaks at each scan segment (one segment is one TIFF page corresponding to 200 repeats)
for i=1:size(image,3)
   
    image(:,:,i) = medfilt2(image(:,:,i), [5 5]) ;
    image(:,:,i)=imfilter(image(:,:,i),h,'same','replicate'); %applying the Gaussian filter to 2D image (gaussian blurring)

    % Subtracting the spatial mean over the scan from image to remove light inhomogeneities
    weightfactor = 0.995;
    column_mean=mean(image(:,:,i),1);
    for j=1:size(image,2)
        image(:,j,i)=image(:,j,i) - weightfactor .* column_mean(j);
    end

    image_rot = imrotate(image(:,:,i),-angle(i),'loose'); % Rotates the image so the streaks are vertical
    image_rot = image_rot(round(0.3*size(image_rot,1)):round(0.7*size(image_rot,1)),:);
   
    AvgWindow = 7; % smaller amounts will not be able to detect some peaks
    across = mean(image_rot); % means of each column in a row vector (converts the 2D graph to 1D curve)
    smooth_across = moving_avg(across,AvgWindow); %Moving average of the mean curve
 
    [xmax,imax,xmin,imin] = extrema(smooth_across);
    
    NumberOfStreaks(i) = size(xmin,2);
    if sum(imin==1) == 1
        NumberOfStreaks(i) = NumberOfStreaks(i) -1;
    end
    
    if sum(imin == size(smooth_across,2)) == 1
        NumberOfStreaks(i) = NumberOfStreaks(i) -1;
    end  

    t(i) = ( (i-1)*LineScanRepeat + LineScanRepeat/2 ) /LineScanFrequency; % time corresponding to the center of the scan segment i (in sec)
    
end

end