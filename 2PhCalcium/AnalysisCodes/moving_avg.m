function smooth_y = moving_avg(y,N)
% y must be row vector
if size(y,1)>size(y,2)
    y = y';
    end

% Moving average over N points
a = 1;
b = ones(1,N)/N;



% pad_y = padarray(y,[0 N],mean(y(1:2*N)),'both'); 
pad_y = padarray(y,[0 N],mean(y(1:2*N)),'pre'); 
smooth_y = filter(b,a,pad_y);
% smooth_y = smooth_y(N+round(N/1.1)+1:end-N+round(N/1.1));
smooth_y = smooth_y(N+round(N/1.1)+1:end);






% smooth_y = filter(b,a,y);



end