function varargout = RemoveImageSlides(varargin)
% REMOVEIMAGESLIDES MATLAB code for RemoveImageSlides.fig
%      REMOVEIMAGESLIDES, by itself, creates a new REMOVEIMAGESLIDES or raises the existing
%      singleton*.
%
%      H = REMOVEIMAGESLIDES returns the handle to a new REMOVEIMAGESLIDES or the handle to
%      the existing singleton*.
%
%      REMOVEIMAGESLIDES('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in REMOVEIMAGESLIDES.M with the given input arguments.
%
%      REMOVEIMAGESLIDES('Property','Value',...) creates a new REMOVEIMAGESLIDES or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before RemoveImageSlides_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to RemoveImageSlides_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help RemoveImageSlides

% Last Modified by GUIDE v2.5 29-May-2014 15:33:21

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @RemoveImageSlides_OpeningFcn, ...
                   'gui_OutputFcn',  @RemoveImageSlides_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before RemoveImageSlides is made visible.
function RemoveImageSlides_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to RemoveImageSlides (see VARARGIN)

% Choose default command line output for RemoveImageSlides
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes RemoveImageSlides wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = RemoveImageSlides_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Load_Button.
function Load_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Load_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Read Vertical Scan for analysis
handles.folder = uigetdir('D:\Users\Maryam.OPTIQUE-05\Documents\MATLAB');
handles.LineNumber = inputdlg('Enter the line number: ');
load([handles.folder '/LineScanInfo_' handles.LineNumber{1} '.mat']); %Linescan information

FileTif = [handles.folder '/linescan_520nm_' handles.LineNumber{1} '.tif'];
InfoImage=imfinfo(FileTif);
mImage=InfoImage(1).Width;
nImage=InfoImage(1).Height;
handles.NumberModifiedImages=length(InfoImage);
set(handles.slider_ModifiedScan,'Max',handles.NumberModifiedImages-1);
set(handles.slider_ModifiedScan, 'SliderStep', [1/(handles.NumberModifiedImages-1) 1/(handles.NumberModifiedImages-1)]);

handles.ModifiedImage=zeros(nImage,mImage,handles.NumberModifiedImages,'uint16');
for i=1:handles.NumberModifiedImages
   handles.ModifiedImage(:,:,i)=imread(FileTif,'Index',i);
end

handles.ImageInfo = scan;
clear scan

handles.FrameMatrix = linspace(1,handles.NumberModifiedImages,handles.NumberModifiedImages);


% Update display

% Shifted Vertical lineScan Segments
axes(handles.ModifiedScan)
imagesc(squeeze(handles.ModifiedImage(:,:,1))); colormap 'gray'
set(handles.slider_ModifiedScan,'Value',0)
set(handles.ModifiedScan_Stext,'String', (get(handles.slider_ModifiedScan,'Value')+1));


% Update handles structure
guidata(hObject, handles);



% --- Executes on button press in Save_button.
function Save_button_Callback(hObject, eventdata, handles)
% hObject    handle to Save_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Saving the modified scan
for i=1:size(handles.ModifiedImage,3)
    imwrite(handles.ModifiedImage(:,:,i),[handles.folder '/ModifiedImage_' handles.LineNumber{1} '.tif'],'WriteMode','append');
end

FrameMatrix = handles.FrameMatrix;
save([handles.folder '/OriginalFrameNo_' handles.LineNumber{1} '.mat'], 'FrameMatrix');



% --- Executes on slider movement.
function slider_ModifiedScan_Callback(hObject, eventdata, handles)
% hObject    handle to slider_ModifiedScan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

%update display
axes(handles.ModifiedScan)
imagesc(squeeze(handles.ModifiedImage(:,:,uint8(get(handles.slider_ModifiedScan,'Value')+1)))); colormap 'gray'
set(handles.ModifiedScan_Stext,'String', (get(handles.slider_ModifiedScan,'Value')+1));

% Update handles structure
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function slider_ModifiedScan_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_ModifiedScan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function ModifiedScan_Stext_Callback(hObject, eventdata, handles)
% hObject    handle to ModifiedScan_Stext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ModifiedScan_Stext as text
%        str2double(get(hObject,'String')) returns contents of ModifiedScan_Stext as a double


% --- Executes during object creation, after setting all properties.
function ModifiedScan_Stext_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ModifiedScan_Stext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Delete_button.
function Delete_button_Callback(hObject, eventdata, handles)
% hObject    handle to Delete_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Calculating the shifted image
CurrentSlide = uint8(get(handles.slider_ModifiedScan,'Value'))+1;
handles.ModifiedImage(:,:,CurrentSlide) = [];
handles.NumberModifiedImages = handles.NumberModifiedImages -1;
set(handles.slider_ModifiedScan,'Max',handles.NumberModifiedImages-1);
set(handles.slider_ModifiedScan, 'SliderStep', [1/(handles.NumberModifiedImages-1) 1/(handles.NumberModifiedImages-1)]);


if CurrentSlide ==1
    set(handles.ModifiedScan_Stext,'String', CurrentSlide);
    set(handles.slider_ModifiedScan,'Value',CurrentSlide -1);
    
    % Display update
    axes(handles.ModifiedScan)
    imagesc(squeeze(handles.ModifiedImage(:,:,CurrentSlide))); colormap 'gray'   
else
    set(handles.ModifiedScan_Stext,'String', CurrentSlide-1);
    set(handles.slider_ModifiedScan,'Value',CurrentSlide -2);
    
    % Display update
    axes(handles.ModifiedScan)
    imagesc(squeeze(handles.ModifiedImage(:,:,CurrentSlide -1))); colormap 'gray'
end

handles.FrameMatrix(:,CurrentSlide) = []; 


% Update handles structure
guidata(hObject, handles);
