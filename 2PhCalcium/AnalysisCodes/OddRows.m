function OddRows(folder, LineNumber)

% Verify if odd-row image already exists
tmp=dir([folder '/linescan_520nm_' LineNumber  '_oddrows.tif']);

% If odd-row image does not exists, creates the image, otherwise doesn't do anything
if size(tmp,1) == 0
    FileTif = [folder '/linescan_520nm_' LineNumber '.tif'];
    InfoImage=imfinfo(FileTif);
    mImage=InfoImage(1).Width;
    nImage=InfoImage(1).Height;
    NumberImages=length(InfoImage);
    
    Image=zeros(nImage,mImage,NumberImages,'uint16');
    OddRowsImage=zeros(floor(nImage/2),mImage,NumberImages,'uint16');
    
    for i=1:NumberImages
        Image(:,:,i)=imread(FileTif,'Index',i);
        OddRowsImage(:,:,i)= Image(1:2:end-1,:,i);
        
        imwrite(OddRowsImage(:,:,i),[folder '/linescan_593nm_' LineNumber  '_oddrows.tif'],'WriteMode','append');
        
    end
        
end
% imshow3D(OddRowsImage)

end