% the problem is when I am defining pos_X and pos_y.

classdef LifetimeMeasurementFunctions < handle
    % Test class to delegate display of images
    
    properties
        handles;
    end
    
    methods
        
        function obj = LifetimeMeasurementFunctions(handles)
            obj.handles = handles;
        end
        
        function [stds, tau] = lifetime_tau_std (obj,h)
            %concentrations = [0.00 0.017 0.038 0.065 0.100 0.150 0.225 0.351 0.602 1.35  39];% to convolute later
            
            repeats = size(h,1); % the number of repeats each point has
            
            curves = zeros(repeats,65536);
            for j= 1:repeats
                curves(j,:) = sum(h(j,:),1)/length(65536); 
            end
            
            taus = zeros(repeats,1);
            datakit = [];
            
            for i = 1:repeats
                
                datakit = h(i,:);
  
                [~,index] = max(datakit);
                start_fit=index+100;
                end_fit=round((36.8/100)*start_fit+start_fit);
                
                
                % lifetime's exp decay
                t=(1:length(datakit))*0.004; %ns %intiger operand error
                cutout = datakit(start_fit:end_fit);
                tt = t(start_fit:end_fit);
                P = polyfit(tt,log(double(cutout)),1);
                %yfit = P(1)*(tt)+P(2);
                taus(i,1)= -(1/P(1)); %ns
            end
            
            maxx = zeros(repeats,1);
            minn = zeros(repeats,1);
            
            for i =1:repeats
                maxx(i,1) = max(curves(i,:));
                minn(i,1) = min(curves(i,:));
                curves(i,:) = ((curves(i,:) - minn(i,1)) ./ (maxx(i,1) - minn(i,1)));
            end
            
            t=(1:65536)*0.004; %ps
            for i =1:repeats
                subplot(obj.handles.NormCurves)
                
                semilogy(t,curves(i,:),'LineWidth',1);
                xlim([1 repeats]);
                ylim([0 1.5]);
                ylabel('Normalized','FontSize',5);
                xlabel('Time (ns)','FontSize',5);
                hold all
            end
            hold off

            for i=1:repeats
                subplot(obj.handles.LifetimeCurves)
                plot(i, taus(i,1),'o','LineWidth',2)
                grid on
                hold all
                ylabel('Lifetime (ns)','FontSize',5);
                xlabel('Trial Number','FontSize',5);
            end
            hold off
            
            stds = std(taus);
            tau = mean(taus);
        end
        
    end
end

