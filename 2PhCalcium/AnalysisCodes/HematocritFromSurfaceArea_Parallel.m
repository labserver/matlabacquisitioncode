function [t, Hematocrit] = HematocritFromSurfaceArea_Parallel(scan, image)
% scan: scan information
% image: longitudinal scan

ScanHeight = scan.scan_height; % Height of live view where linescan was drawn (in um)
ScanWidth = scan.scan_width;  % Width of live view where linescan was drawn (in um)
XPixels = scan.nx; % Number of pixels of live view where linescan was drawn in X direction 
YPixels = scan.ny; % Number of pixels of live view where linescan was drawn in Y direction

XPixelSize = ScanHeight / XPixels;  % Pixel size in X direction (in um) 
YPixelSize = ScanWidth / YPixels;   % Pixel size in Y direction (in um)

LinePosition = getPosition(scan.segment_handles{1});  % [X1 Y1; X2 Y2] first row: X and Y of the start of the line, second row: X and Y of the end of the line (in pixels)
LineLength = sqrt(((LinePosition(2,2)-LinePosition(1,2))*YPixelSize)^2. + ((LinePosition(2,1)-LinePosition(1,1))*XPixelSize)^2.);  % Line length (in um)

LineScanRepeat = scan.repeat_line_scan; % Line Scan Repeat
LineScanFrequency = scan.line_frequency_line; % Line Scan Frequency (lines per sec)

sigma = 1.0;
KernelSize = [2.*round((6*sigma+1)/2)-1];
h = fspecial('gaussian', KernelSize, sigma); % creates a two-dimensional Gaussian lowpass filter. 

%Finding the number of streaks at each scan segment (one segment is one TIFF page corresponding to 200 repeats)
for i=1:size(image,3)
    
     image(:,:,i) = medfilt2(image(:,:,i), [3 3]) ;
     image(:,:,i) = imadjust(image(:,:,i));%     image(:,:,i) = imadjust(image(:,:,i));
     image(:,:,i)=imfilter(image(:,:,i),h,'same','replicate'); %applying the Gaussian filter to 2D image (gaussian blurring)
     image(:,:,i) = imadjust(image(:,:,i));% 
  
%    Subtracting the spatial mean over the scan from image to remove light inhomogeneities
    column_mean=mean(image(:,:,i),1);
    for j=1:size(image,2)
        image(:,j,i)=image(:,j,i)-0.6 .* column_mean(j);
    end 

%     imagesection = image(:,:,i); 
    imagesection = image(:, round(0.3*size(image,2)):round(0.7*size(image,2)),i);
    
    imagesection = im2bw(imagesection, 0.15);

    BrightArea = sum(imagesection(:));   % (pixel^2)
    TotalArea = size(imagesection,2) * size(imagesection,1); % (pixel^2)
    DarkArea = TotalArea - BrightArea; % (pixel^2)
    
    Hematocrit(i) = 100 * DarkArea / TotalArea;
    t(i) = ( (i-1)*LineScanRepeat + LineScanRepeat/2 ) /LineScanFrequency; % time corresponding to the center of the scan segment i (in sec)
    
end



end

