function RBC_Flux = RBCFlux(scan, peakcounts)

LineScanRepeat = scan.repeat_line_scan; % Line Scan Repeat
LineScanFrequency = scan.line_frequency_line; % Line Scan Frequency (lines per sec)

RBC_Flux = peakcounts ./ (LineScanRepeat./LineScanFrequency); %RBC flux (cell/sec)

end