function varargout = LineScanAnalysis_gui(varargin)
% LINESCANANALYSIS_GUI MATLAB code for LineScanAnalysis_gui.fig
%      LINESCANANALYSIS_GUI, by itself, creates a new LINESCANANALYSIS_GUI or raises the existing
%      singleton*.
%
%      H = LINESCANANALYSIS_GUI returns the handle to a new LINESCANANALYSIS_GUI or the handle to
%      the existing singleton*.
%
%      LINESCANANALYSIS_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LINESCANANALYSIS_GUI.M with the given input arguments.
%
%      LINESCANANALYSIS_GUI('Property','Value',...) creates a new LINESCANANALYSIS_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before LineScanAnalysis_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to LineScanAnalysis_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help LineScanAnalysis_gui

% Last Modified by GUIDE v2.5 05-Jan-2015 14:27:41

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @LineScanAnalysis_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @LineScanAnalysis_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



% --- Executes just before LineScanAnalysis_gui is made visible.
function LineScanAnalysis_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to LineScanAnalysis_gui (see VARARGIN)

% Choose default command line output for LineScanAnalysis_gui
handles.output = hObject;

handles.OddMode = 0; % default value for OddMode 

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes LineScanAnalysis_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);



% --- Outputs from this function are returned to the command line.
function varargout = LineScanAnalysis_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes on button press in Load_Vertical_Button.
function Load_Vertical_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Load_Vertical_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Read Vertical Scan for analysis
handles.Verticalfolder = uigetdir('D:\Users\Maryam.OPTIQUE-05\Documents\MATLAB');
VerticalLineNumber = inputdlg('Enter the line number: ');
load([handles.Verticalfolder '/LineScanInfo_' VerticalLineNumber{1} '.mat']); %Linescan information
load([handles.Verticalfolder '/OriginalFrameNo_' VerticalLineNumber{1} '.mat']); %Frame No of original scans (before removing some frames)
handles.VerticalOriginalFrameNo = FrameMatrix;
clear FrameMatrix

set(handles.ExpTypeVertical,'String', scan.experiment_type);


% If odd-row image is required, generates and load it, otherwisse loads the original image.
if (handles.OddMode == 0)
    VerticalFileTif = [handles.Verticalfolder '/ModifiedImage_' VerticalLineNumber{1} '.tif'];
elseif (handles.OddMode ==1)
    OddRows(handles.Verticalfolder, VerticalLineNumber{1})
    VerticalFileTif = [handles.Verticalfolder '/ModifiedImage_' VerticalLineNumber{1} '_oddrows.tif'];
else
    error('OddMode not valid')
end

load([handles.Verticalfolder '/image_pmt.mat']); % PMT live image (to locate the linescan)
handles.VerticalPMTImage = image_ref_pmt;
clear image_ref_pmt

VerticalInfoImage=imfinfo(VerticalFileTif);
mImageVertical=VerticalInfoImage(1).Width;
nImageVertical=VerticalInfoImage(1).Height;
NumberImagesVertical=length(VerticalInfoImage);
set(handles.slider_OriginalVerticalScan,'Max',NumberImagesVertical-1);
set(handles.slider_OriginalVerticalScan, 'SliderStep', [1/(NumberImagesVertical-1) 1/(NumberImagesVertical-1)]);
set(handles.slider_ShiftedVerticalScan,'Max',NumberImagesVertical-1);
set(handles.slider_ShiftedVerticalScan, 'SliderStep', [1/(NumberImagesVertical-1) 1/(NumberImagesVertical-1)]);

FinalVerticalImage=zeros(nImageVertical,mImageVertical,NumberImagesVertical,'uint16');
for i=1:NumberImagesVertical
   FinalVerticalImage(:,:,i)=imread(VerticalFileTif,'Index',i);
end

handles.FinalVerticalImage = FinalVerticalImage; % Line scan image
handles.ShiftedVerticalImage = FinalVerticalImage;
handles.VerticalImageInfo = scan;
clear scan


set(handles.VerticalShift_text,'String',0);


% Update display

% Unshifted Vertical lineScan Segments
axes(handles.OriginalVerticalScan)
imagesc(squeeze(FinalVerticalImage(:,:,1))); colormap 'gray'
set(handles.slider_OriginalVerticalScan,'Value',0)
set(handles.OriginalVerticalScan_Stext,'String', (get(handles.slider_OriginalVerticalScan,'Value')+1));
% axis off;


% Shifted Vertical lineScan Segments
axes(handles.ShiftedVerticalScan)
imagesc(squeeze(handles.ShiftedVerticalImage(:,:,1))); colormap 'gray'
set(handles.slider_ShiftedVerticalScan,'Value',0)
set(handles.ShiftedVerticalScan_Stext,'String', (get(handles.slider_ShiftedVerticalScan,'Value')+1));

% plot the live scan image and the position of the vertical linescan
axes(handles.LivePMT_Vertical)
imagesc(handles.VerticalPMTImage); colormap 'gray'
VerticalLinePosition = getPosition(handles.VerticalImageInfo.segment_handles{1});
line(squeeze(VerticalLinePosition(:,1)), squeeze(VerticalLinePosition(:,2)), 'Color', 'yellow'); %show line scan on the live PMT image

% Update handles structure
guidata(hObject, handles);




% --- Executes on button press in Load_Parallel_Button.
function Load_Parallel_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Load_Parallel_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Read Parallel Scan for analysis
handles.Parallelfolder = uigetdir('D:\Users\Maryam.OPTIQUE-05\Documents\MATLAB');
ParallelLineNumber = inputdlg('Enter the line number: ');
load([handles.Parallelfolder '/LineScanInfo_' ParallelLineNumber{1} '.mat']); %Linescan information
load([handles.Parallelfolder '/OriginalFrameNo_' ParallelLineNumber{1} '.mat']); %Frame No of original scans (before removing some frames)
handles.ParallelOriginalFrameNo = FrameMatrix;
clear FrameMatrix

set(handles.ExpTypeParallel,'String', scan.experiment_type);

% If odd-row image is required, generates and load it, otherwisse loads the original image.
if (handles.OddMode == 0)
    ParallelFileTif = [handles.Parallelfolder '/ModifiedImage_' ParallelLineNumber{1} '.tif'];
elseif (handles.OddMode ==1)
    OddRows(handles.Parallelfolder, ParallelLineNumber{1})
    ParallelFileTif = [handles.Parallelfolder '/ModifiedImage_' ParallelLineNumber{1} '_oddrows.tif'];
else
    error('OddMode not valid')
end

load([handles.Parallelfolder '/image_pmt.mat']); % PMT live image (to locate the linescan)
handles.ParallelPMTImage = image_ref_pmt;
clear image_ref_pmt

ParallelInfoImage=imfinfo(ParallelFileTif);
mImageParallel=ParallelInfoImage(1).Width;
nImageParallel=ParallelInfoImage(1).Height;
NumberImagesParallel=length(ParallelInfoImage);
set(handles.slider_OriginalParallelScan,'Max',NumberImagesParallel-1);
set(handles.slider_OriginalParallelScan, 'SliderStep', [1/(NumberImagesParallel-1) 1/(NumberImagesParallel-1)]);
set(handles.slider_ShiftedParallelScan,'Max',NumberImagesParallel-1);
set(handles.slider_ShiftedParallelScan, 'SliderStep', [1/(NumberImagesParallel-1) 1/(NumberImagesParallel-1)]);

FinalParallelImage=zeros(nImageParallel,mImageParallel,NumberImagesParallel,'uint16');
for i=1:NumberImagesParallel
   FinalParallelImage(:,:,i)=imread(ParallelFileTif,'Index',i);
end

handles.FinalParallelImage = FinalParallelImage; % Line scan image
handles.ShiftedParallelImage = FinalParallelImage;
handles.ParallelImageInfo = scan;
clear scan


set(handles.ParallelShift_text,'String',0);


% Update display

% Unshifted Parallel lineScan Segments
axes(handles.OriginalParallelScan)
imagesc(squeeze(FinalParallelImage(:,:,1))); colormap 'gray'
set(handles.slider_OriginalParallelScan,'Value',0)
set(handles.OriginalParallelScan_Stext,'String', (get(handles.slider_OriginalParallelScan,'Value')+1));
% axis off;


% Shifted Parallel lineScan Segments
axes(handles.ShiftedParallelScan)
imagesc(squeeze(handles.ShiftedParallelImage(:,:,1))); colormap 'gray'
set(handles.slider_ShiftedParallelScan,'Value',0)
set(handles.ShiftedParallelScan_Stext,'String', (get(handles.slider_ShiftedParallelScan,'Value')+1));

% plot the live scan image and the position of the Parallel linescan
axes(handles.LivePMT_Parallel)
imagesc(handles.ParallelPMTImage); colormap 'gray'
ParallelLinePosition = getPosition(handles.ParallelImageInfo.segment_handles{1});
line(squeeze(ParallelLinePosition(:,1)), squeeze(ParallelLinePosition(:,2)), 'Color', 'yellow'); %show line scan on the live PMT image

% Update handles structure
guidata(hObject, handles);



function Even_Row_CheckBox_Callback(hObject, eventdata, handles)

% Determins if only even rows are used for analysis (returns toggle state of Even_Row_CheckBox)
handles.OddMode = get(hObject,'Value'); % 1 if selected, 0 if not selected

% Update handles structure
guidata(hObject, handles);



% --- Executes on slider movement.
function slider_OriginalVerticalScan_Callback(hObject, eventdata, handles)


%update display
axes(handles.OriginalVerticalScan)
imagesc(squeeze(handles.FinalVerticalImage(:,:,(get(handles.slider_OriginalVerticalScan,'Value')+1)))); colormap 'gray'
set(handles.OriginalVerticalScan_Stext,'String', (get(handles.slider_OriginalVerticalScan,'Value')+1));


guidata(hObject, handles);




function slider_OriginalVerticalScan_CreateFcn(hObject, eventdata, handles)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function OriginalVerticalScan_Stext_Callback(hObject, eventdata, handles)

function OriginalVerticalScan_Stext_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ShiftedVerticalScan_Stext_Callback(hObject, eventdata, handles)


function ShiftedVerticalScan_Stext_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider_ShiftedVerticalScan_Callback(hObject, eventdata, handles)

%update display
axes(handles.ShiftedVerticalScan)
imagesc(squeeze(handles.ShiftedVerticalImage(:,:,(get(handles.slider_ShiftedVerticalScan,'Value')+1)))); colormap 'gray'
set(handles.ShiftedVerticalScan_Stext,'String', (get(handles.slider_ShiftedVerticalScan,'Value')+1));


guidata(hObject, handles);

function slider_ShiftedVerticalScan_CreateFcn(hObject, eventdata, handles)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function VerticalShift_text_Callback(hObject, eventdata, handles)

guidata(hObject, handles);

function VerticalShift_text_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

function VerticalShift_button_Callback(hObject, eventdata, handles)

% Calculating the shifted image
handles.ShiftedVerticalImage = ShiftCorrection(handles.FinalVerticalImage, int16(str2num(get(handles.VerticalShift_text,'String'))));

% Display update
axes(handles.ShiftedVerticalScan)
imagesc(squeeze(handles.ShiftedVerticalImage(:,:,(get(handles.slider_ShiftedVerticalScan,'Value')+1)))); colormap 'gray'

% Update handles structure
guidata(hObject, handles);

function Calculate_button_Callback(hObject, eventdata, handles)

% Calculating the actual time intervals for parallel and vertical scans based on original frame numbers (before removing some frames) and % measured frame/sec
FramesPerSec = 2.4;   %%ASK   % 2.4 frames per second (the rate of data acquisition)
handles.tVerticalCorrected = [];
for i = 1: size(handles.VerticalOriginalFrameNo, 2)
    handles.tVerticalCorrected (i) = (handles.VerticalOriginalFrameNo(1, i) - 0.5) / FramesPerSec; % time corresponding to the center of the scan segment i (in sec)      
end

handles.tParallelCorrected = [];
for i = 1: size(handles.ParallelOriginalFrameNo, 2)
    handles.tParallelCorrected (i) = ( handles.ParallelOriginalFrameNo(1, i) - 0.5) / FramesPerSec; % time corresponding to the center of the scan segment i (in sec)   
end
%%


%% lifetime analysis

        computor=LifetimeMeasurementFunctions(handles);
        h = cell2mat(handles.data); %handles.data(j) if you have two astrocytes saved in one scan
        [stdss,tau] = computor.lifetime_tau_std(h);%one stds because it's one astrocyte
        

%% Vessel diameter (um) over time (sec)
[handles.tVertical, handles.VesselD] = VesselDiameter3(handles.VerticalImageInfo, handles.ShiftedVerticalImage);
handles.VesselDAvg = mean2(handles.VesselD); % average vessel diameter (um)
handles.VesselD_SD = std2(handles.VesselD);  % SD of vessel diameter


% plot vessel diameter over time and the average value
axes(handles.Graph_Diameter)
plot(handles.tVerticalCorrected, handles.VesselD, 'LineWidth', 2.0)
xlabel('time (sec)')
ylabel('Capillary Diameter (um)')
line([0 handles.tVerticalCorrected(end)],[handles.VesselDAvg handles.VesselDAvg], 'LineWidth', 2.0, 'color', 'r')
     

%% Angle of streaks (o) over time (sec)
[handles.tParallel, handles.Angle] = StreakAngle(handles.ParallelImageInfo, handles.ShiftedParallelImage);
handles.AngleAvg = mean2(handles.Angle); % average angle (o)
handles.Angle_SD = std2(handles.Angle);  % SD of vessel diameter


% plot Angle of streaks over time and the average value
axes(handles.Graph_Angle)
plot(handles.tParallelCorrected, handles.Angle, 'LineWidth', 2.0)
xlabel('time (sec)')
ylabel('Streaks Angle (o)')
line([0 handles.tParallelCorrected(end)],[handles.AngleAvg handles.AngleAvg], 'LineWidth', 2.0, 'color', 'r')
        
%% Red Blood Cells Speed (mm/sec) over time (sec)
handles.Speed = RBCSpeed(handles.Angle, handles.ParallelImageInfo, handles.OddMode); % RBC speed (mm/s)
handles.SpeedAvg = mean2(handles.Speed); % average Speed (mm/s)
handles.Speed_SD = std2(handles.Speed);  % SD of Speed
        
% plot Red Blood Cells Speed over time and the average value
axes(handles.Graph_RBCSpeed)
plot(handles.tParallelCorrected, handles.Speed, 'LineWidth', 2.0)
xlabel('time (sec)')
ylabel('RBC Speed (mm/s)')
line([0 handles.tParallelCorrected(end)],[handles.SpeedAvg handles.SpeedAvg], 'LineWidth', 2.0, 'color', 'r')


%% Number of streaks (cells) over time (sec)
[handles.tParallel, handles.NumberOfStreaks] = CountStreaks3(handles.ParallelImageInfo, handles.ShiftedParallelImage, handles.Angle); %from longitudinal scans
[handles.tVertical, handles.NumberOfStreaksVertical] = CountStreaksFromPerpendicular3(handles.VerticalImageInfo, handles.ShiftedVerticalImage); %from perpendicular scans

% plot Number of streaks over time
axes(handles.Graph_PeakNumber)
plot(handles.tParallelCorrected, handles.NumberOfStreaks, 'LineWidth', 2.0)
xlabel('time (sec)')
ylabel('Streak counts (from parallel)')

axes(handles.Graph_PeakNumber2)
plot(handles.tVerticalCorrected, handles.NumberOfStreaksVertical, 'LineWidth', 2.0)
xlabel('time (sec)')
ylabel('Streak counts (from vertical)')

%% RBC Flux (cell/sec) over time (sec)
%from longitudinal scans
handles.Flux = RBCFlux(handles.ParallelImageInfo, handles.NumberOfStreaks); % RBC Flux (cell/sec)
handles.FluxAvg = mean2(handles.Flux); % average RBC Flux (cell/sec)
handles.Flux_SD = std2(handles.Flux); % SD of RBC Flux
%from perpendicular scans
handles.FluxVertical = RBCFlux(handles.VerticalImageInfo, handles.NumberOfStreaksVertical); % RBC Flux (cell/sec)
handles.FluxVerticalAvg = mean2(handles.FluxVertical); % average RBC Flux (cell/sec)
handles.FluxVertical_SD = std2(handles.FluxVertical); % SD of RBC Flux

% plot RBC Flux over time and the average value
axes(handles.Graph_RBCFlux)
plot(handles.tParallelCorrected, handles.Flux, 'LineWidth', 2.0)
xlabel('time (sec)')
ylabel('RBC Flux (cell/s)')
line([0 handles.tParallelCorrected(end)],[handles.FluxAvg handles.FluxAvg], 'LineWidth', 2.0, 'color', 'r')

%% Hematocrit (RBC volume fraction, %) over time (sec)
%using flux from longitudinal scans
handles.Hct = Hematocrit(handles.Flux, handles.Speed, handles.VesselDAvg); % Hematocrit (%)
handles.HctAvg = mean2(handles.Hct); % average Hematocrit (%)
handles.Hct_SD = std2(handles.Hct); % SD of Hematocrit

%using dark and bright surface areas on perpendiculatr scans
[handles.tVertical, handles.HctVertical] = HematocritFromSurfaceArea_Vertical(handles.VerticalImageInfo, handles.ShiftedVerticalImage, handles.VesselD);  % Hematocrit (%)
handles.HctVerticalAvg = mean2(handles.HctVertical); % average Hematocrit (%)
handles.HctVertical_SD = std2(handles.HctVertical); % SD of Hematocrit

%using dark and bright surface areas on longitudinal scans
[handles.tVertical, handles.HctParallel] = HematocritFromSurfaceArea_Parallel(handles.ParallelImageInfo, handles.ShiftedParallelImage); % Hematocrit (%)
handles.HctParallelAvg = mean2(handles.HctParallel); % average Hematocrit (%)
handles.HctParallel_SD = std2(handles.HctParallel); % SD of Hematocrit


% plot Hematocrit over time and the average value
axes(handles.Graph_Hematocrit)
plot(handles.tParallelCorrected, handles.Hct, 'LineWidth', 2.0)
xlabel('time (sec)')
ylabel('Hematocrit (%) (from streak counts)')
line([0 handles.tParallelCorrected(end)],[handles.HctAvg handles.HctAvg], 'LineWidth', 2.0, 'color', 'r')

axes(handles.Graph_Hematocrit2)
plot(handles.tVerticalCorrected, handles.HctVertical, 'LineWidth', 2.0)
xlabel('time (sec)')
ylabel('Hematocrit (%) (from dark surface)')
line([0 handles.tVerticalCorrected(end)],[handles.HctVerticalAvg handles.HctVerticalAvg], 'LineWidth', 2.0, 'color', 'r')


%% Update the summary table

%Capillary diameter
TableData(1,1) = handles.VesselDAvg;
TableData(1,2) = handles.VesselD_SD;
TableData(1,3) = size(handles.VesselD, 2);

% Streak Angle
TableData(2,1) = handles.AngleAvg;
TableData(2,2) = handles.Angle_SD;
TableData(2,3) = size(handles.Angle, 2);

% RBC Speed
TableData(3,1) = handles.SpeedAvg;
TableData(3,2) = handles.Speed_SD;
TableData(3,3) = size(handles.Speed, 2);

%RBC Flux (from parallel)
TableData(4,1) = handles.FluxAvg;
TableData(4,2) = handles.Flux_SD;
TableData(4,3) = size(handles.Flux,2);

%RBC Flux (from vertical)
TableData(5,1) = handles.FluxVerticalAvg;
TableData(5,2) = handles.FluxVertical_SD;
TableData(5,3) = size(handles.FluxVertical,2);

% Hematocrit (from streak counts)
TableData(6,1) = handles.HctAvg;
TableData(6,2) = handles.Hct_SD;
TableData(6,3) = size(handles.Hct,2);

% Hematocrit (from streak surface-vertical)
TableData(7,1) = handles.HctVerticalAvg;
TableData(7,2) = handles.HctVertical_SD;
TableData(7,3) = size(handles.HctVertical,2);

% Hematocrit (from streak surface-parallel)
TableData(8,1) = handles.HctParallelAvg;
TableData(8,2) = handles.HctParallel_SD;
TableData(8,3) = size(handles.HctParallel,2);

% Lifetime values
TableData(9,1)= tau;
TableData(9,2) = stdss;

%Update the table
set(handles.SummaryTable,'Data',TableData);


% Update handles structure
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function Graph_Diameter_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Graph_Diameter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate Graph_Diameter


% --- Executes during object creation, after setting all properties.
function Graph_RBCFlux_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Graph_RBCFlux (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate Graph_RBCFlux

% --- Executes during object creation, after setting all properties.
function Graph_RBCSpeed_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Graph_RBCSpeed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate Graph_RBCSpeed


% --- Executes during object creation, after setting all properties.
function SummaryTable_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SummaryTable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



function OriginalParallelScan_Stext_Callback(hObject, eventdata, handles)
% hObject    handle to OriginalParallelScan_Stext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of OriginalParallelScan_Stext as text
%        str2double(get(hObject,'String')) returns contents of OriginalParallelScan_Stext as a double


% --- Executes during object creation, after setting all properties.
function OriginalParallelScan_Stext_CreateFcn(hObject, eventdata, handles)
% hObject    handle to OriginalParallelScan_Stext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider_OriginalParallelScan_Callback(hObject, eventdata, handles)
% hObject    handle to slider_OriginalParallelScan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

%update display
axes(handles.OriginalParallelScan)
imagesc(squeeze(handles.FinalParallelImage(:,:,(int16(get(handles.slider_OriginalParallelScan,'Value'))+1)))); colormap 'gray'
set(handles.OriginalParallelScan_Stext,'String', (int16(get(handles.slider_OriginalParallelScan,'Value'))+1));

% Update handles structure
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function slider_OriginalParallelScan_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_OriginalParallelScan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function slider_ShiftedParallelScan_Callback(hObject, eventdata, handles)

axes(handles.ShiftedParallelScan)
imagesc(squeeze(handles.ShiftedParallelImage(:,:,(int16(get(handles.slider_ShiftedParallelScan,'Value'))+1)))); colormap 'gray'
set(handles.ShiftedParallelScan_Stext,'String', (int16(get(handles.slider_ShiftedParallelScan,'Value'))+1));

guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function slider_ShiftedParallelScan_CreateFcn(hObject, eventdata, handles)

if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function ShiftedParallelScan_Stext_Callback(hObject, eventdata, handles)
% hObject    handle to ShiftedParallelScan_Stext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ShiftedParallelScan_Stext as text
%        str2double(get(hObject,'String')) returns contents of ShiftedParallelScan_Stext as a double


% --- Executes during object creation, after setting all properties.
function ShiftedParallelScan_Stext_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ShiftedParallelScan_Stext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ParallelShift_button.
function ParallelShift_button_Callback(hObject, eventdata, handles)
% hObject    handle to ParallelShift_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Calculating the shifted image
handles.ShiftedParallelImage = ShiftCorrection(handles.FinalParallelImage, int16(str2num(get(handles.ParallelShift_text,'String'))));

% Display update
axes(handles.ShiftedParallelScan)
imagesc(squeeze(handles.ShiftedParallelImage(:,:,(int16(get(handles.slider_ShiftedParallelScan,'Value'))+1)))); colormap 'gray'

% Update handles structure
guidata(hObject, handles);



function ParallelShift_text_Callback(hObject, eventdata, handles)
% hObject    handle to ParallelShift_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ParallelShift_text as text
%        str2double(get(hObject,'String')) returns contents of ParallelShift_text as a double

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function ParallelShift_text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ParallelShift_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Save_Button.
function Save_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Save_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.Savefolder = uigetdir('C:\Users\moeini\Mohammad\2 photon data');
Results.VesselNumber = inputdlg('Enter the vessel number: ');
Results.tVertical = handles.tVerticalCorrected; %(sec)
Results.tParallel = handles.tParallelCorrected; % (sec)
Results.VesselDiameter = handles.VesselD; % Vessel diameter (um)
Results.AvgVesselDiameter = handles.VesselDAvg; % Average vessel diameter (um)
Results.VesselDiameter_SD = handles.VesselD_SD; % SD of vessel diameter
Results.StreakAngle = handles.Angle; % Streak angle (o)
Results.AvgStreakAngle = handles.AngleAvg; % average streak angle (o)
Results.StreakAngle_SD = handles.Angle_SD;  % SD of streak vessel diameter
Results.RBCSpeed = handles.Speed; % RBC speed (mm/s)
Results.AvgRBCSpeed = handles.SpeedAvg; % average RBC Speed (mm/s)
Results.RBCSpeed_SD = handles.Speed_SD;  % SD of RBC Speed
Results.NumberOfStreaks = handles.NumberOfStreaks; % Number of streaks per frame (from parallel)
Results.NumberOfStreaksVertical = handles.NumberOfStreaksVertical; % Number of streaks per frame (from vertical)
Results.RBCFlux = handles.Flux;  % RBC Flux (cell/sec) (from parallel)
Results.AvgRBCFlux = handles.FluxAvg; % average RBC Flux (cell/sec) (from parallel)
Results.RBCFlux_SD = handles.Flux_SD; % SD of RBC Flux (from parallel)
Results.RBCFluxVertical = handles.FluxVertical; % RBC Flux (cell/sec) (from vertical)
Results.AvgRBCFluxVertical = handles.FluxVerticalAvg; % average RBC Flux (cell/sec) (from vertical)
Results.RBCFluxVertical_SD = handles.FluxVertical_SD; % SD of RBC Flux (from vertical)
Results.Hematocrit = handles.Hct ; % Hematocrit (%) (from streak counts)
Results.AvgHematocrit = handles.HctAvg; % average Hematocrit (%) (from streak counts)
Results.Hematocrit_SD = handles.Hct_SD; % SD of Hematocrit (from streak counts)
Results.HematocritVertical = handles.HctVertical; % Hematocrit (%) (from streak surface-vertical)
Results.AvgHematocritVertical = handles.HctVerticalAvg; % average Hematocrit (%) (from streak surface-vertical)
Results.HematocritVertical_SD = handles.HctVertical_SD; % SD of Hematocrit (from streak surface-vertical)
Results.HematocritParallel = handles.HctParallel; % Hematocrit (%)(from streak surface-parallel)
Results.AvgHematocritParallel = handles.HctParallelAvg; % average Hematocrit (%)(from streak surface-parallel)
Results.HematocritParallel_SD = handles.HctParallel_SD; % SD of Hematocrit(from streak surface-parallel)
Results.VerticalImageInfo = handles.VerticalImageInfo; 
Results.ParallelImageInfo = handles.ParallelImageInfo;

save([handles.Savefolder '/Results.mat'],'Results')

% Saving the shifted vertical and parallel scans
for i=1:size(handles.ShiftedVerticalImage,3)
    imwrite(handles.ShiftedVerticalImage(:,:,i),[handles.Savefolder '/ShiftedVerticalImage.tif'],'WriteMode','append');
end

for i=1:size(handles.ShiftedParallelImage,3)
    imwrite(handles.ShiftedParallelImage(:,:,i),[handles.Savefolder '/ShiftedParallelImage.tif'],'WriteMode','append');
end


 
% --- Executes when entered data in editable cell(s) in SummaryTable.
function SummaryTable_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to SummaryTable (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function LivePMT_Vertical_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LivePMT_Vertical (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate LivePMT_Vertical



function ExpTypeVertical_Callback(hObject, eventdata, handles)
% hObject    handle to ExpTypeVertical (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ExpTypeVertical as text
%        str2double(get(hObject,'String')) returns contents of ExpTypeVertical as a double


% --- Executes during object creation, after setting all properties.
function ExpTypeVertical_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ExpTypeVertical (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ExpTypeParallel_Callback(hObject, eventdata, handles)
% hObject    handle to ExpTypeParallel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ExpTypeParallel as text
%        str2double(get(hObject,'String')) returns contents of ExpTypeParallel as a double



% --- Executes during object creation, after setting all properties.
function ExpTypeParallel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ExpTypeParallel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in lifetimedata.
function lifetimedata_Callback(hObject, eventdata, handles)
% hObject    handle to lifetimedata (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.data =[];

handles.Histogramfolder = uigetdir('D:\Users\Maryam.OPTIQUE-05\Documents\MATLAB');
LineFileNumber = inputdlg('Enter the Line_?? number: ');
load([handles.Histogramfolder '/LineScanInfo_' LineFileNumber{1} '.mat']); %scan info
load([handles.Histogramfolder '/LineScanHistogram_' LineFileNumber{1} '.mat']); %histogram info
handles.HistogFrameNum = size(histogSave,1);
handles.data = cell({histogSave});
guidata(hObject, handles);
