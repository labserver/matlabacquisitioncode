function ShiftedImage = ShiftCorrection(image, shift)
% function ShiftCorrection(filename)

% handles.folder = uigetdir;

ShiftedImage=zeros(size(image,1),size(image,2),size(image,3),'uint16');
for i=1:size(image,3)
   ShiftedImage(:,:,i)=image(:,:,i);
   oddrows  = ShiftedImage(1:2:end-1,:,i);
   evenrows = ShiftedImage(2:2:end,:,i);
   if shift>=0
       oddrows = shiftr(oddrows,0,shift,1);
   else
       oddrows = shiftl(oddrows,0,-shift,1);
   end
   ShiftedImage(1:2:end-1,:,i) = oddrows;
   ShiftedImage(2:2:end,:,i) = evenrows;
end



