function Speed = RBCSpeed(Angle, scan, OddMode)

%% Scan Parameters
ScanHeight = scan.scan_height; % Height of live view where linescan was drawn (in um)
ScanWidth = scan.scan_width;  % Width of live view where linescan was drawn (in um)
XPixels = scan.nx; % Number of pixels of live view where linescan was drawn in X direction 
YPixels = scan.ny; % Number of pixels of live view where linescan was drawn in Y direction
XPixelSize = ScanHeight / XPixels;  % Pixel size in X direction (in um) 
YPixelSize = ScanWidth / YPixels;   % Pixel size in Y direction (in um)
LinePosition = getPosition(scan.segment_handles{1});  % [X1 Y1; X2 Y2] first row: X and Y of the start of the line, second row: X and Y of the end of the line (in pixels)
LineLength = sqrt(((LinePosition(2,2)-LinePosition(1,2))*YPixelSize)^2. + ((LinePosition(2,1)-LinePosition(1,1))*XPixelSize)^2.);  % Line length (in um)
LinePoints = scan.number_points_per_segment; % Number of points on the linescan

if (OddMode == 0)
    LineScanFrequency = scan.line_frequency_line; % Line Scan Frequency (lines per sec)
elseif (OddMode == 1)
    LineScanFrequency = scan.line_frequency_line/2; % Line Scan Frequency (lines per sec)
else
    error('OddMode not valid')
end

%% calculate the Speed
Angle(Angle==90) = 89.9; % tan(pi/2)-> Inf
Speed = (tan(2.*pi.*Angle./360)).*LineLength.*LineScanFrequency./LinePoints/1000; % Red Blood Cells Speed (mm/sec)

end
