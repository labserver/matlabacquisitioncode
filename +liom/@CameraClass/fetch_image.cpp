
#include <mex.h>
#include "LiomCameraStruct.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    // Get the camera handle from the object
    const mxArray* in = prhs[1];
    LiomCameraStruct* h= reinterpret_cast<LiomCameraStruct*>(*((uint64_t *)mxGetData(in)));

        
	if( h != 0 && h->m_hG != 0 )
	{
        is_FreezeVideo( h->m_hG, IS_WAIT );
        plhs[0]=mxCreateNumericMatrix(h->m_nSizeX, h->m_nSizeY, mxINT32_CLASS, mxREAL);
        int* resultPtr = (int*) mxGetPr(plhs[0]);
        memcpy(resultPtr, h->m_pcImageMemory, h->m_nSizeX*h->m_nSizeY*4);
	}
    return;
    //is_RenderBitmap( m_hG, m_lMemoryId, m_hWnd, IS_RENDER_FIT_TO_WINDOW );
}