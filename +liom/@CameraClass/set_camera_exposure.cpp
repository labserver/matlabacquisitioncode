
#include <mex.h>
#include "LiomCameraStruct.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    // Get the camera handle from the object
    const mxArray* in = prhs[1];
    LiomCameraStruct* h= reinterpret_cast<LiomCameraStruct*>(*((uint64_t *)mxGetData(in)));

    // Get user exposure time
    double exp_time = *((double*) mxGetData(prhs[2]));
    double real_exp_time;
    
	if( h != 0 && h->m_hG != 0 )
	{
        is_SetExposureTime( h->m_hG, exp_time, &real_exp_time );
	}
    return;
}