classdef CameraClass < handle
    properties (Hidden = true, SetAccess = private)
        camera_handle;        
    end

    methods (Access = private)
        [handle] = open_camera(obj)
        [image] = fetch_image(obj,handle)
        close_camera(obj,handle)
        set_camera_exposure(obj,handle,exposure)
    end
    methods
        
        % Constructor opens the scope
        function this = CameraClass()
            [h] = this.open_camera();
            this.camera_handle = h;
            if(h==0)
                delete(this);
            end
        end
        % Destructor
        function delete(this)
            this.close_camera(this.camera_handle);
        end
        % Example method
        function [image] = get_image(this)
            [image] = this.fetch_image(this.camera_handle);
        end
        function set_exposure(this,exposure)
            this.set_camera_exposure(this.camera_handle,exposure);
        end
    end
end