
#include <mex.h>
#include "LiomCameraStruct.h"

void GetMaxImageSize(HCAM m_hG, INT *pnSizeX, INT *pnSizeY);

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

	INT		m_Ret;			// return value for SDK functions
	INT		m_nColorMode;	// Y8/RGB16/RGB24/REG32
	INT		m_nBitsPerPixel;// number of bits needed store one pixel

	
	
    
    // Create the handle for matlab and set output at no-error point
    // This will allow clean-up if necessary
    LiomCameraStruct* h=new LiomCameraStruct;
    plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT64_CLASS, mxREAL);
    *((uint64_t *)mxGetData(plhs[0])) = reinterpret_cast<uint64_t>(h);
    
        
	// init camera
	h->m_hG = (HCAM) 0;							// open next camera
	m_Ret = is_InitCamera( &h->m_hG, NULL );    // init camera - no window handle for live required
	
	if( m_Ret == IS_SUCCESS )
	{
		// retrieve original image size
		SENSORINFO sInfo;
		is_GetSensorInfo( h->m_hG, &sInfo );

        GetMaxImageSize(h->m_hG, &h->m_nSizeX, &h->m_nSizeY);

		// setup the color depth to the current windows setting
		is_GetColorDepth(h->m_hG, &m_nBitsPerPixel, &m_nColorMode);
		is_SetColorMode(h->m_hG, m_nColorMode);

		// memory initialization
		 is_AllocImageMem(	h->m_hG,
							h->m_nSizeX,
							h->m_nSizeY,
							m_nBitsPerPixel,
							&h->m_pcImageMemory,
							&h->m_lMemoryId);
		 is_SetImageMem(h->m_hG, h->m_pcImageMemory, h->m_lMemoryId );	// set memory active

		// display initialization
		is_SetImageSize(h->m_hG, h->m_nSizeX, h->m_nSizeY );
		is_SetDisplayMode(h->m_hG, IS_SET_DM_DIB);


	}
  else
  {
	 mexPrintf("\nCould not open thorlabs camera!\n");
     delete h;
     h=NULL;
     *((uint64_t *)mxGetData(plhs[0])) = reinterpret_cast<uint64_t>(h);
  }        
  return;
}

void GetMaxImageSize(HCAM m_hG, INT *pnSizeX, INT *pnSizeY)
{
    // Check if the camera supports an arbitrary AOI
    INT nAOISupported = 0;
    BOOL bAOISupported = TRUE;
    if (is_ImageFormat(m_hG,
                       IMGFRMT_CMD_GET_ARBITRARY_AOI_SUPPORTED, 
                       (void*)&nAOISupported, 
                       sizeof(nAOISupported)) == IS_SUCCESS)
    {
        bAOISupported = (nAOISupported != 0);
    }

    if (bAOISupported)
    {
        // Get maximum image size
	    SENSORINFO sInfo;
	    is_GetSensorInfo (m_hG, &sInfo);
	    *pnSizeX = sInfo.nMaxWidth;
	    *pnSizeY = sInfo.nMaxHeight;
    }
    else
    {
        // Get image size of the current format
        *pnSizeX = is_SetImageSize(m_hG, IS_GET_IMAGE_SIZE_X, 0);
        *pnSizeY = is_SetImageSize(m_hG, IS_GET_IMAGE_SIZE_Y, 0);
    }
}

