
#include <mex.h>
#include "LiomCameraStruct.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{    
    // Get the camera handle from the object
    const mxArray* in = prhs[1];
    LiomCameraStruct* h= reinterpret_cast<LiomCameraStruct*>(*((uint64_t *)mxGetData(in)));

        
	if( h != 0 && h->m_hG != 0 )
	{
		//free old image mem.
		is_FreeImageMem( h->m_hG, h->m_pcImageMemory, h->m_lMemoryId );
		is_ExitCamera( h->m_hG );
        h->m_hG = NULL;
	}
    return;
}