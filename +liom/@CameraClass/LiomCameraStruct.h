#include "uc480.h"

typedef unsigned long long uint64_t;
struct LiomCameraStruct
{
    HCAM m_hG;
    char*	m_pcImageMemory;// grabber memory - pointer to buffer
    INT		m_lMemoryId;	// grabber memory - buffer ID
    INT		m_nSizeX;		// width of video 
	INT		m_nSizeY;		// height of video
    };