classdef TrigAIClass < handle
    
    properties
        hTask;
        deviceName;
        chanID;
        everyNSamples;
        sampRate;
        callbackPeriod = 0.5; %seconds
    end
    
    properties (SetAccess=private)
        hChan;
    end
    
    events
        haveData;
        Done;
    end
    
    methods
        
        function obj = TrigAIClass(deviceName,channelIDs)
            obj.deviceName = deviceName;
            obj.chanID = channelIDs;
        end
        
        function config(obj,sampRate,nhist,taskName)
            
            import dabs.ni.daqmx.*
            
            obj.hTask = Task(taskName);
            obj.hChan = obj.hTask.createAIVoltageChan(obj.deviceName,obj.chanID,[],[],[],[],'DAQmx_Val_RSE');
            obj.sampRate = sampRate;
            obj.hTask.cfgSampClkTiming(obj.sampRate, 'DAQmx_Val_FiniteSamps',nhist);
            obj.hTask.cfgDigEdgeStartTrig('/Dev1/ao/StartTrigger','DAQmx_Val_Rising');
            obj.hTask.set('startTrigRetriggerable',1);
            obj.hTask.registerDoneEvent(@obj.doneCallback);
        end
        
        function delete(obj)
            obj.hTask.stop();
            delete(obj.hTask);
        end
        
        function start(obj)
            if obj.hTask.isTaskDone()
                obj.hTask.stop();
                obj.hTask.start();
            else
                disp('Task already started');
            end
        end
        
        function stop(obj)
            obj.hTask.stop();
        end
        
        function data=read(obj,ndata)
            data= readAnalogData(obj.hTask,ndata);
        end
    end
end

