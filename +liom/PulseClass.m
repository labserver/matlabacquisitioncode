classdef PulseClass < hgsetget
% Test class for prototype two photon scanner DAQ control
    
    properties
        % Default values
        deviceName;
        chanID;
%         freq = 1000;
%         duration_us = 100;
    end
    properties (SetAccess=private)
        hTask;
        hChan;        
    end
    
    methods
        
        function obj = PulseClass(period,duration_us,deviceName,channelIDs,obj)
            import dabs.ni.daqmx.*
            obj.deviceName = deviceName;
            obj.chanID = channelIDs;            
            obj.hTask = Task('LaserPulseTask');
            
            % Compute duty cycle form pulse duration and rep rate.
            dutyCycle = duration_us*1e-6/(period);
            obj.hChan = obj.hTask.createCOPulseChanFreq(obj.deviceName, obj.chanID, '', 1/period, dutyCycle);
            obj.hTask.cfgDigEdgeStartTrig('/Dev2/ao/StartTrigger');
            obj.hTask.set('startTrigRetriggerable',1)
            obj.hTask.cfgImplicitTiming('DAQmx_Val_ContSamps', 1000);
        end
                    
        function start(obj)
            % Each channel must be in a single column
            obj.hTask.start();
        end
        function stop(obj)
            % Each channel must be in a single column
            obj.hTask.stop();
        end
        function delete(obj)
           delete(obj.hTask);
        end          
    end
end

