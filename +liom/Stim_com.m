classdef Stim_com < handle
   properties
      m_Task;
      m_Freq = 5;
      m_DutyCycle = 5;
      m_Temps_ON = 5;
      m_Temps_OFF = 30;
      m_NbRep = 10;
      m_Jitter = 3;
      
      m_Timer_ON;
      m_Timer_OFF;
   end
   events
       e_StimDone;       
   end
   methods
       %Constructeur
       function obj = Stim_com(TaskName, Channels, F, W, T_On, T_Off, NbRep, Jitter)
           
           obj.m_Freq = F;
           obj.m_DutyCycle = W*F/1000;
           obj.m_Temps_ON = T_On;
           obj.m_Temps_OFF = T_Off;
           obj.m_NbRep = NbRep;
           obj.m_Jitter = Jitter;
           
           import dabs.ni.daqmx.*
           obj.m_Task = Task(TaskName);        
           obj.m_Task.createCOPulseChanFreq('Dev1',Channels, '', obj.m_Freq, obj.m_DutyCycle); 
           obj.m_Task.cfgImplicitTiming('DAQmx_Val_ContSamps', 5000);
           
           obj.m_Timer_OFF = timer;
           obj.m_Timer_OFF.StartDelay = obj.m_Temps_OFF + round(1000*obj.m_Jitter*rand)/1000;
           obj.m_Timer_OFF.TimerFcn = @obj.T_OFF_Callback;
           
           obj.m_Timer_ON = timer;
           obj.m_Timer_ON.StartDelay = obj.m_Temps_ON;
           obj.m_Timer_ON.TimerFcn = @obj.T_ON_Callback;
       end
       
       %Destructeur
       function delete(obj)
           obj.Stop;
           delete(obj.m_Task);
           delete(obj.m_Timer_ON);
           delete(obj.m_Timer_OFF);
       end
       
       %Fonction de demarrage
       function Start(obj)
           start(obj.m_Timer_OFF);           
       end
       
       %Fonction pour l'arret
       function Stop(obj)
           obj.m_Task.stop();
           stop(obj.m_Timer_OFF);
           stop(obj.m_Timer_ON);
       end
   end
   methods(Access = private)
       function T_OFF_Callback(obj, src, evnt)
           
           obj.m_Task.start();
           start(obj.m_Timer_ON);
           disp('Start');
       end
       
       function T_ON_Callback(obj, src, evnt)
           obj.m_Task.stop();  
           obj.m_Timer_OFF.StartDelay = obj.m_Temps_OFF + round(1000*obj.m_Jitter*rand)/1000;
           start(obj.m_Timer_OFF);
           disp('Stop');
       end
   end
end