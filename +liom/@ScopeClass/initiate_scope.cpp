#include <matrix.h>
#include <mex.h>
#include "niScope.h"
#include "LiomScopeStruct.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {   

    // Initiate the acquisition
    bool error = true;
    ViChar   errorMessage[MAX_ERROR_DESCRIPTION] = " ";
    ViChar   errorSource[MAX_FUNCTION_NAME_SIZE];
    plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
    const mxArray* in = prhs[1];
    LiomScopeStruct* h= reinterpret_cast<LiomScopeStruct*>(*((uint64_t *)mxGetData(in)));
    
    handleErr(niScope_InitiateAcquisition(h->vi));
 
    // Error code
    error = false;
    
    Error :
        // Return handle and associated error...
        if(error){
            (*mxGetPr(plhs[0]))=1;
        }
        else{
            (*mxGetPr(plhs[0]))=0;
        }
        return;
}