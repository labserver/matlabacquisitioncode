classdef ScopeClass < handle
    properties (Hidden = true, SetAccess = private)
        scope_handle;        
    end
    properties (Hidden = false)
        acq_freq=1e5;
        buffer_size=100;
        binning = 1;
    end
    methods (Access = private)
        [handle,error] = open_scope(obj,acq_freq,buffer_size,binning)
        [data,error] = fetch_scope(obj,handle)
        close_scope(obj,handle)
    end
    methods
        
        % Constructor opens the scope
        function this = ScopeClass(freq,npts,nbin)
            this.acq_freq = freq;
            this.buffer_size = npts;
            this.binning = nbin;
            [h,error] = this.open_scope(this.acq_freq,this.buffer_size,this.binning);
            this.scope_handle = h;
            %if(error)
            %    delete(this);
            %end
        end
        % Destructor
        function delete(this)
            this.close_scope(this.scope_handle);
        end
        % Fetch method
        function [data,error] = fetch(this)
            [data,error] = this.fetch_scope(this.scope_handle);
            if(error)
                delete(this);
            end
        end
        % Start Acquiring method
        function [error] = initiate(this)
            [error] = this.initiate_scope(this.scope_handle);
            if(error)
                delete(this);
            end
        end
    end
end