#include <matrix.h>
#include <mex.h>
#include "niScope.h"
#include "LiomScopeStruct.h"
#define MAX_STRING_SIZE 50

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {   // Initiate the acquisition
    
    const mxArray* in = prhs[1];
    LiomScopeStruct* h= reinterpret_cast<LiomScopeStruct*>(*((uint64_t *)mxGetData(in)));

    // Free all the allocated memory
    if (h->wfmInfoPtr)
        free(h->wfmInfoPtr);
    
    
    if (h->binaryWfmPtr)
        free(h->binaryWfmPtr);
    
    // Close the session
    if (h->vi)
        niScope_close(h->vi);

    delete h;
    return;
}