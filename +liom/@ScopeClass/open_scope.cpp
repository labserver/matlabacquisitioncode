#include <matrix.h>
#include <mex.h>
#include "niScope.h"
#include "LiomScopeStruct.h"
#define MAX_STRING_SIZE 50

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {
    
    bool completed = false;
    LiomScopeStruct* h=new LiomScopeStruct;
    h->timeout = 5.0;
    h->binaryWfmPtr = NULL;
    h->wfmInfoPtr = NULL;
    // Create the handle for matlab and set output at no-error point
    // This will allow clean-up if necessary
    plhs[0] = mxCreateNumericMatrix(1, 1, mxUINT64_CLASS, mxREAL);
    *((uint64_t *)mxGetData(plhs[0])) = reinterpret_cast<uint64_t>(h);
    
    // Get user parameters, prhs[0] is class handle
    double acq_rate = *((double*) mxGetData(prhs[1]));
    double n_samps = *((double*) mxGetData(prhs[2]));
    h->n_samps = (int) n_samps;
    double n_binning = *((double*) mxGetData(prhs[3]));
    
    // Increase acq_rate and number of samples according to binning
    acq_rate = acq_rate*n_binning;
    n_samps = n_samps*n_binning;
    h->n_binning = (int) n_binning;

    ViStatus error = VI_SUCCESS;
    ViChar   errorMessage[MAX_ERROR_DESCRIPTION] = " ";
    ViChar   errorSource[MAX_FUNCTION_NAME_SIZE];
    
    // Variables used to get values from the GUI
    ViChar resourceName[MAX_STRING_SIZE];
    strcpy(resourceName, "Dev2");    // Scope device name
    strcpy(h->channelName, "0,1,2,3,4,5");        // Channel 0
    
    // Vertical settings (from matlab)
    ViReal64 verticalRange = 6;
    ViReal64 verticalOffset = 0;
    
    // Horizontal settings (from matlab)
    ViReal64 minSampleRate = acq_rate;
    ViInt32 minRecordLength = (ViInt32) n_samps;
    ViReal64 refPosition = 0.0;
    
    
    // Open the NI-SCOPE instrument handle
    handleErr(niScope_init(resourceName, NISCOPE_VAL_FALSE, NISCOPE_VAL_FALSE, &(h->vi)));
    // Configure the vertical parameters
    handleErr(niScope_ConfigureVertical(h->vi, h->channelName, verticalRange, verticalOffset,
            NISCOPE_VAL_DC, 1.0, NISCOPE_VAL_TRUE));
    
    // Configure the horizontal parameters
    handleErr(niScope_ConfigureHorizontalTiming(h->vi, minSampleRate, minRecordLength, refPosition,
            1, VI_TRUE));
    
    
    // Configure the trigger (For analog)
    niScope_ConfigureTriggerDigital (h->vi, NISCOPE_VAL_PFI_1, NISCOPE_VAL_POSITIVE, 1e-3, 0); 
    
    // Find out the number of waveforms, based on # of channels and records
    handleErr(niScope_ActualNumWfms(h->vi, h->channelName, &h->numWaveform));
    
    // Query the coerced record length
    handleErr(niScope_ActualRecordLength(h->vi, &(h->actualRecordLength)));
    handleErr(niScope_SampleRate (h->vi, &h->actualSampleRate));
    // Allocate space for the waveform ,waveform info, and binary waveform
    // according to the record length and number of waveforms
    h->wfmInfoPtr = (niScope_wfmInfo*) malloc(sizeof (struct niScope_wfmInfo) * h->numWaveform);
    h->binaryWfmPtr = (ViInt16*) malloc(2 * h->actualRecordLength * h->numWaveform);

    // Error code
    plhs[1] = mxCreateDoubleMatrix(1, 1, mxREAL);
    completed = true;
    Error :
        // Return handle and associated error...
        if(completed){
            (*mxGetPr(plhs[1]))=h->actualSampleRate;
        }
        else{
            (*mxGetPr(plhs[1]))=0;
        }        
        return;
}