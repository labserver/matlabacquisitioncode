#include <matrix.h>
#include <mex.h>
#include "niScope.h"
#include "LiomScopeStruct.h"

void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]) {   

    // Initiate the acquisition
    bool error = true;
    ViChar   errorMessage[MAX_ERROR_DESCRIPTION] = " ";
    ViChar   errorSource[MAX_FUNCTION_NAME_SIZE];
    // Create this early in case of error
    plhs[1] = mxCreateDoubleMatrix(1, 1, mxREAL);

    
    const mxArray* in = prhs[1];
    LiomScopeStruct* h= reinterpret_cast<LiomScopeStruct*>(*((uint64_t *)mxGetData(in)));

    // Use the 16 bit fetching function
    handleErr(niScope_FetchBinary16(h->vi, h->channelName, h->timeout, h->actualRecordLength,
            (ViInt16*) h->binaryWfmPtr, h->wfmInfoPtr));
    
    // Do the binning
    int output_size = h->n_samps;
    plhs[0]=mxCreateNumericMatrix(output_size, h->numWaveform,  mxINT16_CLASS, mxREAL);
    ViInt16* resultPtr = (ViInt16*) mxGetPr(plhs[0]);
    for( int iwfm=0;iwfm<h->numWaveform;iwfm++){
        // Place ptr at beginning of stride
        ViInt16* start_ptr=h->binaryWfmPtr+iwfm*h->actualRecordLength;
        for(int i=0;i<output_size;i++){
            int start_idx = i*h->n_binning;
            int temp=start_ptr[start_idx];
            for(int ibin=1;ibin<h->n_binning;ibin++){
                temp+=start_ptr[start_idx+ibin];
            }
            resultPtr[i+iwfm*output_size]=(short int)(temp/h->n_binning);
        }
    }
   
    
    //memcpy(resultPtr, h->binaryWfmPtr, 2*h->actualRecordLength*h->numWaveform);

    // Error code
    error = false;
    
    Error :
        // Return handle and associated error...
        if(error){
            (*mxGetPr(plhs[1]))=1;
        }
        else{
            (*mxGetPr(plhs[1]))=0;
        }
        return;
}