#include "niScope.h"
#define MAX_STRING_SIZE 50
typedef unsigned long long uint64_t;
struct LiomScopeStruct
{
    ViSession vi;
    ViChar channelName[MAX_STRING_SIZE];
    ViReal64 timeout;
    ViInt32 actualRecordLength;
    ViInt16* binaryWfmPtr;
    struct niScope_wfmInfo* wfmInfoPtr;
    ViInt32 numWaveform;
    int n_binning;
    int n_samps;
    ViReal64 actualSampleRate;
 };