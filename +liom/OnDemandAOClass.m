classdef OnDemandAOClass < hgsetget
% Test class for prototype two photon scanner DAQ control
    
    properties
        % Default values
        deviceName;
        chanID;
    end
    properties (SetAccess=private)
        hTask;
        hChan;        
    end
    
    methods
        
        function obj = OnDemandAOClass(deviceName,channelIDs,taskname)
            import dabs.ni.daqmx.*
            obj.deviceName = deviceName;
            obj.chanID = channelIDs;            
%             obj.hTask = Task('OnDemandGalvoAOTask');
            obj.hTask = Task(taskname);
            obj.hChan = obj.hTask.createAOVoltageChan(obj.deviceName,obj.chanID); 
        end
        
        function write(obj,values)
            % Each channel must be in a single column
            obj.hTask.writeAnalogData(values);
        end
       
        function delete(obj)
           delete(obj.hTask);
        end          
    end
end

