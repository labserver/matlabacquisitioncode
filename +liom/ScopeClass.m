classdef ScopeClass < hgsetget
% Test class for prototype two photon scanner DAQ control
     %Private properties
     properties (GetAccess=private, Constant)

     end
     
     properties         
     end
    
    methods
        
        function obj = ScopeClass()
             folderbin = 'C:\Program Files\IVI Foundation\IVI\Bin';
             folderinc = 'C:\Program Files\IVI Foundation\IVI\include';
             loadlibrary(fullfile(folderbin,'niScope_64'),fullfile(folderinc,'niScope.h'), 'alias', 'Scopelib');
             [ret, LibVersion] = calllib('Scopelib', 'niScope_init', ("DAQ::1", VI_TRUE, VI_TRUE, &vi););
            if (ret<0)
                fprintf('Error in GetLibVersion. Aborted.\n');
                delete(obj);
            end;
        end
       
        function delete(obj)
            unloadlibrary('Scopelib')
        end      
        

    
    end
end

