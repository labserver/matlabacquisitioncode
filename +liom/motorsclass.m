classdef MotorsClass < hgsetget
    % Test class for prototype two photon scanner DAQ control
    
    properties
        deviceName;
        lineID;
    end
    properties (SetAccess=private)
        hComPort;
    end
    
    methods
        
        function obj = MotorsClass(comPort)
            obj.hComPort = serial(comPort,'BaudRate',19200,'DataBits',8, ...
                'Parity','none','StopBits',1);
            set(obj.hComPort,'Terminator','CR');
            fopen(obj.hComPort);
            % Turn motors on
            fprintf(obj.hComPort,'1MO');
            fprintf(obj.hComPort,'2MO');
            fprintf(obj.hComPort,'3MO');
        end
        
        function home(obj)
            
            % Wait until homing is done to return from this function to
            % make sure we have accurate positionning
            
            % Motor 1 = X
            disp('Homing X Motor')
            done = 0;
            fprintf(obj.hComPort,'1OR0');
            while(~done)
                fprintf(obj.hComPort,'1MD?');
                done = fscanf(obj.hComPort,'%d');
                pause(1)
            end
            % Motor 2 = Y
            disp('Homing Y Motor')
            done = 0;
            fprintf(obj.hComPort,'2OR0');
            while(~done)
                fprintf(obj.hComPort,'2MD?');
                done = fscanf(obj.hComPort,'%d');
                pause(1)
            end
            % Motor 3 = Z
            disp('Homing Z Motor')
            done = 0;
            fprintf(obj.hComPort,'3OR0');
            while(~done)
                fprintf(obj.hComPort,'3MD?');
                done = fscanf(obj.hComPort,'%d');
                pause(1)
            end
            % Center each motor in the center of their stride
            % Motor 1 = X
            disp('Centering X Motor')
            done = 0;
            fprintf(obj.hComPort,'1PA12.5');
            while(~done)
                fprintf(obj.hComPort,'1MD?');
                done = fscanf(obj.hComPort,'%d');
                pause(1)
            end
            % Motor 2 = Y
            disp('Centering Y Motor')
            done = 0;
            fprintf(obj.hComPort,'2PA12.5');
            while(~done)
                fprintf(obj.hComPort,'2MD?');
                done = fscanf(obj.hComPort,'%d');
                pause(1)
            end
            % Motor 3 = Z
            disp('Putting Z Motor at top limit')
            done = 0;
            fprintf(obj.hComPort,'3PA0.95');
            while(~done)
                fprintf(obj.hComPort,'3MD?');
                done = fscanf(obj.hComPort,'%d');
                pause(1)
            end
        end
        function move_dx(obj,dx)
            done = 0;
            fprintf(obj.hComPort,['1PR',num2str(dx)]);
            while(~done)
                fprintf(obj.hComPort,'1MD?');
                done = fscanf(obj.hComPort,'%d');
                pause(0.1)
            end
        end
        function move_dy(obj,dy)
            done = 0;
            fprintf(obj.hComPort,['2PR',num2str(dy)]);
            while(~done)
                fprintf(obj.hComPort,'2MD?');
                done = fscanf(obj.hComPort,'%d');
                pause(0.1)
            end
        end
        function move_dz(obj,dz)
            done = 0;
            fprintf(obj.hComPort,['3PR',num2str(dz)]);
            while(~done)
                fprintf(obj.hComPort,'3MD?');
                done = fscanf(obj.hComPort,'%d');
                pause(0.1)
            end
        end
        function move_ax(obj,x_pos)
            done = 0;
            fprintf(obj.hComPort,['1PA',num2str(x_pos)]);
            while(~done)
                fprintf(obj.hComPort,'1MD?');
                done = fscanf(obj.hComPort,'%d');
                pause(0.1)
            end
        end
        function move_ay(obj,y_pos)
            done = 0;
            fprintf(obj.hComPort,['2PA',num2str(y_pos)]);
            while(~done)
                fprintf(obj.hComPort,'2MD?');
                done = fscanf(obj.hComPort,'%d');
                pause(0.1)
            end
        end
        function move_az(obj,z_pos)
            done = 0;
            fprintf(obj.hComPort,['3PA',num2str(z_pos)]);
            while(~done)
                fprintf(obj.hComPort,'3MD?');
                done = fscanf(obj.hComPort,'%d');
                pause(0.1)
            end
        end
        function [x,y,z] = get_pos(obj)
            fprintf(obj.hComPort,'1TP');
            x = fscanf(obj.hComPort,'%f');
            fprintf(obj.hComPort,'2TP');
            y = fscanf(obj.hComPort,'%f');
            fprintf(obj.hComPort,'3TP');
            z = fscanf(obj.hComPort,'%f');
        end
        function delete(obj)
            % Turn motors off
            fprintf(obj.hComPort,'1MF');
            fprintf(obj.hComPort,'2MF');
            fprintf(obj.hComPort,'3MF');
            
            fclose(obj.hComPort);
            delete(obj.hComPort);
        end
        
    end
end

