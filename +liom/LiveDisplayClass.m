classdef LiveDisplayClass < handle
    % Test class to delegate display of images
    
    properties (SetAccess=private)
        handles;
        total_size;
        n_extra_points;
        
    end
    
    methods
        
        function obj = LiveDisplayClass(handles,total_size,n_extra_points)
            obj.handles = handles;
            obj.total_size = total_size;
            obj.n_extra_points=n_extra_points;
            
        end
        
        function acq_done(obj,src,evt)
            
            % Get data and truncate to the right size
            [img_data]=obj.handles.scope.fetch();
            img_data = img_data(1:obj.total_size,:);
            tmp_pmt = zeros((obj.n_extra_points+obj.handles.scan.nx),(obj.handles.scan.ny*3));
            tmp_pmt_line = zeros((obj.handles.scan.sum_lines_points*3),obj.handles.scan.repeat_line_scan);
            
            if (obj.handles.lineScan) %line scan
                if (obj.handles.Exist_points)
                    histogram = obj.handles.td.read_meas();
                    % in this section I have to present the % of the laser paulse i send to astrocyte.
                    %sum(histogram) will give me the counts from read_meas
                    percent_count = (sum(histogram)/obj.handles.counting_pulse)*100 ;
                    set(obj.handles.ph_count,'String',num2str(percent_count));
                    
                    if (percent_count > 5)
                        set(obj.handles.current,'String','Lower AOM to get less than 5% counts');
                    else
                        set(obj.handles.current,'String','Scanning segments');
                    end
                    
                    ns = 12.4/0.004 ; %this is for plotting up to 12.4 ns
                    t = (1:ns)*0.004; %ns
                    semilogy(t,histogram(1:ns),'Parent',obj.handles.lifetime_image);
                    
                    %   obj.handles.scan.histog = [obj.handles.scan.histog,histogram]; %I wonder if it saves handles.scan.histog
                end
                
                switch (obj.handles.scan.LineRamp)  %line scan
                    case 1 % triangle
                        %PMT   
                        for i =1:3
                            tmp_pmt_line((1+(obj.handles.scan.sum_lines_points*(i-1)):obj.handles.scan.sum_lines_points*i),1:end)...
                                = double(reshape(img_data(:,i),[obj.handles.scan.sum_lines_points, obj.handles.scan.repeat_line_scan]));

                        odd_rows_pmt_line = tmp_pmt_line((1+(obj.handles.scan.sum_lines_points*(i-1)):obj.handles.scan.sum_lines_points*i),1:2:end-1);
                        even_rows_pmt_line = tmp_pmt_line((1+(obj.handles.scan.sum_lines_points*(i-1)):obj.handles.scan.sum_lines_points*i),2:2:end);
                        
                        if( obj.handles.scan.shift_linescan >= 0 )
                            odd_rows_pmt_line = liom.shiftu(odd_rows_pmt_line,0,round(obj.handles.scan.shift_linescan/2),1);
                            even_rows_pmt_line = liom.shiftd(flipud(even_rows_pmt_line),0,round(obj.handles.scan.shift_linescan/2),1);
                        else
                            odd_rows_pmt_line = liom.shiftd(odd_rows_pmt_line,0,round(-obj.handles.scan.shift_linescan/2),1);
                            even_rows_pmt_line = liom.shiftu(flipud(even_rows_pmt_line),0,round(-obj.handles.scan.shift_linescan/2),1);                  
                        end
                        
                        tmp_pmt_line((1+(obj.handles.scan.sum_lines_points*(i-1)):obj.handles.scan.sum_lines_points*i),1:2:end-1)=odd_rows_pmt_line;
                        tmp_pmt_line((1+(obj.handles.scan.sum_lines_points*(i-1)):obj.handles.scan.sum_lines_points*i),2:2:end)=even_rows_pmt_line;
                        
                        end  
                          %Show Image tirangle
                         
                        switch (obj.handles.channel ) %line scan triangle
                            case 1
                                
                                imagesc(-tmp_pmt_line(1:obj.handles.scan.sum_lines_points-1,1:end-5)'...
                                    ,'Parent',obj.handles.line_image);
                                colorbar('peer',obj.handles.line_image);
                                
                            case 2
                                imagesc(-tmp_pmt_line(obj.handles.scan.sum_lines_points+1:...
                                    (obj.handles.scan.sum_lines_points*2)-1,1:end-5)','Parent',obj.handles.line_image);
                                colorbar('peer',obj.handles.line_image);
                                
                            case 3
                                imagesc(-tmp_pmt_line((obj.handles.scan.sum_lines_points*2)+1:...
                                    (obj.handles.scan.sum_lines_points*3)-1,1:end-5)','Parent',obj.handles.line_image);
                                colorbar('peer',obj.handles.line_image);
                        end
                        
                        set(obj.handles.line_image, 'Visible','off');
                        axis(obj.handles.line_image,'equal')
                        
                        % Galvos, plot ramps over all segments
                        tmp_galvo = reshape(img_data(:,5),[obj.handles.scan.sum_lines_points,obj.handles.scan.repeat_line_scan]);
                        plot(squeeze(tmp_galvo(1:obj.handles.scan.sum_lines_points*2)),'Parent',obj.handles.galvo)
                        hold(obj.handles.galvo)
                        tmp_galvo = reshape(img_data(:,6),[obj.handles.scan.sum_lines_points,obj.handles.scan.repeat_line_scan]);
                        plot(squeeze(tmp_galvo(1:obj.handles.scan.sum_lines_points*2)),'r','Parent',obj.handles.galvo)
                        hold(obj.handles.galvo)
                        set(obj.handles.galvo,'XTick',[],'YTIck',[]);
                        
                        
                        
                    case 2 % sawtooth line scan
                        % PMT
                        for i =1:3
                            tmp_pmt_line((1+(obj.handles.scan.sum_lines_points*(i-1)):obj.handles.scan.sum_lines_points*i),1:end)...
                                = reshape(img_data(:,i),[obj.handles.scan.sum_lines_points, obj.handles.scan.repeat_line_scan]);
                        end
                        
                        %Show Image line scan sawtooth
                        switch (obj.handles.channel )
                            case 1
                                
                                imagesc(-tmp_pmt_line(obj.handles.scan.n_extra_points:obj.handles.scan.sum_lines_points-1,:)','Parent',obj.handles.line_image);
                                colorbar('peer',obj.handles.line_image);
                                
                            case 2
                                imagesc(-tmp_pmt_line(obj.handles.scan.sum_lines_points+obj.handles.scan.n_extra_points:(obj.handles.scan.sum_lines_points*2)-1,:)','Parent',obj.handles.line_image);
                                colorbar('peer',obj.handles.line_image);
                                
                            case 3
                                imagesc(-tmp_pmt_line((obj.handles.scan.sum_lines_points*2)+obj.handles.scan.n_extra_points:(obj.handles.scan.sum_lines_points*3)-1,:)','Parent',obj.handles.line_image);
                                colorbar('peer',obj.handles.line_image);
                                
                        end
                        
                        set(obj.handles.line_image, 'Visible','off');
                        axis(obj.handles.line_image,'equal')
                        
                        % Galvos, plot ramps over all segments
                        tmp_galvo = reshape(img_data(:,5),[obj.handles.scan.sum_lines_points,obj.handles.scan.repeat_line_scan]);
                        plot(squeeze(tmp_galvo(1:obj.handles.scan.sum_lines_points*2)),'Parent',obj.handles.galvo)
                        hold(obj.handles.galvo)
                        tmp_galvo = reshape(img_data(:,6),[obj.handles.scan.sum_lines_points,obj.handles.scan.repeat_line_scan]);
                        plot(squeeze(tmp_galvo(1:obj.handles.scan.sum_lines_points*2)),'r','Parent',obj.handles.galvo)
                        hold(obj.handles.galvo)
                        set(obj.handles.galvo,'XTick',[],'YTIck',[]);
                        
                end
                %Save all PMTs
                
                if(obj.handles.save) %32678 is 2^15
                    switch(obj.handles.scan.LineRamp)
                        case 1 %triangle save
                            
                            imwrite(uint16(double(-tmp_pmt_line(1:obj.handles.scan.sum_lines_points-1,1:end-5)')+32768),fullfile(obj.handles.folder_name,...
                                ['Line_',num2str(obj.handles.line_scan_num)], ...
                                ['linescan_460nm_',num2str(obj.handles.line_scan_num),'.tif']),...
                                'writemode','append');
                            imwrite(uint16(double(-tmp_pmt_line(obj.handles.scan.sum_lines_points+1:...
                                (obj.handles.scan.sum_lines_points*2)-1,1:end-5)')+32768),...
                                fullfile(obj.handles.folder_name,['Line_',num2str(obj.handles.line_scan_num)], ...
                                ['linescan_520nm_',num2str(obj.handles.line_scan_num),'.tif']),...
                                'writemode','append');
                            imwrite(uint16(double(-tmp_pmt_line((obj.handles.scan.sum_lines_points*2)+1:...
                                (obj.handles.scan.sum_lines_points*3)-1,1:end-5)')+32768),...
                                fullfile(obj.handles.folder_name,['Line_',num2str(obj.handles.line_scan_num)], ...
                                ['linescan_593nm_',num2str(obj.handles.line_scan_num),'.tif']),...
                                'writemode','append');
                            
                        case 2 %sawtooth save
                            
                            imwrite(uint16(double(-tmp_pmt_line(obj.handles.scan.n_extra_points:obj.handles.scan.sum_lines_points-1,:)')+32768),fullfile(obj.handles.folder_name,...
                                ['Line_',num2str(obj.handles.line_scan_num)], ...
                                ['linescan_460nm_',num2str(obj.handles.line_scan_num),'.tif']),...
                                'writemode','append');
                            imwrite(uint16(double(-tmp_pmt_line(obj.handles.scan.sum_lines_points+obj.handles.scan.n_extra_points:(obj.handles.scan.sum_lines_points*2)-1,:)')+32768),...
                                fullfile(obj.handles.folder_name,['Line_',num2str(obj.handles.line_scan_num)], ...
                                ['linescan_520nm_',num2str(obj.handles.line_scan_num),'.tif']),...
                                'writemode','append');
                            imwrite(uint16(double(-tmp_pmt_line((obj.handles.scan.sum_lines_points*2)+obj.handles.scan.n_extra_points:(obj.handles.scan.sum_lines_points*3)-1,:)')+32768),...
                                fullfile(obj.handles.folder_name,['Line_',num2str(obj.handles.line_scan_num)], ...
                                ['linescan_593nm_',num2str(obj.handles.line_scan_num),'.tif']),...
                                'writemode','append');
                    end
                end
                
            else % 3D and live
                
                switch(obj.handles.scan.ramp_type)
                    case 1 %sawtooth  % 3D and live
                        % APD
                        tmp = reshape(-img_data(:,4),[(obj.n_extra_points+obj.handles.scan.nx),obj.handles.scan.ny]);
                        imagesc(tmp(1:obj.handles.scan.nx,1:end-1),'Parent',obj.handles.image);
                        colorbar('peer',obj.handles.image);
                        set(obj.handles.image, 'Visible','off');
                        axis(obj.handles.image,'equal')
                        
                        % PMT
                        for i =1:3
                            tmp_pmt(1:end, (1+ obj.handles.scan.ny*(i-1)):obj.handles.scan.ny*i)= ...
                                reshape(img_data(:,i),[(obj.n_extra_points+obj.handles.scan.nx),obj.handles.scan.ny]);
      
                        end
                        
                        %Show Image
                        switch (obj.handles.channel )
                            case 1
                                imagesc(-tmp_pmt(1:obj.handles.scan.nx,1:obj.handles.scan.ny-1),'Parent',obj.handles.pmt_image);
                                colorbar('peer',obj.handles.pmt_image);
                                                                
                            case 2
                                imagesc(-tmp_pmt(1:obj.handles.scan.nx,obj.handles.scan.ny+1:2*obj.handles.scan.ny-1),'Parent',obj.handles.pmt_image);
                                colorbar('peer',obj.handles.pmt_image);
                                
                            case 3
                                imagesc(-tmp_pmt(1:obj.handles.scan.nx,2*obj.handles.scan.ny+1:3*obj.handles.scan.ny-1),'Parent',obj.handles.pmt_image);
                                colorbar('peer',obj.handles.pmt_image);
                                
                        end
                        
                        set(obj.handles.pmt_image, 'Visible','off');
                        axis(obj.handles.pmt_image,'equal')
                        
                        % Galvos
                        tmp_galvo = reshape(img_data(:,5),[(obj.handles.scan.nx+obj.n_extra_points),obj.handles.scan.ny]);
                        plot(squeeze(tmp_galvo(1:2*(obj.handles.scan.nx+obj.n_extra_points))),'Parent',obj.handles.galvo)
                        hold(obj.handles.galvo)
                        tmp_galvo = reshape(img_data(:,6),[(obj.handles.scan.nx+obj.n_extra_points),obj.handles.scan.ny]);
                        plot(squeeze(tmp_galvo(1:2*(obj.handles.scan.nx+obj.n_extra_points))),'r','Parent',obj.handles.galvo)
                        hold(obj.handles.galvo)
                        set(obj.handles.galvo,'XTick',[],'YTIck',[]);
                        
                    case 2 %triangle with shift
                        
                        
                        
                        %APD
                      tmp = reshape(-img_data(:,4),[obj.handles.scan.nx+obj.n_extra_points,obj.handles.scan.ny]);
                        odd_rows = tmp(:,1:2:end-1);
                        even_rows = tmp(:,2:2:end-1);
                        even_rows = flipud(liom.shiftu(even_rows,0,obj.handles.scan.shift,1));
                        tmp(1:end-(obj.n_extra_points-1),1:2:end) = odd_rows((obj.n_extra_points):end,:);
                        tmp(1:end-(obj.n_extra_points-1),2:2:end-1) = even_rows(1:end-(obj.n_extra_points-1),:);
                        imagesc(tmp(1:end-(obj.n_extra_points-1),1:end-1),'Parent',obj.handles.image)
                        colorbar('peer',obj.handles.pmt_image);
                        set(obj.handles.image, 'Visible','off');
                        axis(obj.handles.image,'equal')
                        
                        % PMT
                        
                        for i =1:3
                            tmp_pmt(1:end, (1+ obj.handles.scan.ny*(i-1)):obj.handles.scan.ny*i)= ...
                                reshape(img_data(:,i),[(obj.n_extra_points+obj.handles.scan.nx),obj.handles.scan.ny]);
                        end
                        
                        odd_rows_pmt = tmp_pmt(:,1:2:end-1);
                        even_rows_pmt = tmp_pmt(:,2:2:end-1);
                        even_rows_pmt = flipud(liom.shiftu(even_rows_pmt,0,obj.handles.scan.shift,1));
                        tmp_pmt(1:end-(obj.n_extra_points-1),1:2:end) = odd_rows_pmt((obj.n_extra_points):end,:);
                        tmp_pmt(1:end-(obj.n_extra_points-1),2:2:end-1) = even_rows_pmt(1:end-(obj.n_extra_points-1),:);
                        
                        
                        %Show Image
                        switch (obj.handles.channel )
                            case 1
                                imagesc(-tmp_pmt(1:obj.handles.scan.nx,1:obj.handles.scan.ny-1),'Parent',obj.handles.pmt_image);
                                colorbar('peer',obj.handles.pmt_image);
                                
                            case 2
                                imagesc(-tmp_pmt(1:obj.handles.scan.nx,obj.handles.scan.ny+1:2*obj.handles.scan.ny-1),'Parent',obj.handles.pmt_image);
                                colorbar('peer',obj.handles.pmt_image);
                                
                            case 3
                                imagesc(-tmp_pmt(1:obj.handles.scan.nx,2*obj.handles.scan.ny+1:3*obj.handles.scan.ny-1),'Parent',obj.handles.pmt_image);
                                colorbar('peer',obj.handles.pmt_image);
                                
                        end
                        
                        set(obj.handles.pmt_image, 'Visible','off');
                        axis(obj.handles.pmt_image,'equal')
                        
                        % Galvos
                        tmp_galvo = reshape(img_data(:,5),[obj.handles.scan.nx+obj.n_extra_points,obj.handles.scan.ny]);
                        plot(squeeze(tmp_galvo(1:2*(obj.handles.scan.nx+obj.n_extra_points))),'Parent',obj.handles.galvo)
                        hold(obj.handles.galvo)
                        tmp_galvo = reshape(img_data(:,6),[obj.handles.scan.nx+obj.n_extra_points,obj.handles.scan.ny]);
                        plot(squeeze(tmp_galvo(1:2*(obj.handles.scan.nx+obj.n_extra_points))),'r','Parent',obj.handles.galvo)
                        hold(obj.handles.galvo)
                        set(obj.handles.galvo,'XTick',[],'YTIck',[]);
                        
                        %M: case 3
                        
                end
                
                if(obj.handles.save)
                    if(obj.handles.scan3D==0)
                        
                        % SAVE PMTs
                        imwrite(uint16(double(-tmp_pmt(1:obj.handles.scan.nx,1:obj.handles.scan.ny-1))+32768),fullfile(obj.handles.folder_name,['Live_',num2str(obj.handles.live_scan_num)], ...
                            ['livescan_460nm_',num2str(obj.handles.live_scan_num),'.tif']),...
                            'writemode','append');
                        imwrite(uint16(double(-tmp_pmt(1:obj.handles.scan.nx,obj.handles.scan.ny+1:2*obj.handles.scan.ny-1))+32768),fullfile(obj.handles.folder_name,['Live_',num2str(obj.handles.live_scan_num)], ...
                            ['livescan_520nm_',num2str(obj.handles.live_scan_num),'.tif']),...
                            'writemode','append');
                        imwrite(uint16(double(-tmp_pmt(1:obj.handles.scan.nx,2*obj.handles.scan.ny+1:3*obj.handles.scan.ny-1))+32768),fullfile(obj.handles.folder_name,['Live_',num2str(obj.handles.live_scan_num)], ...
                            ['livescan_593nm_',num2str(obj.handles.live_scan_num),'.tif']),...
                            'writemode','append');
                        % SAVE ADP
                        imwrite(uint16(double(tmp(1:obj.handles.scan.nx,1:end-1))+32768),fullfile(obj.handles.folder_name,['Live_',num2str(obj.handles.live_scan_num)], ...
                            ['livescanADP_',num2str(obj.handles.live_scan_num),'.tif']),...
                            'writemode','append');
                        
                    else
                        %SAVE PMT 3D
                        
                        imwrite(uint16(double(-tmp_pmt(1:obj.handles.scan.nx,1:obj.handles.scan.ny-1))+32768),fullfile(obj.handles.folder_name,['Volume_',num2str(obj.handles.vol_scan_num)], ...
                            ['volume_460nm_',num2str(obj.handles.vol_scan_num),'.tif']),...
                            'writemode','append');
                        imwrite(uint16(double(-tmp_pmt(1:obj.handles.scan.nx,obj.handles.scan.ny+1:2*obj.handles.scan.ny-1))+32768),fullfile(obj.handles.folder_name,['Volume_',num2str(obj.handles.vol_scan_num)], ...
                            ['volume_520nm_',num2str(obj.handles.vol_scan_num),'.tif']),...
                            'writemode','append');
                        imwrite(uint16(double(-tmp_pmt(1:obj.handles.scan.nx,2*obj.handles.scan.ny+1:3*obj.handles.scan.ny-1))+32768),fullfile(obj.handles.folder_name,['Volume_',num2str(obj.handles.vol_scan_num)], ...
                            ['volume_593nm_',num2str(obj.handles.vol_scan_num),'.tif']),...
                            'writemode','append');
                        % SAVE ADP 3D
                        imwrite(uint16(double(tmp(1:obj.handles.scan.nx,1:end-1))+32768),fullfile(obj.handles.folder_name,['Volume_',num2str(obj.handles.vol_scan_num)], ...
                            ['volumeADP_',num2str(obj.handles.vol_scan_num),'.tif']),...
                            'writemode','append');
                        
                    end
                end
            end %end lineScan
            if(obj.handles.scan3D==0)
                % Then restart next acquisition if live mode
                obj.handles.scope.initiate();
                obj.handles.ao.write_signals(obj.handles.data);
                if (obj.handles.Exist_points && obj.handles.lineScan)
                    obj.handles.td.start_meas();
                end
                obj.handles.ao.start()
            end
            
        end
        
    end
end

