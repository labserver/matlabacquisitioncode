classdef CounterClass < hgsetget
    properties
        n_avg;
        n_hist_points;
        laser_period;
        data;
    end
    properties
        hTask_pulse_train;
        hTask_counter;
    end
    methods
        
        function obj = CounterClass(n_avg,n_hist_points,laser_period)
            import dabs.ni.daqmx.*
            obj.n_avg = n_avg;
            obj.n_hist_points = n_hist_points;
            obj.laser_period = laser_period;
            train_freq = floor(obj.n_hist_points/(obj.laser_period*1));
            obj.hTask_pulse_train = Task('DigPulseTrain');
            obj.hTask_pulse_train.createCOPulseChanFreq('Dev1',2,'PulseTrain',train_freq,0.5,0,'DAQmx_Val_Low','DAQmx_Val_Hz');
            % AOM Start Trigger
            obj.hTask_pulse_train.cfgDigEdgeStartTrig('/Dev1/ao/StartTrigger','DAQmx_Val_Rising');
            obj.hTask_pulse_train.set('startTrigRetriggerable',1);
            obj.hTask_pulse_train.cfgImplicitTiming('DAQmx_Val_ContSamps',obj.n_hist_points);
            %             obj.hTask_pulse_train.cfgImplicitTiming('DAQmx_Val_FiniteSamps',obj.n_hist_points);
            
            obj.hTask_counter = Task('Counter');
            obj.hTask_counter.createCICountEdgesChan('Dev1',1,'ClockSamp','DAQmx_Val_CountUp','DAQmx_Val_Rising');
            %             obj.hTask_counter.cfgSampClkTiming(train_freq,'DAQmx_Val_FiniteSamps',obj.n_avg*obj.n_hist_points,'PFI14','DAQmx_Val_Rising');
            obj.hTask_counter.cfgSampClkTiming(train_freq,'DAQmx_Val_ContSamps',obj.n_avg*obj.n_hist_points,'PFI14','DAQmx_Val_Rising');
            
        end
        
        function start(obj)
            obj.hTask_counter.start();
            obj.hTask_pulse_train.start();
        end
        function data=read(obj)
            
            data=obj.hTask_counter.readCounterData(obj.n_avg*obj.n_hist_points);
            
        end
        function stop(obj)
            obj.hTask_counter.stop();
            obj.hTask_pulse_train.stop();
        end
        
        function delete(obj)
            delete(obj.hTask_counter);
            delete(obj.hTask_pulse_train);
        end
        
    end
end
