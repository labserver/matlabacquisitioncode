classdef FiniteAIClass < handle
    
    properties
        hTask;
        deviceName;
        chanID;
        everyNSamples;
        sampRate;
        callbackPeriod = 0.5; %seconds
    end
    
    properties (SetAccess=private)
        hChan;
    end
    
    events
        haveData;
        Done;
    end
    
    methods
        
        function obj = FiniteAIClass(deviceName,channelIDs)
            obj.deviceName = deviceName;
            obj.chanID = channelIDs;
        end
        
        function config(obj,sampRate,sampsPerChanToAcquire, source, taskName)
            
            import dabs.ni.daqmx.*
            
            obj.hTask = Task(taskName);
            obj.hChan = obj.hTask.createAIVoltageChan(obj.deviceName,obj.chanID);
            obj.sampRate = sampRate;
            obj.everyNSamples = round(obj.sampRate * obj.callbackPeriod);
            obj.hTask.cfgSampClkTiming(obj.sampRate, 'DAQmx_Val_FiniteSamps',sampsPerChanToAcquire);
            obj.hTask.registerEveryNSamplesEvent(@obj.dataCallback,obj.everyNSamples);
            obj.hTask.registerDoneEvent(@obj.doneCallback);
        end
        
        function cfgDigEdgeStartTrig(obj, triggerSource)
            obj.hTask.cfgDigEdgeStartTrig(triggerSource);
        end
        function delete(obj)
            obj.hTask.stop();
            delete(obj.hTask);
        end
        
        function start(obj)
            if obj.hTask.isTaskDone()
                obj.hTask.stop();
                obj.hTask.start();
            else
                disp('Task already started');
            end
        end
        
        function stop(obj)
            obj.hTask.stop();
        end
        
    end
    
    methods (Access=private)

        function dataCallback(obj,src,evnt)
            obj.notify('haveData')
        end
        
        function doneCallback(obj,src,evnt)
            obj.notify('Done')
        end
        
    end
    
end

