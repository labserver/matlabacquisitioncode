classdef ShutterClass < hgsetget
% Test class for prototype two photon scanner DAQ control
    
    properties
        deviceName;
        lineID;
    end
    properties (SetAccess=private)
        hTask;
        hChan;        
    end
    
    methods
        
        function obj = ShutterClass(deviceName,lineID)
            import dabs.ni.daqmx.*
            obj.deviceName = deviceName;
            obj.lineID = lineID;
            obj.hTask = dabs.ni.daqmx.Task('Shutter');
            obj.hChan = obj.hTask.createDOChan(obj.deviceName,obj.lineID,'','DAQmx_Val_ChanPerLine');
            % Always start with shutter closed
            obj.hTask.writeDigitalData(0);

        end
  
        function open(obj)
            obj.hTask.writeDigitalData(1);
        end
        
        function close(obj)
            obj.hTask.writeDigitalData(0);
        end     
        
        function delete(obj)
           delete(obj.hTask);
        end        
 
    end
end

