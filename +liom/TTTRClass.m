classdef TTTRClass < hgsetget
% Test class for prototype two photon scanner DAQ control
     %Private properties
     properties (GetAccess=private, Constant)
         % Constants from Phdefin.h   
         REQLIBVER =  '2.3';     % this is the version this program expects
         MAXDEVNUM =      8;
         TTREADMAX = 131072;     % 128K event records
         HISTCHAN  =  65536;	 % number of histogram channels
         RANGES	  =      8;
         MODE_HIST =      0;
         MODE_T2	  =      2;
         MODE_T3	  =      3;
         
         FLAG_OVERFLOW = hex2dec('0040');
         FLAG_FIFOFULL = hex2dec('0003');
         
         ZCMIN		  =          0;		% mV
         ZCMAX		  =         20;		% mV
         DISCRMIN	  =          0;	    % mV
         DISCRMAX	  =        800;	    % mV
         OFFSETMIN	  =          0;		% ps
         OFFSETMAX	  = 1000000000;	    % ps
         
         ACQTMIN		  =          1;		% ms
         ACQTMAX		  =  360000000;	    % ms  (100*60*60*1000ms = 100h)
         
         % Errorcodes from errorcodes.h
         
         PH_ERROR_DEVICE_OPEN_FAIL		 = -1;
     end
     
     properties         
         dev;
         % Settings for the measurement, default values
         Mode; % you can change this
         Offset       = 0;       %  you can change this
         CFDZeroX0    = 10;      %  you can change this
         CFDLevel0    = 150;     %  you can change this
         CFDZeroX1    = 10;      %  you can change this
         CFDLevel1    = 150;     %  you can change this
         SyncDiv      = 1;       %  you can change this
         Range        = 0;       %  you can change this
         Tacq         = 10000;    %  you can change this
     end
    
    methods
        
        function obj = TTTRClass()
             Mode         = obj.MODE_T2; 
             [folder,name,ext]=fileparts(mfilename('fullpath'));
             loadlibrary(fullfile(folder,'PHLib64'),fullfile(folder,'phlib.h'), 'alias', 'PHlib');
             LibVersion    = '????'; %enough length!
             LibVersionPtr = libpointer('cstring', LibVersion);
            [ret, LibVersion] = calllib('PHlib', 'PH_GetLibraryVersion', LibVersionPtr);
            if (ret<0)
                fprintf('Error in GetLibVersion. Aborted.\n');
                delete(obj);
            end;
        end
       
        function delete(obj)
            obj.close_device();
            unloadlibrary('PHlib')
        end      
        
        function initialize(obj)
            obj.find_device();
            if( obj.dev > -1 )
                [ret] = calllib('PHlib', 'PH_Initialize', obj.dev, obj.MODE_HIST);
                if(ret<0)
                    fprintf('\nPH init error %d. Aborted.\n',retcode);
                end
                
                [ret] = calllib('PHlib', 'PH_Calibrate', obj.dev);
                if (ret<0)
                    fprintf('\nPH_Calibrate error %1d. Aborted.\n',ret);
                    obj.close_device();
                    return;
                end;
                
                [ret] = calllib('PHlib', 'PH_SetSyncDiv', obj.dev, obj.SyncDiv);
                if (ret<0)
                    fprintf('\nPH_SetSyncDiv error %1d. Aborted.\n',ret);
                    obj.close_device();
                    return;
                end;
                
                [ret] = calllib('PHlib', 'PH_SetCFDLevel', obj.dev, 0, obj.CFDLevel0);
                if (ret<0)
                    fprintf('\nPH_SetCFDLevel error %ld. Aborted.\n', ret);
                    obj.close_device();
                    return;
                end;
                
                [ret] = calllib('PHlib', 'PH_SetCFDLevel', obj.dev, 1, obj.CFDLevel1);
                if (ret<0)
                    fprintf('\nPH_SetCFDLevel error %ld. Aborted.\n', ret);
                    obj.close_device();
                    return;
                end;
                
                [ret] = calllib('PHlib', 'PH_SetCFDZeroCross', obj.dev, 0, obj.CFDZeroX0);
                if (ret<0)
                    fprintf('\nPH_SetCFDZeroCross error %ld. Aborted.\n', ret);
                    obj.close_device();
                    return;
                end;
                
                [ret] = calllib('PHlib', 'PH_SetCFDZeroCross', obj.dev, 1, obj.CFDZeroX1);
                if (ret<0)
                    fprintf('\nPH_SetCFDZeroCross error %ld. Aborted.\n', ret);
                    obj.close_device();
                    return;
                end;
                
                [ret] = calllib('PHlib', 'PH_SetRange', obj.dev, obj.Range);
                if (ret<0)
                    fprintf('\nPH_SetRange error %ld. Aborted.\n', ret);
                    obj.close_device();
                    return;
                end;
                
                [Offset] = calllib('PHlib', 'PH_SetOffset', obj.dev, obj.Offset);
                if (Offset<0)
                    fprintf('\nPH_SetOffset error %ld. Aborted.\n', ret);
                    obj.close_device();
                    return;
                end;
                
                ret = calllib('PHlib', 'PH_SetStopOverflow', obj.dev, 1, 65535);
                if (ret<0)
                    fprintf('\nPH_SetStopOverflow error %ld. Aborted.\n', ret);
                    obj.close_device();
                    return;
                end;
                
                [Resolution] = calllib('PHlib', 'PH_GetResolution', obj.dev);
                if (Resolution<0)
                    fprintf('\nPH_GetResolution error %ld. Aborted.\n', ret);
                    obj.close_device();
                    return;
                end;
                % Note: after Init or SetSyncDiv you must allow 100 ms for valid new count rate readings
                pause(0.2);
                Countrate0 = calllib('PHlib', 'PH_GetCountRate', obj.dev,0);
                Countrate1 = calllib('PHlib', 'PH_GetCountRate', obj.dev,1);
                
                fprintf('\nResolution=%1dps Countrate0=%1d/s Countrate1=%1d/s', Resolution, Countrate0, Countrate1);
            end
        end
        
        function buffer = take_meas(obj)
            buffer  = uint32(zeros(1,obj.TTREADMAX));
            bufferptr = libpointer('uint32Ptr', buffer);
            
            % from here you can repeat the measurement (with the same settings)
            
            Progress = 0;
            fprintf('\nProgress:%9d',Progress);
            
            ret = calllib('PHlib', 'PH_StartMeas', obj.dev,obj.Tacq);
            if (ret<0)
                fprintf('\nPH_StartMeas error %ld. Aborted.\n', ret);
                obj.close_device();
                return;
            end;
            
            while(1)
                
                flags = calllib('PHlib', 'PH_GetFlags', obj.dev);
                FiFoWasFull=bitand(flags,obj.FLAG_FIFOFULL);
                
                if (FiFoWasFull)
                    fprintf('\nFiFo Overrun!\n');
                    break;
                end;
                
                [ret, buffer] = calllib('PHlib','PH_TTReadData', obj.dev, bufferptr, obj.TTREADMAX);
                %Note that PH_TTReadData may return less than requested
                if (ret<0)
                    fprintf('\nPH_TTReadData error %d\n',ret);
                    break;
                end;
                
                if(ret)
                    cnt = fwrite(fid, buffer(1:ret),'uint32');
                    if(cnt ~= ret)
                        fprintf('\nfile write error\n');
                        break;
                    end;
                    Progress = Progress + ret;
                    fprintf('\b\b\b\b\b\b\b\b\b%9d',Progress);
                else
                    CTCDone = calllib('PHlib', 'PH_CTCStatus', obj.dev);
                    if (CTCDone)
                        fprintf('\nDone\n');
                        break;
                    end;
                end;
                Countrate0 = calllib('PHlib', 'PH_GetCountRate', obj.dev,0); %call only if needed
                Countrate1 = calllib('PHlib', 'PH_GetCountRate', obj.dev,1); %call only if needed
                
            end; %while
            
            
            ret = calllib('PHlib', 'PH_StopMeas', obj.dev);
            if (ret<0)
                fprintf('\nPH_StopMeas error %ld. Aborted.\n', ret);
                obj.close_device();
                return;
            end;
            
            obj.close_device();           
            fprintf('\nData is in tttrmode.out\n');
            
            
        end
        
        function find_device(obj)
            obj.dev = -1;
            Serial     = '12345678'; %enough length!
            SerialPtr  = libpointer('cstring', Serial);
            
            [ret, Serial] = calllib('PHlib', 'PH_OpenDevice', 0, SerialPtr);
            if (ret==0)       
                obj.dev=0; 
            else
                fprintf('\nPH_OpenDevice: Did not find any device\n');
            end
        end
        
        function close_device(obj)
            if (libisloaded('PHlib'))
                calllib('PHlib', 'PH_CloseDevice',0);
            end
        end
    end
    
    
end

