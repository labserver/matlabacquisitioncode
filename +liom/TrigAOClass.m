classdef TrigAOClass < handle
    % Test class for prototype two photon scanner DAQ control
    properties
        % Default values
        deviceName;
        chanID;
        sampRate = 50e3;     % 50 kHz default = 1000 lignes de 200 points
        hTask;
        taskname
    end
    properties (SetAccess=private)
        hChan;
        delegate;
    end
    events
        AODone
    end
    methods
        
        function obj = TrigAOClass(deviceName,channelIDs, taskname)
            obj.deviceName = deviceName;
            obj.chanID = channelIDs;
            obj.taskname = taskname;
        end
        function add_delegate(obj,delegate)
            obj.delegate=delegate;
        end
        
        function config(obj,varargin)
            import dabs.ni.daqmx.*
            
%             obj.hTask = Task('GalvoAOTask');
            obj.hTask = Task(obj.taskname);
            obj.hChan = obj.hTask.createAOVoltageChan(obj.deviceName,obj.chanID);
            obj.hTask.registerDoneEvent(@obj.doneCallback);
            if nargin >= 2
                obj.sampRate = varargin{1};
            end
        end
        
        function write_signals(obj,signals)
            % Each channel must be in a single column
            obj.hTask.cfgSampClkTiming(obj.sampRate, 'DAQmx_Val_FiniteSamps', size(signals,1));
            obj.hTask.writeAnalogData(signals,inf,false,size(signals,1));
        end
        
        function delete(obj)
            delete(obj.hTask);
        end
        
        function start(obj)
            if obj.hTask.isTaskDone()
                obj.hTask.stop();
                obj.hTask.start();
            else
                disp('Task already started');
            end
        end
        function stop(obj)
          %  disp('Stopping analog input');
            obj.hTask.abort();
        end
        % These function are just there by default, in general the user
        % will provide his/her callback during the config call
        function setRetriggerable(obj)
            obj.hTask.set('startTrigRetriggerable',1);
        end
        
    end
    methods (Access = private)
        function doneCallback(obj,src,evnt)
            obj.hTask.stop();
            obj.notify('AODone');
        end
    end
    
    
    
end

