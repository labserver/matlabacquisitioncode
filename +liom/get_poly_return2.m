function y = get_poly_return2(line1_VoltStart, line1_VoltEnd, n_pts_line1, line2_VoltStart, line2_VoltEnd, n_pts_line2, n_extra_pts )
%
% Returns polynomial for galvo ramp to interpolate between two ramps. This
% function solves the polynomial return so that: start and end values are
% the end of line 1 et start of line 2. Slope at the start = slope of line
% 1. Slope at the end = slope of line 2.
% n_extra_pts is the number of points used in the polynomial interpolation.   
%
%_______________________________________________________________________________
% Copyright (C) 2012 LIOM Laboratoire d'Imagerie Optique et Mol�culaire
%                    �cole Polytechnique de Montr�al
%_______________________________________________________________________________


% Polynomial equation left-side
aEqu = [1 1 1 1]';                                      % Initial value
bEqu = [1 n_extra_pts n_extra_pts^2 n_extra_pts^3]';    % End value
cEqu = [0 1 2 3]';                                      % Initial slope 
dEqu = [0 1 2*n_extra_pts 3*n_extra_pts^2]';            % End slope
% Polynomial equation right-side
polyEqu = [line1_VoltEnd line2_VoltStart (line1_VoltEnd-line1_VoltStart)/n_pts_line1 (line2_VoltEnd-line2_VoltStart)/n_pts_line2];
% Solve poly equation
x = [aEqu bEqu cEqu dEqu]'\polyEqu';
if n_extra_pts > 0
    yRampReturn = linspace(1, n_extra_pts, n_extra_pts);
    y = x(1) + x(2).*yRampReturn + x(3)*yRampReturn.^2 + x(4)*yRampReturn.^3;
else
    y=[];
end
end

% EOF
