function [p, ci, PO2, FitResults, x, y, R2] = CalcPO2_Batch2(HistData, PulseDuration, PulseHighTime, NeglectTime)
% HistData: Histogram data 
% PulseDuration: laser pulse duration for one lifetime measurement (us)
% PulseHighTime: laser high pulse duration (us)

NeglectPoints = ceil(NeglectTime / (PulseDuration/length(HistData)));
temps = linspace(0,PulseDuration, length(HistData)); % in us
DecayStartPoint = ceil(length(HistData)*PulseHighTime/PulseDuration); % the index of temps in which the decay starts
% We do the fit from the point that the decay starts
x=temps(DecayStartPoint+NeglectPoints:end-1)';
x=x-temps(DecayStartPoint);
y=HistData(DecayStartPoint+NeglectPoints:end-1);
AvgSteadyY = mean(HistData(end-4:end));
y = y - AvgSteadyY;
y=y/max(y);

ffun=fittype(@(a,b,c,x)(a*(exp(-x/b)))+c,'coefficients',{'a','b','c'},'independent','x');
startpoint=[1 50 0]; % the initial guess of of the coefficients to start the iretation
[FitResults gof]=fit(x,y,ffun,'Startpoint',startpoint,'Lower',[0 0 -100],'Upper',[1e7 500 500]);
p=coeffvalues(FitResults); %coefficients from the fitting
ci = confint(FitResults,0.95); % confidence intervals for fit coefficients

% fun=@(p)((p(1)*(exp(-x/p(2))))+p(3))-y;
% x0 = [1 50 0];
% FitOptions = optimset('Display','off','LevenbergMarquardt','on');
% [p,resnorm,residual,exitflag,output,lambda,jacobian]  = lsqnonlin(fun,x0,[],[],FitOptions);
% ci=0;
% FitResults=0;


tau=p(2)*1.0e-6; % lifetime (in s)

Mean_y = mean(y);
SS_tot = 0; SS_res = 0;
for i = 1 : length(y)
    SS_tot = SS_tot + (y(i) - Mean_y)^2;
    SS_res = SS_res + (y(i) - (p(1)*exp(-x(i)/p(2))+p(3)))^2;
end
R2 = 1 - (SS_res / SS_tot);

%Calibration curve coefficients
if NeglectTime <4
    y0 = -7.9696; A1 = 286.7229; t1 = 1.4103e-05; A2 = 9.0867e+06; t2 = 8.9046e-07;  % parameters for 0 point ignored
elseif NeglectTime<8
    y0 = -8.2877; A1 = 317.8207; t1 = 1.3988e-05; A2 = 7.7918e+06; t2 = 1.0261e-06; % parameters for 1 point ignored
elseif NeglectTime<12
    y0 = -6.2143; A1 = 376.7995; t1 = 1.2702e-05; A2 = 9.2900e+04; t2 = 1.6623e-06; % parameters for 2 points ignored
elseif NeglectTime<16
    y0 = -7.2201; A1 = 393.7629; t1 = 1.3041e-05; A2 = 8.4393e+05; t2 = 1.4602e-06; % parameters for 3 points ignored
elseif NeglectTime<20
    y0 = -8.6347; A1 = 370.6810; t1 = 1.3899e-05; A2 = 6.5758e+04; t2 = 2.1262e-06;  % parameters for 4 points ignored
else
    y0 = -10.07748; A1 = 4163.10268; t1 = 3.95074e-6; A2 = 264.02718; t2 = 1.61237e-5; %Sergei's values (5 points ignored)
end

PO2 = A1*exp(-tau/t1) + A2*exp(-tau/t2) + y0;  % PO2 value in mmHg

end