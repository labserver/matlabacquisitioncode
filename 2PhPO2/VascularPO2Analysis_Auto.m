function VascularPO2Analysis_Auto(FolderNumber)

handles.ProbeBatch = 2; % 1: 2014 batch; 2: 2015 batch

AvgPO2_4_96 =[]; MaxPO2_4_96 =[]; MaxPO2_2_4_96 =[]; VesselD= [];
AvgPO2_8_96 =[]; MaxPO2_8_96 =[]; MaxPO2_2_8_96 =[];
AvgPO2_12_96 =[]; MaxPO2_12_96 =[]; MaxPO2_2_12_96 =[];
NoOfDecays =[]; AOM=[]; Corodinates = []; Depth =[]; MeanPointX=[]; MeanPointY=[];

handles.folder = uigetdir('C:\Users\moeini\Mohammad\Data\PO2 data\VascularAwakePO2\Vascular PO2\');
handles.Savefolder = uigetdir('C:\Users\moeini\Mohammad\Data\PO2 data\Awake 2\Analyzed data\');

mkdir([handles.Savefolder '/4_96' ]) 
mkdir([handles.Savefolder '/8_96' ]) 
mkdir([handles.Savefolder '/12_96' ]) 

for f = 1 : length(FolderNumber)
        %% Read Scan Info for analysis
        load([handles.folder '/PO2_' num2str(FolderNumber(f)) '/PO2ScanInfo_' num2str(FolderNumber(f)) '.mat']); %scan information
        handles.ScanInfo = scan;
        clear scan
%         handles.scale = (handles.ScanInfo.scan_height/400);   
%         handles.ScanInfo.ramp_y = handles.ScanInfo.ramp_y + 20 * handles.scale ; % correcting the shift between the pO2 grid coordinate and the pmt coordinate      
% 
        load([handles.folder '/PO2_' num2str(FolderNumber(f)) '/image_pmt.mat']); % PMT live image 
        handles.PMTImage = image_ref_pmt;
        clear image_ref_pmt        
%         handles.PMTImage(1:round(handles.scale * 50),:) = min(handles.PMTImage(:)); % remove the mirror artefact of the PMT image
       
        %% Calculating the pO2 values 4_96

        handles.NeglectTime = 4;
        handles.AcceptableR2 = 0.96;

        handles.PO2=[]; handles.R_square=[];
        for i = 1: length(handles.ScanInfo.ramp_x)
            handles.RepStart=1;
            handles.RepEnd=handles.ScanInfo.PO2Repitition * handles.ScanInfo.PointRepetition;

            if handles.ProbeBatch == 1
                if max(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2)) == min(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2))
                    handles.PO2(i) = NaN; handles.R_square(i)=NaN;
                else
                    [~, ~, handles.PO2(i), ~, ~, ~, handles.R_square(i)] = CalcPO2_2(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, handles.NeglectTime);
                end
            elseif handles.ProbeBatch == 2
                if max(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2)) == min(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2))
                    handles.PO2(i) = NaN; handles.R_square(i)=NaN;
                else
                    [~, ~, handles.PO2(i), ~, ~, ~, handles.R_square(i)] = CalcPO2_2015(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, handles.NeglectTime);
                end
            end
        end


        %% Filtering the points
        handles.FilteredPO2 = [];
        handles.FilteredX = [];
        handles.FilteredY = [];

        for i = 1: length(handles.PO2)
            if handles.PO2(i) > 0 && handles.PO2(i) < 250
                    if handles.R_square(i) > handles.AcceptableR2
                        handles.FilteredPO2 = [handles.FilteredPO2, handles.PO2(i)];
                        handles.FilteredX = [handles.FilteredX, handles.ScanInfo.ramp_x(i)];
                        handles.FilteredY = [handles.FilteredY, handles.ScanInfo.ramp_y(i)];
                    end
            end
        end
       

        %% Saving the results
        Corodinates(f,:) = [handles.ScanInfo.xpos, handles.ScanInfo.ypos, handles.ScanInfo.zpos];
        if isfield(handles.ScanInfo,'top_z')
            Depth(f) =1000*(handles.ScanInfo.top_z - handles.ScanInfo.zpos); %depth in um
        else
            Depth(f)=NaN;
        end        
        AOM(f) = handles.ScanInfo.aom_value;;
        NoOfDecays(f) = handles.ScanInfo.PO2Repitition * handles.ScanInfo.PointRepetition;
        MeanPointX(f) = mean(handles.ScanInfo.ramp_x);
        MeanPointY(f) = mean(handles.ScanInfo.ramp_y);
        
       
        yyy=[];
        for  m = 1 : length(handles.FilteredPO2)
            yyy(m)= (handles.FilteredX(m) - handles.FilteredX(1))^2 + (handles.FilteredY(m) - handles.FilteredY(1))^2;
            yyy(m)=sqrt(yyy(m));
        end
        PO2Curve = figure;
        plot(yyy, handles.FilteredPO2);
        SmoothF = 4;
        hold on; plot(yyy, smooth(handles.FilteredPO2',SmoothF),'r'); hold off;
        print(PO2Curve,'-dtiff', [handles.Savefolder '/4_96/' num2str(FolderNumber(f)) '.tif']);
        close(PO2Curve);
%         
        AvgPO2_4_96(f) = mean(handles.FilteredPO2);
        if isempty(handles.FilteredPO2)
            Max_PO2_4_96(f)=NaN; Max_PO2_2_4_96(f)=NaN;
        else
            Max_PO2_4_96(f) = max(handles.FilteredPO2);
            Max_PO2_2_4_96(f) = max(smooth(handles.FilteredPO2,SmoothF));
        end
          
        
        % Saving the PMT/PO2 image
        PMTPO2Image = figure;
        hPMTPO2Image=imagesc(handles.PMTImage);  colormap gray; cbFITC = colorbar;
        set(gca, 'YTick', []);
        set(gca, 'XTick', []);
        colormap([gray(64);jet(64)]);
        maxFTC = max(handles.PMTImage(:));
        minFTC = min(handles.PMTImage(:));
        ncmp=64;
        c_FITC = min(ncmp,  round(   (ncmp-1).*(handles.PMTImage-minFTC)./(maxFTC-minFTC)  )+1  );

        set(hPMTPO2Image,'CDataMapping','Direct');
        set(hPMTPO2Image,'CData',c_FITC);
        figure(PMTPO2Image);
        caxis([min(c_FITC(:)) max(c_FITC(:))]);
        set(cbFITC,'YLim',[ncmp+1 2*ncmp]);

        cmap1=jet(64);
        CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
%         minPO2 = min(handles.FilteredPO2);
%         maxPO2 = max(handles.FilteredPO2);
        minPO2 = 0;
        maxPO2 = 160;
        
        cmapPO2idx = ones(length(handles.FilteredPO2),1); % vector of colormap indexes for pO2
        if maxPO2 == minPO2,
            cmapPO2idx = cmapPO2idx.*round(CmapN/2);
        else
            cmapPO2idx = round(   (  CmapN.*(handles.FilteredPO2-minPO2)+maxPO2-handles.FilteredPO2  )    ./(maxPO2-minPO2) );
            cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
            cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
        end;

        for i = 1: length(handles.FilteredPO2)
             colMap = squeeze(cmap1(cmapPO2idx(i),:));
             rectangle('Position',[(handles.FilteredX(i) - 7),(handles.FilteredY(i) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
             daspect ([1,1,1])
        end

        step = 20;         
        newTickLabels = step*ceil(minPO2/step)  : step  : step*floor(maxPO2/step);
        newTicks =( newTickLabels - (minPO2*ncmp-maxPO2)/(ncmp-1) ) .* (ncmp-1) ./ (maxPO2-minPO2);
        newTickLabelsStr=num2str(newTickLabels(1),'%i');
        for idxLab = 2:length(newTickLabels),
            newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
        end;
        pause(0);
        set(cbFITC,'YTick',newTicks+ncmp);
        set(cbFITC,'YTickLabel',newTickLabelsStr, 'fontsize',30);

        print(PMTPO2Image,'-dtiff', [handles.Savefolder '/4_96/PMTPO2Image' num2str(FolderNumber(f)) '.tif']);
       
        
        
        % Vessel diameter
       
        [line_x, line_y] = getline(PMTPO2Image);
        LineScan = improfile(handles.PMTImage,[line_x(1) line_x(2)],[line_y(1) line_y(2)]);
        XPixelSize = handles.ScanInfo.scan_height / handles.ScanInfo.nx;  % Pixel size in X direction (in um) 
        YPixelSize = handles.ScanInfo.scan_width / handles.ScanInfo.ny;   % Pixel size in Y direction (in um)    
        scale = sqrt(((line_x(2)-line_x(1))*XPixelSize)^2. + ((line_y(2)-line_y(1))*YPixelSize)^2.)/length(LineScan);
        VesselD(f) = scale * VesselDiameter(LineScan)
           
        close(PMTPO2Image);         

             
        
                %% Calculating the pO2 values 8_96

        handles.NeglectTime = 8;
        handles.AcceptableR2 = 0.96;

        handles.PO2=[]; handles.R_square=[];
        for i = 1: length(handles.ScanInfo.ramp_x)
            handles.RepStart=1;
            handles.RepEnd=handles.ScanInfo.PO2Repitition * handles.ScanInfo.PointRepetition;

            if handles.ProbeBatch == 1
                if max(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2)) == min(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2))
                    handles.PO2(i) = NaN; handles.R_square(i)=NaN;
                else
                    [~, ~, handles.PO2(i), ~, ~, ~, handles.R_square(i)] = CalcPO2_2(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, handles.NeglectTime);
                end
            elseif handles.ProbeBatch == 2
                if max(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2)) == min(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2))
                    handles.PO2(i) = NaN; handles.R_square(i)=NaN;
                else
                    [~, ~, handles.PO2(i), ~, ~, ~, handles.R_square(i)] = CalcPO2_2015(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, handles.NeglectTime);
                end
            end
        end

        %% Filtering the points
        handles.FilteredPO2 = [];
        handles.FilteredX = [];
        handles.FilteredY = [];

        for i = 1: length(handles.PO2)
            if handles.PO2(i) > 0 && handles.PO2(i) < 250
                    if handles.R_square(i) > handles.AcceptableR2
                        handles.FilteredPO2 = [handles.FilteredPO2, handles.PO2(i)];
                        handles.FilteredX = [handles.FilteredX, handles.ScanInfo.ramp_x(i)];
                        handles.FilteredY = [handles.FilteredY, handles.ScanInfo.ramp_y(i)];
                    end
            end
        end
       
        
        yyy=[];
        for  m = 1 : length(handles.FilteredPO2)
            yyy(m)= (handles.FilteredX(m) - handles.FilteredX(1))^2 + (handles.FilteredY(m) - handles.FilteredY(1))^2;
            yyy(m)=sqrt(yyy(m));
        end
        PO2Curve = figure;
        plot(yyy, handles.FilteredPO2);
        SmoothF = 4;
        hold on; plot(yyy, smooth(handles.FilteredPO2',SmoothF),'r'); hold off;
        print(PO2Curve,'-dtiff', [handles.Savefolder '/8_96/' num2str(FolderNumber(f)) '.tif']);
        close(PO2Curve);
%         
        AvgPO2_8_96(f) = mean(handles.FilteredPO2);
        if isempty(handles.FilteredPO2)
            Max_PO2_8_96(f)=NaN; Max_PO2_2_8_96(f)=NaN;
        else
            Max_PO2_8_96(f) = max(handles.FilteredPO2);
            Max_PO2_2_8_96(f) = max(smooth(handles.FilteredPO2,SmoothF));
        end
          
        
        % Saving the PMT/PO2 image
        PMTPO2Image = figure;
        hPMTPO2Image=imagesc(handles.PMTImage);  colormap gray; cbFITC = colorbar;
        set(gca, 'YTick', []);
        set(gca, 'XTick', []);
        colormap([gray(64);jet(64)]);
        maxFTC = max(handles.PMTImage(:));
        minFTC = min(handles.PMTImage(:));
        ncmp=64;
        c_FITC = min(ncmp,  round(   (ncmp-1).*(handles.PMTImage-minFTC)./(maxFTC-minFTC)  )+1  );

        set(hPMTPO2Image,'CDataMapping','Direct');
        set(hPMTPO2Image,'CData',c_FITC);
        figure(PMTPO2Image);
        caxis([min(c_FITC(:)) max(c_FITC(:))]);
        set(cbFITC,'YLim',[ncmp+1 2*ncmp]);

        cmap1=jet(64);
        CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
%         minPO2 = min(handles.FilteredPO2);
%         maxPO2 = max(handles.FilteredPO2);
        minPO2 = 0;
        maxPO2 = 160;
        
        cmapPO2idx = ones(length(handles.FilteredPO2),1); % vector of colormap indexes for pO2
        if maxPO2 == minPO2,
            cmapPO2idx = cmapPO2idx.*round(CmapN/2);
        else
            cmapPO2idx = round(   (  CmapN.*(handles.FilteredPO2-minPO2)+maxPO2-handles.FilteredPO2  )    ./(maxPO2-minPO2) );
            cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
            cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
        end;

        for i = 1: length(handles.FilteredPO2)
             colMap = squeeze(cmap1(cmapPO2idx(i),:));
             rectangle('Position',[(handles.FilteredX(i) - 7),(handles.FilteredY(i) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
             daspect ([1,1,1])
        end

        step = 20;         
        newTickLabels = step*ceil(minPO2/step)  : step  : step*floor(maxPO2/step);
        newTicks =( newTickLabels - (minPO2*ncmp-maxPO2)/(ncmp-1) ) .* (ncmp-1) ./ (maxPO2-minPO2);
        newTickLabelsStr=num2str(newTickLabels(1),'%i');
        for idxLab = 2:length(newTickLabels),
            newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
        end;
        pause(0);
        set(cbFITC,'YTick',newTicks+ncmp);
        set(cbFITC,'YTickLabel',newTickLabelsStr, 'fontsize',30);

        print(PMTPO2Image,'-dtiff', [handles.Savefolder '/8_96/PMTPO2Image' num2str(FolderNumber(f)) '.tif']);
       
       close(PMTPO2Image); 
        
        
        
                %% Calculating the pO2 values 12_96

        handles.NeglectTime = 12;
        handles.AcceptableR2 = 0.95;

        handles.PO2=[]; handles.R_square=[];
        for i = 1: length(handles.ScanInfo.ramp_x)
            handles.RepStart=1;
            handles.RepEnd=handles.ScanInfo.PO2Repitition * handles.ScanInfo.PointRepetition;

            if handles.ProbeBatch == 1
                if max(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2)) == min(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2))
                    handles.PO2(i) = NaN; handles.R_square(i)=NaN;
                else
                    [~, ~, handles.PO2(i), ~, ~, ~, handles.R_square(i)] = CalcPO2_2(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, handles.NeglectTime);
                end
            elseif handles.ProbeBatch == 2
                if max(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2)) == min(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2))
                    handles.PO2(i) = NaN; handles.R_square(i)=NaN;
                else
                    [~, ~, handles.PO2(i), ~, ~, ~, handles.R_square(i)] = CalcPO2_2015(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, handles.NeglectTime);
                end
            end
        end

        %% Filtering the points
        handles.FilteredPO2 = [];
        handles.FilteredX = [];
        handles.FilteredY = [];

        for i = 1: length(handles.PO2)
            if handles.PO2(i) > 0 && handles.PO2(i) < 250
                    if handles.R_square(i) > handles.AcceptableR2
                        handles.FilteredPO2 = [handles.FilteredPO2, handles.PO2(i)];
                        handles.FilteredX = [handles.FilteredX, handles.ScanInfo.ramp_x(i)];
                        handles.FilteredY = [handles.FilteredY, handles.ScanInfo.ramp_y(i)];
                    end
            end
        end
       
        
        yyy=[];
        for  m = 1 : length(handles.FilteredPO2)
            yyy(m)= (handles.FilteredX(m) - handles.FilteredX(1))^2 + (handles.FilteredY(m) - handles.FilteredY(1))^2;
            yyy(m)=sqrt(yyy(m));
        end
        PO2Curve = figure;
        plot(yyy, handles.FilteredPO2);
        SmoothF = 4;
        hold on; plot(yyy, smooth(handles.FilteredPO2',SmoothF),'r'); hold off;
        print(PO2Curve,'-dtiff', [handles.Savefolder '/12_96/' num2str(FolderNumber(f)) '.tif']);
        close(PO2Curve);
%         
        AvgPO2_12_96(f) = mean(handles.FilteredPO2);
        if isempty(handles.FilteredPO2)
            Max_PO2_12_96(f)=NaN; Max_PO2_2_12_96(f)=NaN;
        else
            Max_PO2_12_96(f) = max(handles.FilteredPO2);
            Max_PO2_2_12_96(f) = max(smooth(handles.FilteredPO2,SmoothF));
        end
          
        
        % Saving the PMT/PO2 image
        PMTPO2Image = figure;
        hPMTPO2Image=imagesc(handles.PMTImage);  colormap gray; cbFITC = colorbar;
        set(gca, 'YTick', []);
        set(gca, 'XTick', []);
        colormap([gray(64);jet(64)]);
        maxFTC = max(handles.PMTImage(:));
        minFTC = min(handles.PMTImage(:));
        ncmp=64;
        c_FITC = min(ncmp,  round(   (ncmp-1).*(handles.PMTImage-minFTC)./(maxFTC-minFTC)  )+1  );

        set(hPMTPO2Image,'CDataMapping','Direct');
        set(hPMTPO2Image,'CData',c_FITC);
        figure(PMTPO2Image);
        caxis([min(c_FITC(:)) max(c_FITC(:))]);
        set(cbFITC,'YLim',[ncmp+1 2*ncmp]);

        cmap1=jet(64);
        CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
%         minPO2 = min(handles.FilteredPO2);
%         maxPO2 = max(handles.FilteredPO2);
        minPO2 = 0;
        maxPO2 = 160;
        
        cmapPO2idx = ones(length(handles.FilteredPO2),1); % vector of colormap indexes for pO2
        if maxPO2 == minPO2,
            cmapPO2idx = cmapPO2idx.*round(CmapN/2);
        else
            cmapPO2idx = round(   (  CmapN.*(handles.FilteredPO2-minPO2)+maxPO2-handles.FilteredPO2  )    ./(maxPO2-minPO2) );
            cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
            cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
        end;

        for i = 1: length(handles.FilteredPO2)
             colMap = squeeze(cmap1(cmapPO2idx(i),:));
             rectangle('Position',[(handles.FilteredX(i) - 7),(handles.FilteredY(i) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
             daspect ([1,1,1])
        end

        step = 20;         
        newTickLabels = step*ceil(minPO2/step)  : step  : step*floor(maxPO2/step);
        newTicks =( newTickLabels - (minPO2*ncmp-maxPO2)/(ncmp-1) ) .* (ncmp-1) ./ (maxPO2-minPO2);
        newTickLabelsStr=num2str(newTickLabels(1),'%i');
        for idxLab = 2:length(newTickLabels),
            newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
        end;
        pause(0);
        set(cbFITC,'YTick',newTicks+ncmp);
        set(cbFITC,'YTickLabel',newTickLabelsStr, 'fontsize',30);

        print(PMTPO2Image,'-dtiff', [handles.Savefolder '/12_96/PMTPO2Image' num2str(FolderNumber(f)) '.tif']);
       
       close(PMTPO2Image);         
         
        
end

%1-3: x,y,x of imaging plane
%4-5: average x and y of pO2 points 
%6: AOM
%7: No of decays
%8: Depth
%9: Vessel Diameter (FWHM)
%10: Avg pO2
%11: Max pO2
%12: Max pO2 of smoothed curve
Summary.Results4_96(:,1:3)=Corodinates;
Summary.Results4_96(:,4:5)=[MeanPointX', MeanPointY'];
Summary.Results4_96(:,6)=AOM;
Summary.Results4_96(:,7)=NoOfDecays;
Summary.Results4_96(:,8)=Depth';
Summary.Results4_96(:,9)=VesselD';
Summary.Results4_96(:,10)=AvgPO2_4_96';
Summary.Results4_96(:,11)=Max_PO2_4_96';
Summary.Results4_96(:,12)=Max_PO2_2_4_96';

Summary.Results8_96(:,1:3)=Corodinates;
Summary.Results8_96(:,4:5)=[MeanPointX', MeanPointY'];
Summary.Results8_96(:,6)=AOM;
Summary.Results8_96(:,7)=NoOfDecays;
Summary.Results8_96(:,8)=Depth';
Summary.Results8_96(:,9)=VesselD';
Summary.Results8_96(:,10)=AvgPO2_8_96';
Summary.Results8_96(:,11)=Max_PO2_8_96';
Summary.Results8_96(:,12)=Max_PO2_2_8_96';

Summary.Results12_96(:,1:3)=Corodinates;
Summary.Results12_96(:,4:5)=[MeanPointX', MeanPointY'];
Summary.Results12_96(:,6)=AOM;
Summary.Results12_96(:,7)=NoOfDecays;
Summary.Results12_96(:,8)=Depth';
Summary.Results12_96(:,9)=VesselD';
Summary.Results12_96(:,10)=AvgPO2_12_96';
Summary.Results12_96(:,11)=Max_PO2_12_96';
Summary.Results12_96(:,12)=Max_PO2_2_12_96';

save([handles.Savefolder '/Summary.mat'],'Summary')

end