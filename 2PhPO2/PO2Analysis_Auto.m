function PO2Analysis_Auto(FolderNumber)

handles.ProbeBatch = 2; % 1: 2014 batch; 2: 2015 batch
handles.NeglectTime = 4;
handles.AcceptableR2 = 0.965;

handles.folder = uigetdir('C:\Users\moeini\Mohammad\Data\PO2 data\Awake 2\');
handles.Savefolder = uigetdir('C:\Users\moeini\Mohammad\Data\PO2 data\Awake 2\Analyzed data\');


for f = 1 : length(FolderNumber)

        %% Read Scan Info for analysis

        load([handles.folder '/PO2_' num2str(FolderNumber(f)) '/PO2ScanInfo_' num2str(FolderNumber(f)) '.mat']); %scan information
        handles.ScanInfo = scan;
        clear scan
        handles.scale = (handles.ScanInfo.scan_height/400);   
        handles.ScanInfo.ramp_y = handles.ScanInfo.ramp_y + 20 * handles.scale ; % correcting the shift between the pO2 grid coordinate and the pmt coordinate      

        load([handles.folder '/PO2_' num2str(FolderNumber(f)) '/image_pmt.mat']); % PMT live image 
        handles.PMTImage = image_ref_pmt;
        clear image_ref_pmt
         
       
        %% Calculating the pO2 values

        xx=[];yy=[]; handles.PO2=[]; handles.R_square=[]; handles.LifetimeVal=[]; Predicted_y=[];
        for i = 1: length(handles.ScanInfo.ramp_x)
            handles.RepStart=1;
            handles.RepEnd=handles.ScanInfo.PO2Repitition * handles.ScanInfo.PointRepetition;

            if handles.ProbeBatch == 1
                [p, ~, handles.PO2(i), ~, xx(:,i), yy(:,i), handles.R_square(i)] = CalcPO2_2(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, handles.NeglectTime);
            elseif handles.ProbeBatch == 2
                [p, ~, handles.PO2(i), ~, xx(:,i), yy(:,i), handles.R_square(i)] = CalcPO2_2015(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, handles.NeglectTime);
            end

            handles.LifetimeVal(i) = p(2);
            Predicted_y(:,i) = (p(1).*(exp(-xx(:,i)./p(2))))+p(3);

        end
        handles.xx = xx;
        handles.yy = yy;
        handles.Predicted_y =Predicted_y;


        %% Filtering the points
        handles.FilteredPO2 = [];
        handles.FilteredLifetime = [];
        handles.FilteredX = [];
        handles.FilteredY = [];

        for i = 1: length(handles.PO2)
            if handles.PO2(i) > 0 && handles.PO2(i) < 160
                    if handles.R_square(i) > handles.AcceptableR2
                        handles.FilteredPO2 = [handles.FilteredPO2, handles.PO2(i)];
                        handles.FilteredLifetime = [handles.FilteredLifetime, handles.LifetimeVal(i)];
                        handles.FilteredX = [handles.FilteredX, handles.ScanInfo.ramp_x(i)];
                        handles.FilteredY = [handles.FilteredY, handles.ScanInfo.ramp_y(i)];
                    end
            end
        end


        %% Saving the results

        Results = [];

        if isfield(handles.ScanInfo,'z_surface')
            Results.Depth =1000*(handles.ScanInfo.z_surface - handles.ScanInfo.zpos); %depth in um
        end
        Results.ImageDimention = num2str(handles.ScanInfo.scan_height);
        if isfield(handles.ScanInfo,'x_surface')
            Results.SurfaceX =  handles.ScanInfo.x_surface;
        end
        if isfield(handles.ScanInfo,'y_surface')
            Results.SurfaceY =  handles.ScanInfo.y_surface;
        end
        Results.PO2 = handles.FilteredPO2;
        Results.Lifetime = handles.FilteredLifetime;
        Results.PointX = handles.FilteredX;
        Results.PointY = handles.FilteredY;
        Results.AOM = handles.ScanInfo.aom_value;
        Results.RepStart = handles.RepStart;
        Results.RepEnd = handles.RepEnd;
        Results.NumHistPoints =handles.ScanInfo.PO2HistPoints;
        Results.NegTime = handles.NeglectTime;
        Results.AcceptedR2 = handles.AcceptableR2;
        Results.PulseDuration = handles.ScanInfo.PO2PulsePeriod;
        Results.PulseHighTime =handles.ScanInfo.PO2PulseHighTime;
        Results.xpos = handles.ScanInfo.xpos;
        Results.ypos = handles.ScanInfo.ypos;
        
        mkdir([handles.Savefolder '/' num2str(FolderNumber(f))])     
        save([handles.Savefolder '/' num2str(FolderNumber(f)) '/Results.mat'],'Results')
        copyfile([handles.folder '/PO2_' num2str(FolderNumber(f)) '/image_pmt.mat'],[handles.Savefolder '/' num2str(FolderNumber(f)) '/image_pmt.mat'],'f');

        % Saving the PMT image
        PMTImage = figure;
        imagesc(handles.PMTImage); colormap 'gray'
        set(gca, 'YTick', []);
        set(gca, 'XTick', []);
        print(PMTImage,'-dtiff', [handles.Savefolder '/' num2str(FolderNumber(f)) '/PMT.tif']);
        close(PMTImage);

        % Saving the PMT/PO2 image
        PMTPO2Image = figure;
        hPMTPO2Image=imagesc(handles.PMTImage);  colormap gray; cbFITC = colorbar;
        set(gca, 'YTick', []);
        set(gca, 'XTick', []);
        colormap([gray(64);jet(64)]);
        maxFTC = max(handles.PMTImage(:));
        minFTC = min(handles.PMTImage(:));
        ncmp=64;
        c_FITC = min(ncmp,  round(   (ncmp-1).*(handles.PMTImage-minFTC)./(maxFTC-minFTC)  )+1  );

        set(hPMTPO2Image,'CDataMapping','Direct');
        set(hPMTPO2Image,'CData',c_FITC);
        figure(PMTPO2Image);
        caxis([min(c_FITC(:)) max(c_FITC(:))]);
        set(cbFITC,'YLim',[ncmp+1 2*ncmp]);

        cmap1=jet(64);
        CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
        minPO2 = min(Results.PO2);
        maxPO2 = max(Results.PO2);
        cmapPO2idx = ones(length(Results.PO2),1); % vector of colormap indexes for pO2
        if maxPO2 == minPO2,
            cmapPO2idx = cmapPO2idx.*round(CmapN/2);
        else
            cmapPO2idx = round(   (  CmapN.*(Results.PO2-minPO2)+maxPO2-Results.PO2  )    ./(maxPO2-minPO2) );
            cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
            cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
        end;

        for i = 1: length(Results.PO2)
             colMap = squeeze(cmap1(cmapPO2idx(i),:));
             rectangle('Position',[(Results.PointX(i) - 7),(Results.PointY(i) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
             daspect ([1,1,1])
        end

        step = 10;         
        newTickLabels = step*ceil(minPO2/step)  : step  : step*floor(maxPO2/step);
        newTicks =( newTickLabels - (minPO2*ncmp-maxPO2)/(ncmp-1) ) .* (ncmp-1) ./ (maxPO2-minPO2);
        newTickLabelsStr=num2str(newTickLabels(1),'%i');
        for idxLab = 2:length(newTickLabels),
            newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
        end;
        pause(0);
        set(cbFITC,'YTick',newTicks+ncmp);
        set(cbFITC,'YTickLabel',newTickLabelsStr, 'fontsize',30);

        print(PMTPO2Image,'-dtiff', [handles.Savefolder '/' num2str(FolderNumber(f)) '/PMTPO2Image.tif']);
        close(PMTPO2Image);


        % Saving the PMT/PO2 image scale fixed 0-70 mmHg
        PMTPO2Image = figure;
        hPMTPO2Image=imagesc(handles.PMTImage);  colormap gray; cbFITC = colorbar;
        set(gca, 'YTick', []);
        set(gca, 'XTick', []);
        colormap([gray(64);jet(64)]);
        maxFTC = max(handles.PMTImage(:));
        minFTC = min(handles.PMTImage(:));
        ncmp=64;
        c_FITC = min(ncmp,  round(   (ncmp-1).*(handles.PMTImage-minFTC)./(maxFTC-minFTC)  )+1  );

        set(hPMTPO2Image,'CDataMapping','Direct');
        set(hPMTPO2Image,'CData',c_FITC);
        figure(PMTPO2Image);
        caxis([min(c_FITC(:)) max(c_FITC(:))]);
        set(cbFITC,'YLim',[ncmp+1 2*ncmp]);

        cmap1=jet(64);
        CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
        minPO2 = 0;
        % maxPO2 = 160;
        maxPO2 = 70;
        cmapPO2idx = ones(length(Results.PO2),1); % vector of colormap indexes for pO2
        if maxPO2 == minPO2,
            cmapPO2idx = cmapPO2idx.*round(CmapN/2);
        else
            cmapPO2idx = round(   (  CmapN.*(Results.PO2-minPO2)+maxPO2-Results.PO2  )    ./(maxPO2-minPO2) );
            cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
            cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
        end;

        for i = 1: length(Results.PO2)
             colMap = squeeze(cmap1(cmapPO2idx(i),:));
             rectangle('Position',[(Results.PointX(i) - 7),(Results.PointY(i) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
             daspect ([1,1,1])
        end

        step = 20;         
        newTickLabels = step*ceil(minPO2/step)  : step  : step*floor(maxPO2/step);
        newTicks =( newTickLabels - (minPO2*ncmp-maxPO2)/(ncmp-1) ) .* (ncmp-1) ./ (maxPO2-minPO2);
        newTickLabelsStr=num2str(newTickLabels(1),'%i');
        for idxLab = 2:length(newTickLabels),
            newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
        end;
        pause(0);
        set(cbFITC,'YTick',newTicks+ncmp);
        set(cbFITC,'YTickLabel',newTickLabelsStr, 'fontsize',30);

        % print(PMTPO2Image,'-dtiff', [handles.Savefolder '/PMTPO2Image0-160.tif']);
        print(PMTPO2Image,'-dtiff', [handles.Savefolder '/' num2str(FolderNumber(f)) '/PMTPO2Image0-60.tif']);
        close(PMTPO2Image);
end


end