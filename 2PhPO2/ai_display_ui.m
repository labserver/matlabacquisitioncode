function varargout = ai_display_ui(varargin)
% AI_DISPLAY_UI MATLAB code for ai_display_ui.fig
%      AI_DISPLAY_UI, by itself, creates a new AI_DISPLAY_UI or raises the existing
%      singleton*.
%
%      H = AI_DISPLAY_UI returns the handle to a new AI_DISPLAY_UI or the handle to
%      the existing singleton*.
%
%      AI_DISPLAY_UI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in AI_DISPLAY_UI.M with the given input arguments.
%
%      AI_DISPLAY_UI('Property','Value',...) creates a new AI_DISPLAY_UI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before ai_display_ui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to ai_display_ui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help ai_display_ui

% Last Modified by GUIDE v2.5 05-Dec-2013 15:40:34

addpath('..')
addpath('../SCANIMAGE_r3.8')

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @ai_display_ui_OpeningFcn, ...
                   'gui_OutputFcn',  @ai_display_ui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end

% End initialization code - DO NOT EDIT


% --- Executes just before ai_display_ui is made visible.
function ai_display_ui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to ai_display_ui (see VARARGIN)

import liom.*;
handles.save_dir=varargin{1}{1};
[handles.ai_config,device]=ai_config_ui;
channelIDs =cell2mat(handles.ai_config(:,1));
acq_freq=max(cell2mat(handles.ai_config(:,3)));
% Find ECG and RESP channel if there
pathname = uigetdir;
if ~exist([pathname '\' date])
mkdir([pathname '\'],date);
end
handles.path_name = [pathname '\' date];
handles.file_name = 'AA01E01';
if ~exist([handles.path_name '\' handles.file_name])
mkdir([handles.path_name '\'], handles.file_name);
end
handles.ai = ContAIClass(['Dev',num2str(device)],channelIDs);
handles.ai.config(acq_freq,'AIPhysioTask');
display_delegate = AIDisplayDelegate(handles,acq_freq);

for i=1:size(handles.ai_config,1)
    if (strcmp(cell2mat(handles.ai_config(i,2)),'ECG'))
        display_delegate.setECG(i);
    end
    if (strcmp(cell2mat(handles.ai_config(i,2)),'RESP'))
        display_delegate.setRESP(i);
    end    
end
display_delegate.setSaveData(0);
handles.ai.addlistener('haveData',@display_delegate.updategraph);

% Set popupmenu values
handles.channelList(1)=mod(0,size(handles.ai_config,1))+1;
handles.channelList(2)=mod(1,size(handles.ai_config,1))+1;
handles.channelList(3)=mod(2,size(handles.ai_config,1))+1;

set(handles.signal_popupmenu1,'String',handles.ai_config(:,2));
set(handles.signal_popupmenu1,'Value',handles.channelList(1));

set(handles.signal_popupmenu2,'String',handles.ai_config(:,2));
set(handles.signal_popupmenu2,'Value',handles.channelList(2));

set(handles.signal_popupmenu3,'String',handles.ai_config(:,2));
set(handles.signal_popupmenu3,'Value',handles.channelList(3));
display_delegate.setDisplayChannels(handles.channelList)

handles.display_delegate=display_delegate;
% Choose default command line output for ai_display_ui
handles.output = hObject;
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes ai_display_ui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = ai_display_ui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in signal_popupmenu1.
function signal_popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to signal_popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns signal_popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from signal_popupmenu1
%display_delegate.setDisplayChannels(obj,channelList)
handles.channelList(1)=get(hObject,'Value');
handles.display_delegate.setDisplayChannels(handles.channelList);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function signal_popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to signal_popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in signal_popupmenu2.
function signal_popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to signal_popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns signal_popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from signal_popupmenu2
handles.channelList(2)=get(hObject,'Value');
handles.display_delegate.setDisplayChannels(handles.channelList);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function signal_popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to signal_popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in signal_popupmenu3.
function signal_popupmenu3_Callback(hObject, eventdata, handles)
% hObject    handle to signal_popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns signal_popupmenu3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from signal_popupmenu3
handles.channelList(3)=get(hObject,'Value');
handles.display_delegate.setDisplayChannels(handles.channelList);
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function signal_popupmenu3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to signal_popupmenu3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in start_pushbutton.
function start_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to start_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.ai.start();

% --- Executes on button press in stop_pushbutton.
function stop_pushbutton_Callback(hObject, eventdata, handles)
% hObject    handle to stop_pushbutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% set(handles.display_delegate.hDisplay.file_name,'String',handles.file_name);
% set(path_name,'String',handles.file_name);
handles.display_delegate.file_name = handles.file_name;
handles.ai.stop();
handles.display_delegate.reset();


% --- Executes on button press in save_checkbox.
function save_checkbox_Callback(hObject, eventdata, handles)
% hObject    handle to save_checkbox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of save_checkbox
if( get(hObject,'Value') )
    handles.display_delegate.setSaveData(1);
else
    handles.display_delegate.setSaveData(0);
end


% --- Executes during object creation, after setting all properties.
function axes3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes3



function file_name_Callback(hObject, eventdata, handles)
% hObject    handle to file_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.file_name = get(hObject,'String');
if ~exist([handles.path_name '\' handles.file_name])
mkdir([handles.path_name '\'],handles.file_name);
end
guidata(hObject, handles);
% Hints: get(hObject,'String') returns contents of file_name as text
%        str2double(get(hObject,'String')) returns contents of file_name as a double


% --- Executes during object creation, after setting all properties.
function file_name_CreateFcn(hObject, eventdata, handles)
% hObject    handle to file_name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function bpm_text_CreateFcn(hObject, eventdata, handles)
% hObject    handle to bpm_text (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
