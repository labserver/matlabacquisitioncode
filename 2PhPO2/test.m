
addpath('..')
addpath('../SCANIMAGE_r3.8')
aom_pulse = [ones(1,12),zeros(1,108)];

%Generating a laser-pulse which will be used for triggering the
%TrigAOClass. The output is CTR0 OUT (PFI12)
laser_pulse=MohammadPulseClass(600/1000000.0,10,'Dev2',0);

%(1) Generating a pulse train for histogram measurements (number of pulses on each laser pulse equal to: PO2HistPoints) which is retriggered
%with each AOM pulse. It is generated through counter 2. The generated pulse train goes to CTR 2 OUT (PFI14)
%(2) Creating a counter (CTR 1) to count the pulses from PMT based on clock (generated at step (1)). The input of the counter clock is CTR 2 OUT (PFI14).
counter = liom.CounterClass(10,120,600/1000000.0);
ao=MohammadTrigAOClass('Dev2',2,'aomAOTask'); %AOM
ao.config(200000);
ao.write_signals(aom_pulse'); 
counter.start();
ao.start();
laser_pulse.start();
counter.read( );
laser_pulse.stop();% shouldn't be before ao.stop
ao.stop();
counter.stop(); 
delete(laser_pulse);
delete(ao);
delete(counter);
