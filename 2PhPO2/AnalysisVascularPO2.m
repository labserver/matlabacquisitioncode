function varargout = AnalysisVascularPO2(varargin)
% ANALYSISVASCULARPO2 MATLAB code for AnalysisVascularPO2.fig
%      ANALYSISVASCULARPO2, by itself, creates a new ANALYSISVASCULARPO2 or raises the existing
%      singleton*.
%
%      H = ANALYSISVASCULARPO2 returns the handle to a new ANALYSISVASCULARPO2 or the handle to
%      the existing singleton*.
%
%      ANALYSISVASCULARPO2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in ANALYSISVASCULARPO2.M with the given input arguments.
%
%      ANALYSISVASCULARPO2('Property','Value',...) creates a new ANALYSISVASCULARPO2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before AnalysisVascularPO2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to AnalysisVascularPO2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help AnalysisVascularPO2

% Last Modified by GUIDE v2.5 31-Oct-2016 18:40:11

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @AnalysisVascularPO2_OpeningFcn, ...
                   'gui_OutputFcn',  @AnalysisVascularPO2_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before AnalysisVascularPO2 is made visible.
function AnalysisVascularPO2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to AnalysisVascularPO2 (see VARARGIN)

% Choose default command line output for AnalysisVascularPO2
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes AnalysisVascularPO2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = AnalysisVascularPO2_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function slider_Angio_Callback(hObject, eventdata, handles)
% hObject    handle to slider_Angio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

%update display
axes(handles.axes_PMT)
imagesc(squeeze(handles.Angio(:,:,(int16(get(handles.slider_Angio,'Value'))+1)))); colormap 'gray'; hold on;
plot(handles.XPoint,handles.YPoint,'Marker','s'); hold off;
set(handles.txt_AngioSlider,'String', (int16(get(handles.slider_Angio,'Value'))+1));
set(handles.txt_AngioSliderDepth,'String', handles.AngioScanInfo.z_steps_3D*1000*(int16(get(handles.slider_Angio,'Value'))));




% Update handles structure
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function slider_Angio_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_Angio (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in LoadAngio_Button.
function LoadAngio_Button_Callback(hObject, eventdata, handles)
% hObject    handle to LoadAngio_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[fname pname] = uigetfile('*.tif','Select the Angio file','C:\Users\moeini\Mohammad\Data\PO2 data\');
load([pname 'VolumeScanInfo_1.mat']); 
handles.AngioScanInfo = scan;
clear scan

handles.XAngio = 1000*handles.AngioScanInfo.xpos;
handles.YAngio = 1000*handles.AngioScanInfo.ypos;
handles.ZAngioSurface =  1000*handles.AngioScanInfo.top_z;

set(handles.slider_Angio,'Max',handles.AngioScanInfo.slices_3D-1);
set(handles.slider_Angio, 'SliderStep', [1/(handles.AngioScanInfo.slices_3D-1) 1/(handles.AngioScanInfo.slices_3D-1)]);

scale = (handles.AngioScanInfo.scan_height/handles.AngioScanInfo.nx)

for i = 1 : handles.AngioScanInfo.slices_3D
    A = imread([pname fname], i);
    handles.Angio(:,:,i) = imresize(A,scale);
end

axes(handles.axes_PMT)
imagesc(handles.Angio(:,:,1)); colormap 'gray'

set(handles.txt_AngioSlider,'String', 1);
set(handles.txt_AngioSliderDepth,'String', 0);

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in LoadPoints_Button.
function LoadPoints_Button_Callback(hObject, eventdata, handles)
% hObject    handle to LoadPoints_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[fname pname] = uigetfile('*.mat','Select the scaninfo file','C:\Users\moeini\Mohammad\Data\PO2 data\');
load([pname fname]);  
handles.PointInfo = scan;
clear scan

handles.XPointPlane = 1000*handles.PointInfo.xpos;
handles.YPointPlane = 1000*handles.PointInfo.ypos;
handles.ZPointPlane = 1000*handles.PointInfo.zpos;

scale = (handles.PointInfo.scan_height/handles.PointInfo.nx)

handles.XPoint = scale*handles.PointInfo.ramp_x + 0.5*(handles.AngioScanInfo.scan_height - handles.PointInfo.scan_height) + (handles.YAngio - handles.YPointPlane);
handles.YPoint = scale*handles.PointInfo.ramp_y + 0.5*(handles.AngioScanInfo.scan_height - handles.PointInfo.scan_height) + (handles.XAngio - handles.XPointPlane);


% Update handles structure
guidata(hObject, handles);

