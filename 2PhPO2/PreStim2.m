function varargout = PreStim2(varargin)
% PRESTIM2 MATLAB code for PreStim2.fig
%      PRESTIM2, by itself, creates a new PRESTIM2 or raises the existing
%      singleton*.
%
%      H = PRESTIM2 returns the handle to a new PRESTIM2 or the handle to
%      the existing singleton*.
%
%      PRESTIM2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PRESTIM2.M with the given input arguments.
%
%      PRESTIM2('Property','Value',...) creates a new PRESTIM2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PreStim2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PreStim2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PreStim2

% Last Modified by GUIDE v2.5 18-Jan-2017 09:19:58

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PreStim2_OpeningFcn, ...
                   'gui_OutputFcn',  @PreStim2_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PreStim2 is made visible.
function PreStim2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PreStim2 (see VARARGIN)

% Choose default command line output for PreStim2
handles.output = hObject;

opengl software
handles.ProbeBatch = 2;
handles.NeglectTime = 4;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes PreStim2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = PreStim2_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Load_Button.
function Load_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Load_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

depths = []; MinX = []; MinY = []; handles.PMT = []; handles.XpO2 = []; handles.YpO2 = []; handles.pO2 = [];
handles.vein_x = []; handles.vein_y = []; handles.artery_x = []; handles.artery_y = [];

%% Read 2D scans
handles.folder = uigetdir('C:\Users\moeini\Desktop\Awake 2\Rest\','Select the directory of 2D grids');
NoOfScans = inputdlg('Enter the numbers for 2D scans: ');
NoOfScans = str2num(NoOfScans{1});

load([handles.folder '/Results' num2str(NoOfScans(1)) '.mat']); %pO2 scans
depths(1) = Results.Depth;
MinX = - Results.SurfaceX;
MinY = - Results.SurfaceY;
clear Results

for i = 2 : length(NoOfScans)
    load([handles.folder '/Results' num2str(NoOfScans(i)) '.mat']); %pO2 scans
    NoOfScans(i)
    depths(i) = Results.Depth;
    if - Results.SurfaceX < MinX
        MinX = - Results.SurfaceX;
    end
    if  - Results.SurfaceY < MinY
        MinY = - Results.SurfaceY;
    end
    clear Results
end
handles.UniqueDepths = unique(int16(depths));


%% Load ref points

[fname pname] = uigetfile('*.mat','Select .mat gradient file containing the ref points','C:\Users\moeini\Desktop\Awake 2\Rest\');
load([pname fname],'-mat'); 

handles.vein_x = Gradients.vein_x;
handles.vein_y = Gradients.vein_y;
handles.artery_x = Gradients.artery_x;
handles.artery_y = Gradients.artery_y;
clear Gradients


%% load stimulation pO2 data
[fname handles.pname] = uigetfile('*.mat','Select .mat PO2 stim results file','C:\Users\moeini\Mohammad\Data\PO2 data\Awake 2\');
load([handles.pname fname],'-mat'); 
handles.ScanInfo = scan;
clear scan
handles.scale = (handles.ScanInfo.scan_height/400);   
% handles.ScanInfo.ramp_y = handles.ScanInfo.ramp_y + 20 * handles.scale; % correcting the shift between the pO2 grid coordinate and the pmt coordinate      


        %% Calculating the pO2 values

        handles.pO2=[]; handles.R_square=[];
        for i = 1: length(handles.ScanInfo.ramp_x)
            handles.RepStart=1;
            handles.RepEnd=handles.ScanInfo.PO2Repitition * handles.ScanInfo.PointRepetition;

            if handles.ProbeBatch == 1
                [~, ~, handles.pO2(i), ~, ~, ~, handles.R_square(i)] = CalcPO2_2(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, handles.NeglectTime);
            elseif handles.ProbeBatch == 2
                [~, ~, handles.pO2(i), ~, ~, ~, handles.R_square(i)] = CalcPO2_2015(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, handles.NeglectTime);
            end
        end
        
        
%%
SurfaceX = - handles.ScanInfo.xpos - MinX;
SurfaceY = - handles.ScanInfo.ypos - MinY;
handles.scale = handles.ScanInfo.scan_height/400;
    
StimDepth = 1000*(handles.ScanInfo.z_surface - handles.ScanInfo.zpos) %depth in um

if isempty(find(handles.UniqueDepths == int16(StimDepth)))
    
    Indm = min(find(handles.UniqueDepths > int16(StimDepth)));
    for m = 1: size(handles.vein_x,1)
        if handles.vein_x(m,2) >= Indm
            handles.vein_x(m,2) = handles.vein_x(m,2)+1;
            handles.vein_y(m,2) = handles.vein_y(m,2)+1;
        end
    end
    for m = 1: size(handles.artery_x,1)
        if handles.artery_x(m,2) >= Indm
            handles.artery_x(m,2) = handles.artery_x(m,2)+1;
            handles.artery_y(m,2) = handles.artery_y(m,2)+1;
        end
    end
    
    depths = [depths StimDepth];
    handles.UniqueDepths = unique(int16(depths));   
    DepthIndex = find(handles.UniqueDepths==int16(StimDepth));
    load([handles.pname '/image_pmt.mat']); %PMT images  
    image_ref_pmt = image_ref_pmt / max(image_ref_pmt(:));
    image_ref_pmt = imadjust(image_ref_pmt);
    image_ref_pmt(1:round(handles.scale * 50),:) = min(image_ref_pmt(:)); % remove the mirror artefact of the PMT image
    
    handles.PMT(1+1000*SurfaceY : ceil(400*handles.scale)+1000*SurfaceY ,1+1000*SurfaceX + (1-handles.scale)*400 : ceil(399*handles.scale)+1000*SurfaceX + (1-handles.scale)*400,DepthIndex) = imresize(image_ref_pmt,handles.scale);
    
end

DepthIndex = find(handles.UniqueDepths==int16(StimDepth));
handles.XpO2 =  [handles.scale * handles.ScanInfo.ramp_x + 1000*SurfaceX + (1-handles.scale)*400 ; repmat(DepthIndex,1,length(handles.ScanInfo.ramp_x))];
handles.YpO2 =  [handles.scale * handles.ScanInfo.ramp_y + 1000*SurfaceY ; repmat(DepthIndex,1,length(handles.ScanInfo.ramp_y))];
handles.pO2 =  [handles.pO2; repmat(DepthIndex,1,length(handles.pO2))];

for i = 1 : length(NoOfScans)
        load([handles.folder '/Results' num2str(NoOfScans(i)) '.mat']); %pO2 scans
        load([handles.folder '/image_pmt' num2str(NoOfScans(i)) '.mat']); %PMT images

        image_ref_pmt = image_ref_pmt / max(image_ref_pmt(:));
        image_ref_pmt = imadjust(image_ref_pmt);

        SurfaceX = - Results.SurfaceX - MinX;
        SurfaceY = - Results.SurfaceY - MinY;
        DepthIndex = find(handles.UniqueDepths==int16(depths(i)));   
        handles.scale = (str2num(Results.ImageDimention)/400);

        image_ref_pmt(1:round(handles.scale * 50),:) = min(image_ref_pmt(:)); % remove the mirror artefact of the PMT image

        handles.PMT(1+1000*SurfaceY : ceil(400*handles.scale)+1000*SurfaceY ,1+1000*SurfaceX + (1-handles.scale)*400 : ceil(399*handles.scale)+1000*SurfaceX + (1-handles.scale)*400,DepthIndex) = imresize(image_ref_pmt,handles.scale);

        clear image_ref_pmt
        clear Results
end
    
set(handles.slider,'Max',length(handles.UniqueDepths)-1);
if length(handles.UniqueDepths) ~= 1
    set(handles.slider, 'SliderStep', [1/(length(handles.UniqueDepths)-1) 1/(length(handles.UniqueDepths)-1)]);
end
    
handles.OriginalXpO2 = handles.XpO2;
handles.OriginalYpO2 = handles.YpO2;     



%% Update display
cla(handles.LivePMTPO2)
axes(handles.LivePMTPO2)
imagesc(squeeze(handles.PMT(:,:,1))); colormap 'gray'
set(handles.slider,'Value',0)
set(handles.txt_slider,'String', handles.UniqueDepths(int16(get(handles.slider,'Value'))+1));
 % axis off;

% plotting the ref points
axes(handles.LivePMTPO2); hold on;
if length(handles.vein_x) ~ 0;
    PointIndices = find(handles.vein_x(:,2)==(int16(get(handles.slider,'Value'))+1));
    plot(handles.vein_x(PointIndices,1),handles.vein_y(PointIndices,1),'ob');
end
if length(handles.artery_x) ~ 0;
    PointIndices = find(handles.artery_x(:,2)==(int16(get(handles.slider,'Value'))+1));
    plot(handles.artery_x(PointIndices,1),handles.artery_y(PointIndices,1),'or');
end
hold off


% plotting the PO2 values
PO2Indices = find(handles.pO2(2,:)==1);
if ~isempty(PO2Indices)
    PO2Data = handles.pO2(1,PO2Indices);
    PO2X = handles.XpO2(1,PO2Indices);
    PO2Y = handles.YpO2(1,PO2Indices);
    ColorSegments = 50;
    color_value = jet(ColorSegments+1);
    Interval = max(PO2Data)/ColorSegments;

    for i = 1: length(PO2Data)
        ColorIndex = ceil(PO2Data(i)/Interval);
        if ColorIndex <=0
            ColorIndex=1;
        end
        ColorValue(i,:) = color_value(ColorIndex,:);
        plot(PO2X(i),PO2Y(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
    end
    hold off
    set(handles.LivePMTPO2, 'YTick', []);
    set(handles.LivePMTPO2, 'XTick', []);

    axes(handles.PO2_Colorbar)
    ylim([0 max(PO2Data)]);
    xlim([0 1]);
    hold on
    BarDevisions = 5;
    for i = 1: ColorSegments
        for j=1:BarDevisions
            line([0 1],[(i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions (i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
        end
    end
    hold off
    set(handles.PO2_Colorbar, 'YTick', linspace(0,max(PO2Data),5));
    set(handles.PO2_Colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(PO2Data),5)));  
end

% Update handles structure
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function Popup_AnalysisType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Popup_AnalysisType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in RefPoints_Veins.
function RefPoints_Veins_Callback(hObject, eventdata, handles)
% hObject    handle to RefPoints_Veins (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[vein_x, vein_y] = getpts(handles.LivePMTPO2);
handles.vein_x = [handles.vein_x; [vein_x repmat(int16(get(handles.slider,'Value'))+1,length(vein_x),1)]];
handles.vein_y = [handles.vein_y; [vein_y repmat(int16(get(handles.slider,'Value'))+1,length(vein_y),1)]];
axes(handles.LivePMTPO2); hold on;
plot(vein_x,vein_y,'ob')
hold off

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in PlotGradient_Button.
function PlotGradient_Button_Callback(hObject, eventdata, handles)
% hObject    handle to PlotGradient_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Closest vein
handles.SmallestVeinDistance=[];
if isempty(handles.vein_x) == 0
    for i = 1: size(handles.pO2,2)
        vein_distance = []; 
        for j = 1: size(handles.vein_x,1)
            y = (double(handles.XpO2(1,i)) - double(handles.vein_x(j,1)))^2 + (double(handles.YpO2(1,i)) - double(handles.vein_y(j,1)))^2 + double((handles.UniqueDepths(handles.XpO2(2,i)))-double(handles.UniqueDepths(handles.vein_x(j,2))))^2;
            vein_distance(j) = sqrt(y);
        end
        handles.SmallestVeinDistance(i) = min(vein_distance);
    end

    VeinGradientCurve = figure; hold on;
    plot(handles.SmallestVeinDistance,handles.pO2(1,:),'ob','MarkerSize',4)
    tredline = polyfit(handles.SmallestVeinDistance,handles.pO2(1,:),str2num(get(handles.PolyDeg_txt,'String')));
    x1 = linspace(min(handles.SmallestVeinDistance),max(handles.SmallestVeinDistance),100);
    y1 = polyval(tredline,x1);
    plot(x1,y1,'k','LineWidth',2)
    hold off
end

% Closest artery
handles.SmallestArteryDistance=[];
if isempty(handles.artery_x) == 0
    for i = 1: size(handles.pO2,2)
        artery_distance = []; 
        for j = 1: size(handles.artery_x,1)
            y = (double(handles.XpO2(1,i)) - double(handles.artery_x(j,1)))^2 + (double(handles.YpO2(1,i)) - double(handles.artery_y(j,1)))^2 + (double(handles.UniqueDepths(handles.XpO2(2,i)))-double(handles.UniqueDepths(handles.artery_x(j,2))))^2;
            artery_distance(j) = sqrt(y);
        end
        handles.SmallestArteryDistance(i) = min(artery_distance);
    end


    ArteryGradientCurve = figure; hold on;
    plot(handles.SmallestArteryDistance,handles.pO2(1,:),'or','MarkerSize',4)
    tredline = polyfit(handles.SmallestArteryDistance,handles.pO2(1,:),str2num(get(handles.PolyDeg_txt,'String')));
    x1 = linspace(min(handles.SmallestArteryDistance),max(handles.SmallestArteryDistance),100);
    y1 = polyval(tredline,x1);
    plot(x1,y1,'k','LineWidth',2)
    hold off
end

%%%% CMRO2 from all points

if isempty(handles.artery_x) == 0

    DistanceLimit_input = inputdlg('Distance limit (um):','How far from the vessel to consider for CMRO2? ',1,{num2str(max(handles.SmallestArteryDistance))});
    DistanceLimit = str2num(DistanceLimit_input{1});
       
    [handles.CMRO2, handles.CMRO2_x, handles.CMRO2_y, handles.CMRO2_xx, handles.CMRO2_FittedCurve] = FitCMRO2(handles.VesselDiameter, handles.SmallestArteryDistance, handles.pO2(1,:), DistanceLimit);
    
    set(handles.txt_CMRO2, 'String',  handles.CMRO2.CMRO2);
    
    VesselFig = figure;
    plot(handles.CMRO2_x,handles.CMRO2_y,'b.'); hold on;
    plot(handles.CMRO2_xx,handles.CMRO2_FittedCurve,'k','LineWidth',2);
    xlabel('distance (um)') % x-axis label
    ylabel('pO2 (mmHg)') % y-axis label
    hold off


end

% Update handles structure
guidata(hObject, handles);



% --- Executes on button press in Save_Button.
function Save_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Save_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isempty(handles.SmallestVeinDistance)
    handles.SmallestVeinDistance = nan(1,length(handles.pO2(1,:)));
end

if isempty(handles.SmallestArteryDistance)
    handles.SmallestArteryDistance = nan(1,length(handles.pO2(1,:)));
end

mkdir([handles.pname '\PreStim']);

PreStimResults = [];
PreStimResults.PO2 = handles.pO2(1,:);
PreStimResults.X = handles.XpO2(1,:);
PreStimResults.Y = handles.YpO2(1,:);
PreStimResults.Depth =1000*(handles.ScanInfo.z_surface - handles.ScanInfo.zpos); %depth in um
PreStimResults.Distances = handles.SmallestArteryDistance; % artery distances
PreStimResults.VeinDistances = handles.SmallestVeinDistance; % vein distances
PreStimResults.RefX = handles.artery_x; %artery ref points
PreStimResults.RefY = handles.artery_y; %artery ref points
PreStimResults.RefXVein = handles.vein_x; %vein ref points
PreStimResults.RefYVein = handles.vein_y; %vein ref points
PreStimResults.VesselDiameter = handles.VesselDiameter;

save([handles.pname '/PreStim/PreStimResults.mat'],'PreStimResults')

% Saving the PMT/PO2/Vessels image
for k = 1 : length(handles.UniqueDepths)
    PMTPO2Image = figure;
    hPMTPO2Image=imagesc(squeeze(handles.PMT(:,:,k)));  colormap gray; cbFITC = colorbar;
    set(gca, 'YTick', []);
    set(gca, 'XTick', []);
    colormap([gray(64);jet(64)]);
    PMTData = squeeze(handles.PMT(:,:,k));
    maxFTC = max(PMTData(:));
    minFTC = min(PMTData(:));
    ncmp=64;
    c_FITC = min(ncmp,  round(   (ncmp-1).*(squeeze(handles.PMT(:,:,k))-minFTC)./(maxFTC-minFTC)  )+1  );
    
    set(hPMTPO2Image,'CDataMapping','Direct');
    set(hPMTPO2Image,'CData',c_FITC);
    figure(PMTPO2Image);
    caxis([min(c_FITC(:)) max(c_FITC(:))]);
    set(cbFITC,'YLim',[ncmp+1 2*ncmp]);

    PO2Indices = find(handles.pO2(2,:)==k);
    if ~isempty(PO2Indices)
        PO2Data = handles.pO2(1,PO2Indices);
        PO2X = handles.XpO2(1,PO2Indices);
        PO2Y = handles.YpO2(1,PO2Indices);

        cmap1=jet(64);
        CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
        minPO2 = min(PO2Data);
        maxPO2 = max(PO2Data);
        cmapPO2idx = ones(length(PO2Data),1); % vector of colormap indexes for pO2
        if maxPO2 == minPO2,
            cmapPO2idx = cmapPO2idx.*round(CmapN/2);
        else
            cmapPO2idx = round((CmapN.*(PO2Data-minPO2)+maxPO2-PO2Data)./(maxPO2-minPO2) );
            cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
            cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
        end;

        for i = 1: length(PO2Data)
             colMap = squeeze(cmap1(cmapPO2idx(i),:));
             rectangle('Position',[(PO2X(i) - 7),(PO2Y(i) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
             daspect ([1,1,1])
        end
        % hold on;
        % plot(handles.reference_x,handles.reference_y,'o','MarkerSize',10,'MarkerFaceColor','k')
        % hold off

        step = 10;         
        newTickLabels = step*ceil(minPO2/step)  : step  : step*floor(maxPO2/step);
        newTicks =( newTickLabels - (minPO2*ncmp-maxPO2)/(ncmp-1) ) .* (ncmp-1) ./ (maxPO2-minPO2);
        newTickLabelsStr=num2str(newTickLabels(1),'%i');
        for idxLab = 2:length(newTickLabels),
            newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
        end;
        pause(0);
        set(cbFITC,'YTick',newTicks+ncmp);
        set(cbFITC,'YTickLabel',newTickLabelsStr,'fontsize',30);
    end
    
    hold on;
    if length(handles.vein_x) ~= 0;
        PointIndices = find(handles.vein_x(:,2)== k);
        plot(handles.vein_x(PointIndices,1),handles.vein_y(PointIndices,1),'ob');
    end
    if length(handles.artery_x) ~= 0;
        PointIndices = find(handles.artery_x(:,2)== k);
        plot(handles.artery_x(PointIndices,1),handles.artery_y(PointIndices,1),'or');
    end
    hold off
     
    print(PMTPO2Image,'-dtiff', [handles.pname '/PreStim/PMTPO2VesselsImage'  num2str(k)  '.tif']);
    close(PMTPO2Image);
end




function PolyDeg_txt_Callback(hObject, eventdata, handles)
% hObject    handle to PolyDeg_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PolyDeg_txt as text
%        str2double(get(hObject,'String')) returns contents of PolyDeg_txt as a double


% --- Executes during object creation, after setting all properties.
function PolyDeg_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PolyDeg_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function XOffset_txt_Callback(hObject, eventdata, handles)
% hObject    handle to XOffset_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of XOffset_txt as text
%        str2double(get(hObject,'String')) returns contents of XOffset_txt as a double
% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function XOffset_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to XOffset_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function YOffset_txt_Callback(hObject, eventdata, handles)
% hObject    handle to YOffset_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of YOffset_txt as text
%        str2double(get(hObject,'String')) returns contents of YOffset_txt as a double
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function YOffset_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YOffset_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Update_button.
function Update_button_Callback(hObject, eventdata, handles)
% hObject    handle to Update_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.slider,'Value',1);
handles.XpO2(1,:) =  handles.OriginalXpO2(1,:) + str2num(get(handles.XOffset_txt,'String')); %
handles.YpO2(1,:) =  handles.OriginalYpO2(1,:) + str2num(get(handles.YOffset_txt,'String')); %

% Update display
cla(handles.LivePMTPO2)
axes(handles.LivePMTPO2)
imagesc(squeeze(handles.PMT(:,:,1))); colormap 'gray'
set(handles.slider,'Value',0)
set(handles.txt_slider,'String', handles.UniqueDepths(int16(get(handles.slider,'Value'))+1));
 % axis off;

% plotting the PO2 values
ColorSegments = 50;
color_value = jet(ColorSegments+1);
PO2Indices = find(handles.pO2(2,:)==1);
PO2Data = handles.pO2(1,PO2Indices);
PO2X = handles.XpO2(1,PO2Indices);
PO2Y = handles.YpO2(1,PO2Indices);
Interval = max(PO2Data)/ColorSegments;
 
hold on
for i = 1: length(PO2Data)
    ColorIndex = ceil(PO2Data(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(PO2X(i),PO2Y(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
end
hold off
set(handles.LivePMTPO2, 'YTick', []);
set(handles.LivePMTPO2, 'XTick', []);

axes(handles.PO2_Colorbar)
ylim([0 max(PO2Data)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions (i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off
set(handles.PO2_Colorbar, 'YTick', linspace(0,max(PO2Data),5));
set(handles.PO2_Colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(PO2Data),5)));  

% Update handles structure
guidata(hObject, handles);


% --- Executes on slider movement.
function slider_Callback(hObject, eventdata, handles)
% hObject    handle to slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% Update display
cla(handles.LivePMTPO2)
cla(handles.PO2_Colorbar)
axes(handles.LivePMTPO2)
imagesc(squeeze(handles.PMT(:,:,(int16(get(handles.slider,'Value'))+1)))); colormap 'gray'
set(handles.txt_slider,'String', handles.UniqueDepths(int16(get(handles.slider,'Value'))+1));
set(handles.txt_slider,'String', handles.UniqueDepths(int16(get(handles.slider,'Value'))+1));
 % axis off;

% plotting the ref ponts
 axes(handles.LivePMTPO2); hold on;
if length(handles.vein_x) ~ 0;
    PointIndices = find(handles.vein_x(:,2)==(int16(get(handles.slider,'Value'))+1));
    plot(handles.vein_x(PointIndices,1),handles.vein_y(PointIndices,1),'ob');
end
if length(handles.artery_x) ~ 0;
    PointIndices = find(handles.artery_x(:,2)==(int16(get(handles.slider,'Value'))+1));
    plot(handles.artery_x(PointIndices,1),handles.artery_y(PointIndices,1),'or');
end
hold off



% plotting the PO2 values
PO2Indices = find(handles.pO2(2,:)==(int16(get(handles.slider,'Value'))+1));
if ~isempty(PO2Indices)
    PO2Data = handles.pO2(1,PO2Indices);
    PO2X = handles.XpO2(1,PO2Indices);
    PO2Y = handles.YpO2(1,PO2Indices);
    ColorSegments = 50;
    color_value = jet(ColorSegments+1);
    Interval = max(PO2Data)/ColorSegments;

    hold on
    for i = 1: length(PO2Data)
        ColorIndex = ceil(PO2Data(i)/Interval);
        if ColorIndex <=0
            ColorIndex=1;
        end
        ColorValue(i,:) = color_value(ColorIndex,:);
        plot(PO2X(i),PO2Y(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
    end
    hold off
    set(handles.LivePMTPO2, 'YTick', []);
    set(handles.LivePMTPO2, 'XTick', []);

    axes(handles.PO2_Colorbar)
    ylim([0 max(PO2Data)]);
    xlim([0 1]);
    hold on
    BarDevisions = 5;
    for i = 1: ColorSegments
        for j=1:BarDevisions
            line([0 1],[(i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions (i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
        end
    end
    hold off
    set(handles.PO2_Colorbar, 'YTick', linspace(0,max(PO2Data),5));
    set(handles.PO2_Colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(PO2Data),5)));  
end


% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in RefPoints_Arteries.
function RefPoints_Arteries_Callback(hObject, eventdata, handles)
% hObject    handle to RefPoints_Arteries (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[artery_x, artery_y] = getpts(handles.LivePMTPO2);
handles.artery_x = [handles.artery_x; [artery_x repmat(int16(get(handles.slider,'Value'))+1,length(artery_x),1)]];
handles.artery_y = [handles.artery_y; [artery_y repmat(int16(get(handles.slider,'Value'))+1,length(artery_y),1)]];
axes(handles.LivePMTPO2); hold on;
plot(artery_x,artery_y,'or')
hold off

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in button_zoom.
function button_zoom_Callback(hObject, eventdata, handles)
% hObject    handle to button_zoom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

axes(handles.LivePMTPO2);
zoom

% Update handles structure
guidata(hObject, handles);



% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% % % axes(handles.LivePMTPO2);
% % % c = improfile;
% % % figure; plot(c)

[handles.line_x, handles.line_y] = getline(handles.LivePMTPO2);
handles.VesselDiameter = sqrt((handles.line_x(2)-handles.line_x(1))^2+(handles.line_y(2)-handles.line_y(1))^2);
% handles.VesselDiameter = handles.VesselDiameter * handles.scale;
set(handles.VesselDiameter_txt, 'String',  handles.VesselDiameter);

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in RemoveRegion_Button.
function RemoveRegion_Button_Callback(hObject, eventdata, handles)
% hObject    handle to RemoveRegion_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

axes(handles.LivePMTPO2);
BW = roipoly;
ROI = find(BW == 1);
x= ceil(ROI / size(BW,1));
y= mod(ROI , size(BW,1) );

for i = 1: length(handles.XpO2)
    d=[];
    if handles.XpO2(2,i) == int16(get(handles.slider,'Value'))+1
        for j = 1: length(x)
            d(j) = (double(handles.XpO2(1,i)) - double(x(j)))^2 + (double(handles.YpO2(1,i)) - double(y(j)))^2;
            d(j) = sqrt(d(j));
        end
        distance(i) = min(d);
    else
        distance(i) = 100;
    end
end

Inds = find(distance <= 2);

handles.pO2(:,Inds)=[];
handles.XpO2(:,Inds)=[];
handles.YpO2(:,Inds)=[];
handles.OriginalXpO2(:,Inds)=[];
handles.OriginalYpO2(:,Inds)=[];


% Update display
cla(handles.LivePMTPO2)
axes(handles.LivePMTPO2)
imagesc(squeeze(handles.PMT(:,:,1))); colormap 'gray'
set(handles.slider,'Value',0)
set(handles.txt_slider,'String', handles.UniqueDepths(int16(get(handles.slider,'Value'))+1));
 % axis off;

% plotting the PO2 values
PO2Indices = find(handles.pO2(2,:)==1);
if ~isempty(PO2Indices)
    PO2Data = handles.pO2(1,PO2Indices);
    PO2X = handles.XpO2(1,PO2Indices);
    PO2Y = handles.YpO2(1,PO2Indices);
    ColorSegments = 50;
    color_value = jet(ColorSegments+1);
    Interval = max(PO2Data)/ColorSegments;

    hold on
    for i = 1: length(PO2Data)
        ColorIndex = ceil(PO2Data(i)/Interval);
        if ColorIndex <=0
            ColorIndex=1;
        end
        ColorValue(i,:) = color_value(ColorIndex,:);
        plot(PO2X(i),PO2Y(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
    end
    hold off
    set(handles.LivePMTPO2, 'YTick', []);
    set(handles.LivePMTPO2, 'XTick', []);

    axes(handles.PO2_Colorbar)
    ylim([0 max(PO2Data)]);
    xlim([0 1]);
    hold on
    BarDevisions = 5;
    for i = 1: ColorSegments
        for j=1:BarDevisions
            line([0 1],[(i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions (i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
        end
    end
    hold off
    set(handles.PO2_Colorbar, 'YTick', linspace(0,max(PO2Data),5));
    set(handles.PO2_Colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(PO2Data),5)));  
end

% Update handles structure
guidata(hObject, handles);
