function varargout = TwoPhPO2_v2(varargin)
% TWOPHPO2_V2 M-file for TwoPhPO2_v2.fig

%      TWOPHPO2_V2, by itself, creates a new TWOPHPO2_V2 or raises the
%      existing singleton*.
%
%      H = TWOPHPO2_V2 returns the handle to a new TWOPHPO2_V2 or the
%      handle to the existing singleton*.
%
%      TWOPHPO2_V2('CALLBACK',hObject,eventData,handles,...) calls the
%      local function named CALLBACK in TWOPHPO2_V2.M with the given input
%      arguments.
%
%      TWOPHPO2_V2('Property','Value',...) creates a new TWOPHPO2_V2 or
%      raises the existing singleton*.  Starting from the left, property
%      value pairs are applied to the GUI before TwoPhPO2_v2_OpeningFcn
%      gets called.  An unrecognized property name or invalid value makes
%      property application stop.  All inputs are passed to
%      TwoPhPO2_v2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help TwoPhPO2_v2

% Last Modified by GUIDE v2.5 11-Nov-2015 17:11:25

% Begin initialization code - DO NOT EDIT

addpath('..')
addpath('../SCANIMAGE_r3.8')


gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @TwoPhPO2_v2_OpeningFcn, ...
    'gui_OutputFcn',  @TwoPhPO2_v2_OutputFcn, ...
    'gui_LayoutFcn',  [] , ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before TwoPhPO2_v2 is made visible.
function TwoPhPO2_v2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn. hObject    handle to
% figure eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA) varargin
% command line arguments to TwoPhPO2_v2 (see VARARGIN)

clc;

handles.save = 0;
handles.vol_scan_num = 0;
handles.line_scan_num = 0;
handles.live_scan_num = 0;
handles.PO2_scan_num = 0;
handles.PO2_survey_num = 0;
handles.scan.binning = 1;
handles.scan.experiment_type = 'Rest-ISO';
handles.IntegrateOnlyDecay = 0;
handles.PlotDecayCurves = 0;





handles.Syn_point = dabs.ni.daqmx.Task('Syn_point');%points for synchronization with stimulation
handles.Syn_point.createDOChan('Dev3','port0/line0');
    
    
    
    
    
set(handles.loop_scan,'Enable','off');

% System setup: Connect AO start trigger to scope trigger...
% May need to change for continuous mode

system = dabs.ni.daqmx.System();
system.connectTerms('/Dev1/ao/StartTrigger','/Dev1/PFI1');
handles.td = liom.TDClass();
%% SHOW IMAGE
bkg_image=imread('mouse.jpg');
axes(handles.pmt_image);
axis off;
imshow(bkg_image);
handles.channel = get(handles.PMT_image_popup,'Value');
colormap gray;

set(handles.galvo, 'XTick', []);
set(handles.galvo, 'YTick', []);

set(handles.PO2colorbar, 'XTick', []);
set(handles.PO2colorbar, 'YTick', []);

set(handles.pmt_image, 'YTick', []);
set(handles.pmt_image, 'XTick', []);

set(handles.image, 'YTick', []);
set(handles.image, 'XTick', []);

set(handles.lifetime_image, 'YTick', []);
set(handles.lifetime_image, 'XTick', []);

set(handles.line_image, 'YTick', []);
set(handles.line_image, 'XTick', []);

%% SAVE DIRECTORY
[handles.folder_name,handles.live_scan_num,handles.line_scan_num,handles.vol_scan_num, handles.PO2_scan_num] = FindDirectory();%check

handles.output = hObject;
handles.chosen_folder =0;

%% Choose default command line output for TwoPhPO2_v2
handles.shutter = liom.ShutterClass('Dev1','port0/line28');
handles.shutter_open = 0;

%% IMAGE SCAN
%sample points
handles.scan.nx= str2double(get(handles.nx,'String'));
handles.scan.ny= str2double(get(handles.ny,'String'));
handles.polynomial_drop = [];
handles.scan.shift = str2double(get(handles.shift,'String'));
%image size in micro meters
handles.scan.scan_height= str2double(get(handles.height,'String'));
handles.scan.scan_width= str2double(get(handles.width,'String'));
handles.scan3D=0;
handles.interupt = 0;% flag for interupting 3D scan
handles.scan.zpos = 0;
handles.scan.xpos = 0 ;
handles.scan.ypos = 0 ;

%% Voltage
handles.scan.volt_x = double(handles.scan.scan_width/(100*2)); %unit = volts (for 1v = 58 um) ~ 2.79 v
handles.scan.volt_y = double(handles.scan.scan_height/(100*2)); %unit = volts (for 1v = 58 um) ~ 2.79 v
handles.scan.ramp_type = 1;

%% MOTOR
handles.z_motor_steps = str2double(get(handles.z_motor_step,'String'))/1000;
handles.xy_motor_steps = str2double(get(handles.xy_motor_step,'String'))/1000;
handles.motors = liom.MotorsClass('COM3');
[x,y,z]=handles.motors.get_pos();
set(handles.live_x,'String',num2str(x));
set(handles.live_y,'String',num2str(y));
set(handles.live_z,'String',num2str(z));

%% AOM vs Z PLOT
xlabel(handles.interp_plot,'Z(um)','fontsize',7.5);
ylabel(handles.interp_plot,'AOM(% volt)','fontsize',7.5);
set(handles.interp_plot,'LooseInset',get(handles.interp_plot,'TightInset'))

%handles.scan.top_z = z; %$
handles.scan.slices_3D = str2double(get(handles.z_slices,'String'));
handles.scan.z_steps_3D = str2double(get(handles.z_step_size,'String'))/1000; % this is for loop scan, the system reads mm, so this is equivalent to 2 um
%set(handles.minZ,'String',(handles.scan.top_z - handles.scan.slices_3D*handles.scan.z_steps_3D)); %you can not go lower than number of slices*z_steps from where you start your interpolation!

%% AOM
handles.scan.aom_value = 0;
handles.aom_task=liom.OnDemandAOClass('Dev1',2,'OnDemandAOMAOTask');
handles.aom_task.write(0);

%% Scan Positions
handles.data =[];

%% LINES POINTS ON IMAGE
handles.scan.LineRamp = 1;
handles.line_handles = []; % this handle is made so that we could delete the lines from screen with reset button
handles.point_handles =[]; % this handle is made so that we could delete the points from screen with reset button
handles.scan.histog = [];

handles.Exist_points = 0;
handles.scan.segment_handles=[];
handles.scan.number_points_per_segment = [];

handles.line_pos_handles=[];
handles.scan.repeat_line_scan = str2double(get(handles.repeat_scan,'String'));
handles.lineScan = 0; %M: it means line_scan is OFF
handles.scan.sum_lines_points = 0;

%% LINES POINTS PO2
handles.point_PO2_handles = []; % this handle is made so that we could delete the points from screen with reset button
handles.line_PO2_handles= [];
handles.TwoDGrid_PO2_handles=[];
handles.scan.PO2Repitition = str2double(get(handles.text_Repetition_PO2,'String'));
handles.scan.PO2HistPoints = str2double(get(handles.text_HistPoints_PO2,'String'));
handles.scan.PO2PulsePeriod = str2double(get(handles.text_PulsePeriod,'String'));
handles.scan.PO2PulseHighTime = str2double(get(handles.text_PulseHighTime,'String'));
handles.scan.PointRepetition = str2double(get(handles.txt_PointRep,'String'));
handles.PointsOnGraph = 0;

%% RAMPS
handles.scan.ramp_x = [];
handles.scan.ramp_y = [];

%% TABLE
set(handles.interp_table,'Data',[]);

%% LINE FREQ, put 100Hz, this will lead to 4Hz image rate in a 400x400 acquisition
handles.scan.line_frequency_live = str2double(get(handles.line_freq_live,'String'));
handles.scan.line_frequency_3D = str2double(get(handles.line_freq_3D,'String'));
handles.scan.line_frequency_line = str2double(get(handles.line_freq_line,'String'));

set(handles.line_per_sec_live,'String',num2str( handles.scan.line_frequency_live));% line (nx) per frequency (Hz)
set(handles.frame_per_sec_live,'String',num2str( handles.scan.line_frequency_live / handles.scan.ny ));%frame (nx*ny) per frequency(Hz)

set(handles.line_per_sec_3D,'String',num2str( handles.scan.line_frequency_3D));% line (nx) per frequency (Hz)
set(handles.frame_per_sec_3D,'String',num2str( handles.scan.line_frequency_3D / handles.scan.ny));%frame (nx*ny) per frequency(Hz)

set(handles.line_per_sec_line,'String',num2str(handles.scan.line_frequency_line));
set(handles.frame_per_sec_line,'String',num2str(handles.scan.line_frequency_line/handles.scan.repeat_line_scan));

handles.scan.shift_linescan = str2double(get(handles.shift_lineScan,'String'));


guidata(hObject, handles);
% UIWAIT makes TwoPhPO2_v2 wait for user response (see UIRESUME)
% uiwait(handles.figure1); --- Outputs from this function are returned to
% the command line.
%%
function varargout = TwoPhPO2_v2_OutputFcn(hObject, eventdata, handles)
varargout{1} = handles.output;

%% Scan Dimentions
function ny_Callback(hObject, eventdata, handles)

handles.scan.ny=str2double(get(hObject,'String'));
guidata(hObject, handles);
function nx_Callback(hObject, eventdata, handles)
handles.scan.nx=str2double(get(hObject,'String'));
set(handles.frame_per_sec_live,'String',num2str( handles.scan.line_frequency_live / handles.scan.nx ));%frame (nx*ny) per frequency(Hz)
set(handles.frame_per_sec_3D,'String',num2str( handles.scan.line_frequency_3D / handles.scan.nx));%frame (nx*ny) per frequency(Hz)
guidata(hObject, handles);
function height_Callback(hObject, eventdata, handles)

handles.scan.scan_height=str2double(get(hObject,'String'));
handles.scan.volt_y = (handles.scan.scan_height)/(58*2);
guidata(hObject, handles);
function width_Callback(hObject, eventdata, handles)
handles.scan.scan_width=str2double(get(hObject,'String'));
handles.scan.volt_x = (handles.scan.scan_width)/(58*2);

guidata(hObject, handles);

%% SHUTTER
function Shutter_checkbox_Callback(hObject, eventdata, handles)

if( handles.shutter_open )
    handles.shutter.close();
    handles.shutter_open = 0;
    set(handles.current,'String','Shutter is closed');
else
    handles.shutter.open();
    handles.shutter_open = 1;
    set(handles.current,'String','Shutter is opened');
end
guidata(hObject, handles);

%% Saving the x,y, and z position of the brain surface (to obtain the imaging depth or moving the motors to the surface)
function button_brainsurface_Callback(hObject, eventdata, handles)
% hObject    handle to button_brainsurface (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.select_brainsurface = get(hObject,'Value');
if handles.select_brainsurface
    set(hObject,'BackgroundColor','g');
    [handles.scan.x_surface,handles.scan.y_surface,handles.scan.z_surface]=handles.motors.get_pos();
else
    set(hObject,'BackgroundColor','r');
end
guidata(hObject, handles);

%% MOTOR CONTROL
function home_motor_Callback(hObject, eventdata, handles)

handles.motor_home = get(handles.home_motor,'Value');
if (handles.motor_home)
    set(handles.current,'String','Motors are ON');
    handles.motors.home();
    [x,y,z]=handles.motors.get_pos();
    set(handles.live_x,'String',num2str(x));
    set(handles.live_y,'String',num2str(y));
    set(handles.live_z,'String',num2str(z));
    
end
set(handles.current,'String','Motors are HOME');
guidata(hObject, handles);

function GoToBrainSurface_Callback(hObject, eventdata, handles)
% hObject    handle to GoToBrainSurface (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if(handles.select_brainsurface)
handles.motors.move_az(handles.scan.z_surface)
handles.motors.move_ax(handles.scan.x_surface)
handles.motors.move_ay(handles.scan.y_surface)
[x,y,z]=handles.motors.get_pos();
    set(handles.live_x,'String',num2str(x));
    set(handles.live_y,'String',num2str(y));
    set(handles.live_z,'String',num2str(z));
    set(handles.current,'String','Motors Are at Brain Surface Coordinates');

else
    set(handles.current,'String','Record Brain Surface Coordinates First');
end
guidata(hObject, handles);

function xy_motor_step_Callback(hObject, eventdata, handles)
handles.xy_motor_steps = str2double(get(hObject,'String'))/1000;
guidata(hObject, handles);
function up_Callback(hObject, eventdata, handles)
handles.motors.move_dy(handles.xy_motor_steps);
[~,y,~]=handles.motors.get_pos();
set(handles.live_x,'String',num2str(y));
guidata(hObject, handles);
function down_Callback(hObject, eventdata, handles)
handles.motors.move_dy(-handles.xy_motor_steps)
[~,y,~]=handles.motors.get_pos();
set(handles.live_x,'String',num2str(y));
guidata(hObject, handles);
function right_Callback(hObject, eventdata, handles)
handles.motors.move_dx(-handles.xy_motor_steps)
[x,~,~]=handles.motors.get_pos();
set(handles.live_y,'String',num2str(x));
guidata(hObject, handles);
function left_Callback(hObject, eventdata, handles)
handles.motors.move_dx(handles.xy_motor_steps)
[x,~,~]=handles.motors.get_pos();
set(handles.live_y,'String',num2str(x));
guidata(hObject, handles);

function z_motor_step_Callback(hObject, eventdata, handles)

handles.z_motor_steps = str2double(get(hObject,'String'))/1000; % user inputs 200 um, which is 0.2 mm
guidata(hObject, handles);
function z_raise_Callback(hObject, eventdata, handles)

% We should never go over 2mm, need to recheck since it changes
z_top_limit = 5;
[~,~,z]=handles.motors.get_pos();
if( z + handles.z_motor_steps < z_top_limit )
    handles.motors.move_dz(handles.z_motor_steps);
    [~,~,z]=handles.motors.get_pos();
    set(handles.live_z,'String',num2str(z));
    guidata(hObject, handles);
else
    msgbox('Trying to move z-stage over limit!','Careful','warn')
end
function z_desend_Callback(hObject, eventdata, handles)

handles.motors.move_dz(-handles.z_motor_steps)
[~,~,z]=handles.motors.get_pos();
set(handles.live_z,'String',num2str(z));
guidata(hObject, handles);

%% AOM
function aom_slider_Callback(hObject, eventdata, handles)

% AOM Slider is between 0-100%, need to translate to 0-2Volts
handles.scan.aom_value = get(hObject,'Value');
set(handles.aom_text,'String',handles.scan.aom_value);
handles.aom_task.write(handles.scan.aom_value/50);
guidata(hObject, handles);
function add_aom_Callback(hObject, eventdata, handles)

current_aom_value = handles.scan.aom_value; %$
[~,~,z]=handles.motors.get_pos();

% Add row to Table
table_data = get(handles.interp_table,'Data');
% Which row number does the new row get
next_row = size(table_data,1)+1;
if next_row == 1
    % Data is converted the first time, just empty
    table_data = num2cell([]);
end
table_data{next_row,1} = current_aom_value;
table_data{next_row,2} = z;
set(handles.interp_table,'Data',table_data)

guidata(hObject, handles);
function interpolate_Callback(hObject, eventdata, handles)
table_data = get(handles.interp_table, 'Data');
z_interp_points = cell2mat(table_data(:,2));
aom_interp_points=cell2mat(table_data(:,1));
% Assuming all scans start from highest value
handles.scan.top_z = max(z_interp_points);
bottom_z = handles.scan.top_z - handles.scan.slices_3D*handles.scan.z_steps_3D;%$
% watch out for the step size and number of slice. you don't want to go
% over the minimum z
z_interp = linspace(handles.scan.top_z,bottom_z,handles.scan.slices_3D);

handles.aom_interp = interp1(z_interp_points,aom_interp_points,z_interp,'linear',max(aom_interp_points));%$aom_interp didn't need it
depth=linspace(0,handles.scan.slices_3D*handles.scan.z_steps_3D,handles.scan.slices_3D);
plot(handles.interp_plot,depth,handles.aom_interp);
xlabel(handles.interp_plot,'Z(um)','fontsize',7);
ylabel(handles.interp_plot,'AOM(% volt)','fontsize',7.5);
set(handles.interp_plot,'LooseInset',get(handles.interp_plot,'TightInset'))
set(handles.loop_scan,'Enable','on');

guidata(hObject, handles);
function reset_aom_Callback(hObject, eventdata, handles)
set(handles.interp_table,'Data',[]);
cla(handles.interp_plot,'reset');
xlabel(handles.interp_plot,'Z(um)','fontsize',7.5);
ylabel(handles.interp_plot,'AOM(% volt)','fontsize',7.5);
set(handles.interp_plot,'LooseInset',get(handles.interp_plot,'TightInset'))
hold off
set(handles.loop_scan,'Enable','off');
guidata(hObject, handles);

%% IMAGE DISPLAY
function PMT_image_popup_Callback(hObject, eventdata, handles)

switch get(handles.PMT_image_popup,'Value')
    case 1
        handles.channel = 1;
    case 2
        handles.channel = 2;
    case 3
        handles.channel = 3;
end

guidata(hObject, handles);
function ramp_type_popupmenu_Callback(hObject, eventdata, handles)
handles.scan.ramp_type = get(hObject,'Value');
guidata(hObject, handles);

%% LIVE SCAN
function line_freq_live_Callback(hObject, eventdata, handles)
handles.scan.line_frequency_live = str2double(get(handles.line_freq_live,'String'));

% Assuming fast axis is always in x direction
set(handles.line_per_sec_live,'String',num2str(handles.scan.line_frequency_live));
set(handles.frame_per_sec_live,'String',num2str( handles.scan.line_frequency_live / handles.scan.ny));

guidata(hObject, handles);
function shift_Callback(hObject, eventdata, handles)
handles.scan.shift = str2double(get(hObject,'String'));
guidata(hObject, handles);
function extra_points_live_Callback(hObject, eventdata, handles)
function live_scan_start_Callback(hObject, eventdata, handles)

cla(handles.PO2colorbar)
set(handles.PO2colorbar, 'YTick', [])

handles.scan.volt_x = double(handles.scan.scan_height/(58*2)); %unit = volts (for 1v = 58 um) ~ 2.79 v
handles.scan.volt_y = double(handles.scan.scan_width/(58*2)); %unit = volts (for 1v = 58 um) ~ 2.79 v

if(handles.save)
    
    handles.live_scan_num = handles.live_scan_num+1;
    scan = handles.scan;
    mkdir(fullfile(handles.folder_name,['Live_',num2str(handles.live_scan_num)]));
    save(fullfile(handles.folder_name,['Live_',num2str(handles.live_scan_num)],[['LiveScanInfo_',num2str(handles.live_scan_num)],'.mat']),'scan');
    
end

handles.shutter.open();
%reset_lines_Callback(hObject, eventdata, handles);
n_extra_points =str2double(get(handles.extra_points_live,'String'));

switch(handles.scan.ramp_type)
    % First type of ramp is the sawtooth, standard ramp but slow since the
    % galvos take a long time to get down to zero
    case 1
        
        poly_ramp = liom.get_poly_return2(-handles.scan.volt_x, handles.scan.volt_x, handles.scan.nx, ...
            -handles.scan.volt_x, handles.scan.volt_x, handles.scan.nx, n_extra_points );
        ramp_x = [linspace(-handles.scan.volt_x,handles.scan.volt_x,handles.scan.nx),poly_ramp];
        handles.pos_x = repmat(ramp_x,[1 handles.scan.ny]);
        handles.pos_y = repmat(linspace(-handles.scan.volt_y,handles.scan.volt_y,handles.scan.ny),[length(ramp_x),1]);
        handles.data =[handles.pos_y(:),handles.pos_x(:)];
        total_size=(handles.scan.nx+n_extra_points)*handles.scan.ny;
        handles.freq_galvos=(handles.scan.nx+n_extra_points)* handles.scan.line_frequency_live;
        
        % Second type of ramp is the triangle, faster but need to reform the
        % image for every line
    case 2
        poly_ramp_top = liom.get_poly_return2(-handles.scan.volt_x, handles.scan.volt_x, handles.scan.nx, ...
            handles.scan.volt_x, -handles.scan.volt_x, handles.scan.nx, n_extra_points );
        poly_ramp_bot = liom.get_poly_return2(handles.scan.volt_x, -handles.scan.volt_x, handles.scan.nx, ...
            -handles.scan.volt_x, handles.scan.volt_x, handles.scan.nx, n_extra_points );
        
        handles.pos_x = repmat([poly_ramp_bot,linspace(-handles.scan.volt_x,handles.scan.volt_x,handles.scan.nx),...
            poly_ramp_top,linspace(handles.scan.volt_x,-handles.scan.volt_x,handles.scan.nx)],[1 handles.scan.ny/2]);
        
        handles.pos_y = repmat(linspace(-handles.scan.volt_y,handles.scan.volt_y,handles.scan.ny),[(handles.scan.nx+n_extra_points),1]);
        handles.data =[handles.pos_y(:),handles.pos_x(:)];
        total_size=(handles.scan.nx+n_extra_points)*handles.scan.ny;
        handles.freq_galvos=(handles.scan.nx+n_extra_points)*handles.scan.line_frequency_live;
        
    case 3 %UNDER CONSTRUCTION
        
        
end

% Ramp generation and image display setup

handles.scope=liom.ScopeClass(handles.freq_galvos,total_size,handles.scan.binning);
handles.scope.initiate();
handles.ao=liom.TrigAOClass('Dev1',0:1,'GalvoAOTask');
handles.ao.config(handles.freq_galvos);
display_delegate = LiveDisplayClass2(handles,total_size,n_extra_points);
handles.ao.addlistener('AODone',@display_delegate.acq_done);

%Write signals and move galvos
handles.ao.write_signals(handles.data);
handles.ao.start();
set(handles.current,'String','Live scan is ON');
guidata(hObject, handles);
function live_scan_stop_Callback(hObject, eventdata, handles)
handles.ao.stop();
delete(handles.ao);
handles.shutter.close();
if (handles.save)
    answer = questdlg('Do you want to save this scan?','Save','yes','no','yes');
    if strcmp(answer,'no')
        fullfile(handles.folder_name,['Live_',num2str(handles.live_scan_num)])
        rmdir(fullfile(handles.folder_name,['Live_',num2str(handles.live_scan_num)]),'s');
        handles.live_scan_num = handles.live_scan_num -1;
    end
end
set(handles.current,'String','Live scan/Shutter is OFF');
guidata(hObject, handles);

%% SAVE and BINNING
function save_Callback(hObject, eventdata, handles)
button_state = get(hObject,'Value');
if button_state == get(hObject,'Max')
    handles.save = 1;
elseif button_state == get(hObject,'Min')
    handles.save = 0;
end
guidata(hObject, handles);
function bin_popupmenu_Callback(hObject, eventdata, handles)
contents = cellstr(get(hObject,'String'));
handles.scan.binning= str2double(contents{get(hObject,'Value')});
guidata(hObject, handles);

%% 3D SCAN
function line_freq_3D_Callback(hObject, eventdata, handles)
handles.scan.line_frequency_3D = str2double(get(handles.line_freq_3D,'String'));
set(handles.line_per_sec_3D,'String',num2str(handles.scan.line_frequency_3D ));
set(handles.frame_per_sec_3D,'String',num2str(handles.scan.line_frequency_3D/handles.scan.ny ));
guidata(hObject, handles);
function z_slices_Callback(hObject, eventdata, handles) %number of slices in 3D scan
handles.scan.slices_3D = str2double(get(handles.z_slices,'String'));%$
guidata(hObject, handles);
function z_step_size_Callback(hObject, eventdata, handles)
handles.scan.z_steps_3D = str2double(get(handles.z_step_size,'String'))/1000;
guidata(hObject, handles); % loop scan
function extra_points_3D_Callback(hObject, eventdata, handles) %the value is read in loop scan directly
function loop_scan_Callback(hObject, eventdata, handles)

  handles.motors.move_az(handles.scan.top_z);
  [~,~,z]=handles.motors.get_pos();
  set(handles.live_z,'String',num2str(z));

set(handles.ramp_type_popupmenu,'Enable','off');

if(handles.save)
    
    handles.vol_scan_num = handles.vol_scan_num+1;
    % Force update of handles so that saving works
    guidata(hObject, handles);
    scan = handles.scan;
    mkdir(fullfile(handles.folder_name,['Volume_',num2str(handles.vol_scan_num)]));
    save(fullfile(handles.folder_name,['Volume_',num2str(handles.vol_scan_num)],[['VolumeScanInfo_',num2str(handles.vol_scan_num)],'.mat']),'scan');
    
end
handles.scan3D=1;
% Configure galvo
handles.shutter.open();%should check for if shutter is already open or not
%handles.ao=TrigAOClass('Dev1',0:1,'GalvoAOTask');
n_extra_points= str2double(get(handles.extra_points_3D,'String'));

switch(handles.scan.ramp_type)
    % First type of ramp is the sawtooth, standard ramp but slow since the
    % galvos take a long time to get down to zero
    case 1
        
        poly_ramp = liom.get_poly_return2(-handles.scan.volt_x, handles.scan.volt_x, handles.scan.nx, ...
            -handles.scan.volt_x, handles.scan.volt_x, handles.scan.nx, n_extra_points );
        ramp_x = [linspace(-handles.scan.volt_x,handles.scan.volt_x,handles.scan.nx),poly_ramp];
        handles.pos_x = repmat(ramp_x,[1 handles.scan.ny]);
        handles.pos_y = repmat(linspace(-handles.scan.volt_y,handles.scan.volt_y,handles.scan.ny),[length(ramp_x),1]);
        handles.data =[handles.pos_y(:),handles.pos_x(:)];
        total_size=(handles.scan.nx+n_extra_points)*handles.scan.ny;
        handles.freq_galvos=(handles.scan.nx+n_extra_points)*handles.scan.line_frequency_3D;
        
        % Second type of ramp is the triangle, faster but need to reform the
        % image for every line
    case 2
        poly_ramp_top = liom.get_poly_return2(-handles.scan.volt_x, handles.scan.volt_x, handles.scan.nx, ...
            handles.scan.volt_x, -handles.scan.volt_x, handles.scan.nx, n_extra_points );
        poly_ramp_bot = liom.get_poly_return2(handles.scan.volt_x, -handles.scan.volt_x, handles.scan.nx, ...
            -handles.scan.volt_x, handles.scan.volt_x, handles.scan.nx, n_extra_points );
        
        handles.pos_x = repmat([poly_ramp_bot,linspace(-handles.scan.volt_x,handles.scan.volt_x,handles.scan.nx),...
            poly_ramp_top,linspace(handles.scan.volt_x,-handles.scan.volt_x,handles.scan.nx)],[1 handles.scan.ny/2]);
        
        handles.pos_y = repmat(linspace(-handles.scan.volt_y,handles.scan.volt_y,handles.scan.ny),[(handles.scan.nx+n_extra_points),1]);
        handles.data =[handles.pos_y(:),handles.pos_x(:)];
        total_size=(handles.scan.nx+n_extra_points)*handles.scan.ny;
        handles.freq_galvos=(handles.scan.nx+n_extra_points)*handles.scan.line_frequency_3D;
        
        % Third type is spiral scans, for future analyses
    case 3
        disp('Not implemented yet')
end

handles.scope=liom.ScopeClass(handles.freq_galvos,total_size,handles.scan.binning);
handles.scope.initiate();
handles.ao=liom.TrigAOClass('Dev1',0:1,'GalvoAOTask');
handles.ao.config(handles.freq_galvos);
display_delegate = LiveDisplayClass2(handles,total_size,n_extra_points);
handles.ao.addlistener('AODone',@display_delegate.acq_done);

%**** This is how much time it takes to take one image with the current
% parameters
image_acq_time = total_size / handles.freq_galvos;

for i=1:handles.scan.slices_3D
    handles.interupt = get(handles.interupt_3D,'Value');
    
    if(handles.interupt)
        handles.interupt =0;
        set(handles.ramp_type_popupmenu,'Enable','on');

        break;
    else
        % Adjust power with AOM, handles.aom_interp is slider value between
        % 0-100%
        set(handles.current,'String',i);
        handles.aom_task.write(handles.aom_interp(i)/50);
        
        %Write signals and move galvos
        handles.ao.write_signals(handles.data);
        handles.ao.start();
        
        %Wait approx time to generate image + 0.1 sec
        pause(image_acq_time + 0.1);
        
        %move motor to lower position and repeat
        handles.motors.move_dz(-str2double(get(handles.z_step_size,'String'))/1000); % in mm %$
        handles.scope.initiate();
    end
end

set(handles.current,'String','Loop Scan Is Done or Interupted');
handles.ao.stop();
delete(handles.ao);
handles.shutter.close();
handles.scan3D=0;
if (handles.save)
    answer = questdlg('Do you want to save this scan?','Save','yes','no','yes');
    if strcmp(answer,'no')
        fullfile(handles.folder_name,['Volume_',num2str(handles.vol_scan_num)])
        rmdir(fullfile(handles.folder_name,['Volume_',num2str(handles.vol_scan_num)]),'s');
        handles.vol_scan_num = handles.vol_scan_num -1;
    end
end
set(handles.ramp_type_popupmenu,'Enable','on');

guidata(hObject, handles);
function interupt_3D_Callback(hObject, eventdata, handles)

button_state = get(hObject,'Value');
if button_state == get(hObject,'Max')
    handles.interupt = 1;
elseif button_state == get(hObject,'Min')
    handles.interupt = 0;
end
guidata(hObject, handles);

%% LINE/POINT SCAN
function line_freq_line_Callback(hObject, eventdata, handles)
handles.scan.line_frequency_line = str2double(get(handles.line_freq_line,'String'));
set(handles.line_per_sec_line,'String',num2str( handles.scan.line_frequency_line ));
set(handles.frame_per_sec_line,'String',num2str( handles.scan.line_frequency_line / handles.scan.repeat_line_scan ));
guidata(hObject, handles);
function linescan_type_Callback(hObject, eventdata, handles)
handles.scan.LineRamp = get(hObject,'Value');
guidata(hObject, handles);
function Lines_button_Callback(hObject, eventdata, handles)
set(handles.current,'String','Draw your line');
axes(handles.pmt_image);
current_line_handle = imline();
set(current_line_handle, 'UserData',1);
% Here we only keep the handles to the line, we will get the positions at
% the end so that the user can move the line

handles.scan.segment_handles = [handles.scan.segment_handles, {current_line_handle}];
handles.line_handles = [handles.line_handles, current_line_handle];
prompt = 'Number of points per line?';
set(handles.current,'String','Enter NUMBER of points per line');
handles.scan.number_points_per_segment = [handles.scan.number_points_per_segment, str2double((cell2mat(inputdlg(prompt))))];%bug:user input should be limited to int only
set(handles.current,'String','Click "Lines" to draw another line or click "Start Line Scan" when ready');
guidata(hObject, handles);
function reset_lines_Callback(hObject, eventdata, handles)
set(handles.current,'String','Draw Lines Again');
handles.scan.number_points_per_segment = [];
delete(handles.line_handles);
handles.Exist_points = 0;
delete(handles.point_handles);
handles.scan.segment_handles= [];
handles.scan.sum_lines_points = [];
handles.scan.histog = [];
delete(handles.line_PO2_handles);
delete(handles.point_PO2_handles);
guidata(hObject, handles);
function repeat_scan_Callback(hObject, eventdata, handles)
handles.scan.repeat_line_scan = str2double(get(handles.repeat_scan,'String'));
guidata(hObject, handles);
function extra_points_line_Callback(hObject, eventdata, handles) % the value is read in line_scan directly.
function point_Callback(hObject, eventdata, handles)
% hObject    handle to point (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.current,'String','select points');
axes(handles.pmt_image);
handles.Exist_points = 1;
current_point_handle = impoint();
set(current_point_handle,'UserData',0);
handles.scan.segment_handles = [handles.scan.segment_handles, {current_point_handle}];
handles.point_handles = [handles.point_handles, current_point_handle];

prompt = 'time spent scanning the point and counting photons?'; %the input should be converted to exta points
set(handles.current,'String','Enter the time at which the galvo will stay on the point');
handles.scan.number_points_per_segment = [handles.scan.number_points_per_segment, str2double((cell2mat(inputdlg(prompt))))];
set(handles.current,'String','Click "Points" to select another line or click "Start Line Scan" when ready');

guidata(hObject, handles);
function shift_lineScan_Callback(hObject, eventdata, handles)
handles.scan.shift_linescan = str2double(get(hObject,'String'));
guidata(hObject, handles);
function line_scan_start_Callback(hObject, eventdata, handles)

set(handles.live_scan_stop,'Enable','off');
set(handles.live_scan_start,'Enable','off');

[x,y,z]=handles.motors.get_pos();
handles.scan.zpos = z ;
handles.scan.xpos = x ;
handles.scan.ypos = y ;

positions=[];
handles.counting_pulse = 0;
handles.lineScan = 1;
n_extra_points = str2double(get(handles.extra_points_line,'String'));



handles.ao=liom.TrigAOClass('Dev1',0:1,'GalvoAOTask');
if(handles.Exist_points)
    handles.gate = dabs.ni.daqmx.Task('Syn_gate');
    handles.gate.createDOChan('Dev1','port0/line3','','DAQmx_Val_ChanPerLine');
end

% Count segments (either a point or a line)
n_segments = length(handles.scan.segment_handles);
handles.scan.sum_lines_points = sum(handles.scan.number_points_per_segment)+n_segments*n_extra_points;
total_size = handles.scan.sum_lines_points * handles.scan.repeat_line_scan;
% output number of points per second that galvo moves and takes data in.
freq_galvos= handles.scan.sum_lines_points*handles.scan.line_frequency_line;

ramp_x=[];
ramp_y=[];
gate_signal=[];


% Problem now is that we need to know the reference in which the lines were
% drawn
% Do the ramp for a single line, the line moves in both dimensions
switch(handles.scan.LineRamp)
    
    case 1 %triangle
        
        for i=1:length(handles.scan.segment_handles)
            if (get(handles.scan.segment_handles{i},'UserData'))
                % This is a line
                line_positions = getPosition(handles.scan.segment_handles{i});
                positions = [positions,line_positions];
                gate_signal = [gate_signal, repmat(zeros(1,n_extra_points+handles.scan.number_points_per_segment(i)),[1,round(handles.scan.repeat_line_scan/n_segments)])];
                
            else
                % This is a point
                point_positions = getPosition(handles.scan.segment_handles{i});
                double_the_point = [point_positions;point_positions];
                positions = [positions,double_the_point];
                gate_signal = [gate_signal, repmat(ones(1,n_extra_points+handles.scan.number_points_per_segment(i)),[1,round(handles.scan.repeat_line_scan/n_segments)])];
            end
        end
        
        handles.pos_x = [];
        handles.pos_y = [];
        
        for i = 1:n_segments
            curr_line = positions(1:2,(2*i)-1:(2*i));
            % x
            poly_ramp_top = liom.get_poly_return2(curr_line(1,1), curr_line(2,1), handles.scan.number_points_per_segment(i), ...
                curr_line(2,1), curr_line(1,1), handles.scan.number_points_per_segment(i), ...
                n_extra_points );
            poly_ramp_bot = liom.get_poly_return2(curr_line(2,1), curr_line(1,1), handles.scan.number_points_per_segment(i), ...
                curr_line(1,1), curr_line(2,1), handles.scan.number_points_per_segment(i), ...
                n_extra_points );
            ramp_x=[poly_ramp_bot,linspace(curr_line(1,1),curr_line(2,1),handles.scan.number_points_per_segment(i)),...
                poly_ramp_top,linspace(curr_line(2,1),curr_line(1,1),handles.scan.number_points_per_segment(i)),];
            
            handles.pos_x = [handles.pos_x, repmat(ramp_x,[1 round(handles.scan.repeat_line_scan/(2*n_segments))])];
            % y
            poly_ramp_top = liom.get_poly_return2(curr_line(1,2), curr_line(2,2), handles.scan.number_points_per_segment(i), ...
                curr_line(2,2), curr_line(1,2), handles.scan.number_points_per_segment(i), ...
                n_extra_points );
            
            poly_ramp_bot = liom.get_poly_return2(curr_line(2,2), curr_line(1,2),  handles.scan.number_points_per_segment(i), ...
                curr_line(1,2), curr_line(2,2), handles.scan.number_points_per_segment(i), ...
                n_extra_points );
            
            ramp_y = [poly_ramp_bot,linspace(curr_line(1,2),curr_line(2,2),handles.scan.number_points_per_segment(i)),...
                poly_ramp_top,linspace(curr_line(2,2),curr_line(1,2),handles.scan.number_points_per_segment(i)) ];
            handles.pos_y = [handles.pos_y, repmat(ramp_y,[1 round(handles.scan.repeat_line_scan/(2*n_segments))])];
            
        end
        gate_signal = gate_signal';
        
        frame_time =length(gate_signal)/freq_galvos; % time it takes to create one frame
        laser_pulse = 80*10^6 *frame_time; %how many laser pulses are in a single frame
        handles.counting_pulse = laser_pulse *(numel(find(gate_signal))/length(gate_signal)); %how many pulses we count when we gate_signal is 1
        
        
        
        
    case 2 %sawtooth
        
        for i=1:length(handles.scan.segment_handles)
            if (get(handles.scan.segment_handles{i},'UserData'))
                % This is a line
                line_positions = getPosition(handles.scan.segment_handles{i});
                positions = [positions,line_positions];
                gate_signal = [gate_signal, zeros(1,n_extra_points),zeros(1,handles.scan.number_points_per_segment(i))];
                
            else
                % This is a point
                point_positions = getPosition(handles.scan.segment_handles{i});
                double_the_point = [point_positions;point_positions];
                positions = [positions,double_the_point];
                gate_signal = [gate_signal,zeros(1,n_extra_points),ones(1,handles.scan.number_points_per_segment(i))];
            end
        end
        
        
        prev_line = positions(end-1:end,end-1:end);%get the positoin of the last line/point coordinates (2x2 matrix)
        prev_number_of_points = handles.scan.number_points_per_segment(n_segments); %get the number points per line/point of last line
        
        for i=1:n_segments
            curr_line = positions(1:2,(2*i)-1:(2*i));
            % Build polynomial ramp to start of line with equal end points and
            % slopes
            poly_ramp = liom.get_poly_return2(prev_line(1,1), prev_line(2,1), prev_number_of_points, ...
                curr_line(1,1), curr_line(2,1), handles.scan.number_points_per_segment(i), ...
                n_extra_points );
            
            ramp_x=[ramp_x,poly_ramp];
            ramp_x=[ramp_x,linspace(curr_line(1,1),curr_line(2,1),handles.scan.number_points_per_segment(i))];
            
            % Do the actual ramp for imaging of the i-th line, x1,y1
            % Move from previous line to this one using the extra ramp points set
            % Build polynomial ramp to start of line with equal end points and
            % slopes
            poly_ramp = liom.get_poly_return2(prev_line(1,2), prev_line(2,2), prev_number_of_points, ...
                curr_line(1,2), curr_line(2,2), handles.scan.number_points_per_segment(i), ...
                n_extra_points );
            
            ramp_y=[ramp_y,poly_ramp];
            % Do the actual ramp for imaging of the i-th line, x1,y1
            ramp_y=[ramp_y,linspace(curr_line(1,2),curr_line(2,2),handles.scan.number_points_per_segment(i))];
            
            prev_line = curr_line;
            prev_number_of_points=handles.scan.number_points_per_segment(i);
        end
        % Now repmat this line to generate the image
        handles.pos_x = repmat(ramp_x,[1 handles.scan.repeat_line_scan]);
        handles.pos_y = repmat(ramp_y,[1 handles.scan.repeat_line_scan]);
        gate_signal=repmat(gate_signal,[1 handles.scan.repeat_line_scan])';
        
        
        frame_time =length(gate_signal) /freq_galvos; %time it takes to create one frame
        laser_pulse = 80*10^6 *frame_time; %how many laser pulses are in a single frame
        handles.counting_pulse = laser_pulse *(numel(find(gate_signal))/length(gate_signal)); %how many pulses we count when we gate_signal is 1
        
end
handles.scan.n_extra_points = n_extra_points;
handles.scan.ramp_x = ramp_x;
handles.scan.ramp_y = ramp_y;

handles.scope=liom.ScopeClass(freq_galvos,total_size,handles.scan.binning);
handles.scope.initiate();
handles.ao.config(freq_galvos);

% calculating photon count rate during lifetime measurement

% Now need to scale from the pixel values returned by imline to voltage
% values: We assume that we are drawing on the latest image, so nx and ny
% as well as voltx and volty are valid.
handles.pos_x = (handles.pos_x-(handles.scan.nx/2))*(handles.scan.volt_x*2)/(handles.scan.nx);
handles.pos_y = (handles.pos_y -(handles.scan.ny/2))* (handles.scan.volt_y*2)/(handles.scan.ny);
handles.data =[handles.pos_x(:),handles.pos_y(:)];

handles.shutter.open();

if (handles.Exist_points)
    handles.gate.cfgSampClkTiming(freq_galvos,'DAQmx_Val_FiniteSamps',size(gate_signal,1));
    handles.gate.cfgDigEdgeStartTrig('/Dev1/ao/StartTrigger');
    handles.gate.set('startTrigRetriggerable',1)
    handles.gate.writeDigitalData(gate_signal, inf,false,size(gate_signal,1));
    handles.gate.start();
    handles.td.Tacq=length(gate_signal)/freq_galvos*1000+200;
    handles.td.initialize();
    handles.td.start_meas();
end

if(handles.save)
    handles.line_scan_num = handles.line_scan_num+1;
    scan =handles.scan;
    mkdir(fullfile(handles.folder_name,['Line_',num2str(handles.line_scan_num)]));
    save(fullfile(handles.folder_name,['Line_',num2str(handles.line_scan_num)],[['LineScanInfo_',num2str(handles.line_scan_num)],'.mat']),'scan');
    image_ref_pmt = getimage(handles.pmt_image);
    image_ref_adp = getimage(handles.image);
    save(fullfile(handles.folder_name,['Line_',num2str(handles.line_scan_num)],'image_pmt.mat'),'image_ref_pmt');
    save(fullfile(handles.folder_name,['Line_',num2str(handles.line_scan_num)],'image_adp.mat'),'image_ref_adp');

end
display_delegate = LiveDisplayClass2(handles,total_size,n_extra_points);
handles.ao.addlistener('AODone',@display_delegate.acq_done);
set(handles.current,'String','Scanning segments');
handles.ao.write_signals(handles.data);
handles.ao.start();


guidata(hObject, handles);
function line_scan_stop_Callback(hObject, eventdata, handles)
set(handles.live_scan_start,'Enable','ON');
set(handles.live_scan_stop, 'Enable','ON');
handles.lineScan = 0; % it means line_scan is OFF
handles.ao.stop();
if (handles.Exist_points)
    handles.gate.abort();
    delete(handles.gate);
end
delete(handles.ao);

handles.shutter.close();
set(handles.ramp_type_popupmenu,'Enable','on');
if (handles.save)
    answer = questdlg('Do you want to save this scan?','Save','yes','no','yes');
    if strcmp(answer,'no')
        fullfile(handles.folder_name,['Line_',num2str(handles.line_scan_num)])
        rmdir(fullfile(handles.folder_name,['Line_',num2str(handles.line_scan_num)]),'s');
        handles.line_scan_num = handles.line_scan_num -1;
    end
end
set(handles.current,'String','Line scan/Shutter is OFF');
guidata(hObject, handles);


%% PHOTON COUNTING
function text_Repetition_PO2_Callback(hObject, eventdata, handles)
% hObject    handle to text_Repetition_PO2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.scan.PO2Repitition = str2double(get(handles.text_Repetition_PO2,'String'));
guidata(hObject, handles);

function text_HistPoints_PO2_Callback(hObject, eventdata, handles)
% hObject    handle to text_HistPoints_PO2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.scan.PO2HistPoints = str2double(get(handles.text_HistPoints_PO2,'String'));
guidata(hObject, handles);

function text_PulsePeriod_Callback(hObject, eventdata, handles)
% hObject    handle to text_PulsePeriod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.scan.PO2PulsePeriod = str2double(get(handles.text_PulsePeriod,'String'));
guidata(hObject, handles);

function text_PulseHighTime_Callback(hObject, eventdata, handles)
% hObject    handle to text_PulseHighTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.scan.PO2PulseHighTime = str2double(get(handles.text_PulseHighTime,'String'));
guidata(hObject, handles);

function Points_PO2_Callback(hObject, eventdata, handles)
% hObject    handle to Points_PO2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.current,'String','select points for PO2 measurement');
axes(handles.pmt_image);
current_point_handle = impoint();
set(current_point_handle,'UserData',0);
% Here we only keep the handles to the point, we will get the positions at the end so that the user can move the point
handles.scan.segment_handles = [handles.scan.segment_handles, {current_point_handle}];
handles.point_PO2_handles = [handles.point_PO2_handles, current_point_handle];
handles.scan.number_points_per_segment = [handles.scan.number_points_per_segment, 1];
set(handles.current,'String','Click "Points" to select another point or click "Start" when ready');

guidata(hObject, handles);


function LineGrid_PO2_Callback(hObject, eventdata, handles)
% hObject    handle to LineGrid_PO2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.current,'String','Draw your line');
axes(handles.pmt_image);
current_line_handle = imline();
set(current_line_handle, 'UserData',1);
% Here we only keep the handles to the line, we will get the positions at the end so that the user can move the line
handles.scan.segment_handles = [handles.scan.segment_handles, {current_line_handle}];
handles.line_PO2_handles = [handles.line_PO2_handles, current_line_handle];
prompt = 'Number of points per line?';
set(handles.current,'String','Enter NUMBER of points per line');
handles.scan.number_points_per_segment = [handles.scan.number_points_per_segment, str2double((cell2mat(inputdlg(prompt))))];%bug:user input should be limited to int only
set(handles.current,'String','Click "Lines" to draw another line or click "Start" when ready');

guidata(hObject, handles);


function TwoDGrid_PO2_Callback(hObject, eventdata, handles)
% hObject    handle to TwoDGrid_PO2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.current,'String','Drag or resize the 2D Grid');
axes(handles.pmt_image);
current_2DGrid_handle = imrect(gca,[10 10 50 50]);
setFixedAspectRatioMode(current_2DGrid_handle,'True')
set(current_2DGrid_handle, 'UserData',2);
handles.scan.segment_handles = [handles.scan.segment_handles, {current_2DGrid_handle}];
handles.TwoDGrid_PO2_handles = [handles.TwoDGrid_PO2_handles, current_2DGrid_handle];
prompt = 'Number of points in x and y directions?';
set(handles.current,'String','Enter the number of points in x and y directions');
handles.scan.number_points_per_segment = [handles.scan.number_points_per_segment, str2double((cell2mat(inputdlg(prompt))))];%bug:user input should be limited to int only
set(handles.current,'String','Click "2D Grid" to draw another grid or click "Start" when ready');

guidata(hObject, handles);


function Reset_PO2_Callback(hObject, eventdata, handles)
% hObject    handle to Reset_PO2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.current,'String','Select Points, Lines, or 2D Grid Again');

delete(handles.point_PO2_handles);
delete(handles.line_PO2_handles);
delete(handles.TwoDGrid_PO2_handles);

delete(handles.point_handles);
delete(handles.line_handles);
handles.Exist_points = 0;

handles.scan.segment_handles= [];
handles.scan.number_points_per_segment = [];
handles.scan.sum_lines_points = [];
handles.scan.histog = [];

if handles.PointsOnGraph == 1;
%     delete(handles.h)
    handles.PointsOnGraph = 0;
end
cla(handles.PO2colorbar)
set(handles.PO2colorbar, 'YTick', [])

guidata(hObject, handles);


function Start_PO2_Callback(hObject, eventdata, handles)
% hObject    handle to Start_PO2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.Stop_PO2,'Value',0);
set(handles.Stop_PO2,'BackgroundColor',[0.94,0.94,0.94]);
handles.scan.counter_technique = 0;
set(handles.live_scan_stop,'Enable','on');
set(handles.live_scan_start,'Enable','off');
set(hObject,'BackgroundColor','r');
set(hObject,'String','Scanning!');
[x,y,z]=handles.motors.get_pos();
handles.scan.zpos = z ;
handles.scan.xpos = x ;
handles.scan.ypos = y ;

positions=[];
handles.scan.PO2data = [];
n_segments = length(handles.scan.segment_handles); % Count segments (either a point or a line or grid)

for i=1:n_segments
    if (get(handles.scan.segment_handles{i},'UserData') == 0)
        % This is a point
        point_positions = getPosition(handles.scan.segment_handles{i}); % it gives x and y of the point
        positions = [positions;point_positions];% each row represents x and y of one point
    elseif (get(handles.scan.segment_handles{i},'UserData') == 1)
        % This is a line
        line_positions = getPosition(handles.scan.segment_handles{i});
        line_positionsX = linspace(line_positions(1,1),line_positions(2,1), handles.scan.number_points_per_segment(i))';
        line_positionsY = linspace(line_positions(1,2),line_positions(2,2), handles.scan.number_points_per_segment(i))';
        line_positions = [line_positionsX  line_positionsY]; % each row represents x and y of one point on the line
        positions = [positions;line_positions]; % each row represents x and y of one point 
    else
        % This is a 2D grid
        grid_positions = getPosition(handles.scan.segment_handles{i}); %gives the position of the grid: [xmin ymin width height]
        X1= grid_positions(1);
        X2= X1 + grid_positions(3);
        Y1= grid_positions(2);
        Y2= Y1 + grid_positions(4);
        [grid_positionsX,grid_positionsY] = meshgrid(linspace(X1,X2,handles.scan.number_points_per_segment(i)),linspace(Y1,Y2,handles.scan.number_points_per_segment(i)));
        grid_positionsX=reshape(grid_positionsX,size(grid_positionsX,1)*size(grid_positionsX,2),1);
        grid_positionsY=reshape(grid_positionsY,size(grid_positionsY,1)*size(grid_positionsY,2),1);
        grid_positions = [grid_positionsX  grid_positionsY]; % each row represents x and y of one point on the line
        positions = [positions;grid_positions]; % each row represents x and y of one point 
    end
end

% we don't use polynomial ramps between lines or points:
handles.pos_x = positions(:,1)';
handles.pos_y = positions(:,2)';
handles.scan.ramp_x = positions(:,1)';
handles.scan.ramp_y = positions(:,2)';
        
% Now need to scale from the pixel values returned by imline to voltage
% values: We assume that we are drawing on the latest image, so nx and ny
% as well as voltx and volty are valid.
handles.pos_x = (handles.pos_x-(handles.scan.nx/2))*(handles.scan.volt_x*2)/(handles.scan.nx);
handles.pos_y = (handles.pos_y -(handles.scan.ny/2))* (handles.scan.volt_y*2)/(handles.scan.ny);
handles.data =[handles.pos_x(:),handles.pos_y(:)];

handles.shutter.open();

aom_ON = floor((handles.scan.PO2PulseHighTime/handles.scan.PO2PulsePeriod)*handles.scan.PO2HistPoints);
aom_pulse = [ones(1,aom_ON),zeros(1, (handles.scan.PO2HistPoints-aom_ON))];
aom_pulse = aom_pulse * (handles.scan.aom_value/50);
aom_freq = handles.scan.PO2HistPoints/(handles.scan.PO2PulsePeriod/1000000.0); % 

%Generating a laser-pulse which will be used for triggering the
%TrigAOClass. The output is CTR0 OUT (PFI12)
handles.LaserPulse=MohammadPulseClass(handles.scan.PO2PulsePeriod/1000000.0,handles.scan.PO2Repitition,'Dev1',0);

%(1) Generating a pulse train for histogram measurements (number of pulses on each laser pulse equal to: PO2HistPoints) which is retriggered
%with each AOM pulse. It is generated through counter 2. The generated pulse train goes to CTR 2 OUT (PFI14)
%(2) Creating a counter (CTR 1) to count the pulses from PMT based on clock (generated at step (1)). The input of the counter clock is CTR 2 OUT (PFI14).
if handles.scan.counter_technique
    timeaxis = linspace(0,handles.scan.PO2PulsePeriod,handles.scan.PO2HistPoints-1)'; % in us
    handles.counter = liom.CounterClass(handles.scan.PO2Repitition,handles.scan.PO2HistPoints,handles.scan.PO2PulsePeriod/1000000.0);
else
    timeaxis = linspace(0,handles.scan.PO2PulsePeriod,handles.scan.PO2HistPoints)'; % in us
    handles.ai_acq = liom.TrigAIClass('Dev1',2);
    train_freq = floor(handles.scan.PO2HistPoints/(handles.scan.PO2PulsePeriod/1000000.0));
    handles.ai_acq.config(train_freq,handles.scan.PO2HistPoints,'AITASK');
end

% handles.ao is being deleted after live, 3D or line scans, no need to delete here;
delete(handles.aom_task);
handles.galvos = liom.OnDemandAOClass('Dev1',0:1,'OnDemandGalvoAOTask');

handles.ao=MohammadTrigAOClass('Dev1',2,'aomAOTask'); %AOM            
handles.ao.config(aom_freq);
handles.ao.write_signals(aom_pulse'); 

cla(handles.lifetime_image)
legend(handles.lifetime_image,'off')
axes(handles.lifetime_image);
% plot(1,1);
xlabel('time (us)');
if handles.scan.counter_technique
    ylabel('photon counts')
else
    ylabel('Intensity');
end
% axesObjs = get(handles.lifetime_image, 'Children');  %axes handles   
% set(axesObjs,'XData',timeaxis(2:end));

for k = 1: handles.scan.PointRepetition
    for i = 1: length(handles.pos_x)
        handles.galvos.write(handles.data(i,:));

        %pause 0.01 sec for the galvo mirrors to relax
        tic;
        while toc < 0.01
        end
        if handles.scan.counter_technique
            handles.counter.start();
        else
            handles.ai_acq.start();
        end
        handles.ao.start();
        handles.LaserPulse.start();

        if handles.scan.counter_technique
            data=handles.counter.read( );
            data = reshape(data,handles.scan.PO2HistPoints,handles.scan.PO2Repitition);
            handles.scan.PO2data(:,1+(k-1)*handles.scan.PO2Repitition:k*handles.scan.PO2Repitition,i)=data(2:end,1:end)-data((1:end-1),1:end);
            %Display the average lifetime curve for each point
            if handles.PlotDecayCurves
                axes(handles.lifetime_image)
                set(axesObjs,'YData',mean(handles.scan.PO2data(:,1+(k-1)*handles.scan.PO2Repitition:k*handles.scan.PO2Repitition,i),2));
            end
        else       
            data=handles.ai_acq.read(handles.scan.PO2HistPoints*handles.scan.PO2Repitition);
            data = reshape(data,handles.scan.PO2HistPoints,handles.scan.PO2Repitition);
            background = mean(squeeze(data(end,:)),2);
            data = background - data;
            handles.scan.PO2data(:,1+(k-1)*handles.scan.PO2Repitition:k*handles.scan.PO2Repitition,i)=data;
            if handles.PlotDecayCurves
                axes(handles.lifetime_image);
                cla(handles.lifetime_image)
                plot(mean(handles.scan.PO2data(8:end,1+(k-1)*handles.scan.PO2Repitition:k*handles.scan.PO2Repitition,i),2));    %           semilogy(mean(handles.scan.PO2data(2:end,1:round(end/2),i),2)+1); hold on
    %           semilogy(mean(handles.scan.PO2data(2:end,round(end/2):end,i),2)+1,'r');
    %           plot(mean(handles.scan.PO2data(2:end,1:round(end/2),i),2)); hold on
    %           plot(mean(handles.scan.PO2data(2:end,round(end/2):end,i),2),'r'); hold off
            end
        end


        handles.LaserPulse.stop();
        handles.ao.stop();
        if handles.scan.counter_technique
            handles.counter.stop( );
        else
            handles.ai_acq.stop( );
        end

        if get(handles.Stop_PO2,'Value')==1
            break
        end
        set(handles.current,'String',[num2str(i) '/' num2str(length(handles.pos_x))]);
    end
end
    
delete(handles.ao);
delete(handles.LaserPulse);
if handles.scan.counter_technique ==0
    delete(handles.ai_acq);
else
    delete(handles.counter);
end

handles.shutter.close();
delete(handles.galvos);
handles.aom_task=liom.OnDemandAOClass('Dev1',2,'OnDemandAOMAOTask');
handles.aom_task.write(handles.scan.aom_value/50);

if get(handles.Stop_PO2,'Value')==0
    if(handles.save)
        answer = questdlg('Do you want to save this scan?','Save','yes','no','yes');
        if strcmp(answer,'yes')
            handles.PO2_scan_num = handles.PO2_scan_num+1;
            scan =handles.scan;
            mkdir(fullfile(handles.folder_name,['PO2_',num2str(handles.PO2_scan_num)]));
            save(fullfile(handles.folder_name,['PO2_',num2str(handles.PO2_scan_num)],[['PO2ScanInfo_',num2str(handles.PO2_scan_num)],'.mat']),'scan');
            image_ref_pmt = getimage(handles.pmt_image);
            image_ref_adp = getimage(handles.image);
            save(fullfile(handles.folder_name,['PO2_',num2str(handles.PO2_scan_num)],'image_pmt.mat'),'image_ref_pmt');
            save(fullfile(handles.folder_name,['PO2_',num2str(handles.PO2_scan_num)],'image_adp.mat'),'image_ref_adp');
        end
    end
end

set(handles.live_scan_stop,'Enable','on');
set(handles.live_scan_start,'Enable','on');
set(hObject,'BackgroundColor',[0.94,0.94,0.94]);
set(hObject,'String','Start');

guidata(hObject, handles);


% --- Executes on button press in Stop_PO2.
function Stop_PO2_Callback(hObject, eventdata, handles)
% hObject    handle to Stop_PO2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if get(hObject,'Value')==1
    set(hObject,'BackgroundColor','red');
else
    set(hObject,'BackgroundColor',[0.94,0.94,0.94]);
end

guidata(hObject, handles);


% --- Executes on button press in button_PO2values.
function button_PO2values_Callback(hObject, eventdata, handles)
% hObject    handle to button_PO2values (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% curve fitting and finding PO2 values from the calibration curve
axes(handles.lifetime_image)
for i = 1: length(handles.pos_x)
    if i == 1
        [p, ~, PO2(i), ~, x, y, R_square(i)] = CalcPO2_2(mean(handles.scan.PO2data(:,:,i),2), handles.scan.PO2PulsePeriod, handles.scan.PO2PulseHighTime, 4);
    else
        [~, ~, PO2(i), ~, ~, ~, R_square(i)] = CalcPO2_2(mean(handles.scan.PO2data(:,:,i),2), handles.scan.PO2PulsePeriod, handles.scan.PO2PulseHighTime, 4);
    end
end

%Ploting the first fitted curve
Predicted_y = (p(1).*(exp(-x./p(2))))+p(3);
cla(handles.lifetime_image)
plot(x, y); hold on %ploting the real data
plot(x,Predicted_y,'r'); % ploting the fitted curve
% plot(FitResults,'predfunc', 0.95); % ploting the fitted curve and the confidence intervals
legend({['Lifetime=' num2str(p(2)) ' us, R2=' num2str(R_square(1))], ['PO2=' num2str(PO2(1)) ' mmHg']},'FontSize',7);
xlabel('time (us)')
ylabel('Intensity')
    
guidata(hObject, handles);

%Removing the weak signals
FilteredPO2 = [];
FilteredX = [];
FilteredY = [];

for i = 1: length(PO2)
    if PO2(i) > 0 && PO2(i) < 160
        if R_square(i) > 0.95
            FilteredPO2 = [FilteredPO2, PO2(i)];
            FilteredX = [FilteredX, handles.scan.ramp_x(i)];
            FilteredY = [FilteredY, handles.scan.ramp_y(i)];
        end
    end
end


% plotting the PO2 values
image_ref_pmt = getimage(handles.pmt_image);
cla(handles.pmt_image);
axes(handles.pmt_image);
imagesc(image_ref_pmt); colormap 'gray'
hold on
ColorSegments = 50;
color_value = jet(ColorSegments+1);
Interval = max(FilteredPO2)/ColorSegments;


for i = 1: length(FilteredPO2)
    ColorIndex = ceil(FilteredPO2(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(FilteredX(i),FilteredY(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
end
set(handles.pmt_image, 'YTick', []);
set(handles.pmt_image, 'XTick', []);
hold off

%color bar

axes(handles.PO2colorbar)
ylim([0 max(FilteredPO2)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(FilteredPO2)/ColorSegments)+(j-1)*(max(FilteredPO2)/ColorSegments)/BarDevisions (i-1)*(max(FilteredPO2)/ColorSegments)+(j-1)*(max(FilteredPO2)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off
set(handles.PO2colorbar, 'YTick', linspace(0,max(FilteredPO2),5));
set(handles.PO2colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(FilteredPO2),5)));
handles.PointsOnGraph = 1;

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in Survey_Scan.
function Survey_Scan_Callback(hObject, eventdata, handles)
% hObject    handle to Survey_Scan (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.Stop_PO2,'Value',0);
set(handles.Stop_PO2,'BackgroundColor',[0.94,0.94,0.94]);
handles.scan.counter_technique = 0;
set(hObject,'BackgroundColor','r');
set(hObject,'String','Scanning!');
set(handles.live_scan_stop,'Enable','off');
set(handles.live_scan_start,'Enable','off');
positions=[];
handles.Survey_data=[];

% n_segments = length(handles.scan.segment_handles); % Count segments (either a point or a line or grid)
% 
% for i=1:n_segments
%     if (get(handles.scan.segment_handles{i},'UserData') == 0)
%         % This is a point
%         point_positions = getPosition(handles.scan.segment_handles{i}); % it gives x and y of the point
%         positions = [positions;point_positions];% each row represents x and y of one point
%     elseif (get(handles.scan.segment_handles{i},'UserData') == 1)
%         % This is a line
%         line_positions = getPosition(handles.scan.segment_handles{i});
%         line_positionsX = linspace(line_positions(1,1),line_positions(2,1), handles.scan.number_points_per_segment(i))';
%         line_positionsY = linspace(line_positions(1,2),line_positions(2,2), handles.scan.number_points_per_segment(i))';
%         line_positions = [line_positionsX  line_positionsY]; % each row represents x and y of one point on the line
%         positions = [positions;line_positions]; % each row represents x and y of one point 
%     else
%         % This is a 2D grid
%         grid_positions = getPosition(handles.scan.segment_handles{i}); %gives the position of the grid: [xmin ymin width height]
%         X1= grid_positions(1);
%         X2= X1 + grid_positions(3);
%         Y1= grid_positions(2);
%         Y2= Y1 + grid_positions(4);
%         [grid_positionsX,grid_positionsY] = meshgrid(linspace(X1,X2,handles.scan.number_points_per_segment(i)),linspace(Y1,Y2,handles.scan.number_points_per_segment(i)));
%         grid_positionsX=reshape(grid_positionsX,size(grid_positionsX,1)*size(grid_positionsX,2),1);
%         grid_positionsY=reshape(grid_positionsY,size(grid_positionsY,1)*size(grid_positionsY,2),1);
%         grid_positions = [grid_positionsX  grid_positionsY]; % each row represents x and y of one point on the line
%         positions = [positions;grid_positions]; % each row represents x and y of one point 
%     end
% end

prompt = 'Number of points in x and y directions?';
Survey_points = str2double((cell2mat(inputdlg(prompt))));%bug:user input should be limited to int only
X1=0; Y1=0; X2=X1+handles.scan.nx; Y2=Y1+handles.scan.ny;
[grid_positionsX,grid_positionsY] = meshgrid(linspace(X1,X2,Survey_points),linspace(Y1,Y2,Survey_points));
grid_positionsX=reshape(grid_positionsX,size(grid_positionsX,1)*size(grid_positionsX,2),1);
grid_positionsY=reshape(grid_positionsY,size(grid_positionsY,1)*size(grid_positionsY,2),1);
positions = [grid_positionsX  grid_positionsY]; % each row represents x and y of one point  

% we don't use polynomial ramps between lines or points:
handles.pos_x = positions(:,1)';
handles.pos_y = positions(:,2)';
handles.scan.ramp_x = positions(:,1)';
handles.scan.ramp_y = positions(:,2)';
        
% Now need to scale from the pixel values returned by imline to voltage
% values: We assume that we are drawing on the latest image, so nx and ny
% as well as voltx and volty are valid.
handles.pos_x = (handles.pos_x-(handles.scan.nx/2))*(handles.scan.volt_x*2)/(handles.scan.nx);
handles.pos_y = (handles.pos_y -(handles.scan.ny/2))* (handles.scan.volt_y*2)/(handles.scan.ny);
handles.data =[handles.pos_x(:),handles.pos_y(:)];

handles.shutter.open();

PO2PulseHighTime = 40; %us
PO2PulsePeriod = 80; %us
PO2HistPoints = 20;
PO2Repitition = 50; % to average

aom_ON = floor((PO2PulseHighTime/PO2PulsePeriod)*PO2HistPoints);
aom_pulse = [ones(1,aom_ON),zeros(1, (PO2HistPoints-aom_ON))];
aom_pulse = aom_pulse * (handles.scan.aom_value/50);
aom_freq = PO2HistPoints/(PO2PulsePeriod/1000000.0); % 

%Generating a laser-pulse which will be used for triggering the
%TrigAOClass. The output is CTR0 OUT (PFI12)
handles.LaserPulse=MohammadPulseClass(PO2PulsePeriod/1000000.0,PO2Repitition,'Dev1',0);

%(1) Generating a pulse train for histogram measurements (number of pulses on each laser pulse equal to: PO2HistPoints) which is retriggered
%with each AOM pulse. It is generated through counter 2. The generated pulse train goes to CTR 2 OUT (PFI14)
%(2) Creating a counter (CTR 1) to count the pulses from PMT based on clock (generated at step (1)). The input of the counter clock is CTR 2 OUT (PFI14).
if handles.scan.counter_technique
    handles.counter = liom.CounterClass(PO2Repitition,PO2HistPoints,PO2PulsePeriod/1000000.0);
else
    handles.ai_acq = liom.TrigAIClass('Dev1',2);
    train_freq = floor(PO2HistPoints/(PO2PulsePeriod/1000000.0));
    handles.ai_acq.config(train_freq,PO2HistPoints,'AITASK');
end

% handles.ao is being deleted after live, 3D or line scans, no need to delete here;
delete(handles.aom_task);
handles.galvos = liom.OnDemandAOClass('Dev1',0:1,'OnDemandGalvoAOTask');

handles.ao=MohammadTrigAOClass('Dev1',2,'aomAOTask'); %AOM
handles.ao.config(aom_freq);
handles.ao.write_signals(aom_pulse'); 

for i = 1: length(handles.pos_x)
    handles.galvos.write(handles.data(i,:));
    %pause 0.01 sec for the galvo mirrors to relax
    tic;
    while toc < 0.01
    end  
    if handles.scan.counter_technique
        handles.counter.start();
    else
        handles.ai_acq.start();
    end
    handles.ao.start();
    handles.LaserPulse.start();
    
    if handles.scan.counter_technique
        data=handles.counter.read( );
        data = reshape(data,PO2HistPoints,PO2Repitition);
        data = data(2:end,1:end)-data((1:end-1),1:end);
        handles.scan.PO2Surveydata(:,:,i)=data;
        if handles.IntegrateOnlyDecay == 0
            handles.Survey_data(i)=sum(sum(data));
        else
            handles.Survey_data(i)=sum(sum(data(11:end,:)));
        end
    else
        data=handles.ai_acq.read(PO2HistPoints*PO2Repitition);
        data = reshape(data,PO2HistPoints,PO2Repitition);
        background = mean(squeeze(data(end,:)),2);
%         data = data - background;
        data = background - data;
        handles.scan.PO2Surveydata(:,:,i)=data;
        if handles.IntegrateOnlyDecay == 0
            handles.Survey_data(i)=sum(sum(data));
        else
            handles.Survey_data(i)=sum(sum(data(11:end,:)));
        end
    end
    
    handles.LaserPulse.stop();
    handles.ao.stop();
    
    if handles.scan.counter_technique
        handles.counter.stop(); 
    else
        handles.ai_acq.stop( );
    end
    
    if get(handles.Stop_PO2,'Value')== 1
        break
    end
    set(handles.current,'String',[num2str(i) '/' num2str(length(handles.pos_x))]);
end

delete(handles.ao);
delete(handles.LaserPulse);
if handles.scan.counter_technique ==0
    delete(handles.ai_acq);
else
    delete(handles.counter);
end
handles.shutter.close();
delete(handles.galvos);
handles.aom_task=liom.OnDemandAOClass('Dev1',2,'OnDemandAOMAOTask');
handles.aom_task.write(handles.scan.aom_value/50);     

if get(handles.Stop_PO2,'Value')==0
    if(handles.save)
        answer = questdlg('Do you want to save this survey?','Save','yes','no','yes');
        if strcmp(answer,'yes')
            handles.PO2_survey_num = handles.PO2_survey_num+1;
            scan =handles.scan;
            mkdir(fullfile(handles.folder_name,['PO2_Survey',num2str(handles.PO2_survey_num)]));
            save(fullfile(handles.folder_name,['PO2_Survey',num2str(handles.PO2_survey_num)],[['PO2ScanInfo_',num2str(handles.PO2_survey_num)],'.mat']),'scan');
            image_ref_pmt = getimage(handles.pmt_image);
            image_ref_adp = getimage(handles.image);
            save(fullfile(handles.folder_name,['PO2_Survey',num2str(handles.PO2_survey_num)],'image_pmt.mat'),'image_ref_pmt');
            save(fullfile(handles.folder_name,['PO2_Survey',num2str(handles.PO2_survey_num)],'image_adp.mat'),'image_ref_adp');
        end
    end
end

set(handles.live_scan_stop,'Enable','on');
set(handles.live_scan_start,'Enable','on');
set(hObject,'BackgroundColor',[0.94,0.94,0.94]);
set(hObject,'String','Start');

% plotting the survey scan
image_ref_pmt = getimage(handles.pmt_image);
cla(handles.pmt_image);
axes(handles.pmt_image)
imagesc(image_ref_pmt); colormap 'gray'
hold on
ColorSegments = 50;
color_value = jet(ColorSegments+1);
Interval = max(handles.Survey_data)/ColorSegments;
 
for i = 1: length(handles.Survey_data)
    if (handles.Survey_data(i) < 0)
        handles.Survey_data(i) = 0;
    end
    ColorIndex = ceil(handles.Survey_data(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(handles.scan.ramp_x(i),handles.scan.ramp_y(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
end
set(handles.pmt_image, 'YTick', []);
set(handles.pmt_image, 'XTick', []);
hold off

%color bar
axes(handles.PO2colorbar)
ylim([0 max(handles.Survey_data)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(handles.Survey_data)/ColorSegments)+(j-1)*(max(handles.Survey_data)/ColorSegments)/BarDevisions (i-1)*(max(handles.Survey_data)/ColorSegments)+(j-1)*(max(handles.Survey_data)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off
set(handles.PO2colorbar, 'YTick', linspace(0,max(handles.Survey_data),5));
set(handles.PO2colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(handles.Survey_data),5)));
handles.PointsOnGraph = 1;

set(hObject,'BackgroundColor',[0.941, 0.941, 0.941]);
set(hObject,'String','Survey Scan');

guidata(hObject, handles);


% --- Executes on button press in Button_Adjust.
function Button_Adjust_Callback(hObject, eventdata, handles)
% hObject    handle to Button_Adjust (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

prompt = 'Enter the min value';
Survey_min = str2double((cell2mat(inputdlg(prompt))));
prompt = 'Enter the max value';
Survey_max = str2double((cell2mat(inputdlg(prompt))));

handles.Adjusted_Survey_data = [];
for i = 1: length(handles.Survey_data)
    if handles.Survey_data(i) < Survey_min 
        handles.Adjusted_Survey_data(i) = Survey_min; 
    elseif handles.Survey_data(i) > Survey_max
        handles.Adjusted_Survey_data(i) = Survey_max; 
    else
        handles.Adjusted_Survey_data(i) =handles.Survey_data(i);
    end
end

% plotting the adjusted survey scan
image_ref_pmt = getimage(handles.pmt_image);
cla(handles.pmt_image);
axes(handles.pmt_image)
imagesc(image_ref_pmt); colormap 'gray'
hold on
ColorSegments = 50;
color_value = jet(ColorSegments+1);
Interval = max(handles.Adjusted_Survey_data)/ColorSegments;
 
for i = 1: length(handles.Adjusted_Survey_data)
    if (handles.Adjusted_Survey_data(i) < 0)
        handles.Adjusted_Survey_data(i) = 0;
    end
    ColorIndex = ceil(handles.Adjusted_Survey_data(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(handles.scan.ramp_x(i),handles.scan.ramp_y(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
end
set(handles.pmt_image, 'YTick', []);
set(handles.pmt_image, 'XTick', []);
hold off

%color bar
axes(handles.PO2colorbar)
ylim([0 max(handles.Adjusted_Survey_data)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(handles.Adjusted_Survey_data)/ColorSegments)+(j-1)*(max(handles.Adjusted_Survey_data)/ColorSegments)/BarDevisions (i-1)*(max(handles.Adjusted_Survey_data)/ColorSegments)+(j-1)*(max(handles.Adjusted_Survey_data)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off









% Update handles structure
guidata(hObject, handles);



%% **********************************CreatFcn ****************************************** %%
function ny_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function nx_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


guidata(hObject, handles);
function height_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function width_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function xy_motor_step_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function z_motor_step_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function aom_slider_CreateFcn(hObject, eventdata, handles)
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end
function interp_table_CreateFcn(hObject, eventdata, handles)
function PMT_image_popup_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function ramp_type_popupmenu_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function line_freq_live_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function shift_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function extra_points_live_CreateFcn(hObject, eventdata, handles)
% hObject    handle to extra_points_live (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function line_freq_3D_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function extra_points_3D_CreateFcn(hObject, eventdata, handles)
% hObject    handle to extra_points_3D (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function z_slices_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function z_step_size_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function line_freq_line_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function repeat_scan_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function extra_points_line_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function bin_popupmenu_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function linescan_type_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
function shift_lineScan_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
%% Bugs
%7-11-2013
%--- When lines disappear, they should be reseted or deleted. Example: After
%we do a line scan, if we click on Live scan Start button, the lines
%disappear, meaning they shoud be deleted. Then, after choosing another
%site, we should click on Lines button to start over again. This doesn't
%happen at the moment...
%---Double check if all the extra points correspond to the correct scan type
%---Changing pmt popup is not simultanious. Meaning that the scan has to be
%stopped, pmt channel changed, then re-start the scan to see the change in
%channel.
%---The line scan image axes corresponds to the channel chosen on pmt
%popup. Could this be a potential bug?
%---Save directory should be fixed to save a folder EXP1 etc...
%---SaveFor function should be checked, I think line axes slows down when I
%capture the frame image...
%---Motor position status does not change simultaniously
%---Truncation should be added-Should each scan type have it's own trunc
%value? should one be enough for all channels(except galvos of course)?


% --- Executes on selection change in exp_type_popupmenu.
function exp_type_popupmenu_Callback(hObject, eventdata, handles)
% hObject    handle to exp_type_popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns exp_type_popupmenu contents as cell array
%        contents{get(hObject,'Value')} returns selected item from exp_type_popupmenu
contents = cellstr(get(hObject,'String'));
handles.scan.experiment_type = contents{get(hObject,'Value')};
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function exp_type_popupmenu_CreateFcn(hObject, eventdata, handles)
% hObject    handle to exp_type_popupmenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function text_PulseHighTime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_PulseHighTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function text_PulsePeriod_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_PulsePeriod (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function text_HistPoints_PO2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_HistPoints_PO2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes during object creation, after setting all properties.
function text_Repetition_PO2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to text_Repetition_PO2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox_survey.
function checkbox_survey_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_survey (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_survey
% Determins if only even rows are used for analysis (returns toggle state of Even_Row_CheckBox)
handles.IntegrateOnlyDecay = get(hObject,'Value'); % 1 if selected, 0 if not selected

% Update handles structure
guidata(hObject, handles);



function txt_PointRep_Callback(hObject, eventdata, handles)
% hObject    handle to txt_PointRep (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_PointRep as text
%        str2double(get(hObject,'String')) returns contents of txt_PointRep as a double

handles.scan.PointRepetition = str2double(get(handles.txt_PointRep,'String'));

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function txt_PointRep_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_PointRep (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in checkbox_plot.
function checkbox_plot_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_plot

handles.PlotDecayCurves = get(hObject,'Value'); % 1 if selected, 0 if not selected

% Update handles structure
guidata(hObject, handles);
