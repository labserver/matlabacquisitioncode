function varargout = PO2Analysis_gui(varargin)
% PO2ANALYSIS_GUI MATLAB code for PO2Analysis_gui.fig
%      PO2ANALYSIS_GUI, by itself, creates a new PO2ANALYSIS_GUI or raises the existing
%      singleton*.
%
%      H = PO2ANALYSIS_GUI returns the handle to a new PO2ANALYSIS_GUI or the handle to
%      the existing singleton*.
%
%      PO2ANALYSIS_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PO2ANALYSIS_GUI.M with the given input arguments.
%
%      PO2ANALYSIS_GUI('Property','Value',...) creates a new PO2ANALYSIS_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PO2Analysis_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PO2Analysis_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PO2Analysis_gui

% Last Modified by GUIDE v2.5 29-Jul-2016 20:21:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PO2Analysis_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @PO2Analysis_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



% --- Executes just before PO2Analysis_gui is made visible.
function PO2Analysis_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PO2Analysis_gui (see VARARGIN)

% Choose default command line output for PO2Analysis_gui
handles.output = hObject;
set(handles.LivePMT, 'YTick', []);
set(handles.LivePMT, 'XTick', []);
set(handles.LivePMTPO2, 'YTick', []);
set(handles.LivePMTPO2, 'XTick', []);
set(handles.PO2colorbar, 'XTick', []);
set(handles.PO2colorbar, 'YTick', []);
handles.ProbeBatch = 'Select the batch';

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes PO2Analysis_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);



% --- Outputs from this function are returned to the command line.
function varargout = PO2Analysis_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



% --- Executes on button press in Load_Button.
function Load_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Load_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Read Scan Info for analysis
handles.folder = uigetdir('C:\Users\moeini\Mohammad\Data\PO2 data\VascularAwakePO2\Vascular PO2');
FolderNumber = inputdlg('Enter the folder number: ');
load([handles.folder '/PO2ScanInfo_' FolderNumber{1} '.mat']); %scan information
handles.ScanInfo = scan;
clear scan
load([handles.folder '/image_pmt.mat']); % PMT live image 
handles.PMTImage = image_ref_pmt;
clear image_ref_pmt
load([handles.folder '/image_adp.mat']); % camera live image
handles.CameraImage = image_ref_adp;
clear image_ref_adp

set(handles.ExpTypeVertical,'String', handles.ScanInfo.experiment_type);
set(handles.ExcitationTime_txt,'String', handles.ScanInfo.PO2PulseHighTime);
set(handles.PulsePeriod_txt,'String', handles.ScanInfo.PO2PulsePeriod);
set(handles.AOM_txt,'String', handles.ScanInfo.aom_value);
set(handles.Repetition_txt,'String', handles.ScanInfo.PO2Repitition);

set(handles.HistPoints_txt,'String', handles.ScanInfo.PO2HistPoints);
if isfield(handles.ScanInfo,'PointRepetition')
    set(handles.PO2Interval_txt,'String', handles.ScanInfo.PO2Repitition * handles.ScanInfo.PointRepetition /2);
    set(handles.PointRep_txt,'String', handles.ScanInfo.PointRepetition);
    set(handles.UntilRep_txt, 'String',  handles.ScanInfo.PO2Repitition * handles.ScanInfo.PointRepetition);
    set(handles.RepToUseTo_txt,'String', handles.ScanInfo.PO2Repitition * handles.ScanInfo.PointRepetition);
else
    set(handles.PO2Interval_txt,'String', handles.ScanInfo.PO2Repitition /2);
    set(handles.UntilRep_txt, 'String',  handles.ScanInfo.PO2Repitition);
    set(handles.RepToUseTo_txt,'String', handles.ScanInfo.PO2Repitition);
end
set(handles.txt_dimensions, 'String',  [num2str(handles.ScanInfo.scan_height) ' x ' num2str(handles.ScanInfo.scan_width)]);
set(handles.HistPointEnd_txt,'String', handles.ScanInfo.PO2HistPoints);
if isfield(handles.ScanInfo,'z_surface')
    set(handles.txt_depth, 'String',  1000*(handles.ScanInfo.z_surface - handles.ScanInfo.zpos));
end
if isfield(handles.ScanInfo,'x_surface')
    set(handles.txt_xLocation, 'String',  handles.ScanInfo.x_surface);
end
if isfield(handles.ScanInfo,'y_surface')
    set(handles.txt_yLocation, 'String',  handles.ScanInfo.y_surface);
end

%Results Summary
set(handles.NumPoints_txt,'string','');
set(handles.AvgPO2_txt,'string','');
set(handles.PO2SD_txt,'string','');

% Update display

% plot the live images and the position of the points
axes(handles.LivePMT)
imagesc(handles.PMTImage); colormap 'gray'
set(handles.LivePMT, 'YTick', []);
set(handles.LivePMT, 'XTick', []);
axes(handles.LivePMTPO2)
imagesc(handles.PMTImage); colormap 'gray'
hold on
for i = 1: size(handles.ScanInfo.PO2data,3)
    plot(handles.ScanInfo.ramp_x(i),handles.ScanInfo.ramp_y(i),'Marker','s') 
end
hold off
set(handles.LivePMTPO2, 'YTick', []);
set(handles.LivePMTPO2, 'XTick', []);

if size(handles.ScanInfo.PO2data,3) ~= 1
    set(handles.slider_Hists,'Max',size(handles.ScanInfo.PO2data,3)-1);
    set(handles.slider_Hists, 'SliderStep', [1/(size(handles.ScanInfo.PO2data,3)-1) 1/(size(handles.ScanInfo.PO2data,3)-1)]);
else
    set(handles.slider_Hists,'Max',0);
    set(handles.slider_Hists, 'SliderStep', [0 0]);
end
set(handles.slider_Hists,'Value',0)
set(handles.Hists_Stext,'String', (int16(get(handles.slider_Hists,'Value'))+1));
axes(handles.Graph_Hist)
handles.timeaxis = linspace(0,handles.ScanInfo.PO2PulsePeriod,handles.ScanInfo.PO2HistPoints)'; % in us
plot(handles.timeaxis(2:end), mean(handles.ScanInfo.PO2data(2:end,:,1),2), 'LineWidth', 2.0)
xlabel('time (us)')
ylabel('Intensity')

        
% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in Calculate_button.
function Calculate_button_Callback(hObject, eventdata, handles)
% hObject    handle to Calculate_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
if isfield(handles,'SelectedPoint')
    clear handles.SelectedPoint;
end
xx=[];yy=[]; handles.PO2=[]; handles.tPO2=[]; handles.tR_square=[]; handles.R_square=[]; handles.LifetimeVal=[]; Predicted_y=[];
for i = 1: length(handles.ScanInfo.ramp_x)
    RepStart=str2num(get(handles.RepToUseFrom_txt,'string'));
    RepEnd=str2num(get(handles.RepToUseTo_txt,'string'));
    
    if handles.ProbeBatch == 1
        [p, ~, handles.PO2(i), ~, xx(:,i), yy(:,i), handles.R_square(i)] = CalcPO2_2(mean(handles.ScanInfo.PO2data(:,RepStart:RepEnd,i),2), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, str2num(get(handles.NeglectPoints_txt,'String')));
    elseif handles.ProbeBatch == 2
        [p, ~, handles.PO2(i), ~, xx(:,i), yy(:,i), handles.R_square(i)] = CalcPO2_2015(mean(handles.ScanInfo.PO2data(:,RepStart:RepEnd,i),2), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, str2num(get(handles.NeglectPoints_txt,'String')));
    else
         h = msgbox('Fisrt chose the probe batch');
    end
    
    handles.LifetimeVal(i) = p(2);
    Predicted_y(:,i) = (p(1).*(exp(-xx(:,i)./p(2))))+p(3);
    
    k=1;
    RepBlocks = str2num(get(handles.PO2Interval_txt,'string'));
    
    for j=RepStart:RepBlocks:RepEnd
        if handles.ProbeBatch == 1
            [~, ~, handles.tPO2(i,k), ~, ~, ~, handles.tR_square(i,k)] = CalcPO2_2(mean(handles.ScanInfo.PO2data(:,j:j+RepBlocks-1,i),2), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, str2num(get(handles.NeglectPoints_txt,'String')));            
        elseif handles.ProbeBatch == 2
            [~, ~, handles.tPO2(i,k), ~, ~, ~, handles.tR_square(i,k)] = CalcPO2_2015(mean(handles.ScanInfo.PO2data(:,j:j+RepBlocks-1,i),2), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, str2num(get(handles.NeglectPoints_txt,'String')));
        else
            h = msgbox('Fisrt chose the probe batch');
        end
        k=k+1;
    end
end
handles.xx = xx;
handles.yy = yy;
handles.Predicted_y =Predicted_y;

set(handles.slider_Hists,'Value',0)
set(handles.Hists_Stext,'String', (int16(get(handles.slider_Hists,'Value'))+1));

axes(handles.Graph_Hist)
handles.timeaxis = linspace(0,handles.ScanInfo.PO2PulsePeriod,handles.ScanInfo.PO2HistPoints)'; % in us
plot(handles.timeaxis(2:end), mean(handles.ScanInfo.PO2data(2:end,:,1),2), 'LineWidth', 2.0)
xlabel('time (us)')
ylabel('Intensity')

% Ploting the first fitted curve
axes(handles.Graph_FitHist)
cla(handles.Graph_FitHist)
plot(handles.xx(:,1), handles.yy(:,1)); hold on %ploting the real data
plot(handles.xx(:,1),handles.Predicted_y(:,1),'r'); % ploting the fitted curve
legend({['Lifetime=' num2str(handles.LifetimeVal(1)) ' us, R2=' num2str(handles.R_square(1))], ['PO2=' num2str(handles.PO2(1)) ' mmHg']},'FontSize',7);
xlabel('time (us)')
ylabel('Intensity')

guidata(hObject, handles);

% plotting the PO2 values
ColorSegments = 50;
color_value = jet(ColorSegments+1);
Interval = max(handles.PO2)/ColorSegments;
cla(handles.LivePMTPO2)
axes(handles.LivePMTPO2)
imagesc(handles.PMTImage); colormap 'gray'

hold on
for i = 1: length(handles.PO2)
%     if handles.PO2(i)<0
%         handles.PO2(i)=0;
%     end
%     ColorIndex = round(length(handles.PO2)*handles.PO2(i)/max(handles.PO2));
    ColorIndex = ceil(handles.PO2(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(handles.ScanInfo.ramp_x(i),handles.ScanInfo.ramp_y(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
%     handles.hpoint(i) = impoint(gca,handles.ScanInfo.ramp_x(i),handles.ScanInfo.ramp_y(i));
%     setColor(handles.hpoint(i),[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)]);
end
hold off
set(handles.LivePMTPO2, 'YTick', []);
set(handles.LivePMTPO2, 'XTick', []);

axes(handles.PO2colorbar)
ylim([0 max(handles.PO2)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(handles.PO2)/ColorSegments)+(j-1)*(max(handles.PO2)/ColorSegments)/BarDevisions (i-1)*(max(handles.PO2)/ColorSegments)+(j-1)*(max(handles.PO2)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off
set(handles.PO2colorbar, 'YTick', linspace(0,max(handles.PO2),5));
set(handles.PO2colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(handles.PO2),5)));   

%Results Summary
set(handles.NumPoints_txt,'string',length(handles.PO2));
set(handles.AvgPO2_txt,'string',mean(handles.PO2));
set(handles.PO2SD_txt,'string',std(handles.PO2));
set(handles.PO2Var_txt,'string','');
set(handles.PO2VarNorm_txt,'string','');

% Update handles structure
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function SummaryTable_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SummaryTable (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



function Hists_Stext_Callback(hObject, eventdata, handles)
% hObject    handle to Hists_Stext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Hists_Stext as text
%        str2double(get(hObject,'String')) returns contents of Hists_Stext as a double


% --- Executes during object creation, after setting all properties.
function Hists_Stext_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Hists_Stext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider_Hists_Callback(hObject, eventdata, handles)
% hObject    handle to slider_Hists (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

%update display
axes(handles.Graph_Hist)
axesObjs = get(handles.Graph_Hist, 'Children');  %axes handles
set(axesObjs,'YData',mean(handles.ScanInfo.PO2data(2:end,:,(int16(get(handles.slider_Hists,'Value'))+1)),2));
set(handles.Hists_Stext,'String', (int16(get(handles.slider_Hists,'Value'))+1));
% axes(handles.LivePMTPO2); hold on;
% plot(handles.ScanInfo.ramp_x(int16(get(handles.slider_Hists,'Value'))+1),handles.ScanInfo.ramp_y(int16(get(handles.slider_Hists,'Value'))+1),'Marker','x','Color','Yellow')
if isfield(handles,'SelectedPoint')
    if isvalid(handles.SelectedPoint)
        setPosition(handles.SelectedPoint, handles.ScanInfo.ramp_x(int16(get(handles.slider_Hists,'Value'))+1),handles.ScanInfo.ramp_y(int16(get(handles.slider_Hists,'Value'))+1));
    else
            handles.SelectedPoint=impoint(handles.LivePMTPO2,handles.ScanInfo.ramp_x(int16(get(handles.slider_Hists,'Value'))+1),handles.ScanInfo.ramp_y(int16(get(handles.slider_Hists,'Value'))+1));
    end
else
    handles.SelectedPoint=impoint(handles.LivePMTPO2,handles.ScanInfo.ramp_x(int16(get(handles.slider_Hists,'Value'))+1),handles.ScanInfo.ramp_y(int16(get(handles.slider_Hists,'Value'))+1));
end

% hold off

%update display
axes(handles.Graph_FitHist)
cla(handles.Graph_FitHist)
plot(handles.xx(:,(int16(get(handles.slider_Hists,'Value'))+1)), handles.yy(:,(int16(get(handles.slider_Hists,'Value'))+1))); hold on %ploting the real data
plot(handles.xx(:,(int16(get(handles.slider_Hists,'Value'))+1)),handles.Predicted_y(:,(int16(get(handles.slider_Hists,'Value'))+1)),'r'); % ploting the fitted curve and the confidence intervals
legend({['Lifetime=' num2str(handles.LifetimeVal(int16(get(handles.slider_Hists,'Value'))+1)) ' us, R2=' num2str(handles.R_square(int16(get(handles.slider_Hists,'Value'))+1))],['PO2=' num2str(handles.PO2(int16(get(handles.slider_Hists,'Value'))+1)) ' mmHg']},'FontSize',7);
xlabel('time (us)')
ylabel('Intensity')

% Update handles structure
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function slider_Hists_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_Hists (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



% --- Executes on button press in Save_Button.
function Save_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Save_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Results = [];

handles.Savefolder = uigetdir('C:\Users\moeini\Mohammad\Data\PO2 data\PO2 recordings Aging\Analyzed data\');
if isfield(handles.ScanInfo,'z_surface')
    Results.Depth =1000*(handles.ScanInfo.z_surface - handles.ScanInfo.zpos); %depth in um
end
Results.ImageDimention = num2str(handles.ScanInfo.scan_height);
if isfield(handles.ScanInfo,'x_surface')
    Results.SurfaceX =  handles.ScanInfo.x_surface;
end
if isfield(handles.ScanInfo,'y_surface')
    Results.SurfaceY =  handles.ScanInfo.y_surface;
end
Results.PO2 = handles.FilteredPO2;
Results.Lifetime = handles.FilteredLifetime;
Results.PointX = handles.FilteredX;
Results.PointY = handles.FilteredY;
Results.AOM = handles.ScanInfo.aom_value;
Results.RepStart = str2num(get(handles.RepToUseFrom_txt,'String'));
Results.RepEnd = str2num(get(handles.RepToUseTo_txt,'String'));
Results.NumHistPoints =handles.ScanInfo.PO2HistPoints;
Results.NegPoints = str2num(get(handles.NeglectPoints_txt,'String'));
Results.AcceptedR2 = str2num(get(handles.R2_txt,'String'));
Results.PulseDuration = handles.ScanInfo.PO2PulsePeriod;
Results.PulseHighTime =handles.ScanInfo.PO2PulseHighTime;
Results.FilteredtPO2 = handles.FilteredtPO2;
Results.StdtPO2 = std(handles.FilteredtPO2,0,2);
Results.StdtPO2Norm = Results.StdtPO2./mean(handles.FilteredtPO2,2);
Results.FilteredtR_square = handles.FilteredtR_square;
Results.PO2Variation = handles.PO2Variation;
Results.PO2VariationNorm = handles.PO2VariationNorm;
Results.xpos = handles.ScanInfo.xpos;
Results.ypos = handles.ScanInfo.ypos;

save([handles.Savefolder '/Results.mat'],'Results')
copyfile([handles.folder '/image_pmt.mat'],[handles.Savefolder '/image_pmt.mat'],'f');

% Saving the PMT image
PMTImage = figure;
imagesc(handles.PMTImage); colormap 'gray'
set(gca, 'YTick', []);
set(gca, 'XTick', []);
print(PMTImage,'-dtiff', [handles.Savefolder '/PMT.tif']);
close(PMTImage);

% % Saving the camera image
% CameraImage = figure;
% imagesc(handles.CameraImage); colormap 'gray'
% set(gca, 'YTick', []);
% set(gca, 'XTick', []);
% print(CameraImage,'-dtiff', [handles.Savefolder '/Camera.tif']);
% close(CameraImage);

% Saving the PMT/PO2 image
PMTPO2Image = figure;
hPMTPO2Image=imagesc(handles.PMTImage);  colormap gray; cbFITC = colorbar;
set(gca, 'YTick', []);
set(gca, 'XTick', []);
colormap([gray(64);jet(64)]);
maxFTC = max(handles.PMTImage(:));
minFTC = min(handles.PMTImage(:));
ncmp=64;
c_FITC = min(ncmp,  round(   (ncmp-1).*(handles.PMTImage-minFTC)./(maxFTC-minFTC)  )+1  );

set(hPMTPO2Image,'CDataMapping','Direct');
set(hPMTPO2Image,'CData',c_FITC);
figure(PMTPO2Image);
caxis([min(c_FITC(:)) max(c_FITC(:))]);
set(cbFITC,'YLim',[ncmp+1 2*ncmp]);

cmap1=jet(64);
CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
minPO2 = min(Results.PO2);
maxPO2 = max(Results.PO2);
cmapPO2idx = ones(length(Results.PO2),1); % vector of colormap indexes for pO2
if maxPO2 == minPO2,
    cmapPO2idx = cmapPO2idx.*round(CmapN/2);
else
    cmapPO2idx = round(   (  CmapN.*(Results.PO2-minPO2)+maxPO2-Results.PO2  )    ./(maxPO2-minPO2) );
    cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
    cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
end;

for i = 1: length(Results.PO2)
     colMap = squeeze(cmap1(cmapPO2idx(i),:));
     rectangle('Position',[(Results.PointX(i) - 7),(Results.PointY(i) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
     daspect ([1,1,1])
end

step = 10;         
newTickLabels = step*ceil(minPO2/step)  : step  : step*floor(maxPO2/step);
newTicks =( newTickLabels - (minPO2*ncmp-maxPO2)/(ncmp-1) ) .* (ncmp-1) ./ (maxPO2-minPO2);
newTickLabelsStr=num2str(newTickLabels(1),'%i');
for idxLab = 2:length(newTickLabels),
    newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
end;
pause(0);
set(cbFITC,'YTick',newTicks+ncmp);
set(cbFITC,'YTickLabel',newTickLabelsStr, 'fontsize',30);

 print(PMTPO2Image,'-dtiff', [handles.Savefolder '/PMTPO2Image.tif']);
close(PMTPO2Image);


% Saving the PMT/PO2 image scale fixed 0-70 mmHg
PMTPO2Image = figure;
hPMTPO2Image=imagesc(handles.PMTImage);  colormap gray; cbFITC = colorbar;
set(gca, 'YTick', []);
set(gca, 'XTick', []);
colormap([gray(64);jet(64)]);
maxFTC = max(handles.PMTImage(:));
minFTC = min(handles.PMTImage(:));
ncmp=64;
c_FITC = min(ncmp,  round(   (ncmp-1).*(handles.PMTImage-minFTC)./(maxFTC-minFTC)  )+1  );

set(hPMTPO2Image,'CDataMapping','Direct');
set(hPMTPO2Image,'CData',c_FITC);
figure(PMTPO2Image);
caxis([min(c_FITC(:)) max(c_FITC(:))]);
set(cbFITC,'YLim',[ncmp+1 2*ncmp]);

cmap1=jet(64);
CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
minPO2 = 0;
% maxPO2 = 160;
maxPO2 = 70;
cmapPO2idx = ones(length(Results.PO2),1); % vector of colormap indexes for pO2
if maxPO2 == minPO2,
    cmapPO2idx = cmapPO2idx.*round(CmapN/2);
else
    cmapPO2idx = round(   (  CmapN.*(Results.PO2-minPO2)+maxPO2-Results.PO2  )    ./(maxPO2-minPO2) );
    cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
    cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
end;

for i = 1: length(Results.PO2)
     colMap = squeeze(cmap1(cmapPO2idx(i),:));
     rectangle('Position',[(Results.PointX(i) - 7),(Results.PointY(i) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
     daspect ([1,1,1])
end

step = 20;         
newTickLabels = step*ceil(minPO2/step)  : step  : step*floor(maxPO2/step);
newTicks =( newTickLabels - (minPO2*ncmp-maxPO2)/(ncmp-1) ) .* (ncmp-1) ./ (maxPO2-minPO2);
newTickLabelsStr=num2str(newTickLabels(1),'%i');
for idxLab = 2:length(newTickLabels),
    newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
end;
pause(0);
set(cbFITC,'YTick',newTicks+ncmp);
set(cbFITC,'YTickLabel',newTickLabelsStr, 'fontsize',30);

% print(PMTPO2Image,'-dtiff', [handles.Savefolder '/PMTPO2Image0-160.tif']);
print(PMTPO2Image,'-dtiff', [handles.Savefolder '/PMTPO2Image0-60.tif']);
close(PMTPO2Image);


% % Saving the PO2 SD images
% PO2SDImage = figure;
% hPO2SDImage=imagesc(handles.PMTImage);  colormap gray; cbFITC = colorbar;
% set(gca, 'YTick', []);
% set(gca, 'XTick', []);
% colormap([gray(64);jet(64)]);
% maxFTC = max(handles.PMTImage(:));
% minFTC = min(handles.PMTImage(:));
% ncmp=64;
% c_FITC = min(ncmp,  round(   (ncmp-1).*(handles.PMTImage-minFTC)./(maxFTC-minFTC)  )+1  );
% 
% set(hPO2SDImage,'CDataMapping','Direct');
% set(hPO2SDImage,'CData',c_FITC);
% figure(PO2SDImage);
% caxis([min(c_FITC(:)) max(c_FITC(:))]);
% set(cbFITC,'YLim',[ncmp+1 2*ncmp]);
% 
% cmap1=jet(64);
% CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
% minStdtPO2 = min(Results.StdtPO2);
% maxStdtPO2 = max(Results.StdtPO2);
% cmapPO2idx = ones(length(Results.StdtPO2),1); % vector of colormap indexes for pO2
% if maxStdtPO2 == minStdtPO2,
%     cmapPO2idx = cmapPO2idx.*round(CmapN/2);
% else
%     cmapPO2idx = round(   (  CmapN.*(Results.StdtPO2-minStdtPO2)+maxStdtPO2-Results.StdtPO2  )    ./(maxStdtPO2-minStdtPO2) );
%     cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
%     cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
% end;
% 
% for i = 1: length(Results.StdtPO2)
%      colMap = squeeze(cmap1(cmapPO2idx(i),:));
%      rectangle('Position',[(Results.PointX(i) - 7),(Results.PointY(i) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
%      daspect ([1,1,1])
% end
% 
% step = 10;         
% newTickLabels = step*ceil(minStdtPO2/step)  : step  : step*floor(maxStdtPO2/step);
% newTicks =( newTickLabels - (minStdtPO2*ncmp-maxStdtPO2)/(ncmp-1) ) .* (ncmp-1) ./ (maxStdtPO2-minStdtPO2);
% newTickLabelsStr=num2str(newTickLabels(1),'%i');
% for idxLab = 2:length(newTickLabels),
%     newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
% end;
% pause(0);
% set(cbFITC,'YTick',newTicks+ncmp);
% set(cbFITC,'YTickLabel',newTickLabelsStr);
% 
%  print(PO2SDImage,'-dtiff', [handles.Savefolder '/PO2SDImage.tif']);
% close(PO2SDImage);
% 
% 
% % Saving the PO2 Variation images
% PO2VariationImage = figure;
% hPO2VariationImage=imagesc(handles.PMTImage);  colormap gray; cbFITC = colorbar;
% set(gca, 'YTick', []);
% set(gca, 'XTick', []);
% colormap([gray(64);jet(64)]);
% maxFTC = max(handles.PMTImage(:));
% minFTC = min(handles.PMTImage(:));
% ncmp=64;
% c_FITC = min(ncmp,  round(   (ncmp-1).*(handles.PMTImage-minFTC)./(maxFTC-minFTC)  )+1  );
% 
% set(hPO2VariationImage,'CDataMapping','Direct');
% set(hPO2VariationImage,'CData',c_FITC);
% figure(PO2VariationImage);
% caxis([min(c_FITC(:)) max(c_FITC(:))]);
% set(cbFITC,'YLim',[ncmp+1 2*ncmp]);
% 
% cmap1=jet(64);
% CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
% minPO2Variation = min(Results.PO2Variation);
% maxPO2Variation = max(Results.PO2Variation);
% cmapPO2idx = ones(length(Results.PO2Variation),1); % vector of colormap indexes for pO2
% if maxPO2Variation == minPO2Variation,
%     cmapPO2idx = cmapPO2idx.*round(CmapN/2);
% else
%     cmapPO2idx = round(   (  CmapN.*(Results.PO2Variation-minPO2Variation)+maxPO2Variation-Results.PO2Variation  )    ./(maxPO2Variation-minPO2Variation) );
%     cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
%     cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
% end;
% 
% for i = 1: length(Results.PO2Variation)
%      colMap = squeeze(cmap1(cmapPO2idx(i),:));
%      rectangle('Position',[(Results.PointX(i) - 7),(Results.PointY(i) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
%      daspect ([1,1,1])
% end
% 
% step = 10;         
% newTickLabels = step*ceil(minPO2Variation/step)  : step  : step*floor(maxPO2Variation/step);
% newTicks =( newTickLabels - (minPO2Variation*ncmp-maxPO2Variation)/(ncmp-1) ) .* (ncmp-1) ./ (maxPO2Variation-minPO2Variation);
% newTickLabelsStr=num2str(newTickLabels(1),'%i');
% for idxLab = 2:length(newTickLabels),
%     newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
% end;
% pause(0);
% set(cbFITC,'YTick',newTicks+ncmp);
% set(cbFITC,'YTickLabel',newTickLabelsStr);
% 
%  print(PO2VariationImage,'-dtiff', [handles.Savefolder '/PO2VariationImage.tif']);
% close(PO2VariationImage);
% 
% 
% % Saving the Normalized PO2 Variation images
% PO2VariationNormImage = figure;
% hPO2VariationNormImage=imagesc(handles.PMTImage);  colormap gray; cbFITC = colorbar;
% set(gca, 'YTick', []);
% set(gca, 'XTick', []);
% colormap([gray(64);jet(64)]);
% maxFTC = max(handles.PMTImage(:));
% minFTC = min(handles.PMTImage(:));
% ncmp=64;
% c_FITC = min(ncmp,  round(   (ncmp-1).*(handles.PMTImage-minFTC)./(maxFTC-minFTC)  )+1  );
% 
% set(hPO2VariationNormImage,'CDataMapping','Direct');
% set(hPO2VariationNormImage,'CData',c_FITC);
% figure(PO2VariationNormImage);
% caxis([min(c_FITC(:)) max(c_FITC(:))]);
% set(cbFITC,'YLim',[ncmp+1 2*ncmp]);
% 
% cmap1=jet(64);
% CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
% minPO2VariationNorm = min(Results.PO2VariationNorm);
% maxPO2VariationNorm = max(Results.PO2VariationNorm);
% cmapPO2idx = ones(length(Results.PO2VariationNorm),1); % vector of colormap indexes for pO2
% if maxPO2VariationNorm == minPO2VariationNorm,
%     cmapPO2idx = cmapPO2idx.*round(CmapN/2);
% else
%     cmapPO2idx = round(   (  CmapN.*(Results.PO2VariationNorm-minPO2VariationNorm)+maxPO2VariationNorm-Results.PO2VariationNorm  )    ./(maxPO2VariationNorm-minPO2VariationNorm) );
%     cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
%     cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
% end;
% 
% for i = 1: length(Results.PO2VariationNorm)
%      colMap = squeeze(cmap1(cmapPO2idx(i),:));
%      rectangle('Position',[(Results.PointX(i) - 7),(Results.PointY(i) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
%      daspect ([1,1,1])
% end
% 
% step = 10;         
% newTickLabels = step*ceil(minPO2VariationNorm/step)  : step  : step*floor(maxPO2VariationNorm/step);
% newTicks =( newTickLabels - (minPO2VariationNorm*ncmp-maxPO2VariationNorm)/(ncmp-1) ) .* (ncmp-1) ./ (maxPO2VariationNorm-minPO2VariationNorm);
% newTickLabelsStr=num2str(newTickLabels(1),'%i');
% for idxLab = 2:length(newTickLabels),
%     newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
% end;
% pause(0);
% set(cbFITC,'YTick',newTicks+ncmp);
% set(cbFITC,'YTickLabel',newTickLabelsStr);
% 
%  print(PO2VariationNormImage,'-dtiff', [handles.Savefolder '/PO2VariationNormImage.tif']);
% close(PO2VariationNormImage);


 
% --- Executes when entered data in editable cell(s) in SummaryTable.
function SummaryTable_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to SummaryTable (see GCBO)
% eventdata  structure with the following fields (see UITABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function LivePMTPO2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LivePMTPO2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate LivePMTPO2



function ExpTypeVertical_Callback(hObject, eventdata, handles)
% hObject    handle to ExpTypeVertical (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ExpTypeVertical as text
%        str2double(get(hObject,'String')) returns contents of ExpTypeVertical as a double


% --- Executes during object creation, after setting all properties.
function ExpTypeVertical_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ExpTypeVertical (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ExcitationTime_txt_Callback(hObject, eventdata, handles)
% hObject    handle to ExcitationTime_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ExcitationTime_txt as text
%        str2double(get(hObject,'String')) returns contents of ExcitationTime_txt as a double


% --- Executes during object creation, after setting all properties.
function ExcitationTime_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ExcitationTime_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PulsePeriod_txt_Callback(hObject, eventdata, handles)
% hObject    handle to PulsePeriod_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PulsePeriod_txt as text
%        str2double(get(hObject,'String')) returns contents of PulsePeriod_txt as a double


% --- Executes during object creation, after setting all properties.
function PulsePeriod_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PulsePeriod_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function AOM_txt_Callback(hObject, eventdata, handles)
% hObject    handle to AOM_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of AOM_txt as text
%        str2double(get(hObject,'String')) returns contents of AOM_txt as a double


% --- Executes during object creation, after setting all properties.
function AOM_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to AOM_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Repetition_txt_Callback(hObject, eventdata, handles)
% hObject    handle to Repetition_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Repetition_txt as text
%        str2double(get(hObject,'String')) returns contents of Repetition_txt as a double


% --- Executes during object creation, after setting all properties.
function Repetition_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Repetition_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function HistPoints_txt_Callback(hObject, eventdata, handles)
% hObject    handle to HistPoints_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of HistPoints_txt as text
%        str2double(get(hObject,'String')) returns contents of HistPoints_txt as a double


% --- Executes during object creation, after setting all properties.
function HistPoints_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to HistPoints_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function Graph_Hist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Graph_Hist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate Graph_Hist


% --- Executes during object creation, after setting all properties.
function Graph_FitHist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Graph_FitHist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate Graph_FitHist


% --- Executes on button press in Filter_Button.
function Filter_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Filter_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isfield(handles,'SelectedPoint')
    clear handles.SelectedPoint;
end

handles.FilteredPO2 = [];
handles.FilteredLifetime = [];
handles.FilteredX = [];
handles.FilteredY = [];
handles.FilteredtPO2 = [];
handles.FilteredtR_square = [];

for i = 1: length(handles.PO2)
    MaxSignal = max(mean(handles.ScanInfo.PO2data(ceil(size(handles.ScanInfo.PO2data,1)*handles.ScanInfo.PO2PulseHighTime/handles.ScanInfo.PO2PulsePeriod)+str2num(get(handles.NeglectPoints_txt,'String')):end,:,i),2));
    if handles.PO2(i) > str2num(get(handles.Min_txt,'String')) && handles.PO2(i) < str2num(get(handles.Max_txt,'String'))
        if (MaxSignal > str2num(get(handles.txt_acceptablesignal_min,'String')))  &&  (MaxSignal  < str2num(get(handles.txt_acceptablesignal_max,'String')))
            if handles.R_square(i) > str2num(get(handles.R2_txt,'String'))
                handles.FilteredPO2 = [handles.FilteredPO2, handles.PO2(i)];
                handles.FilteredLifetime = [handles.FilteredLifetime, handles.LifetimeVal(i)];
                handles.FilteredX = [handles.FilteredX, handles.ScanInfo.ramp_x(i)];
                handles.FilteredY = [handles.FilteredY, handles.ScanInfo.ramp_y(i)];
                handles.FilteredtPO2 = [handles.FilteredtPO2; handles.tPO2(i,:)];
                handles.FilteredtR_square = [handles.FilteredtR_square; handles.tR_square(i,:)];
            end
        end
    end
end

handles.PO2Variation = handles.FilteredtPO2(:,2) - handles.FilteredtPO2(:,1);
handles.PO2VariationNorm = (handles.FilteredtPO2(:,2) - handles.FilteredtPO2(:,1))./mean(handles.FilteredtPO2,2);

% plotting the PO2 values
cla(handles.LivePMTPO2)
axes(handles.LivePMTPO2)
imagesc(handles.PMTImage); colormap 'gray'
hold on

ColorSegments = 50;
color_value = jet(ColorSegments+1);
Interval = max(handles.FilteredPO2)/ColorSegments;
for i = 1: length(handles.FilteredPO2)
    ColorIndex = ceil(handles.FilteredPO2(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(handles.FilteredX(i),handles.FilteredY(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
%     handles.hpoint(i) = impoint(gca,handles.FilteredX(i),handles.FilteredY(i));
%     setColor(handles.hpoint(i),[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)]);
end
set(handles.LivePMTPO2, 'YTick', []);
set(handles.LivePMTPO2, 'XTick', []);
hold off

axes(handles.PO2colorbar)
ylim([0 max(handles.FilteredPO2)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(handles.FilteredPO2)/ColorSegments)+(j-1)*(max(handles.FilteredPO2)/ColorSegments)/BarDevisions (i-1)*(max(handles.FilteredPO2)/ColorSegments)+(j-1)*(max(handles.FilteredPO2)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off
set(handles.PO2colorbar, 'YTick', linspace(0,max(handles.FilteredPO2),5));
set(handles.PO2colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(handles.FilteredPO2),5)));

%Results Summary
set(handles.NumPoints_txt,'string',length(handles.FilteredPO2));
set(handles.AvgPO2_txt,'string',mean(handles.FilteredPO2));
set(handles.PO2SD_txt,'string',std(handles.FilteredPO2));
set(handles.PO2Var_txt,'string',mean(handles.PO2Variation));
set(handles.PO2VarNorm_txt,'string',mean(handles.PO2VariationNorm));

% Update handles structure
guidata(hObject, handles);


function Min_txt_Callback(hObject, eventdata, handles)
% hObject    handle to Min_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Min_txt as text
%        str2double(get(hObject,'String')) returns contents of Min_txt as a double


% --- Executes during object creation, after setting all properties.
function Min_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Min_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function Max_txt_Callback(hObject, eventdata, handles)
% hObject    handle to Max_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of Max_txt as text
%        str2double(get(hObject,'String')) returns contents of Max_txt as a double


% --- Executes during object creation, after setting all properties.
function Max_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Max_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PO2Interval_txt_Callback(hObject, eventdata, handles)
% hObject    handle to PO2Interval_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PO2Interval_txt as text
%        str2double(get(hObject,'String')) returns contents of PO2Interval_txt as a double


% --- Executes during object creation, after setting all properties.
function PO2Interval_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PO2Interval_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in PO2VSTime_Button.
function PO2VSTime_Button_Callback(hObject, eventdata, handles)
% hObject    handle to PO2VSTime_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

PO2Interval = str2num(get(handles.PO2Interval_txt,'String'));
NumberOfIntervals = ceil(str2num(get(handles.UntilRep_txt,'String')) /PO2Interval );
totaltime = str2num(get(handles.UntilRep_txt,'String')) * handles.ScanInfo.PO2PulsePeriod;
PointNo = str2num(get(handles.PointNo_txt, 'String'));

x=[]; PO2=[];
for i=1 : NumberOfIntervals
    Data = [];
    if i == NumberOfIntervals
        Data = mean(handles.ScanInfo.PO2data(:,(i-1)*PO2Interval+1:end, PointNo),2);
        x(i) = totaltime/1000000;
    else
        Data = mean(handles.ScanInfo.PO2data(:,(i-1)*PO2Interval+1:i*PO2Interval, PointNo),2);
        x(i) = i * PO2Interval * handles.ScanInfo.PO2PulsePeriod/1000000;
    end
    background = mean(Data(end-10:end));
    Data = Data - background;
    if handles.ProbeBatch == 1
        [~, ~, PO2(i), ~, ~, ~ , ~] = CalcPO2_2(Data, handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, str2num(get(handles.NeglectPoints_txt,'String')));        
    elseif handles.ProbeBatch == 2
        [~, ~, PO2(i), ~, ~, ~ , ~] = CalcPO2_2015(Data, handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, str2num(get(handles.NeglectPoints_txt,'String')));
    else
        h = msgbox('Fisrt chose the probe batch');
    end
    
end

axes(handles.PO2Consumption)
plot(x, PO2);
xlabel('time (s)')
ylabel('PO2 (mm Hg)')

% Update handles structure
guidata(hObject, handles);

function NeglectPoints_txt_Callback(hObject, eventdata, handles)
% hObject    handle to NeglectPoints_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of NeglectPoints_txt as text
%        str2double(get(hObject,'String')) returns contents of NeglectPoints_txt as a double


% --- Executes during object creation, after setting all properties.
function NeglectPoints_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NeglectPoints_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function UntilRep_txt_Callback(hObject, eventdata, handles)
% hObject    handle to UntilRep_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of UntilRep_txt as text
%        str2double(get(hObject,'String')) returns contents of UntilRep_txt as a double


% --- Executes during object creation, after setting all properties.
function UntilRep_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to UntilRep_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Remove_Button.
function Remove_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Remove_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

SlideNumber = int16(get(handles.slider_Hists,'Value'))+1;
handles.ScanInfo.PO2data(:,:,SlideNumber)=[];
handles.ScanInfo.ramp_x(SlideNumber) = [];
handles.ScanInfo.ramp_y(SlideNumber) = [];
handles.PO2(SlideNumber) = [];
handles.LifetimeVal(SlideNumber) = [];
handles.xx(:,SlideNumber) = [];
handles.yy(:,SlideNumber) = [];
handles.Predicted_y(:,SlideNumber) = [];
handles.R_square(SlideNumber) = [];

if size(handles.ScanInfo.PO2data,3) ~= 1
    set(handles.slider_Hists,'Max',size(handles.ScanInfo.PO2data,3)-1);
    set(handles.slider_Hists, 'SliderStep', [1/(size(handles.ScanInfo.PO2data,3)-1) 1/(size(handles.ScanInfo.PO2data,3)-1)]);
else
    set(handles.slider_Hists,'Max',0);
    set(handles.slider_Hists, 'SliderStep', [0 0]);
end
axes(handles.Graph_Hist)
if (SlideNumber-1) == 0
    set(handles.slider_Hists,'Value', 1)
    set(handles.Hists_Stext,'String', 1);
    plot(handles.timeaxis(2:end), mean(handles.ScanInfo.PO2data(2:end,:,1),2), 'LineWidth', 2.0)    
else
    set(handles.slider_Hists,'Value', SlideNumber-1)
    set(handles.Hists_Stext,'String', SlideNumber-1);
    plot(handles.timeaxis(2:end), mean(handles.ScanInfo.PO2data(2:end,:,SlideNumber-1),2), 'LineWidth', 2.0)
end

axes(handles.Graph_FitHist)
cla(handles.Graph_FitHist)
if (SlideNumber-1) == 0
    plot(handles.xx(:,1), handles.yy(:,1)); hold on %ploting the real data
    plot(handles.xx(:,1),handles.Predicted_y(:,1),'r'); % ploting the fitted curve and the confidence intervals
    legend({['Lifetime=' num2str(handles.LifetimeVal(1)) ' us, R2=' num2str(handles.R_square(1))],['PO2=' num2str(handles.PO2(1)) ' mmHg']},'FontSize',7);    
else
    plot(handles.xx(:,SlideNumber-1), handles.yy(:,SlideNumber-1)); hold on %ploting the real data
    plot(handles.xx(:,SlideNumber-1),handles.Predicted_y(:,SlideNumber-1),'r'); % ploting the fitted curve and the confidence intervals
    legend({['Lifetime=' num2str(handles.LifetimeVal(SlideNumber-1)) ' us, R2=' num2str(handles.R_square(SlideNumber-1))],['PO2=' num2str(handles.PO2(SlideNumber-1)) ' mmHg']},'FontSize',7);
end
xlabel('time (us)')
ylabel('Intensity')

% plotting the PO2 values
ColorSegments = 50;
color_value = jet(ColorSegments+1);
Interval = max(handles.PO2)/ColorSegments;
cla(handles.LivePMTPO2)
axes(handles.LivePMTPO2)
imagesc(handles.PMTImage); colormap 'gray'

hold on
for i = 1: length(handles.PO2)
%     if handles.PO2(i)<0
%         handles.PO2(i)=0;
%     end
%     ColorIndex = round(length(handles.PO2)*handles.PO2(i)/max(handles.PO2));
    ColorIndex = ceil(handles.PO2(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(handles.ScanInfo.ramp_x(i),handles.ScanInfo.ramp_y(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
%     handles.hpoint(i) = impoint(gca,handles.ScanInfo.ramp_x(i),handles.ScanInfo.ramp_y(i));
%     setColor(handles.hpoint(i),[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)]);
end
hold off
set(handles.LivePMTPO2, 'YTick', []);
set(handles.LivePMTPO2, 'XTick', []);

% Update handles structure
guidata(hObject, handles);



function txt_dimensions_Callback(hObject, eventdata, handles)
% hObject    handle to txt_dimensions (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_dimensions as text
%        str2double(get(hObject,'String')) returns contents of txt_dimensions as a double


% --- Executes during object creation, after setting all properties.
function txt_dimensions_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_dimensions (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_depth_Callback(hObject, eventdata, handles)
% hObject    handle to txt_depth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_depth as text
%        str2double(get(hObject,'String')) returns contents of txt_depth as a double


% --- Executes during object creation, after setting all properties.
function txt_depth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_depth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_xLocation_Callback(hObject, eventdata, handles)
% hObject    handle to txt_xLocation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_xLocation as text
%        str2double(get(hObject,'String')) returns contents of txt_xLocation as a double


% --- Executes during object creation, after setting all properties.
function txt_xLocation_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_xLocation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_yLocation_Callback(hObject, eventdata, handles)
% hObject    handle to txt_yLocation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_yLocation as text
%        str2double(get(hObject,'String')) returns contents of txt_yLocation as a double


% --- Executes during object creation, after setting all properties.
function txt_yLocation_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_yLocation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_acceptablesignal_min_Callback(hObject, eventdata, handles)
% hObject    handle to txt_acceptablesignal_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_acceptablesignal_min as text
%        str2double(get(hObject,'String')) returns contents of txt_acceptablesignal_min as a double


% --- Executes during object creation, after setting all properties.
function txt_acceptablesignal_min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_acceptablesignal_min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function R2_txt_Callback(hObject, eventdata, handles)
% hObject    handle to R2_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of R2_txt as text
%        str2double(get(hObject,'String')) returns contents of R2_txt as a double


% --- Executes during object creation, after setting all properties.
function R2_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to R2_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_acceptablesignal_max_Callback(hObject, eventdata, handles)
% hObject    handle to txt_acceptablesignal_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_acceptablesignal_max as text
%        str2double(get(hObject,'String')) returns contents of txt_acceptablesignal_max as a double


% --- Executes during object creation, after setting all properties.
function txt_acceptablesignal_max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_acceptablesignal_max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PointNo_txt_Callback(hObject, eventdata, handles)
% hObject    handle to PointNo_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PointNo_txt as text
%        str2double(get(hObject,'String')) returns contents of PointNo_txt as a double


% --- Executes during object creation, after setting all properties.
function PointNo_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PointNo_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function RepToUseFrom_txt_Callback(hObject, eventdata, handles)
% hObject    handle to RepToUseFrom_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of RepToUseFrom_txt as text
%        str2double(get(hObject,'String')) returns contents of RepToUseFrom_txt as a double


% --- Executes during object creation, after setting all properties.
function RepToUseFrom_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RepToUseFrom_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function RepToUseTo_txt_Callback(hObject, eventdata, handles)
% hObject    handle to RepToUseTo_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of RepToUseTo_txt as text
%        str2double(get(hObject,'String')) returns contents of RepToUseTo_txt as a double


% --- Executes during object creation, after setting all properties.
function RepToUseTo_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RepToUseTo_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CurveInteg.
function CurveInteg_Callback(hObject, eventdata, handles)
% hObject    handle to CurveInteg (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.HistPointStart = str2num(get(handles.HistPointStart_txt,'String'));
handles.HistPointEnd = str2num(get(handles.HistPointEnd_txt,'String'));
handles.RepToUse = str2num(get(handles.RepToUse_txt,'String'));
handles.SignalInteg=[];
for i = 1: length(handles.ScanInfo.ramp_x)
    handles.SignalInteg(i) = sum(mean(handles.ScanInfo.PO2data(handles.HistPointStart:handles.HistPointEnd,1:handles.RepToUse,i),2));
end

guidata(hObject, handles);

% plotting the Integral values
ColorSegments = 50;
color_value = jet(ColorSegments+1);
Interval = max(handles.SignalInteg)/ColorSegments;
cla(handles.LivePMTPO2)
axes(handles.LivePMTPO2)
imagesc(handles.PMTImage); colormap 'gray'

hold on
for i = 1: length(handles.SignalInteg)
    ColorIndex = ceil(handles.SignalInteg(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(handles.ScanInfo.ramp_x(i),handles.ScanInfo.ramp_y(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
end
hold off
set(handles.LivePMTPO2, 'YTick', []);
set(handles.LivePMTPO2, 'XTick', []);

axes(handles.PO2colorbar)
ylim([0 max(handles.SignalInteg)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(handles.SignalInteg)/ColorSegments)+(j-1)*(max(handles.SignalInteg)/ColorSegments)/BarDevisions (i-1)*(max(handles.SignalInteg)/ColorSegments)+(j-1)*(max(handles.SignalInteg)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off
set(handles.PO2colorbar, 'YTick', linspace(0,max(handles.SignalInteg),5));
set(handles.PO2colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(handles.SignalInteg),5)));   
    
% Update handles structure
guidata(hObject, handles);


function HistPointStart_txt_Callback(hObject, eventdata, handles)
% hObject    handle to HistPointStart_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of HistPointStart_txt as text
%        str2double(get(hObject,'String')) returns contents of HistPointStart_txt as a double


% --- Executes during object creation, after setting all properties.
function HistPointStart_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to HistPointStart_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function HistPointEnd_txt_Callback(hObject, eventdata, handles)
% hObject    handle to HistPointEnd_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of HistPointEnd_txt as text
%        str2double(get(hObject,'String')) returns contents of HistPointEnd_txt as a double


% --- Executes during object creation, after setting all properties.
function HistPointEnd_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to HistPointEnd_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function RepToUse_txt_Callback(hObject, eventdata, handles)
% hObject    handle to RepToUse_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of RepToUse_txt as text
%        str2double(get(hObject,'String')) returns contents of RepToUse_txt as a double


% --- Executes during object creation, after setting all properties.
function RepToUse_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to RepToUse_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes during object creation, after setting all properties.
function LivePMT_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LivePMT (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate LivePMT


% --- Executes during object creation, after setting all properties.
function Save_Button_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Save_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



function PointRep_txt_Callback(hObject, eventdata, handles)
% hObject    handle to PointRep_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PointRep_txt as text
%        str2double(get(hObject,'String')) returns contents of PointRep_txt as a double


% --- Executes during object creation, after setting all properties.
function PointRep_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PointRep_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in Popup_ProbeBatch.
function Popup_ProbeBatch_Callback(hObject, eventdata, handles)
% hObject    handle to Popup_ProbeBatch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Popup_ProbeBatch contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Popup_ProbeBatch

contents = cellstr(get(hObject,'String'));
handles.ProbeBatch = str2num(contents{get(hObject,'Value')});

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function Popup_ProbeBatch_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Popup_ProbeBatch (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
