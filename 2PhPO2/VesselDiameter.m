function VesselD = VesselDiameter(LineScan)             % scan: linescan information
                                                                % Image: Linescans
%% Defining the gaussian function for curve fitting (in order to determine the vessel width)
    fgauss = @(x,xdata)(  exp(-(xdata-x(2)).^2/2/x(1).^2) ./ (max(exp(-(xdata-x(2)).^2/2/x(1).^2))) ); %xdata: x, x(1): standard deviation, x(2): mean
                                                                                                      % normalized in a way that the maximum is 1

%% Obtaining vessel diameter 

        xdata = linspace(0,length(LineScan),length(LineScan)); % x in um ( generates a row vector y of n points linearly spaced between and including a and b)
        LineScan = lindetrend(LineScan)'; % removing the trends
        LineScan = LineScan./max(LineScan); % normalizign to make the maximum 1
        [maxx indices] = max(LineScan);  % Maximum element and the index of the maximum element
     
        try
            Xparameters = lsqcurvefit(fgauss,[1 xdata(indices)],xdata,LineScan); % Nonlinear least-squares curev fitting
        catch exception
            disp(exception.identifier);
            disp(exception.stack(1));
            Xparameters = [NaN NaN];
        end
        
        FWHM = Xparameters(1)*2*sqrt(2*log(2));% vessel diameter defined as "full width at half maximum" of the gaussian fit (in um)
        
%         if FWHM > 0.1
            VesselD = FWHM;
%         end

%% plot data and fitted gaussian curve
figure
plot(xdata, LineScan)
hold on
plot(xdata,fgauss(Xparameters,xdata),'r')
plot(VesselD, 'r'); hold off
axis([-inf inf 0 2])

end
