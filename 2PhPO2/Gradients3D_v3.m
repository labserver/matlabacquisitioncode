%%%%% Only works well if all 2D scans are 400x400 um, pmt size 500*399 %%%%%
%%%%% Need to edit to make more general %%%%%%


function varargout = Gradients3D_v3(varargin)
% GRADIENTS3D_V3 MATLAB code for Gradients3D_v3.fig
%      GRADIENTS3D_V3, by itself, creates a new GRADIENTS3D_V3 or raises the existing
%      singleton*.
%
%      H = GRADIENTS3D_V3 returns the handle to a new GRADIENTS3D_V3 or the handle to
%      the existing singleton*.
%
%      GRADIENTS3D_V3('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GRADIENTS3D_V3.M with the given input arguments.
%
%      GRADIENTS3D_V3('Property','Value',...) creates a new GRADIENTS3D_V3 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Gradients3D_v3_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Gradients3D_v3_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Gradients3D_v3

% Last Modified by GUIDE v2.5 23-Oct-2017 15:51:50

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Gradients3D_v3_OpeningFcn, ...
                   'gui_OutputFcn',  @Gradients3D_v3_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Gradients3D_v3 is made visible.
function Gradients3D_v3_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Gradients3D_v3 (see VARARGIN)

% Choose default command line output for Gradients3D_v3
handles.output = hObject;

opengl software

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Gradients3D_v3 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Gradients3D_v3_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Load_Button.
function Load_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Load_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

depths = []; MinX = []; MinY = []; handles.PMT = []; handles.XpO2 = []; handles.YpO2 = []; handles.pO2 = [];
handles.vein_x = []; handles.vein_y = []; handles.artery_x = []; handles.artery_y = []; handles.notsure_x = []; handles.notsure_y = [];

% Read 2D scans
handles.folder = uigetdir('C:\Users\moeini\Mohammad\Post Doc\project\My work\Awake PO2 imaging 2\Rest');
NoOfScans = inputdlg('Enter the numbers for 2D scans: ');
NoOfScans = str2num(NoOfScans{1});

load([handles.folder '/Results' num2str(NoOfScans(1)) '.mat']); %pO2 scans
depths(1) = Results.Depth;
MinX = - Results.ypos;
MinY = - Results.xpos;
clear Results

for i = 2 : length(NoOfScans)
    load([handles.folder '/Results' num2str(NoOfScans(i)) '.mat']); %pO2 scans
    NoOfScans(i)
    depths(i) = Results.Depth;
    if  - Results.ypos < MinX
        MinX =  - Results.ypos;
    end
    if  - Results.xpos < MinY
        MinY = - Results.xpos;
    end
    clear Results
end
handles.UniqueDepths = unique(int16(depths));

set(handles.slider,'Max',length(handles.UniqueDepths)-1);
if length(handles.UniqueDepths) ~= 1
    set(handles.slider, 'SliderStep', [1/(length(handles.UniqueDepths)-1) 1/(length(handles.UniqueDepths)-1)]);
end

for i = 1 : length(NoOfScans)
    load([handles.folder '/Results' num2str(NoOfScans(i)) '.mat']); %pO2 scans
    load([handles.folder '/image_pmt' num2str(NoOfScans(i)) '.mat']); %PMT images
    
    image_ref_pmt = image_ref_pmt / max(image_ref_pmt(:));
    image_ref_pmt = imadjust(image_ref_pmt);
    
    SurfaceX = - Results.ypos - MinX;
    SurfaceY = - Results.xpos - MinY;
    DepthIndex = find(handles.UniqueDepths==int16(depths(i)));   
%     handles.scale = (str2num(Results.ImageDimention)/400);
       
    handles.PMT(1+1000*SurfaceY : size(image_ref_pmt,1)+1000*SurfaceY ,1+1000*SurfaceX : size(image_ref_pmt,2)+1000*SurfaceX,DepthIndex) = image_ref_pmt;
    
    handles.XpO2 =  [handles.XpO2 [Results.PointX + 1000*SurfaceX  ; repmat(DepthIndex,1,length(Results.PointX))]];
    handles.YpO2 =  [handles.YpO2 [Results.PointY + 1000*SurfaceY ; repmat(DepthIndex,1,length(Results.PointY))]];
%     handles.ZpO2(:,i) = repmat(int16(depths(i))),length(Results.PointX));
    handles.pO2 =  [handles.pO2 [Results.PO2; repmat(DepthIndex,1,length(Results.PO2))]];
    clear image_ref_pmt
    clear Results
end

handles.OriginalXpO2 = handles.XpO2;
handles.OriginalYpO2 = handles.YpO2;

handles.YpO2 = handles.YpO2 + 20 ; % correcting the shift between the pO2 grid coordinate and the pmt coordinate

% Update display
cla(handles.LivePMTPO2)
axes(handles.LivePMTPO2)
imagesc(squeeze(handles.PMT(:,:,1))); colormap 'gray'
set(handles.slider,'Value',0)
set(handles.txt_slider,'String', handles.UniqueDepths(int16(get(handles.slider,'Value'))+1));
 % axis off;

% plotting the PO2 values
ColorSegments = 50;
color_value = jet(ColorSegments+1);
PO2Indices = find(handles.pO2(2,:)==1);
PO2Data = handles.pO2(1,PO2Indices);
PO2X = handles.XpO2(1,PO2Indices);
PO2Y = handles.YpO2(1,PO2Indices);
Interval = max(PO2Data)/ColorSegments;
 
hold on
for i = 1: length(PO2Data)
    ColorIndex = ceil(PO2Data(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(PO2X(i),PO2Y(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
end
hold off
set(handles.LivePMTPO2, 'YTick', []);
set(handles.LivePMTPO2, 'XTick', []);

axes(handles.PO2_Colorbar)
ylim([0 max(PO2Data)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions (i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off
set(handles.PO2_Colorbar, 'YTick', linspace(0,max(PO2Data),5));
set(handles.PO2_Colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(PO2Data),5)));  


% Update handles structure
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function Popup_AnalysisType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Popup_AnalysisType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in RefPoints_Veins.
function RefPoints_Veins_Callback(hObject, eventdata, handles)
% hObject    handle to RefPoints_Veins (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[vein_x, vein_y] = getpts(handles.LivePMTPO2);
handles.vein_x = [handles.vein_x; [vein_x repmat(int16(get(handles.slider,'Value'))+1,length(vein_x),1)]];
handles.vein_y = [handles.vein_y; [vein_y repmat(int16(get(handles.slider,'Value'))+1,length(vein_y),1)]];
axes(handles.LivePMTPO2); hold on;
plot(vein_x,vein_y,'ob')
hold off

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in PlotGradient_Button.
function PlotGradient_Button_Callback(hObject, eventdata, handles)
% hObject    handle to PlotGradient_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Closest vein
handles.SmallestVeinDistance=[];
if isempty(handles.vein_x) == 0
    for i = 1: size(handles.pO2,2)
        vein_distance = []; 
        for j = 1: size(handles.vein_x,1)
            y = (double(handles.XpO2(1,i)) - double(handles.vein_x(j,1)))^2 + (double(handles.YpO2(1,i)) - double(handles.vein_y(j,1)))^2 + double((handles.UniqueDepths(handles.XpO2(2,i)))-double(handles.UniqueDepths(handles.vein_x(j,2))))^2;
            vein_distance(j) = sqrt(y);
        end
        handles.SmallestVeinDistance(i) = min(vein_distance);
    end

    VeinGradientCurve = figure; hold on;
    plot(handles.SmallestVeinDistance,handles.pO2(1,:),'ob','MarkerSize',4)
    tredline = polyfit(handles.SmallestVeinDistance,handles.pO2(1,:),str2num(get(handles.PolyDeg_txt,'String')));
    x1 = linspace(min(handles.SmallestVeinDistance),max(handles.SmallestVeinDistance),100);
    y1 = polyval(tredline,x1);
    plot(x1,y1,'k','LineWidth',2)
    hold off
end

% Closest artery
handles.SmallestArteryDistance=[];
if isempty(handles.artery_x) == 0
    for i = 1: size(handles.pO2,2)
        artery_distance = []; 
        for j = 1: size(handles.artery_x,1)
            y = (double(handles.XpO2(1,i)) - double(handles.artery_x(j,1)))^2 + (double(handles.YpO2(1,i)) - double(handles.artery_y(j,1)))^2 + (double(handles.UniqueDepths(handles.XpO2(2,i)))-double(handles.UniqueDepths(handles.artery_x(j,2))))^2;
            artery_distance(j) = sqrt(y);
        end
        handles.SmallestArteryDistance(i) = min(artery_distance);
    end


    ArteryGradientCurve = figure; hold on;
    plot(handles.SmallestArteryDistance,handles.pO2(1,:),'or','MarkerSize',4)
    tredline = polyfit(handles.SmallestArteryDistance,handles.pO2(1,:),str2num(get(handles.PolyDeg_txt,'String')));
    x1 = linspace(min(handles.SmallestArteryDistance),max(handles.SmallestArteryDistance),100);
    y1 = polyval(tredline,x1);
    plot(x1,y1,'k','LineWidth',2)
    hold off

    % Interval curve
    [SortedDistances, Inds] = sort(handles.SmallestArteryDistance);
    SortedPO2 = handles.pO2(1,Inds);

    GroupAvgPO2 = []; n = []; GroupSD = []; GroupAvgDistance = [];
    DistanceInterval = 25;
    for i=1:ceil(SortedDistances(end)/DistanceInterval)
        if i == ceil(SortedDistances(end)/DistanceInterval)
             GroupIndices = find((i-1)*DistanceInterval <= SortedDistances & SortedDistances <= i*DistanceInterval);
        else
             GroupIndices = find((i-1)*DistanceInterval <= SortedDistances & SortedDistances < i*DistanceInterval);
        end
        GroupDistance=SortedDistances(GroupIndices);
        GroupPO2=SortedPO2(GroupIndices);
        n(i)= length(GroupPO2);
        GroupAvgPO2(i)=mean(GroupPO2);
        GroupAvgDistance(i) = mean(GroupDistance);
        GroupSD(i)=std(GroupPO2);
    end

    handles.ArteryGradInterval.n = n;
    handles.ArteryGradInterval.AvgPO2 = GroupAvgPO2;
    handles.ArteryGradInterval.AvgDistance =  GroupAvgDistance;
    handles.ArteryGradInterval.SD = GroupSD;


    DistanceIntervalFig = figure; hold on;
    errorbar(handles.ArteryGradInterval.AvgDistance, handles.ArteryGradInterval.AvgPO2,handles.ArteryGradInterval.SD./sqrt(handles.ArteryGradInterval.n), 'o','MarkerSize',4)
    xlabel('distance (um)') % x-axis label
    ylabel('pO2 (mmHg)') % y-axis label
    hold off
%     print(DistanceIntervalFig,'-dtiff', [handles.Savefolder '\ArteryGradInterval' num2str(DistanceInterval) '.tif']);
%     savefig(DistanceIntervalFig, [handles.Savefolder '\ArteryGradInterval' num2str(DistanceInterval) '.fig'])
%     close(DistanceIntervalFig);

end

% Closest notsure
handles.SmallestNotsureDistance=[];
if isempty(handles.notsure_x) == 0
    for i = 1: size(handles.pO2,2)
        notsure_distance = []; 
        for j = 1: size(handles.notsure_x,1)
            y = (double(handles.XpO2(1,i)) - double(handles.notsure_x(j,1)))^2 + (double(handles.YpO2(1,i)) - double(handles.notsure_y(j,1)))^2 + (double(handles.UniqueDepths(handles.XpO2(2,i)))-double(handles.UniqueDepths(handles.notsure_x(j,2))))^2;
            notsure_distance(j) = sqrt(y);
        end
        handles.SmallestNotsureDistance(i) = min(notsure_distance);
    end

    NotsureGradientCurve = figure; hold on;
    plot(handles.SmallestNotsureDistance,handles.pO2(1,:),'oy','MarkerSize',4)
    tredline = polyfit(handles.SmallestNotsureDistance,handles.pO2(1,:),str2num(get(handles.PolyDeg_txt,'String')));
    x1 = linspace(min(handles.SmallestNotsureDistance),max(handles.SmallestNotsureDistance),100);
    y1 = polyval(tredline,x1);
    plot(x1,y1,'k','LineWidth',2)
    hold off
end

% Closest vessel (any type)
handles.SmallestDistance=[];
if isempty(handles.SmallestVeinDistance) == 0
    if isempty(handles.SmallestArteryDistance) == 0
        if isempty(handles.SmallestNotsureDistance) == 0
            for i = 1: size(handles.pO2,2)
                handles.SmallestDistance(i) = min([handles.SmallestVeinDistance(i),handles.SmallestArteryDistance(i),handles.SmallestNotsureDistance(i)]);
            end
        else
            for i = 1: size(handles.pO2,2)
                handles.SmallestDistance(i) = min([handles.SmallestVeinDistance(i),handles.SmallestArteryDistance(i)]);
            end
        end
    else
        if isempty(handles.SmallestNotsureDistance) == 0
            for i = 1: size(handles.pO2,2)
                handles.SmallestDistance(i) = min([handles.SmallestVeinDistance(i),handles.SmallestNotsureDistance(i)]);
            end
        else
            for i = 1: size(handles.pO2,2)
                handles.SmallestDistance(i) = handles.SmallestVeinDistance(i);
            end
        end
    end
else
    if isempty(handles.SmallestArteryDistance) == 0
        if isempty(handles.SmallestNotsureDistance) == 0
            for i = 1: size(handles.pO2,2)
                handles.SmallestDistance(i) = min([handles.SmallestArteryDistance(i),handles.SmallestNotsureDistance(i)]);
            end
        else
            for i = 1: size(handles.pO2,2)
                handles.SmallestDistance(i) = handles.SmallestArteryDistance(i);
            end
        end
    else
        if isempty(handles.SmallestNotsureDistance) == 0
            for i = 1: size(handles.pO2,2)
                handles.SmallestDistance(i) = handles.SmallestNotsureDistance(i);
            end
        else
            for i = 1: size(handles.pO2,2)
                handles.SmallestDistance(i) = [];
            end
        end
    end
end

GradientCurve = figure; hold on;
plot(handles.SmallestDistance,handles.pO2(1,:),'og','MarkerSize',4)
tredline = polyfit(handles.SmallestDistance,handles.pO2(1,:),str2num(get(handles.PolyDeg_txt,'String')));
x1 = linspace(min(handles.SmallestDistance),max(handles.SmallestDistance),100);
y1 = polyval(tredline,x1);
plot(x1,y1,'k','LineWidth',2)
hold off


%%%% CMRO2 from all points

if isempty(handles.artery_x) == 0

    DistanceLimit_input = inputdlg('Distance limit (um):','How far from the vessel to consider for CMRO2? ',1,{num2str(max(handles.SmallestArteryDistance))});
    DistanceLimit = str2num(DistanceLimit_input{1});
       
    [handles.CMRO2, handles.CMRO2_x, handles.CMRO2_y, handles.CMRO2_xx, handles.CMRO2_FittedCurve] = FitCMRO2(handles.VesselDiameter, handles.SmallestArteryDistance, handles.pO2(1,:), DistanceLimit);
    
    set(handles.txt_CMRO2, 'String',  handles.CMRO2.CMRO2);
    
    VesselFig = figure;
    plot(handles.CMRO2_x,handles.CMRO2_y,'b.'); hold on;
    plot(handles.CMRO2_xx,handles.CMRO2_FittedCurve,'k','LineWidth',2);
    xlabel('distance (um)') % x-axis label
    ylabel('pO2 (mmHg)') % y-axis label
    hold off


end


%%%% CMRO2 from interval curve

if isempty(handles.artery_x) == 0

    DistanceLimit_input = inputdlg('Distance limit (um):','How far from the vessel to consider for CMRO2? ',1,{num2str(max(handles.SmallestArteryDistance))});
    DistanceLimit = str2num(DistanceLimit_input{1});
   
    [handles.CMRO2_Interval, handles.CMRO2_x_Interval, handles.CMRO2_y_Interval, handles.CMRO2_xx_Interval, handles.CMRO2_FittedCurve_Interval] = FitCMRO2(handles.VesselDiameter, handles.ArteryGradInterval.AvgDistance, handles.ArteryGradInterval.AvgPO2, DistanceLimit);
    
    set(handles.txt_CMRO2_Interval, 'String',  handles.CMRO2_Interval.CMRO2);
    
    VesselFig = figure;
    plot(handles.CMRO2_x_Interval,handles.CMRO2_y_Interval,'b.'); hold on;
    plot(handles.CMRO2_xx_Interval,handles.CMRO2_FittedCurve_Interval,'k','LineWidth',2);
    xlabel('distance (um)') % x-axis label
    ylabel('pO2 (mmHg)') % y-axis label
    hold off


end



% Update handles structure
guidata(hObject, handles);



% --- Executes on button press in Save_Button.
function Save_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Save_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Gradients = [];
handles.Savefolder = uigetdir('J:\changed\2Ph PO2\Analyzed data\');

if isempty(handles.SmallestVeinDistance)
    handles.SmallestVeinDistance = nan(1,length(handles.pO2(1,:)));
end

if isempty(handles.SmallestArteryDistance)
    handles.SmallestArteryDistance = nan(1,length(handles.pO2(1,:)));
end

if isempty(handles.SmallestNotsureDistance)
    handles.SmallestNotsureDistance = nan(1,length(handles.pO2(1,:)));
end

if isempty(handles.SmallestDistance)
    handles.SmallestDistance = nan(1,length(handles.pO2(1,:)));
end

Gradients.SuperMatrix = [handles.pO2(1,:) ; handles.XpO2(1,:) ; handles.YpO2(1,:) ; handles.XpO2(2,:) ; double(handles.UniqueDepths(handles.XpO2(2,:))) ; handles.SmallestVeinDistance ; handles.SmallestArteryDistance ; handles.SmallestNotsureDistance ; handles.SmallestDistance];
% Row 1: pO2 values
% Rows 2 and 3: pO2 x and y values
% Row 4: depth indices
% Row 5: depths
% Row 6: distance to coleset vein
% Row 7: distance to coleset artery
% Row 8: distance to coleset Notsure
% Row 9: distance to coleset vessel (any type)

DistanceFromArtery = 50;
ArteryRegion = []; VeinRegion = [];

for i = 1: size(Gradients.SuperMatrix,2)
    if ( isfinite(Gradients.SuperMatrix(6,i)) & isfinite(Gradients.SuperMatrix(7,i)) )
        if (Gradients.SuperMatrix(6,i) < Gradients.SuperMatrix(7,i) & Gradients.SuperMatrix(7,i)> DistanceFromArtery)
            VeinRegion = [VeinRegion, [Gradients.SuperMatrix(1,i);Gradients.SuperMatrix(6,i);Gradients.SuperMatrix(5,i)]];
        else
            ArteryRegion = [ArteryRegion, [Gradients.SuperMatrix(1,i);Gradients.SuperMatrix(7,i);Gradients.SuperMatrix(5,i)]];
        end
    elseif isfinite(Gradients.SuperMatrix(6,i))
        VeinRegion = [VeinRegion, [Gradients.SuperMatrix(1,i);Gradients.SuperMatrix(6,i);Gradients.SuperMatrix(5,i)]];
    elseif isfinite(Gradients.SuperMatrix(7,i))
        ArteryRegion = [ArteryRegion, [Gradients.SuperMatrix(1,i);Gradients.SuperMatrix(7,i);Gradients.SuperMatrix(5,i)]];
    end
end

Gradients.ArteryRegion = ArteryRegion;
Gradients.VeinRegion = VeinRegion;
Gradients.DistanceFromArtery = DistanceFromArtery;

Gradients.vein_x = handles.vein_x;
Gradients.vein_y = handles.vein_y;
Gradients.artery_x = handles.artery_x;
Gradients.artery_y = handles.artery_y;
Gradients.notsure_x = handles.notsure_x;
Gradients.notsure_y = handles.notsure_y;
Gradients.VesselDiameter = handles.VesselDiameter;
Gradients.CMRO2 = handles.CMRO2;
Gradients.CMRO2_Interval = handles.CMRO2_Interval;
Gradients.ArteryGradInterval = handles.ArteryGradInterval;

% Saving the PMT images
for i = 1 : length(handles.UniqueDepths)
    PMTImage = figure;
    imagesc(squeeze(handles.PMT(:,:,i))); colormap 'gray'
    set(gca, 'YTick', []);
    set(gca, 'XTick', []);
    print(PMTImage,'-dtiff', [handles.Savefolder '/PMT' num2str(i) '.tif']);
    close(PMTImage);
end


% Saving the PMT/PO2 image
for k = 1 : length(handles.UniqueDepths)
    PMTPO2Image = figure;
    hPMTPO2Image=imagesc(squeeze(handles.PMT(:,:,k)));  colormap gray; cbFITC = colorbar;
    set(gca, 'YTick', []);
    set(gca, 'XTick', []);
    colormap([gray(64);jet(64)]);
    PMTData = squeeze(handles.PMT(:,:,k));
    maxFTC = max(PMTData(:));
    minFTC = min(PMTData(:));
    ncmp=64;
    c_FITC = min(ncmp,  round(   (ncmp-1).*(squeeze(handles.PMT(:,:,k))-minFTC)./(maxFTC-minFTC)  )+1  );
    
    set(hPMTPO2Image,'CDataMapping','Direct');
    set(hPMTPO2Image,'CData',c_FITC);
    figure(PMTPO2Image);
    caxis([min(c_FITC(:)) max(c_FITC(:))]);
    set(cbFITC,'YLim',[ncmp+1 2*ncmp]);

    PO2Indices = find(handles.pO2(2,:)==k);
    PO2Data = handles.pO2(1,PO2Indices);
    PO2X = handles.XpO2(1,PO2Indices);
    PO2Y = handles.YpO2(1,PO2Indices);

    cmap1=jet(64);
    CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
    minPO2 = min(PO2Data);
    maxPO2 = max(PO2Data);
    cmapPO2idx = ones(length(PO2Data),1); % vector of colormap indexes for pO2
    if maxPO2 == minPO2,
        cmapPO2idx = cmapPO2idx.*round(CmapN/2);
    else
        cmapPO2idx = round((CmapN.*(PO2Data-minPO2)+maxPO2-PO2Data)./(maxPO2-minPO2) );
        cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
        cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
    end;

    for i = 1: length(PO2Data)
         colMap = squeeze(cmap1(cmapPO2idx(i),:));
         rectangle('Position',[(PO2X(i) - 7),(PO2Y(i) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
         daspect ([1,1,1])
    end
    % hold on;
    % plot(handles.reference_x,handles.reference_y,'o','MarkerSize',10,'MarkerFaceColor','k')
    % hold off

    step = 10;         
    newTickLabels = step*ceil(minPO2/step)  : step  : step*floor(maxPO2/step);
    newTicks =( newTickLabels - (minPO2*ncmp-maxPO2)/(ncmp-1) ) .* (ncmp-1) ./ (maxPO2-minPO2);
    if numel(newTickLabels)~=0
        newTickLabelsStr=num2str(newTickLabels(1),'%i');
        for idxLab = 2:length(newTickLabels),
            newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
        end;
    end
    pause(0);
    set(cbFITC,'YTick',newTicks+ncmp);
    set(cbFITC,'YTickLabel',newTickLabelsStr,'fontsize',30);

    print(PMTPO2Image,'-dtiff', [handles.Savefolder '/PMTPO2Image'  num2str(k)  '.tif']);
    close(PMTPO2Image);
end


% Saving the PMT/PO2/Vessels image
for k = 1 : length(handles.UniqueDepths)
    PMTPO2Image = figure;
    hPMTPO2Image=imagesc(squeeze(handles.PMT(:,:,k)));  colormap gray; cbFITC = colorbar;
    set(gca, 'YTick', []);
    set(gca, 'XTick', []);
    colormap([gray(64);jet(64)]);
    PMTData = squeeze(handles.PMT(:,:,k));
    maxFTC = max(PMTData(:));
    minFTC = min(PMTData(:));
    ncmp=64;
    c_FITC = min(ncmp,  round(   (ncmp-1).*(squeeze(handles.PMT(:,:,k))-minFTC)./(maxFTC-minFTC)  )+1  );
    
    set(hPMTPO2Image,'CDataMapping','Direct');
    set(hPMTPO2Image,'CData',c_FITC);
    figure(PMTPO2Image);
    caxis([min(c_FITC(:)) max(c_FITC(:))]);
    set(cbFITC,'YLim',[ncmp+1 2*ncmp]);

    PO2Indices = find(handles.pO2(2,:)==k);
    PO2Data = handles.pO2(1,PO2Indices);
    PO2X = handles.XpO2(1,PO2Indices);
    PO2Y = handles.YpO2(1,PO2Indices);

    cmap1=jet(64);
    CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
    minPO2 = min(PO2Data);
    maxPO2 = max(PO2Data);
    cmapPO2idx = ones(length(PO2Data),1); % vector of colormap indexes for pO2
    if maxPO2 == minPO2,
        cmapPO2idx = cmapPO2idx.*round(CmapN/2);
    else
        cmapPO2idx = round((CmapN.*(PO2Data-minPO2)+maxPO2-PO2Data)./(maxPO2-minPO2) );
        cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
        cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
    end;

    for i = 1: length(PO2Data)
         colMap = squeeze(cmap1(cmapPO2idx(i),:));
         rectangle('Position',[(PO2X(i) - 7),(PO2Y(i) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
         daspect ([1,1,1])
    end
    % hold on;
    % plot(handles.reference_x,handles.reference_y,'o','MarkerSize',10,'MarkerFaceColor','k')
    % hold off

    step = 10;         
    newTickLabels = step*ceil(minPO2/step)  : step  : step*floor(maxPO2/step);
    newTicks =( newTickLabels - (minPO2*ncmp-maxPO2)/(ncmp-1) ) .* (ncmp-1) ./ (maxPO2-minPO2);
    if numel(newTickLabels)~=0
        newTickLabelsStr=num2str(newTickLabels(1),'%i');
        for idxLab = 2:length(newTickLabels),
            newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
        end;
    end
    pause(0);
    set(cbFITC,'YTick',newTicks+ncmp);
    set(cbFITC,'YTickLabel',newTickLabelsStr,'fontsize',30);

    
    hold on;
    if length(handles.vein_x) ~= 0;
        PointIndices = find(handles.vein_x(:,2)== k);
        plot(handles.vein_x(PointIndices,1),handles.vein_y(PointIndices,1),'ob');
    end
    if length(handles.artery_x) ~= 0;
        PointIndices = find(handles.artery_x(:,2)== k);
        plot(handles.artery_x(PointIndices,1),handles.artery_y(PointIndices,1),'or');
    end
    if length(handles.notsure_x) ~= 0;
        PointIndices = find(handles.notsure_x(:,2)== k);
        plot(handles.notsure_x(PointIndices,1),handles.notsure_y(PointIndices,1),'oy');
    end
    hold off
     
    print(PMTPO2Image,'-dtiff', [handles.Savefolder '/PMTPO2VesselsImage'  num2str(k)  '.tif']);
    close(PMTPO2Image);
end


% Saving the gradient curves

% Gradients.SuperMatrix(1,:): pO2 values
% Gradients.SuperMatrix(6,:): distance to coleset vein
% Gradients.SuperMatrix(7,:): distance to coleset artery
% Gradients.SuperMatrix(8,:): distance to coleset Notsure
% Gradients.SuperMatrix(9,:): distance to coleset vessel (any type)

GradientCurves = figure; hold on;
plot(Gradients.SuperMatrix(6,:),Gradients.SuperMatrix(1,:),'ob', 'MarkerSize',2)
tredline = polyfit(Gradients.SuperMatrix(6,:),Gradients.SuperMatrix(1,:),2);
x1 = linspace(min(Gradients.SuperMatrix(6,:)),max(Gradients.SuperMatrix(6,:)),100);
y1 = polyval(tredline,x1);
plot(x1,y1,'k','LineWidth',2)
legend('Distance to closest vein')
print(GradientCurves,'-dtiff', [handles.Savefolder '/GradientCurvesVein.tif']);
savefig(GradientCurves, [handles.Savefolder '/GradientCurvesVein.fig'])
close(GradientCurves);

GradientCurves = figure; hold on;
plot(Gradients.SuperMatrix(7,:),Gradients.SuperMatrix(1,:),'or', 'MarkerSize',2)
tredline = polyfit(Gradients.SuperMatrix(7,:),Gradients.SuperMatrix(1,:),2);
x1 = linspace(min(Gradients.SuperMatrix(7,:)),max(Gradients.SuperMatrix(7,:)),100);
y1 = polyval(tredline,x1);
plot(x1,y1,'k','LineWidth',2)
legend('Distance to closest artery')
print(GradientCurves,'-dtiff', [handles.Savefolder '/GradientCurvesArtery.tif']);
savefig(GradientCurves, [handles.Savefolder '/GradientCurvesArtery.fig'])
close(GradientCurves);

GradientCurves = figure; hold on;
plot(Gradients.SuperMatrix(8,:),Gradients.SuperMatrix(1,:),'o',  'MarkerEdgeColor', [1 .5 0], 'MarkerSize',2)
tredline = polyfit(Gradients.SuperMatrix(8,:),Gradients.SuperMatrix(1,:),2);
x1 = linspace(min(Gradients.SuperMatrix(8,:)),max(Gradients.SuperMatrix(8,:)),100);
y1 = polyval(tredline,x1);
plot(x1,y1,'k','LineWidth',2)
legend('Distance to closest NotSure')
print(GradientCurves,'-dtiff', [handles.Savefolder '/GradientCurvesNotSure.tif']);
% savefig(GradientCurves, [handles.Savefolder '/GradientCurvesNotSure.fig'])
close(GradientCurves);

GradientCurves = figure; hold on;
plot(Gradients.SuperMatrix(9,:),Gradients.SuperMatrix(1,:),'og', 'MarkerSize',2)
tredline = polyfit(Gradients.SuperMatrix(9,:),Gradients.SuperMatrix(1,:),2);
x1 = linspace(min(Gradients.SuperMatrix(9,:)),max(Gradients.SuperMatrix(9,:)),100);
y1 = polyval(tredline,x1);
plot(x1,y1,'k','LineWidth',2)
legend('Distance to closest vessel')
print(GradientCurves,'-dtiff', [handles.Savefolder '/GradientCurves.tif']);
% savefig(GradientCurves, [handles.Savefolder '/GradientCurves.fig'])
close(GradientCurves);


% Saving the CMRO2 fit curve (using all points)
   
VesselFig = figure;
plot(handles.CMRO2_x,handles.CMRO2_y,'b.'); hold on;
plot(handles.CMRO2_xx,handles.CMRO2_FittedCurve,'k','LineWidth',2);
xlabel('distance (um)') % x-axis label
ylabel('pO2 (mmHg)') % y-axis label
print(VesselFig,'-dtiff', [handles.Savefolder '/CMRO2.tif']);
savefig(VesselFig, [handles.Savefolder '/CMRO2.fig'])
hold off
close(VesselFig)


% Saving the CMRO2 fit curve (using interval curve)
   
VesselFig = figure;
plot(handles.CMRO2_x_Interval,handles.CMRO2_y_Interval,'b.'); hold on;
plot(handles.CMRO2_xx_Interval,handles.CMRO2_FittedCurve_Interval,'k','LineWidth',2);
xlabel('distance (um)') % x-axis label
ylabel('pO2 (mmHg)') % y-axis label
print(VesselFig,'-dtiff', [handles.Savefolder '/CMRO2_Interval.tif']);
savefig(VesselFig, [handles.Savefolder '/CMRO2_Interval.fig'])
hold off
close(VesselFig)


% Saving the arterial gradient interval curve
DistanceIntervalFig = figure; hold on;
errorbar(handles.ArteryGradInterval.AvgDistance, handles.ArteryGradInterval.AvgPO2,handles.ArteryGradInterval.SD./sqrt(handles.ArteryGradInterval.n), 'o','MarkerSize',4)
xlabel('distance (um)') % x-axis label
ylabel('pO2 (mmHg)') % y-axis label
hold off
print(DistanceIntervalFig,'-dtiff', [handles.Savefolder '\ArteryGradInterval.tif']);
savefig(DistanceIntervalFig, [handles.Savefolder '\ArteryGradInterval.fig'])
close(DistanceIntervalFig);



% save vessel wall pO2 vs depth

Inds = find(Gradients.SuperMatrix(7,:) < 10);
WallPO2.Artery.PO2 = Gradients.SuperMatrix(1,Inds);
WallPO2.Artery.Depths = Gradients.SuperMatrix(5,Inds);
[WallPO2.Artery.Depths, Inds] = sort(WallPO2.Artery.Depths);
WallPO2.Artery.PO2 = WallPO2.Artery.PO2(Inds);

Inds = find(Gradients.SuperMatrix(6,:) < 10);
WallPO2.Vein.PO2 = Gradients.SuperMatrix(1,Inds);
WallPO2.Vein.Depths = Gradients.SuperMatrix(5,Inds);
[WallPO2.Vein.Depths, Inds] = sort(WallPO2.Vein.Depths);
WallPO2.Vein.PO2 = WallPO2.Vein.PO2(Inds);

% Arterial wall pO2 versus depth
GroupAvgPO2 = []; n = []; GroupSD = []; GroupAvgDistance = [];
DistanceInterval = 50;
for i=1:ceil(WallPO2.Artery.Depths(end)/DistanceInterval)
    if i == ceil(WallPO2.Artery.Depths(end)/DistanceInterval)
         GroupIndices = find((i-1)*DistanceInterval <= WallPO2.Artery.Depths & WallPO2.Artery.Depths <= i*DistanceInterval);
    else
         GroupIndices = find((i-1)*DistanceInterval <= WallPO2.Artery.Depths & WallPO2.Artery.Depths < i*DistanceInterval);
    end
    GroupDistance=WallPO2.Artery.Depths(GroupIndices);
    GroupPO2=WallPO2.Artery.PO2(GroupIndices);
    n(i)= length(GroupPO2);
    GroupAvgPO2(i)=mean(GroupPO2);
    GroupAvgDistance(i) = mean(GroupDistance);
    GroupSD(i)=std(GroupPO2);
end
DistanceIntervalFig = figure; hold on;
errorbar(GroupAvgDistance,GroupAvgPO2,GroupSD./sqrt(n), 'o','MarkerSize',4)
xlabel('depth (um)') % x-axis label
ylabel('pO2 (mmHg)') % y-axis label
hold off
print(DistanceIntervalFig,'-dtiff', [handles.Savefolder '\ArterialWallpO2' num2str(DistanceInterval) '.tif']);
savefig(DistanceIntervalFig, [handles.Savefolder '\ArterialWallpO2' num2str(DistanceInterval) '.fig'])
close(DistanceIntervalFig);

WallPO2.Artery.PO2_Depth.n = n;
WallPO2.Artery.PO2_Depth.AvgPO2 = GroupAvgPO2;
WallPO2.Artery.PO2_Depth.AvgDistance =  GroupAvgDistance;
WallPO2.Artery.PO2_Depth.SD = GroupSD;

% Venular wall pO2 versus depth
GroupAvgPO2 = []; n = []; GroupSD = []; GroupAvgDistance = [];
DistanceInterval = 50;
for i=1:ceil(WallPO2.Vein.Depths(end)/DistanceInterval)
    if i == ceil(WallPO2.Vein.Depths(end)/DistanceInterval)
         GroupIndices = find((i-1)*DistanceInterval <= WallPO2.Vein.Depths & WallPO2.Vein.Depths <= i*DistanceInterval);
    else
         GroupIndices = find((i-1)*DistanceInterval <= WallPO2.Vein.Depths & WallPO2.Vein.Depths < i*DistanceInterval);
    end
    GroupDistance=WallPO2.Vein.Depths(GroupIndices);
    GroupPO2=WallPO2.Vein.PO2(GroupIndices);
    n(i)= length(GroupPO2);
    GroupAvgPO2(i)=mean(GroupPO2);
    GroupAvgDistance(i) = mean(GroupDistance);
    GroupSD(i)=std(GroupPO2);
end
DistanceIntervalFig = figure; hold on;
errorbar(GroupAvgDistance,GroupAvgPO2,GroupSD./sqrt(n), 'o','MarkerSize',4)
xlabel('depth (um)') % x-axis label
ylabel('pO2 (mmHg)') % y-axis label
hold off
print(DistanceIntervalFig,'-dtiff', [handles.Savefolder '\VenularWallpO2' num2str(DistanceInterval) '.tif']);
savefig(DistanceIntervalFig, [handles.Savefolder '\VenularWallpO2' num2str(DistanceInterval) '.fig'])
close(DistanceIntervalFig);

WallPO2.Vein.PO2_Depth.n = n;
WallPO2.Vein.PO2_Depth.AvgPO2 = GroupAvgPO2;
WallPO2.Vein.PO2_Depth.AvgDistance =  GroupAvgDistance;
WallPO2.Vein.PO2_Depth.SD = GroupSD;

Gradients.WallPO2 = WallPO2;


% Arterial and vanular region gradients (as distance intervals)
DistanceInterval = 25;

[SortedDistancesVeinRegion,OriginalIndices]=sort(Gradients.VeinRegion(2,:));
SortedPO2VeinRegion = Gradients.VeinRegion(1,OriginalIndices);

[SortedDistancesArteryRegion,OriginalIndices]=sort(Gradients.ArteryRegion(2,:));
SortedPO2ArteryRegion = Gradients.ArteryRegion(1,OriginalIndices);


    %distance intervals, vein region
            GroupAvgPO2 = []; n = []; GroupSD = []; GroupAvgDistance = [];

                for i=1:ceil(SortedDistancesVeinRegion(end)/DistanceInterval)
                    if i == ceil(SortedDistancesVeinRegion(end)/DistanceInterval)
                        GroupIndices = find((i-1)*DistanceInterval <= SortedDistancesVeinRegion & SortedDistancesVeinRegion <= i*DistanceInterval);
                    else
                        GroupIndices = find((i-1)*DistanceInterval <= SortedDistancesVeinRegion & SortedDistancesVeinRegion < i*DistanceInterval);
                    end
                    GroupDistance=SortedDistancesVeinRegion(GroupIndices);
                    GroupPO2=SortedPO2VeinRegion(GroupIndices);
                    n(i)= length(GroupPO2);
                    GroupAvgPO2(i)=mean(GroupPO2);
                    GroupAvgDistance(i) = mean(GroupDistance);
                    GroupSD(i)=std(GroupPO2);
                end
                DistanceIntervalFig = figure; hold on;
                errorbar(GroupAvgDistance,GroupAvgPO2,GroupSD./sqrt(n), 'o','MarkerSize',4)
                xlabel('distance (um)') % x-axis label
                ylabel('pO2 (mmHg)') % y-axis label
                hold off
                print(DistanceIntervalFig,'-dtiff', [handles.Savefolder '\' num2str(DistanceInterval) 'Vein.tif']);
                savefig(DistanceIntervalFig, [handles.Savefolder '\' num2str(DistanceInterval) 'Vein.fig'])
                close(DistanceIntervalFig);


            %distance intervals, artery region
            GroupAvgPO2 = []; n = []; GroupSD = []; GroupAvgDistance = [];

                for i=1:ceil(SortedDistancesArteryRegion(end)/DistanceInterval)
                    if i == ceil(SortedDistancesArteryRegion(end)/DistanceInterval)
                        GroupIndices = find((i-1)*DistanceInterval <= SortedDistancesArteryRegion & SortedDistancesArteryRegion <= i*DistanceInterval);
                    else
                        GroupIndices = find((i-1)*DistanceInterval <= SortedDistancesArteryRegion & SortedDistancesArteryRegion < i*DistanceInterval);
                    end
                    GroupDistance=SortedDistancesArteryRegion(GroupIndices);
                    GroupPO2=SortedPO2ArteryRegion(GroupIndices);
                    n(i)= length(GroupPO2);
                    GroupAvgPO2(i)=mean(GroupPO2);
                    GroupAvgDistance(i) = mean(GroupDistance);
                    GroupSD(i)=std(GroupPO2);
                end
                DistanceIntervalFig = figure; hold on;
                errorbar(GroupAvgDistance,GroupAvgPO2,GroupSD./sqrt(n), 'or','MarkerSize',4)
                hold off
                xlabel('distance (um)') % x-axis label
                ylabel('pO2 (mmHg)') % y-axis label
                print(DistanceIntervalFig,'-dtiff', [handles.Savefolder '\' num2str(DistanceInterval) 'Artery.tif']);
                savefig(DistanceIntervalFig, [handles.Savefolder '\' num2str(DistanceInterval) 'Artery.fig'])
                close(DistanceIntervalFig);


% Artery and vein regions versus depth

DistanceInterval = 50;

[SortedDepthVeinRegion,OriginalIndices]=sort(Gradients.VeinRegion(3,:));
SortedPO2VeinRegion = Gradients.VeinRegion(1,OriginalIndices);

[SortedDepthArteryRegion,OriginalIndices]=sort(Gradients.ArteryRegion(3,:));
SortedPO2ArteryRegion = Gradients.ArteryRegion(1,OriginalIndices);


    %depth intervals, vein region
            GroupAvgPO2 = []; n = []; GroupSD = []; GroupAvgDistance = [];

                for i=1:ceil(SortedDepthVeinRegion(end)/DistanceInterval)
                    if i == ceil(SortedDepthVeinRegion(end)/DistanceInterval)
                        GroupIndices = find((i-1)*DistanceInterval <= SortedDepthVeinRegion & SortedDepthVeinRegion <= i*DistanceInterval);
                    else
                        GroupIndices = find((i-1)*DistanceInterval <= SortedDepthVeinRegion & SortedDepthVeinRegion < i*DistanceInterval);
                    end
                    GroupDistance=SortedDepthVeinRegion(GroupIndices);
                    GroupPO2=SortedPO2VeinRegion(GroupIndices);
                    n(i)= length(GroupPO2);
                    GroupAvgPO2(i)=mean(GroupPO2);
                    GroupAvgDistance(i) = mean(GroupDistance);
                    GroupSD(i)=std(GroupPO2);
                end
                DistanceIntervalFig = figure; hold on;
                errorbar(GroupAvgDistance,GroupAvgPO2,GroupSD./sqrt(n), 'o','MarkerSize',4)
                xlabel('depth (um)') % x-axis label
                ylabel('pO2 (mmHg)') % y-axis label
                hold off
                print(DistanceIntervalFig,'-dtiff', [handles.Savefolder '\' num2str(DistanceInterval) 'Vein_depth.tif']);
                savefig(DistanceIntervalFig, [handles.Savefolder '\' num2str(DistanceInterval) 'Vein_depth.fig'])
                close(DistanceIntervalFig);


            %depth intervals, artery region
            GroupAvgPO2 = []; n = []; GroupSD = []; GroupAvgDistance = [];

                for i=1:ceil(SortedDepthArteryRegion(end)/DistanceInterval)
                    if i == ceil(SortedDepthArteryRegion(end)/DistanceInterval)
                        GroupIndices = find((i-1)*DistanceInterval <= SortedDepthArteryRegion & SortedDepthArteryRegion <= i*DistanceInterval);
                    else
                        GroupIndices = find((i-1)*DistanceInterval <= SortedDepthArteryRegion & SortedDepthArteryRegion < i*DistanceInterval);
                    end
                    GroupDistance=SortedDepthArteryRegion(GroupIndices);
                    GroupPO2=SortedPO2ArteryRegion(GroupIndices);
                    n(i)= length(GroupPO2);
                    GroupAvgPO2(i)=mean(GroupPO2);
                    GroupAvgDistance(i) = mean(GroupDistance);
                    GroupSD(i)=std(GroupPO2);
                end
                DistanceIntervalFig = figure; hold on;
                errorbar(GroupAvgDistance,GroupAvgPO2,GroupSD./sqrt(n), 'or','MarkerSize',4)
                xlabel('depth (um)') % x-axis label
                ylabel('pO2 (mmHg)') % y-axis label
                hold off
                print(DistanceIntervalFig,'-dtiff', [handles.Savefolder '\' num2str(DistanceInterval) 'Artery_depth.tif']);
                savefig(DistanceIntervalFig, [handles.Savefolder '\' num2str(DistanceInterval) 'Artery_depth.fig'])
                close(DistanceIntervalFig);


save([handles.Savefolder '/Gradients.mat'],'Gradients')


function PolyDeg_txt_Callback(hObject, eventdata, handles)
% hObject    handle to PolyDeg_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PolyDeg_txt as text
%        str2double(get(hObject,'String')) returns contents of PolyDeg_txt as a double


% --- Executes during object creation, after setting all properties.
function PolyDeg_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PolyDeg_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function XOffset_txt_Callback(hObject, eventdata, handles)
% hObject    handle to XOffset_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of XOffset_txt as text
%        str2double(get(hObject,'String')) returns contents of XOffset_txt as a double
% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function XOffset_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to XOffset_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function YOffset_txt_Callback(hObject, eventdata, handles)
% hObject    handle to YOffset_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of YOffset_txt as text
%        str2double(get(hObject,'String')) returns contents of YOffset_txt as a double
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function YOffset_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YOffset_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Update_button.
function Update_button_Callback(hObject, eventdata, handles)
% hObject    handle to Update_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.slider,'Value',1);
handles.XpO2(1,:) =  handles.OriginalXpO2(1,:) + str2num(get(handles.XOffset_txt,'String')); %
handles.YpO2(1,:) =  handles.OriginalYpO2(1,:) + str2num(get(handles.YOffset_txt,'String')); %

% Update display
cla(handles.LivePMTPO2)
axes(handles.LivePMTPO2)
imagesc(squeeze(handles.PMT(:,:,1))); colormap 'gray'
set(handles.slider,'Value',0)
set(handles.txt_slider,'String', handles.UniqueDepths(int16(get(handles.slider,'Value'))+1));
 % axis off;

% plotting the PO2 values
ColorSegments = 50;
color_value = jet(ColorSegments+1);
PO2Indices = find(handles.pO2(2,:)==1);
PO2Data = handles.pO2(1,PO2Indices);
PO2X = handles.XpO2(1,PO2Indices);
PO2Y = handles.YpO2(1,PO2Indices);
Interval = max(PO2Data)/ColorSegments;
 
hold on
for i = 1: length(PO2Data)
    ColorIndex = ceil(PO2Data(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(PO2X(i),PO2Y(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
end
hold off
set(handles.LivePMTPO2, 'YTick', []);
set(handles.LivePMTPO2, 'XTick', []);

axes(handles.PO2_Colorbar)
ylim([0 max(PO2Data)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions (i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off
set(handles.PO2_Colorbar, 'YTick', linspace(0,max(PO2Data),5));
set(handles.PO2_Colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(PO2Data),5)));  

% Update handles structure
guidata(hObject, handles);


% --- Executes on slider movement.
function slider_Callback(hObject, eventdata, handles)
% hObject    handle to slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% Update display
cla(handles.LivePMTPO2)
cla(handles.PO2_Colorbar)
axes(handles.LivePMTPO2)
imagesc(squeeze(handles.PMT(:,:,(int16(get(handles.slider,'Value'))+1)))); colormap 'gray'
set(handles.txt_slider,'String', handles.UniqueDepths(int16(get(handles.slider,'Value'))+1));
set(handles.txt_slider,'String', handles.UniqueDepths(int16(get(handles.slider,'Value'))+1));
 % axis off;

% plotting the PO2 values
ColorSegments = 50;
color_value = jet(ColorSegments+1);
PO2Indices = find(handles.pO2(2,:)==(int16(get(handles.slider,'Value'))+1));
PO2Data = handles.pO2(1,PO2Indices);
PO2X = handles.XpO2(1,PO2Indices);
PO2Y = handles.YpO2(1,PO2Indices);
Interval = max(PO2Data)/ColorSegments;
 
hold on
for i = 1: length(PO2Data)
    ColorIndex = ceil(PO2Data(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(PO2X(i),PO2Y(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
end
hold off
set(handles.LivePMTPO2, 'YTick', []);
set(handles.LivePMTPO2, 'XTick', []);

axes(handles.PO2_Colorbar)
ylim([0 max(PO2Data)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions (i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off
set(handles.PO2_Colorbar, 'YTick', linspace(0,max(PO2Data),5));
set(handles.PO2_Colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(PO2Data),5)));  

axes(handles.LivePMTPO2); hold on;
if length(handles.vein_x) ~ 0;
    PointIndices = find(handles.vein_x(:,2)==(int16(get(handles.slider,'Value'))+1));
    plot(handles.vein_x(PointIndices,1),handles.vein_y(PointIndices,1),'ob');
end
if length(handles.artery_x) ~ 0;
    PointIndices = find(handles.artery_x(:,2)==(int16(get(handles.slider,'Value'))+1));
    plot(handles.artery_x(PointIndices,1),handles.artery_y(PointIndices,1),'or');
end
if length(handles.notsure_x) ~ 0;
    PointIndices = find(handles.notsure_x(:,2)==(int16(get(handles.slider,'Value'))+1));
    plot(handles.notsure_x(PointIndices,1),handles.notsure_y(PointIndices,1),'oy');
end
hold off

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in RefPoints_Arteries.
function RefPoints_Arteries_Callback(hObject, eventdata, handles)
% hObject    handle to RefPoints_Arteries (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[artery_x, artery_y] = getpts(handles.LivePMTPO2);
handles.artery_x = [handles.artery_x; [artery_x repmat(int16(get(handles.slider,'Value'))+1,length(artery_x),1)]];
handles.artery_y = [handles.artery_y; [artery_y repmat(int16(get(handles.slider,'Value'))+1,length(artery_y),1)]];
axes(handles.LivePMTPO2); hold on;
plot(artery_x,artery_y,'or')
hold off

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in RefPoints_NotSure.
function RefPoints_NotSure_Callback(hObject, eventdata, handles)
% hObject    handle to RefPoints_NotSure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[notsure_x, notsure_y] = getpts(handles.LivePMTPO2);
handles.notsure_x = [handles.notsure_x; [notsure_x repmat(int16(get(handles.slider,'Value'))+1,length(notsure_x),1)]];
handles.notsure_y = [handles.notsure_y; [notsure_y repmat(int16(get(handles.slider,'Value'))+1,length(notsure_y),1)]];
axes(handles.LivePMTPO2); hold on;
plot(notsure_x,notsure_y,'oy')
hold off

% Update handles structure
guidata(hObject, handles);



% --- Executes on button press in ClearRefPoints.
function ClearRefPoints_Callback(hObject, eventdata, handles)
% hObject    handle to ClearRefPoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.vein_x = []; handles.vein_y = []; handles.artery_x = []; handles.artery_y = []; handles.notsure_x = []; handles.notsure_y = [];

% Update display
cla(handles.LivePMTPO2)
axes(handles.LivePMTPO2)
imagesc(squeeze(handles.PMT(:,:,(int16(get(handles.slider,'Value'))+1)))); colormap 'gray'
set(handles.txt_slider,'String', handles.UniqueDepths(int16(get(handles.slider,'Value'))+1));
set(handles.txt_slider,'String', handles.UniqueDepths(int16(get(handles.slider,'Value'))+1));
 % axis off;

% plotting the PO2 values
ColorSegments = 50;
color_value = jet(ColorSegments+1);
PO2Indices = find(handles.pO2(2,:)==(int16(get(handles.slider,'Value'))+1));
PO2Data = handles.pO2(1,PO2Indices);
PO2X = handles.XpO2(1,PO2Indices);
PO2Y = handles.YpO2(1,PO2Indices);
Interval = max(PO2Data)/ColorSegments;
 
hold on
for i = 1: length(PO2Data)
    ColorIndex = ceil(PO2Data(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(PO2X(i),PO2Y(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
end
hold off
set(handles.LivePMTPO2, 'YTick', []);
set(handles.LivePMTPO2, 'XTick', []);

axes(handles.PO2_Colorbar)
ylim([0 max(PO2Data)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions (i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off
set(handles.PO2_Colorbar, 'YTick', linspace(0,max(PO2Data),5));
set(handles.PO2_Colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(PO2Data),5)));  

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in button_zoom.
function button_zoom_Callback(hObject, eventdata, handles)
% hObject    handle to button_zoom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

axes(handles.LivePMTPO2);
zoom

% Update handles structure
guidata(hObject, handles);



% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[handles.line_x, handles.line_y] = getline(handles.LivePMTPO2);
handles.VesselDiameter = sqrt((handles.line_x(2)-handles.line_x(1))^2+(handles.line_y(2)-handles.line_y(1))^2);
% handles.VesselDiameter = handles.VesselDiameter * handles.scale;
set(handles.VesselDiameter_txt, 'String',  handles.VesselDiameter);

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in RemoveRegion_Button.
function RemoveRegion_Button_Callback(hObject, eventdata, handles)
% hObject    handle to RemoveRegion_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

axes(handles.LivePMTPO2);
BW = roipoly;
ROI = find(BW == 1);
x= ceil(ROI / size(BW,1));
y= mod(ROI , size(BW,1) );

for i = 1: length(handles.XpO2)
    d=[];
    if handles.XpO2(2,i) == int16(get(handles.slider,'Value'))+1
        for j = 1: length(x)
            d(j) = (double(handles.XpO2(1,i)) - double(x(j)))^2 + (double(handles.YpO2(1,i)) - double(y(j)))^2;
            d(j) = sqrt(d(j));
        end
        distance(i) = min(d);
    else
        distance(i) = 100;
    end
end

Inds = find(distance <= 2);

handles.pO2(:,Inds)=[];
handles.XpO2(:,Inds)=[];
handles.YpO2(:,Inds)=[];
handles.OriginalXpO2(:,Inds)=[];
handles.OriginalYpO2(:,Inds)=[];


% Update display
cla(handles.LivePMTPO2)
axes(handles.LivePMTPO2)
imagesc(squeeze(handles.PMT(:,:,1))); colormap 'gray'
set(handles.slider,'Value',0)
set(handles.txt_slider,'String', handles.UniqueDepths(int16(get(handles.slider,'Value'))+1));
 % axis off;

% plotting the PO2 values
ColorSegments = 50;
color_value = jet(ColorSegments+1);
PO2Indices = find(handles.pO2(2,:)==1);
PO2Data = handles.pO2(1,PO2Indices);
PO2X = handles.XpO2(1,PO2Indices);
PO2Y = handles.YpO2(1,PO2Indices);
Interval = max(PO2Data)/ColorSegments;
 
hold on
for i = 1: length(PO2Data)
    ColorIndex = ceil(PO2Data(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(PO2X(i),PO2Y(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
end
hold off
set(handles.LivePMTPO2, 'YTick', []);
set(handles.LivePMTPO2, 'XTick', []);

axes(handles.PO2_Colorbar)
ylim([0 max(PO2Data)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions (i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off
set(handles.PO2_Colorbar, 'YTick', linspace(0,max(PO2Data),5));
set(handles.PO2_Colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(PO2Data),5)));  


% Update handles structure
guidata(hObject, handles);

