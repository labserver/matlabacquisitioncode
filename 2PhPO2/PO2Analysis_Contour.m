function PO2Analysis_Contour(FolderNumber)

opengl software

handles.ProbeBatch = 2; % 1: 2014 batch; 2: 2015 batch
handles.NeglectTime = 4;
handles.AcceptableR2 = 0.965;

handles.folder = uigetdir('C:\Users\moeini\Mohammad\Data\PO2 data\Awake 2\');
handles.Savefolder = uigetdir('C:\Users\moeini\Mohammad\Data\PO2 data\Awake 2\Analyzed data\');


for f = 1 : length(FolderNumber)

        %% Read Scan Info for analysis

        load([handles.folder '/PO2_' num2str(FolderNumber(f)) '/PO2ScanInfo_' num2str(FolderNumber(f)) '.mat']); %scan information
        handles.ScanInfo = scan;
        handles.scale = (handles.ScanInfo.scan_height/400); 
        handles.ScanInfo.ramp_y = handles.ScanInfo.ramp_y + 20 * handles.scale; % correcting the shift between the pO2 grid coordinate and the pmt coordinate      
        clear scan

        load([handles.folder '/PO2_' num2str(FolderNumber(f)) '/image_pmt.mat']); % PMT live image 
        handles.PMTImage = image_ref_pmt;
        clear image_ref_pmt
   
        handles.PMTImage(1:round(handles.scale * 50),:) = min(handles.PMTImage(:)); % remove the mirror artefact of the PMT image
        
        %% Calculating the pO2 values

        handles.PO2=[]; handles.R_square=[]; handles.LifetimeVal=[];
        for i = 1: length(handles.ScanInfo.ramp_x)
            handles.RepStart=1;
            handles.RepEnd=handles.ScanInfo.PO2Repitition * handles.ScanInfo.PointRepetition;

            if handles.ProbeBatch == 1
                [p, ~, handles.PO2(i), ~, ~, ~, handles.R_square(i)] = CalcPO2_2(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, handles.NeglectTime);
            elseif handles.ProbeBatch == 2
                [p, ~, handles.PO2(i), ~, ~, ~, handles.R_square(i)] = CalcPO2_2015(mean(handles.ScanInfo.PO2data(:,handles.RepStart:handles.RepEnd,i),2), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, handles.NeglectTime);
            end

            handles.LifetimeVal(i) = p(2);
        end
   
        %% reshaping the data
        ReshapeSize = sqrt(length(handles.PO2));
        handles.PO2 = reshape(handles.PO2, ReshapeSize, ReshapeSize);
        handles.R_square = reshape(handles.R_square, ReshapeSize, ReshapeSize);
        handles.ScanInfo.ramp_x = reshape(handles.ScanInfo.ramp_x, ReshapeSize, ReshapeSize);
        handles.ScanInfo.ramp_y = reshape(handles.ScanInfo.ramp_y, ReshapeSize, ReshapeSize);
        
% % % % % % % %         %% removing the points with weak signal
% % % % % % % %         Indices = zeros(size(handles.PO2));
% % % % % % % %         for i = 1: size(handles.PO2,1)
% % % % % % % %             for j = 1: size(handles.PO2,2)
% % % % % % % %                 if handles.PO2(i,j) > 0 && handles.PO2(i,j) < 160
% % % % % % % %                     if handles.R_square(i,j) > handles.AcceptableR2
% % % % % % % %                         Indices(i,j) = 1;
% % % % % % % %                     end
% % % % % % % %                 end
% % % % % % % %             end
% % % % % % % %         end
% % % % % % % % 
% % % % % % % %         handles.PO2(Indices == 0) = NaN;
% % % % % % % %         handles.ScanInfo.ramp_x(Indices == 0) = NaN;
% % % % % % % %         handles.ScanInfo.ramp_y(Indices == 0) = NaN;
% % % % % % % %         
% % % % % % % % 
% % % % % % % %         %% smooting the data
% % % % % % % %         
% % % % % % % %         handles.PO2 = interp2(handles.PO2,2,'spline');
% % % % % % % %         handles.ScanInfo.ramp_x = interp2(handles.ScanInfo.ramp_x,2,'spline');
% % % % % % % %         handles.ScanInfo.ramp_y = interp2(handles.ScanInfo.ramp_y,2,'spline');
       

        

        %% smooting the data

        handles.PO2 = interp2(handles.PO2,2,'spline');
        handles.ScanInfo.ramp_x = interp2(handles.ScanInfo.ramp_x,2,'spline');
        handles.ScanInfo.ramp_y = interp2(handles.ScanInfo.ramp_y,2,'spline');
         
        %% removing the points with weak signal
        Indices = zeros(size(handles.PO2));
        for i = 1: size(handles.R_square,1)
            for j = 1: size(handles.R_square,2)
                if handles.PO2(((i*2-1)*2-1),((j*2-1)*2-1)) > 0 && handles.PO2(((i*2-1)*2-1),((j*2-1)*2-1)) < 160
                    if handles.R_square(i,j) > handles.AcceptableR2
                        if i == 1
                            if j == 1
                                Indices(((i*2-1)*2-1):((i*2-1)*2-1)+2,((j*2-1)*2-1):((j*2-1)*2-1)+2) = 1;
                            elseif j == size(handles.R_square,2)
                                Indices(((i*2-1)*2-1):((i*2-1)*2-1)+2,((j*2-1)*2-1)-2:((j*2-1)*2-1)) = 1;
                            else
                                Indices(((i*2-1)*2-1):((i*2-1)*2-1)+2,((j*2-1)*2-1)-2:((j*2-1)*2-1)+2) = 1;
                            end
                        elseif i == size(handles.R_square,1)
                             if j == 1
                                Indices(((i*2-1)*2-1)-2:((i*2-1)*2-1),((j*2-1)*2-1):((j*2-1)*2-1)+2) = 1;
                            elseif j == size(handles.R_square,2)
                                Indices(((i*2-1)*2-1)-2:((i*2-1)*2-1),((j*2-1)*2-1)-2:((j*2-1)*2-1)) = 1;
                            else
                                Indices(((i*2-1)*2-1)-2:((i*2-1)*2-1),((j*2-1)*2-1)-2:((j*2-1)*2-1)+2) = 1;
                            end                               
                        elseif j == 1
                            Indices(((i*2-1)*2-1)-2:((i*2-1)*2-1)+2,((j*2-1)*2-1):((j*2-1)*2-1)+2) = 1;        
                        elseif j == size(handles.R_square,2)
                            Indices(((i*2-1)*2-1)-2:((i*2-1)*2-1)+2,((j*2-1)*2-1)-2:((j*2-1)*2-1)) = 1;            
                        else
                            Indices(((i*2-1)*2-1)-2:((i*2-1)*2-1)+2,((j*2-1)*2-1)-2:((j*2-1)*2-1)+2) = 1;
                        end
                    end
                end
            end
        end

        handles.PO2(Indices == 0) = NaN;
        handles.ScanInfo.ramp_x(Indices == 0) = NaN;
        handles.ScanInfo.ramp_y(Indices == 0) = NaN;        
        
        
        
        [FX,FY] = gradient(handles.PO2);
        
        Indices5 = find(handles.PO2 < 5);
        Indices10 = find(handles.PO2 < 10);
        
        
        %% Saving the figures
        
        mkdir([handles.Savefolder '/' num2str(FolderNumber(f))])   
        
        % Saving the PMT image
        PMTImage = figure;
        imagesc(handles.PMTImage); colormap 'gray'
        set(gca, 'YTick', []);
        set(gca, 'XTick', []);
        print(PMTImage,'-dtiff', [handles.Savefolder '/' num2str(FolderNumber(f)) '/PMT.tif']);
        close(PMTImage);

        % Saving the PMT/PO2 image
        PMTPO2Image = figure;
        hPMTPO2Image=imagesc(handles.PMTImage);  colormap gray; cbFITC = colorbar;
        set(gca, 'YTick', []);
        set(gca, 'XTick', []);
        colormap([gray(64);jet(64)]);
        maxFTC = max(handles.PMTImage(:));
        minFTC = min(handles.PMTImage(:));
        ncmp=64;
        c_FITC = min(ncmp,  round(   (ncmp-1).*(handles.PMTImage-minFTC)./(maxFTC-minFTC)  )+1  );

        set(hPMTPO2Image,'CDataMapping','Direct');
        set(hPMTPO2Image,'CData',c_FITC);
        figure(PMTPO2Image);
        caxis([min(c_FITC(:)) max(c_FITC(:))]);
        set(cbFITC,'YLim',[ncmp+1 2*ncmp]);

        cmap1=jet(64);
        CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
        minPO2 = min(handles.PO2(:));
        maxPO2 = max(handles.PO2(:));
        cmapPO2idx = ones(size(handles.PO2)); % vector of colormap indexes for pO2
        if maxPO2 == minPO2,
            cmapPO2idx = cmapPO2idx.*round(CmapN/2);
        else
            cmapPO2idx = round(   (  CmapN.*(handles.PO2-minPO2)+maxPO2-handles.PO2  )    ./(maxPO2-minPO2) );
            cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
            cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
        end;

        for i = 1: size(handles.PO2,1)
            for j = 1: size(handles.PO2,2)
                 if isfinite(handles.PO2(i,j))
                     colMap = squeeze(cmap1(cmapPO2idx(i,j),:));
                     rectangle('Position',[(handles.ScanInfo.ramp_x(i,j) - 7),(handles.ScanInfo.ramp_y(i,j) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
                     daspect ([1,1,1])
                 end
            end
        end
        
        step = 10;         
        newTickLabels = step*ceil(minPO2/step)  : step  : step*floor(maxPO2/step);
        newTicks =( newTickLabels - (minPO2*ncmp-maxPO2)/(ncmp-1) ) .* (ncmp-1) ./ (maxPO2-minPO2);
        newTickLabelsStr=num2str(newTickLabels(1),'%i');
        for idxLab = 2:length(newTickLabels),
            newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
        end;
        pause(0);
        set(cbFITC,'YTick',newTicks+ncmp);
        set(cbFITC,'YTickLabel',newTickLabelsStr, 'fontsize',30);

        print(PMTPO2Image,'-dtiff', [handles.Savefolder '/' num2str(FolderNumber(f)) '/PMTPO2Image.tif']);
        close(PMTPO2Image);


        % Saving the PMT/PO2 image scale fixed 0-70 mmHg
        PMTPO2Image = figure;
        hPMTPO2Image=imagesc(handles.PMTImage);  colormap gray; cbFITC = colorbar;
        set(gca, 'YTick', []);
        set(gca, 'XTick', []);
        colormap([gray(64);jet(64)]);
        maxFTC = max(handles.PMTImage(:));
        minFTC = min(handles.PMTImage(:));
        ncmp=64;
        c_FITC = min(ncmp,  round(   (ncmp-1).*(handles.PMTImage-minFTC)./(maxFTC-minFTC)  )+1  );

        set(hPMTPO2Image,'CDataMapping','Direct');
        set(hPMTPO2Image,'CData',c_FITC);
        figure(PMTPO2Image);
        caxis([min(c_FITC(:)) max(c_FITC(:))]);
        set(cbFITC,'YLim',[ncmp+1 2*ncmp]);

        cmap1=jet(64);
        CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
        minPO2 = 0;
        maxPO2 = 70;
        cmapPO2idx = ones(size(handles.PO2)); % vector of colormap indexes for pO2
        if maxPO2 == minPO2,
            cmapPO2idx = cmapPO2idx.*round(CmapN/2);
        else
            cmapPO2idx = round(   (  CmapN.*(handles.PO2-minPO2)+maxPO2-handles.PO2  )    ./(maxPO2-minPO2) );
            cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
            cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
        end;

        for i = 1: size(handles.PO2,1)
            for j = 1: size(handles.PO2,2)
                if isfinite(handles.PO2(i,j))
                     colMap = squeeze(cmap1(cmapPO2idx(i,j),:));
                     rectangle('Position',[(handles.ScanInfo.ramp_x(i,j) - 7),(handles.ScanInfo.ramp_y(i,j) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
                     daspect ([1,1,1])
                end
            end
        end

        step = 20;         
        newTickLabels = step*ceil(minPO2/step)  : step  : step*floor(maxPO2/step);
        newTicks =( newTickLabels - (minPO2*ncmp-maxPO2)/(ncmp-1) ) .* (ncmp-1) ./ (maxPO2-minPO2);
        newTickLabelsStr=num2str(newTickLabels(1),'%i');
        for idxLab = 2:length(newTickLabels),
            newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
        end;
        pause(0);
        set(cbFITC,'YTick',newTicks+ncmp);
        set(cbFITC,'YTickLabel',newTickLabelsStr, 'fontsize',30);

        print(PMTPO2Image,'-dtiff', [handles.Savefolder '/' num2str(FolderNumber(f)) '/PMTPO2Image0-60.tif']);
        close(PMTPO2Image);


        % saving the hypoxic image (pO2 < 5)
        
        if length(Indices5) > 0
            PMTPO2Image = figure;
            imagesc(handles.PMTImage);  colormap gray;
            set(gca, 'YTick', []);
            set(gca, 'XTick', []);
            freezeColors
            
            PO2 = handles.PO2(Indices5);
            x = handles.ScanInfo.ramp_x(Indices5);
            y = handles.ScanInfo.ramp_y(Indices5);

            for i = 1: size(PO2,1)
                for j = 1: size(PO2,2)
                     rectangle('Position',[(x(i,j) - 7),(y(i,j) - 7), 14, 14],'Curvature',[0,0],'FaceColor','b','LineStyle','none');
                     daspect ([1,1,1]) 
                end
            end

            print(PMTPO2Image,'-dtiff', [handles.Savefolder '/' num2str(FolderNumber(f)) '/Hypoxic5.tif']);
            close(PMTPO2Image);
        end
        
        
        
        % saving the hypoxic image (pO2 < 10)
        
        if length(Indices10) > 0
            PMTPO2Image = figure;
            imagesc(handles.PMTImage);  colormap gray;
            set(gca, 'YTick', []);
            set(gca, 'XTick', []);
            freezeColors
            
            PO2 = handles.PO2(Indices10);
            x = handles.ScanInfo.ramp_x(Indices10);
            y = handles.ScanInfo.ramp_y(Indices10);

            for i = 1: size(PO2,1)
                for j = 1: size(PO2,2)
                     rectangle('Position',[(x(i,j) - 7),(y(i,j) - 7), 14, 14],'Curvature',[0,0],'FaceColor','b','LineStyle','none');
                     daspect ([1,1,1]) 
                end
            end

            print(PMTPO2Image,'-dtiff', [handles.Savefolder '/' num2str(FolderNumber(f)) '/Hypoxic10.tif']);
            close(PMTPO2Image);
        end        
           
        
   
   
        PMTContour = figure;
        imagesc(handles.PMTImage); hold on;
        colormap  gray;
        freezeColors     
        contour(handles.ScanInfo.ramp_x,handles.ScanInfo.ramp_y, handles.PO2, 15, 'LineWidth', 1.5);hold on; quiver(handles.ScanInfo.ramp_x(1:4:end,1:4:end),handles.ScanInfo.ramp_y(1:4:end,1:4:end),-FX(1:4:end,1:4:end),-FY(1:4:end,1:4:end),1.3,'w','LineWidth',1.0); hold off;
        colormap jet;
        caxis([min(handles.PO2(:)),max(handles.PO2(:))])
        freezeColors
        colorbar
        set(gca, 'YTick', []);
        set(gca, 'XTick', []);
        print(PMTContour,'-dtiff', [handles.Savefolder '/' num2str(FolderNumber(f)) '/PMTContour.tif']);
        savefig(PMTContour, [handles.Savefolder '/' num2str(FolderNumber(f)) '/PMTContour.fig'])
        close(PMTContour);
        
        
        PMTContour = figure;
        imagesc(handles.PMTImage); hold on;
        colormap  gray;
        freezeColors     
        contour(handles.ScanInfo.ramp_x,handles.ScanInfo.ramp_y, handles.PO2, 15, 'LineWidth', 1.5);hold on; quiver(handles.ScanInfo.ramp_x(1:4:end,1:4:end),handles.ScanInfo.ramp_y(1:4:end,1:4:end),-FX(1:4:end,1:4:end),-FY(1:4:end,1:4:end),1.3,'w','LineWidth',1.0); hold off;
        colormap jet;
        caxis([0,70])
        freezeColors
        colorbar
        set(gca, 'YTick', []);
        set(gca, 'XTick', []);
        print(PMTContour,'-dtiff', [handles.Savefolder '/' num2str(FolderNumber(f)) '/PMTContour0-70.tif']);
        savefig(PMTContour, [handles.Savefolder '/' num2str(FolderNumber(f)) '/PMTContour0-70.fig'])
        close(PMTContour);        
        
        
        PMTContourf = figure;
        imagesc(handles.PMTImage); hold on;
        colormap  gray;
        freezeColors
        [c,h] = contourf(handles.ScanInfo.ramp_x,handles.ScanInfo.ramp_y, handles.PO2, 15);hold on; quiver(handles.ScanInfo.ramp_x(1:4:end,1:4:end),handles.ScanInfo.ramp_y(1:4:end,1:4:end),-FX(1:4:end,1:4:end),-FY(1:4:end,1:4:end),1.3,'k','LineWidth',1.0); hold off;
        colormap jet;
        caxis([min(handles.PO2(:)),max(handles.PO2(:))])     
        freezeColors
        colorbar
        set(gca, 'YTick', []);
        set(gca, 'XTick', []);
        print(PMTContourf,'-dtiff', [handles.Savefolder '/' num2str(FolderNumber(f)) '/PMTContourf.tif']);
        savefig(PMTContourf, [handles.Savefolder '/' num2str(FolderNumber(f)) '/PMTContourf.fig'])
        close(PMTContourf);       

        
        PMTContourf = figure;
        imagesc(handles.PMTImage); hold on;
        colormap  gray;
        freezeColors
        contourf(handles.ScanInfo.ramp_x,handles.ScanInfo.ramp_y, handles.PO2, 15);hold on; quiver(handles.ScanInfo.ramp_x(1:4:end,1:4:end),handles.ScanInfo.ramp_y(1:4:end,1:4:end),-FX(1:4:end,1:4:end),-FY(1:4:end,1:4:end),1.3,'k','LineWidth',1.0); hold off;
        colormap jet;
        caxis([0,70])     
        freezeColors
        colorbar
        set(gca, 'YTick', []);
        set(gca, 'XTick', []);
        print(PMTContourf,'-dtiff', [handles.Savefolder '/' num2str(FolderNumber(f)) '/PMTContourf0-70.tif']);
        savefig(PMTContourf, [handles.Savefolder '/' num2str(FolderNumber(f)) '/PMTContourf0-70.fig'])
        close(PMTContourf);  
        
        
        
        SurfaceFig = figure;
        surf(handles.ScanInfo.ramp_x,handles.ScanInfo.ramp_y,handles.PO2);shading interp;
        set(gca, 'YTick', []);
        set(gca, 'XTick', []);
        savefig(SurfaceFig, [handles.Savefolder '/' num2str(FolderNumber(f)) '/SurfaceFig.fig'])
        close(SurfaceFig);         
             



  


end


end