function varargout = Stimulation(varargin)
% STIMULATION MATLAB code for Stimulation.fig
%      STIMULATION, by itself, creates a new STIMULATION or raises the existing
%      singleton*.
%
%      H = STIMULATION returns the handle to a new STIMULATION or the handle to
%      the existing singleton*.
%
%      STIMULATION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in STIMULATION.M with the given input arguments.
%
%      STIMULATION('Property','Value',...) creates a new STIMULATION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Stimulation_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Stimulation_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Stimulation

% Last Modified by GUIDE v2.5 11-Nov-2015 16:47:50

addpath('..')
addpath('../SCANIMAGE_r3.8')

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Stimulation_OpeningFcn, ...
                   'gui_OutputFcn',  @Stimulation_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Stimulation is made visible.
function Stimulation_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Stimulation (see VARARGIN)

% Choose default command line output for Stimulation
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Stimulation wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Stimulation_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in button_start.
function button_start_Callback(hObject, eventdata, handles)
% hObject    handle to button_start (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

if isfield(handles,'StimAO')
    delete(handles.StimAO);
end
                
freq_ech = str2double(get(handles.txt_freq_ech,'String'));; %sampling rate (Hz)
StimLength = str2double(get(handles.txt_StimLength,'String'));; % Stimulation length (sec)
StimFreq = str2double(get(handles.txt_StimFreq,'String'));; % Stimulation frequency (Hz)
StimPulseWidth = str2double(get(handles.txt_StimPulseWidth,'String'));; % Stimulation pulse width (ms)
Ampl = str2double(get(handles.txt_Ampl,'String'));; % V
Jitter = str2double(get(handles.txt_Jitter,'String'));; % A random time between zero and this number which will be added between the stimulation blocks (sec)
Tr = str2double(get(handles.txt_Tr,'String'));; % time between stimulation blocks
Nb = str2double(get(handles.txt_Nb,'String'));; % Stimulation block repetition

Block = zeros(freq_ech*StimLength,1);
Block(1:freq_ech/StimFreq:end) = Ampl;
Block = conv(Block,[zeros(StimPulseWidth*freq_ech/1000,1);ones(StimPulseWidth*freq_ech/1000,1)],'same');
Block(end+1) = 0;

BetweebBlocks = zeros(freq_ech * Tr,1);

Stim = [];
for i = 1: Nb
    delay = zeros(freq_ech * round(1000*Jitter*rand)/1000,1);
    Stim = [Stim; Block; BetweebBlocks; delay];
end

handles.StimAO=StimAOClass('Dev3',0,'StimAO'); %AOM
handles.StimAO.config(freq_ech);
handles.StimAO.write_signals(Stim); 
handles.StimAO.start();

% figure
% plot((1:length(Stim))/10000, Stim);

guidata(hObject, handles);





function txt_freq_ech_Callback(hObject, eventdata, handles)
% hObject    handle to txt_freq_ech (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_freq_ech as text
%        str2double(get(hObject,'String')) returns contents of txt_freq_ech as a double


% --- Executes during object creation, after setting all properties.
function txt_freq_ech_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_freq_ech (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_StimLength_Callback(hObject, eventdata, handles)
% hObject    handle to txt_StimLength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_StimLength as text
%        str2double(get(hObject,'String')) returns contents of txt_StimLength as a double


% --- Executes during object creation, after setting all properties.
function txt_StimLength_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_StimLength (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_StimFreq_Callback(hObject, eventdata, handles)
% hObject    handle to txt_StimFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_StimFreq as text
%        str2double(get(hObject,'String')) returns contents of txt_StimFreq as a double


% --- Executes during object creation, after setting all properties.
function txt_StimFreq_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_StimFreq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_StimPulseWidth_Callback(hObject, eventdata, handles)
% hObject    handle to txt_StimPulseWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_StimPulseWidth as text
%        str2double(get(hObject,'String')) returns contents of txt_StimPulseWidth as a double


% --- Executes during object creation, after setting all properties.
function txt_StimPulseWidth_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_StimPulseWidth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_Ampl_Callback(hObject, eventdata, handles)
% hObject    handle to txt_Ampl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_Ampl as text
%        str2double(get(hObject,'String')) returns contents of txt_Ampl as a double


% --- Executes during object creation, after setting all properties.
function txt_Ampl_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_Ampl (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_Jitter_Callback(hObject, eventdata, handles)
% hObject    handle to txt_Jitter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_Jitter as text
%        str2double(get(hObject,'String')) returns contents of txt_Jitter as a double


% --- Executes during object creation, after setting all properties.
function txt_Jitter_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_Jitter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_Tr_Callback(hObject, eventdata, handles)
% hObject    handle to txt_Tr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_Tr as text
%        str2double(get(hObject,'String')) returns contents of txt_Tr as a double


% --- Executes during object creation, after setting all properties.
function txt_Tr_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_Tr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function txt_Nb_Callback(hObject, eventdata, handles)
% hObject    handle to txt_Nb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of txt_Nb as text
%        str2double(get(hObject,'String')) returns contents of txt_Nb as a double


% --- Executes during object creation, after setting all properties.
function txt_Nb_CreateFcn(hObject, eventdata, handles)
% hObject    handle to txt_Nb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
