function varargout = PO2StimAnalysis_gui(varargin)
% PO2STIMANALYSIS_GUI MATLAB code for PO2StimAnalysis_gui.fig
%      PO2STIMANALYSIS_GUI, by itself, creates a new PO2STIMANALYSIS_GUI or raises the existing
%      singleton*.
%
%      H = PO2STIMANALYSIS_GUI returns the handle to a new PO2STIMANALYSIS_GUI or the handle to
%      the existing singleton*.
%
%      PO2STIMANALYSIS_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PO2STIMANALYSIS_GUI.M with the given input arguments.
%
%      PO2STIMANALYSIS_GUI('Property','Value',...) creates a new PO2STIMANALYSIS_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PO2StimAnalysis_gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PO2StimAnalysis_gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PO2StimAnalysis_gui

% Last Modified by GUIDE v2.5 26-Apr-2016 16:09:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PO2StimAnalysis_gui_OpeningFcn, ...
                   'gui_OutputFcn',  @PO2StimAnalysis_gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PO2StimAnalysis_gui is made visible.
function PO2StimAnalysis_gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PO2StimAnalysis_gui (see VARARGIN)

% Choose default command line output for PO2StimAnalysis_gui
handles.output = hObject;

handles.ShowAOM = 0;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes PO2StimAnalysis_gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = PO2StimAnalysis_gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in button_Load.
function button_Load_Callback(hObject, eventdata, handles)
% hObject    handle to button_Load (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[fname pname] = uigetfile('*.mat','Select the PO2 data','C:\Users\moeini\Mohammad\Data\PO2 data\Awake');
load([pname fname],'-mat'); 
handles.ScanInfo = scan;
clear scan
load([pname 'image_pmt.mat']); % PMT live image 
handles.PMTImage = image_ref_pmt;
clear image_ref_pmt

[fname pname] = uigetfile('*.mat','Select the synchronization data','C:\Users\moeini\Mohammad\Data\PO2 data\Awake');
load([pname fname],'-mat'); 
handles.stim= aux(:,1); % stimulation
handles.AOMPulse = aux(:,2); % AOM
clear aux

SampFreq = str2num(get(handles.SampFreq_txt,'string'));
StimFreq = str2num(get(handles.StimFreq_txt,'string'));

set(handles.DecayRep_txt,'String', handles.ScanInfo.PO2Repitition);
set(handles.PointRep_txt,'String', handles.ScanInfo.PointRepetition);
set(handles.TotalDecays_txt,'String', handles.ScanInfo.PO2Repitition*handles.ScanInfo.PointRepetition*length(handles.ScanInfo.ramp_x));

handles.time = (1:length(handles.stim))/SampFreq;

axes(handles.axes_stim)
plot(handles.time, handles.stim);
xlabel('time (s)')
ylabel('Stimulation')

axes(handles.axes_aomPulse)
if handles.ShowAOM
    plot(handles.time, handles.AOMPulse);
end
xlabel('time (s)')
ylabel('AOM pulse')

handles.stimBlocks = handles.stim;
HighIndices = find(handles.stimBlocks > 100);
handles.stimBlocks(HighIndices + round(SampFreq/(2*StimFreq))) = max(handles.stimBlocks);
LowIndices = find(handles.stimBlocks < 100);
handles.stimBlocks(LowIndices(round(SampFreq/(2*StimFreq))+1:end) - round(SampFreq/(2*StimFreq))) = min(handles.stimBlocks);

axes(handles.axes_stimBlocks)
plot(handles.time, handles.stimBlocks);

xlabel('time (s)')
ylabel('Stimulation Blocks')


% plot the live images and the position of the points
axes(handles.axes_PMT)
imagesc(handles.PMTImage); colormap 'gray'
hold on
for i = 1: size(handles.ScanInfo.PO2data,3)
    plot(handles.ScanInfo.ramp_x(i),handles.ScanInfo.ramp_y(i),'Marker','s') 
end
hold off
set(handles.axes_PMT, 'YTick', []);
set(handles.axes_PMT, 'XTick', []);

if size(handles.ScanInfo.PO2data,3) ~= 1
    set(handles.Point_slider,'Max',size(handles.ScanInfo.PO2data,3)-1);
    set(handles.Point_slider, 'SliderStep', [1/(size(handles.ScanInfo.PO2data,3)-1) 1/(size(handles.ScanInfo.PO2data,3)-1)]);
else
    set(handles.Point_slider,'Max',0);
    set(handles.Point_slider, 'SliderStep', [0 0]);
end
set(handles.Point_slider,'Value',0)
set(handles.PointNo_txt,'String', (int16(get(handles.Point_slider,'Value'))+1));



% Update handles structure
guidata(hObject, handles);



function StimFreq_txt_Callback(hObject, eventdata, handles)
% hObject    handle to StimFreq_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of StimFreq_txt as text
%        str2double(get(hObject,'String')) returns contents of StimFreq_txt as a double


% --- Executes during object creation, after setting all properties.
function StimFreq_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to StimFreq_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function SampFreq_txt_Callback(hObject, eventdata, handles)
% hObject    handle to SampFreq_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of SampFreq_txt as text
%        str2double(get(hObject,'String')) returns contents of SampFreq_txt as a double


% --- Executes during object creation, after setting all properties.
function SampFreq_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to SampFreq_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in button_zoom.
function button_zoom_Callback(hObject, eventdata, handles)
% hObject    handle to button_zoom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

answer = inputdlg('Select Start and End time to Zoom IN','Select Interval to Zoom IN',1,{'0   10'});
Range= str2num(answer{1});

axes(handles.axes_stim);
xlim(Range);

axes(handles.axes_stimBlocks);
xlim(Range);

axes(handles.axes_aomPulse);
xlim(Range);

axes(handles.axes_PO2);
xlim(Range);


% Update handles structure
guidata(hObject, handles);



function NoDecaysToAvg_txt_Callback(hObject, eventdata, handles)
% hObject    handle to NoDecaysToAvg_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of NoDecaysToAvg_txt as text
%        str2double(get(hObject,'String')) returns contents of NoDecaysToAvg_txt as a double


% --- Executes during object creation, after setting all properties.
function NoDecaysToAvg_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NoDecaysToAvg_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function Point_slider_Callback(hObject, eventdata, handles)
% hObject    handle to Point_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

PointNo = (int16(get(handles.Point_slider,'Value'))+1);
set(handles.PointNo_txt,'String', PointNo);

axes(handles.axes_PO2)
plot(handles.meanTime(PointNo,:),handles.PO2(PointNo,:));
% xlabel('time (s)')
ylabel('PO2 (mm Hg)')


set(handles.slider_Hists,'Value',0)
set(handles.Hists_Stext,'String', (int16(get(handles.slider_Hists,'Value'))+1));

% Ploting the first fitted curve
axes(handles.Graph_Hist)
cla(handles.Graph_Hist)
plot(handles.xx(:, PointNo, 1), handles.yy(:, PointNo, 1)); hold on %ploting the real data
plot(handles.xx(:, PointNo, 1),handles.Predicted_y(:, PointNo, 1),'r'); % ploting the fitted curve
legend({['Lifetime=' num2str(handles.Tau(PointNo, 1)) ' us, R2=' num2str(handles.R_square(PointNo, 1))], ['PO2=' num2str(handles.PO2(PointNo, 1)) ' mmHg']},'FontSize',7);
xlabel('time (us)')
ylabel('Intensity')


if isfield(handles,'SelectedPoint')
    if isvalid(handles.SelectedPoint)
        setPosition(handles.SelectedPoint, handles.ScanInfo.ramp_x(int16(get(handles.Point_slider,'Value'))+1),handles.ScanInfo.ramp_y(int16(get(handles.Point_slider,'Value'))+1));
    else
            handles.SelectedPoint=impoint(handles.axes_PMT,handles.ScanInfo.ramp_x(int16(get(handles.Point_slider,'Value'))+1),handles.ScanInfo.ramp_y(int16(get(handles.Point_slider,'Value'))+1));
    end
else
    handles.SelectedPoint=impoint(handles.axes_PMT,handles.ScanInfo.ramp_x(int16(get(handles.Point_slider,'Value'))+1),handles.ScanInfo.ramp_y(int16(get(handles.Point_slider,'Value'))+1));
end

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function Point_slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Point_slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function NeglectTime_txt_Callback(hObject, eventdata, handles)
% hObject    handle to NeglectTime_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of NeglectTime_txt as text
%        str2double(get(hObject,'String')) returns contents of NeglectTime_txt as a double


% --- Executes during object creation, after setting all properties.
function NeglectTime_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NeglectTime_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in button_Calculate.
function button_Calculate_Callback(hObject, eventdata, handles)
% hObject    handle to button_Calculate (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

dd=[];

if isfield(handles,'SelectedPoint')
    clear handles.SelectedPoint;
end

handles.ImagingRange = find(diff(handles.AOMPulse)<-1000);
bb = handles.AOMPulse(handles.ImagingRange(1):handles.ImagingRange(end));
HighIndices = find(bb > 1000);
bb(HighIndices) = max(handles.AOMPulse);
LowIndices = find(bb < 1000);
bb(LowIndices) = 0;

RisingEdgeIndices = find(diff(bb)>1000) + handles.ImagingRange(1);
RisingEdgeIndices = [handles.ImagingRange(1); RisingEdgeIndices];

set(handles.TotalDecaysDetected_txt,'String', length(RisingEdgeIndices));

aa = handles.time(RisingEdgeIndices);
aa= reshape(aa,handles.ScanInfo.PO2Repitition * length(handles.ScanInfo.ramp_x) , handles.ScanInfo.PointRepetition);
for i = 1 : length(handles.ScanInfo.ramp_x)
    cc = aa(1+(i-1)*handles.ScanInfo.PO2Repitition : i*handles.ScanInfo.PO2Repitition , :);
    dd(:,i) = reshape(cc , handles.ScanInfo.PO2Repitition * handles.ScanInfo.PointRepetition , 1);
end
handles.aomTimes = dd; % sec

handles.NoDecaysToAvg = str2num(get(handles.NoDecaysToAvg_txt,'String'));
NumberOfIntervals = ceil(handles.ScanInfo.PointRepetition * handles.ScanInfo.PO2Repitition /handles.NoDecaysToAvg );
% % % % % % % % % % % % % % totaltime = str2num(get(handles.UntilRep_txt,'String')) * handles.ScanInfo.PO2PulsePeriod;

handles.PO2 = zeros(length(handles.ScanInfo.ramp_x) , NumberOfIntervals);
handles.meanTime = zeros(length(handles.ScanInfo.ramp_x) , NumberOfIntervals);
handles.R_square = zeros(length(handles.ScanInfo.ramp_x) , NumberOfIntervals);
handles.Tau = zeros(length(handles.ScanInfo.ramp_x) , NumberOfIntervals);
handles.xx = []; handles.yy = []; handles.Predicted_y = [];

for j = 1 : length(handles.ScanInfo.ramp_x)
    x=[];
    for i=1 : NumberOfIntervals
        Data = [];
        if i == NumberOfIntervals
            Data = mean(handles.ScanInfo.PO2data(:,(i-1)*handles.NoDecaysToAvg+1:end, j),2);
            meanTime = mean(handles.aomTimes((i-1)*handles.NoDecaysToAvg+1:end , j));
            
    % % % % % % % % % % %         x(i) = totaltime/1000000;
        else
            Data = mean(handles.ScanInfo.PO2data(:,(i-1)*handles.NoDecaysToAvg+1:i*handles.NoDecaysToAvg, j),2);
            meanTime = mean(handles.aomTimes((i-1)*handles.NoDecaysToAvg+1:i*handles.NoDecaysToAvg , j));
    % % % % % % % % % %         x(i) = i * handles.NoDecaysToAvg * handles.ScanInfo.PO2PulsePeriod/1000000;
        end
        background = mean(Data(end-10:end));
        Data = Data - background;
        [p, ~, handles.PO2(j , i), ~, handles.xx(:,j , i), handles.yy(:,j , i), handles.R_square(j , i)] = CalcPO2_2(Data, handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, str2num(get(handles.NeglectTime_txt,'String')));
        handles.Predicted_y(:,j , i) = (p(1).*(exp(-handles.xx(:,j , i)./p(2))))+p(3);
        handles.Tau(j , i) = p(2);
        handles.meanTime(j , i) = meanTime;
    end
end

PointNo = (int16(get(handles.Point_slider,'Value'))+1);

axes(handles.axes_PO2)
plot(handles.meanTime(PointNo,:), handles.PO2(PointNo,:));
% xlabel('time (s)')
ylabel('PO2 (mm Hg)')


if NumberOfIntervals ~= 1
    set(handles.slider_Hists,'Max',NumberOfIntervals-1);
    set(handles.slider_Hists, 'SliderStep', [1/(NumberOfIntervals-1) 1/(NumberOfIntervals-1)]);
else
    set(handles.slider_Hists,'Max',0);
    set(handles.slider_Hists, 'SliderStep', [0 0]);
end
set(handles.slider_Hists,'Value',0)
set(handles.Hists_Stext,'String', (int16(get(handles.slider_Hists,'Value'))+1));

% Ploting the first fitted curve
axes(handles.Graph_Hist)
cla(handles.Graph_Hist)
plot(handles.xx(:, PointNo, 1), handles.yy(:, PointNo, 1)); hold on %ploting the real data
plot(handles.xx(:, PointNo, 1),handles.Predicted_y(:, PointNo, 1),'r'); % ploting the fitted curve
legend({['Lifetime=' num2str(handles.Tau(PointNo, 1)) ' us, R2=' num2str(handles.R_square(PointNo, 1))], ['PO2=' num2str(handles.PO2(PointNo, 1)) ' mmHg']},'FontSize',7);
xlabel('time (us)')
ylabel('Intensity')

guidata(hObject, handles);



% Mean PO2 values for the whole interval

handles.meanPO2=[]; handles.meanPO2_Rsquare=[];
for i = 1: length(handles.ScanInfo.ramp_x)
    [~, ~, handles.meanPO2(i), ~, ~, ~, handles.meanPO2_Rsquare(i)] = CalcPO2_2(mean(handles.ScanInfo.PO2data(:,:,i),2), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, str2num(get(handles.NeglectTime_txt,'String')));
end

% plotting the mean PO2 values
ColorSegments = 50;
color_value = jet(ColorSegments+1);
Interval = max(handles.meanPO2)/ColorSegments;
axes(handles.axes_PMT)
hold on

for i = 1: length(handles.meanPO2)
%     if handles.meanPO2(i)<0
%         handles.meanPO2(i)=0;
%     end
%     ColorIndex = round(length(handles.meanPO2)*handles.meanPO2(i)/max(handles.meanPO2));
    ColorIndex = ceil(handles.meanPO2(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(handles.ScanInfo.ramp_x(i),handles.ScanInfo.ramp_y(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
%     handles.hpoint(i) = impoint(gca,handles.ScanInfo.ramp_x(i),handles.ScanInfo.ramp_y(i));
%     setColor(handles.hpoint(i),[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)]);
end
hold off
set(handles.axes_PMT, 'YTick', []);
set(handles.axes_PMT, 'XTick', []);

axes(handles.PO2colorbar)
ylim([0 max(handles.meanPO2)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(handles.meanPO2)/ColorSegments)+(j-1)*(max(handles.meanPO2)/ColorSegments)/BarDevisions (i-1)*(max(handles.meanPO2)/ColorSegments)+(j-1)*(max(handles.meanPO2)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off
set(handles.PO2colorbar, 'YTick', linspace(0,max(handles.meanPO2),5));
set(handles.PO2colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(handles.meanPO2),5)));   


% Update handles structure
guidata(hObject, handles);


% --- Executes on slider movement.
function slider_Hists_Callback(hObject, eventdata, handles)
% hObject    handle to slider_Hists (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


PointNo = (int16(get(handles.Point_slider,'Value'))+1);
IntervalNo = (int16(get(handles.slider_Hists,'Value'))+1);
set(handles.Hists_Stext,'String', IntervalNo);

% Ploting the fitted curve
axes(handles.Graph_Hist)
cla(handles.Graph_Hist)
plot(handles.xx(:, PointNo, IntervalNo), handles.yy(:, PointNo, IntervalNo)); hold on %ploting the real data
plot(handles.xx(:, PointNo, IntervalNo),handles.Predicted_y(:, PointNo, IntervalNo),'r'); % ploting the fitted curve
legend({['Lifetime=' num2str(handles.Tau(PointNo, IntervalNo)) ' us, R2=' num2str(handles.R_square(PointNo, IntervalNo))], ['PO2=' num2str(handles.PO2(PointNo, IntervalNo)) ' mmHg']},'FontSize',7);
xlabel('time (us)')
ylabel('Intensity')

% Update handles structure
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function slider_Hists_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider_Hists (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in button_StimResponse.
function button_StimResponse_Callback(hObject, eventdata, handles)
% hObject    handle to button_StimResponse (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

PointNo = (int16(get(handles.Point_slider,'Value'))+1);
TimeBeforeStim = 4; % sec

StimStartIndices = [find(diff(handles.stimBlocks)>1000); handles.ImagingRange(end)];
set(handles.NoStimBlocks_txt,'String', length(StimStartIndices)-1);
BetweenIndices = StimStartIndices(2:end) - StimStartIndices(1:end-1);

Length_AOMindices_ji = [];
for j = 1: length(handles.ScanInfo.ramp_x)
    for i = 1:length(BetweenIndices)
        AOMindices2 = find(handles.aomTimes(: , j) >= (handles.time(StimStartIndices(i)) - TimeBeforeStim) & handles.aomTimes(: , j) <= (handles.time(StimStartIndices(i+1)-1) - TimeBeforeStim));
        Length_AOMindices_ji(j,i) = length(AOMindices2);
    end
end

DecaySum = zeros(handles.ScanInfo.PO2HistPoints , max(Length_AOMindices_ji(:)), length(handles.ScanInfo.ramp_x));
IntervalTimes = zeros(max(Length_AOMindices_ji(:)), length(handles.ScanInfo.ramp_x));

for j = 1: length(handles.ScanInfo.ramp_x)
      
    [MaxLength,Ind] = max(Length_AOMindices_ji(j,:));
    
    i = Ind;
        StartTime = handles.time(StimStartIndices(i)) - TimeBeforeStim;
        EndTime = handles.time(StimStartIndices(i+1)-1) - TimeBeforeStim;
        AOMindices = find(handles.aomTimes(: , j) >= StartTime & handles.aomTimes(: , j) <= EndTime);
        DecaySum(:,1:length(AOMindices), j) = DecaySum(:,1:length(AOMindices), j) + handles.ScanInfo.PO2data(: , AOMindices , j);
        IntervalTimes(1:length(AOMindices), j) = handles.aomTimes(AOMindices) - repmat((StartTime + TimeBeforeStim),length(AOMindices) ,1);
    
    for i = 1: Ind-1
        StartTime = handles.time(StimStartIndices(i)) - TimeBeforeStim;
        EndTime = handles.time(StimStartIndices(i+1)-1) - TimeBeforeStim;
        AOMindices = find(handles.aomTimes(: , j) >= StartTime & handles.aomTimes(: , j) <= EndTime);
%         DecaySum(:,1:length(AOMindices), j) = (DecaySum(:,1:length(AOMindices), j) + handles.ScanInfo.PO2data(: , AOMindices , j))/2.0;
        DecaySum(:,1:length(AOMindices), j) = DecaySum(:,1:length(AOMindices), j) + handles.ScanInfo.PO2data(: , AOMindices , j);
        IntervalTimes(1:length(AOMindices), j) = (IntervalTimes(1:length(AOMindices), j) + handles.aomTimes(AOMindices) - repmat((StartTime + TimeBeforeStim),length(AOMindices) ,1))/2.0;
    end
    
    for i = Ind + 1: length(StimStartIndices)-1
        StartTime = handles.time(StimStartIndices(i)) - TimeBeforeStim;
        EndTime = handles.time(StimStartIndices(i+1)-1) - TimeBeforeStim;
        AOMindices = find(handles.aomTimes(: , j) >= StartTime & handles.aomTimes(: , j) <= EndTime);
%         DecaySum(:,1:length(AOMindices), j) = (DecaySum(:,1:length(AOMindices), j) + handles.ScanInfo.PO2data(: , AOMindices , j))/2.0;
        DecaySum(:,1:length(AOMindices), j) = DecaySum(:,1:length(AOMindices), j) + handles.ScanInfo.PO2data(: , AOMindices , j);
        IntervalTimes(1:length(AOMindices), j) = (IntervalTimes(1:length(AOMindices), j) + handles.aomTimes(AOMindices) - repmat((StartTime + TimeBeforeStim),length(AOMindices) ,1))/2.0;
    end
    DecaySum(:,MaxLength+1:end, j) = NaN;
    IntervalTimes(MaxLength+1:end, j) = NaN;
end

handles.DecaySum = DecaySum;
handles.IntervalTimes = IntervalTimes;

handles.NoDecaysToAvg = str2num(get(handles.NoDecaysToAvg_txt,'String'));
NumberOfIntervals = ceil(size(DecaySum,2) /handles.NoDecaysToAvg );

handles.StimPO2 = zeros(length(handles.ScanInfo.ramp_x) , NumberOfIntervals);
handles.StimmeanTime = zeros(length(handles.ScanInfo.ramp_x) , NumberOfIntervals);
handles.StimR_square = zeros(length(handles.ScanInfo.ramp_x) , NumberOfIntervals);
% handles.xx = []; handles.yy = [];

for j = 1 : length(handles.ScanInfo.ramp_x)
    for i=1 : NumberOfIntervals
        Data = [];
        if i == NumberOfIntervals
            Data = mean(DecaySum(:,(i-1)*handles.NoDecaysToAvg+1:end, j),2);
            Time = IntervalTimes((i-1)*handles.NoDecaysToAvg+1:end , j);
        else
            Data = mean(DecaySum(:,(i-1)*handles.NoDecaysToAvg+1:i*handles.NoDecaysToAvg, j),2);
            Time = IntervalTimes((i-1)*handles.NoDecaysToAvg+1:i*handles.NoDecaysToAvg , j);
        end
        
        Ind = find(isfinite(Data));
        Data = Data(Ind);
        Ind = find(isfinite(Time));
        Time = Time(Ind);
        meanTime = mean(Time);
        
        if length(Data) > 0
            [~ , ~, handles.StimPO2(j , i), ~, ~, ~, handles.StimR_square(j , i)] = CalcPO2_2(Data, handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, str2num(get(handles.NeglectTime_txt,'String')));
            handles.StimmeanTime(j , i) = meanTime;
        else
            handles.StimPO2(j , i) = NaN;
            handles.StimR_square(j , i) = NaN;
            handles.StimmeanTime(j , i) = NaN;
        end
    end
end


figure
plot(mean(handles.StimmeanTime,1) , mean(handles.StimPO2,1))
% plot(handles.StimmeanTime(PointNo , :) , handles.StimPO2(PointNo,:))
% plot(TimeFromStim , mean(handles.StimPO2, 1))


% Mean decay curves for all points

handles.meanStimPO2 = zeros(NumberOfIntervals,1);
handles.meanStimmeanTime = zeros(NumberOfIntervals,1);
handles.meanStimR_square = zeros(NumberOfIntervals,1);

MeanDecaySum = mean(DecaySum , 3);
MeanIntervalTimes = mean(IntervalTimes , 2);
    for i=1 : NumberOfIntervals
        Data = [];
        if i == NumberOfIntervals
            Data = mean(MeanDecaySum(:,(i-1)*handles.NoDecaysToAvg+1:end),2);
            Time = MeanIntervalTimes((i-1)*handles.NoDecaysToAvg+1:end);
        else
            Data = mean(MeanDecaySum(:,(i-1)*handles.NoDecaysToAvg+1:i*handles.NoDecaysToAvg),2);
            Time = MeanIntervalTimes((i-1)*handles.NoDecaysToAvg+1:i*handles.NoDecaysToAvg);
        end
        
        Ind = find(isfinite(Data));
        Data = Data(Ind);
        Ind = find(isfinite(Time));
        Time = Time(Ind);
        meanTime = mean(Time);
        
        if length(Data) > 0
            background = mean(Data(end-10:end));
            Data = Data - background;
            [~ , ~, handles.meanStimPO2(i), ~, ~, ~, handles.meanStimR_square(i)] = CalcPO2_2(Data, handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, str2num(get(handles.NeglectTime_txt,'String')));
            handles.meanStimmeanTime(i) = meanTime;
        else
            handles.meanStimPO2(i) = NaN;
            handles.meanStimR_square(i) = NaN;
            handles.meanStimmeanTime(i) = NaN;
        end
    end

figure
plot(handles.meanStimmeanTime , handles.meanStimPO2)


% Baseline pO2 from the data before the first stimulation block
BLDecaySum = [];  handles.BLPO2 = []; handles.BLDecaySum = [];
for j = 1: length(handles.ScanInfo.ramp_x)
    BLindices = find(handles.aomTimes(: , j) < handles.time(StimStartIndices(1)));
%     length(BLindices)
    BLDecaySum = mean(handles.ScanInfo.PO2data(: , BLindices , j),2);
    handles.BLDecaySum(:,j) = BLDecaySum;
    [~ , ~, handles.BLPO2(j), ~, ~, ~, handles.BL_RSquare(j)] = CalcPO2_2(BLDecaySum, handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, str2num(get(handles.NeglectTime_txt,'String')));
end


% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in button_Save.
function button_Save_Callback(hObject, eventdata, handles)
% hObject    handle to button_Save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Results file
Results.StimPO2 = handles.StimPO2;
Results.StimR_square = handles.StimR_square;
Results.StimmeanTime = handles.StimmeanTime;
Results.meanStimPO2 = handles.meanStimPO2;
Results.meanStimR_square = handles.meanStimR_square;
Results.meanStimmeanTime = handles.meanStimmeanTime;
Results.DecaySum = handles.DecaySum;
Results.IntervalTimes = handles.IntervalTimes;
Results.NoDecaysToAvg = handles.NoDecaysToAvg;
Results.meanPO2 = handles.meanPO2;
Results.meanPO2_Rsquare = handles.meanPO2_Rsquare;
Results.BLPO2 = handles.BLPO2;
Results.BLDecaySum = handles.BLDecaySum;
Results.BL_RSquare = handles.BL_RSquare;
if isfield(handles.ScanInfo,'z_surface')
    Results.Depth = 1000*(handles.ScanInfo.z_surface - handles.ScanInfo.zpos);
end

handles.Savefolder = uigetdir('C:\Users\moeini\Desktop');
save([handles.Savefolder '/Results.mat'],'Results')            

% Response curve
ResponseFig = figure;
plot(mean(handles.StimmeanTime,1) , mean(handles.StimPO2,1))
print(ResponseFig,'-dtiff', [handles.Savefolder '/' num2str(handles.NoDecaysToAvg) '.tif']);
savefig(ResponseFig, [handles.Savefolder '/' num2str(handles.NoDecaysToAvg) '.fig'])
close(ResponseFig)

ResponseFig = figure;
plot(handles.meanStimmeanTime , handles.meanStimPO2)
print(ResponseFig,'-dtiff', [handles.Savefolder '/' num2str(handles.NoDecaysToAvg) 'Mean.tif']);
savefig(ResponseFig, [handles.Savefolder '/' num2str(handles.NoDecaysToAvg) 'Mean.fig'])
close(ResponseFig)



% Saving the PMT/PO2 image
PMTPO2Image = figure;
hPMTPO2Image=imagesc(handles.PMTImage);  colormap gray; cbFITC = colorbar;
set(gca, 'YTick', []);
set(gca, 'XTick', []);
colormap([gray(64);jet(64)]);
maxFTC = max(handles.PMTImage(:));
minFTC = min(handles.PMTImage(:));
ncmp=64;
c_FITC = min(ncmp,  round(   (ncmp-1).*(handles.PMTImage-minFTC)./(maxFTC-minFTC)  )+1  );

set(hPMTPO2Image,'CDataMapping','Direct');
set(hPMTPO2Image,'CData',c_FITC);
figure(PMTPO2Image);
caxis([min(c_FITC(:)) max(c_FITC(:))]);
set(cbFITC,'YLim',[ncmp+1 2*ncmp]);

cmap1=jet(64);
CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
minPO2 = min(handles.meanPO2);
maxPO2 = max(handles.meanPO2);

cmapPO2idx = ones(length(handles.meanPO2),1); % vector of colormap indexes for pO2
if maxPO2 == minPO2,
    cmapPO2idx = cmapPO2idx.*round(CmapN/2);
else
    cmapPO2idx = round(   (  CmapN.*(handles.meanPO2-minPO2)+maxPO2-handles.meanPO2  )    ./(maxPO2-minPO2) );
    cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
    cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
end;

for i = 1: length(handles.meanPO2)
     colMap = squeeze(cmap1(cmapPO2idx(i),:));
     rectangle('Position',[(handles.ScanInfo.ramp_x(i) - 7),(handles.ScanInfo.ramp_y(i) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
     daspect ([1,1,1])
end

step = round((maxPO2-minPO2)/5);         
newTickLabels = step*ceil(minPO2/step)  : step  : step*floor(maxPO2/step);
newTicks =( newTickLabels - (minPO2*ncmp-maxPO2)/(ncmp-1) ) .* (ncmp-1) ./ (maxPO2-minPO2);
newTickLabelsStr=num2str(newTickLabels(1),'%i');
for idxLab = 2:length(newTickLabels),
    newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
end;
pause(0);
set(cbFITC,'YTick',newTicks+ncmp);
set(cbFITC,'YTickLabel',newTickLabelsStr, 'fontsize',30);

print(PMTPO2Image,'-dtiff', [handles.Savefolder '/PMTPO2Image.tif']);
close(PMTPO2Image);


% Update handles structure
guidata(hObject, handles);




function PointRep_txt_Callback(hObject, eventdata, handles)
% hObject    handle to PointRep_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PointRep_txt as text
%        str2double(get(hObject,'String')) returns contents of PointRep_txt as a double


% --- Executes during object creation, after setting all properties.
function PointRep_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PointRep_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TotalDecays_txt_Callback(hObject, eventdata, handles)
% hObject    handle to TotalDecays_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TotalDecays_txt as text
%        str2double(get(hObject,'String')) returns contents of TotalDecays_txt as a double


% --- Executes during object creation, after setting all properties.
function TotalDecays_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TotalDecays_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function TotalDecaysDetected_txt_Callback(hObject, eventdata, handles)
% hObject    handle to TotalDecaysDetected_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of TotalDecaysDetected_txt as text
%        str2double(get(hObject,'String')) returns contents of TotalDecaysDetected_txt as a double


% --- Executes during object creation, after setting all properties.
function TotalDecaysDetected_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to TotalDecaysDetected_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function DecayRep_txt_Callback(hObject, eventdata, handles)
% hObject    handle to DecayRep_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of DecayRep_txt as text
%        str2double(get(hObject,'String')) returns contents of DecayRep_txt as a double


% --- Executes during object creation, after setting all properties.
function DecayRep_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to DecayRep_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in ShowAOMPulses.
function ShowAOMPulses_Callback(hObject, eventdata, handles)
% hObject    handle to ShowAOMPulses (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of ShowAOMPulses

handles.ShowAOM = get(hObject,'Value');

% Update handles structure
guidata(hObject, handles);



function NoStimBlocks_txt_Callback(hObject, eventdata, handles)
% hObject    handle to NoStimBlocks_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of NoStimBlocks_txt as text
%        str2double(get(hObject,'String')) returns contents of NoStimBlocks_txt as a double


% --- Executes during object creation, after setting all properties.
function NoStimBlocks_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to NoStimBlocks_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
