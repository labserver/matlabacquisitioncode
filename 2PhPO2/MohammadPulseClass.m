classdef MohammadPulseClass < hgsetget
% Test class for prototype two photon scanner DAQ control
    
    properties
        % Default values
        deviceName;
        chanID;
%         freq = 1000;
%         duration_us = 100;
    end
    properties (SetAccess=private)
        hTask;
        hChan;        
    end
    
    methods
        
        function obj = MohammadPulseClass(period, num_pulses,deviceName,channelIDs,obj)
            import dabs.ni.daqmx.*
            obj.deviceName = deviceName;
            obj.chanID = channelIDs;  
            obj.hTask = Task('LaserPulseTask');
            
            obj.hChan = obj.hTask.createCOPulseChanFreq(obj.deviceName, obj.chanID, '', 1/period, 0.5);
%             obj.hTask.cfgDigEdgeStartTrig('/Dev1/ao/StartTrigger');
%             obj.hTask.set('startTrigRetriggerable',1)
            obj.hTask.cfgImplicitTiming('DAQmx_Val_FiniteSamps', num_pulses);
        end
                    
        function start(obj)
            % Each channel must be in a single column
            obj.hTask.start();
        end
        function stop(obj)
            % Each channel must be in a single column
            obj.hTask.stop();
        end
        function delete(obj)
           delete(obj.hTask);
        end          
    end
end

