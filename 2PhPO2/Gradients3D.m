function varargout = Gradients3D(varargin)
% GRADIENTS3D MATLAB code for Gradients3D.fig
%      GRADIENTS3D, by itself, creates a new GRADIENTS3D or raises the existing
%      singleton*.
%
%      H = GRADIENTS3D returns the handle to a new GRADIENTS3D or the handle to
%      the existing singleton*.
%
%      GRADIENTS3D('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GRADIENTS3D.M with the given input arguments.
%
%      GRADIENTS3D('Property','Value',...) creates a new GRADIENTS3D or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Gradients3D_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Gradients3D_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Gradients3D

% Last Modified by GUIDE v2.5 25-Jan-2016 11:12:22

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Gradients3D_OpeningFcn, ...
                   'gui_OutputFcn',  @Gradients3D_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Gradients3D is made visible.
function Gradients3D_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Gradients3D (see VARARGIN)

% Choose default command line output for Gradients3D
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Gradients3D wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Gradients3D_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Load_Button.
function Load_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Load_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

depths = []; MinX = []; MinY = []; handles.PMT = []; handles.XpO2 = []; handles.YpO2 = []; handles.pO2 = [];
handles.vein_x = []; handles.vein_y = []; handles.artery_x = []; handles.artery_y = []; handles.notsure_x = []; handles.notsure_y = [];

% Read 2D scans
handles.folder = uigetdir('J:\changed\2Ph PO2\Analyzed data\');
NoOfScans = inputdlg('Enter the number of 2D scans: ');

load([handles.folder '/Results1.mat']); %pO2 scans
depths(1) = Results.Depth;
MinX = - Results.SurfaceY;
MinY = Results.SurfaceX;
clear Results

for i = 2 : str2num(NoOfScans{1})
    load([handles.folder '/Results' num2str(i) '.mat']); %pO2 scans
    depths(i) = Results.Depth;
    if - Results.SurfaceY < MinX
        MinX = - Results.SurfaceY;
    end
    if  Results.SurfaceX < MinY
        MinY = Results.SurfaceX;
    end
    clear Results
end
handles.UniqueDepths = unique(int16(depths));

set(handles.slider,'Max',length(handles.UniqueDepths)-1);
if length(handles.UniqueDepths) ~= 1
    set(handles.slider, 'SliderStep', [1/(length(handles.UniqueDepths)-1) 1/(length(handles.UniqueDepths)-1)]);
end

for i = 1 : str2num(NoOfScans{1})
    load([handles.folder '/Results' num2str(i) '.mat']); %pO2 scans
    load([handles.folder '/image_pmt' num2str(i) '.mat']); %PMT images
    SurfaceX = - Results.SurfaceY - MinX;
    SurfaceY = Results.SurfaceX - MinY;
    DepthIndex = find(handles.UniqueDepths==int16(depths(i)));
    
    handles.scale = (str2num(Results.ImageDimention)/400);
    handles.PMT(1+1000*SurfaceY : ceil(400*handles.scale)+1000*SurfaceY ,1+1000*SurfaceX + (1-handles.scale)*400 : ceil(399*handles.scale)+1000*SurfaceX + (1-handles.scale)*400,DepthIndex) = imresize(image_ref_pmt,handles.scale);
    
    handles.XpO2 =  [handles.XpO2 [handles.scale * Results.PointX + 1000*SurfaceX + (1-handles.scale)*400 ; repmat(DepthIndex,1,length(Results.PointX))]];
    handles.YpO2 =  [handles.YpO2 [handles.scale * Results.PointY + 1000*SurfaceY ; repmat(DepthIndex,1,length(Results.PointY))]];
%     handles.ZpO2(:,i) = repmat(int16(depths(i))),length(Results.PointX));
    handles.pO2 =  [handles.pO2 [Results.PO2; repmat(DepthIndex,1,length(Results.PO2))]];
    clear image_ref_pmt
    clear Results
end

handles.OriginalXpO2 = handles.XpO2;
handles.OriginalYpO2 = handles.YpO2;

% Update display
cla(handles.LivePMTPO2)
axes(handles.LivePMTPO2)
imagesc(squeeze(handles.PMT(:,:,1))); colormap 'gray'
set(handles.slider,'Value',0)
set(handles.txt_slider,'String', handles.UniqueDepths(int16(get(handles.slider,'Value'))+1));
 % axis off;

% plotting the PO2 values
ColorSegments = 50;
color_value = jet(ColorSegments+1);
PO2Indices = find(handles.pO2(2,:)==1);
PO2Data = handles.pO2(1,PO2Indices);
PO2X = handles.XpO2(1,PO2Indices);
PO2Y = handles.YpO2(1,PO2Indices);
Interval = max(PO2Data)/ColorSegments;
 
hold on
for i = 1: length(PO2Data)
    ColorIndex = ceil(PO2Data(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(PO2X(i),PO2Y(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
end
hold off
set(handles.LivePMTPO2, 'YTick', []);
set(handles.LivePMTPO2, 'XTick', []);

axes(handles.PO2_Colorbar)
ylim([0 max(PO2Data)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions (i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off
set(handles.PO2_Colorbar, 'YTick', linspace(0,max(PO2Data),5));
set(handles.PO2_Colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(PO2Data),5)));  


% Update handles structure
guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function Popup_AnalysisType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Popup_AnalysisType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in RefPoints_Veins.
function RefPoints_Veins_Callback(hObject, eventdata, handles)
% hObject    handle to RefPoints_Veins (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[vein_x, vein_y] = getpts(handles.LivePMTPO2);
handles.vein_x = [handles.vein_x; [vein_x repmat(int16(get(handles.slider,'Value'))+1,length(vein_x),1)]];
handles.vein_y = [handles.vein_y; [vein_y repmat(int16(get(handles.slider,'Value'))+1,length(vein_y),1)]];
axes(handles.LivePMTPO2); hold on;
plot(vein_x,vein_y,'ob')
hold off

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in PlotGradient_Button.
function PlotGradient_Button_Callback(hObject, eventdata, handles)
% hObject    handle to PlotGradient_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Closest vein
handles.SmallestVeinDistance=[];
if isempty(handles.vein_x) == 0
    for i = 1: size(handles.pO2,2)
        vein_distance = []; 
        for j = 1: size(handles.vein_x,1)
            y = (double(handles.XpO2(1,i)) - double(handles.vein_x(j,1)))^2 + (double(handles.YpO2(1,i)) - double(handles.vein_y(j,1)))^2 + double((handles.UniqueDepths(handles.XpO2(2,i)))-double(handles.UniqueDepths(handles.vein_x(j,2))))^2;
            vein_distance(j) = sqrt(y);
        end
        handles.SmallestVeinDistance(i) = min(vein_distance);
    end

    VeinGradientCurve = figure; hold on;
    plot(handles.SmallestVeinDistance,handles.pO2(1,:),'ob','MarkerSize',4)
    tredline = polyfit(handles.SmallestVeinDistance,handles.pO2(1,:),str2num(get(handles.PolyDeg_txt,'String')));
    x1 = linspace(min(handles.SmallestVeinDistance),max(handles.SmallestVeinDistance),100);
    y1 = polyval(tredline,x1);
    plot(x1,y1,'k','LineWidth',2)
    hold off
end

% Closest artery
handles.SmallestArteryDistance=[];
if isempty(handles.artery_x) == 0
    for i = 1: size(handles.pO2,2)
        artery_distance = []; 
        for j = 1: size(handles.artery_x,1)
            y = (double(handles.XpO2(1,i)) - double(handles.artery_x(j,1)))^2 + (double(handles.YpO2(1,i)) - double(handles.artery_y(j,1)))^2 + (double(handles.UniqueDepths(handles.XpO2(2,i)))-double(handles.UniqueDepths(handles.artery_x(j,2))))^2;
            artery_distance(j) = sqrt(y);
        end
        handles.SmallestArteryDistance(i) = min(artery_distance);
    end


    ArteryGradientCurve = figure; hold on;
    plot(handles.SmallestArteryDistance,handles.pO2(1,:),'or','MarkerSize',4)
    tredline = polyfit(handles.SmallestArteryDistance,handles.pO2(1,:),str2num(get(handles.PolyDeg_txt,'String')));
    x1 = linspace(min(handles.SmallestArteryDistance),max(handles.SmallestArteryDistance),100);
    y1 = polyval(tredline,x1);
    plot(x1,y1,'k','LineWidth',2)
    hold off

end

% Closest notsure
handles.SmallestNotsureDistance=[];
if isempty(handles.notsure_x) == 0
    for i = 1: size(handles.pO2,2)
        notsure_distance = []; 
        for j = 1: size(handles.notsure_x,1)
            y = (double(handles.XpO2(1,i)) - double(handles.notsure_x(j,1)))^2 + (double(handles.YpO2(1,i)) - double(handles.notsure_y(j,1)))^2 + (double(handles.UniqueDepths(handles.XpO2(2,i)))-double(handles.UniqueDepths(handles.notsure_x(j,2))))^2;
            notsure_distance(j) = sqrt(y);
        end
        handles.SmallestNotsureDistance(i) = min(notsure_distance);
    end

    NotsureGradientCurve = figure; hold on;
    plot(handles.SmallestNotsureDistance,handles.pO2(1,:),'oy','MarkerSize',4)
    tredline = polyfit(handles.SmallestNotsureDistance,handles.pO2(1,:),str2num(get(handles.PolyDeg_txt,'String')));
    x1 = linspace(min(handles.SmallestNotsureDistance),max(handles.SmallestNotsureDistance),100);
    y1 = polyval(tredline,x1);
    plot(x1,y1,'k','LineWidth',2)
    hold off
end

% Closest vessel (any type)
handles.SmallestDistance=[];
if isempty(handles.SmallestVeinDistance) == 0
    if isempty(handles.SmallestArteryDistance) == 0
        if isempty(handles.SmallestNotsureDistance) == 0
            for i = 1: size(handles.pO2,2)
                handles.SmallestDistance(i) = min([handles.SmallestVeinDistance(i),handles.SmallestArteryDistance(i),handles.SmallestNotsureDistance(i)]);
            end
        else
            for i = 1: size(handles.pO2,2)
                handles.SmallestDistance(i) = min([handles.SmallestVeinDistance(i),handles.SmallestArteryDistance(i)]);
            end
        end
    else
        if isempty(handles.SmallestNotsureDistance) == 0
            for i = 1: size(handles.pO2,2)
                handles.SmallestDistance(i) = min([handles.SmallestVeinDistance(i),handles.SmallestNotsureDistance(i)]);
            end
        else
            for i = 1: size(handles.pO2,2)
                handles.SmallestDistance(i) = handles.SmallestVeinDistance(i);
            end
        end
    end
else
    if isempty(handles.SmallestArteryDistance) == 0
        if isempty(handles.SmallestNotsureDistance) == 0
            for i = 1: size(handles.pO2,2)
                handles.SmallestDistance(i) = min([handles.SmallestArteryDistance(i),handles.SmallestNotsureDistance(i)]);
            end
        else
            for i = 1: size(handles.pO2,2)
                handles.SmallestDistance(i) = handles.SmallestArteryDistance(i);
            end
        end
    else
        if isempty(handles.SmallestNotsureDistance) == 0
            for i = 1: size(handles.pO2,2)
                handles.SmallestDistance(i) = handles.SmallestNotsureDistance(i);
            end
        else
            for i = 1: size(handles.pO2,2)
                handles.SmallestDistance(i) = [];
            end
        end
    end
end

GradientCurve = figure; hold on;
plot(handles.SmallestDistance,handles.pO2(1,:),'og','MarkerSize',4)
tredline = polyfit(handles.SmallestDistance,handles.pO2(1,:),str2num(get(handles.PolyDeg_txt,'String')));
x1 = linspace(min(handles.SmallestDistance),max(handles.SmallestDistance),100);
y1 = polyval(tredline,x1);
plot(x1,y1,'k','LineWidth',2)
hold off

% Update handles structure
guidata(hObject, handles);



% --- Executes on button press in Save_Button.
function Save_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Save_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Gradients = [];
handles.Savefolder = uigetdir('J:\changed\2Ph PO2\Analyzed data\');

if isempty(handles.SmallestVeinDistance)
    handles.SmallestVeinDistance = nan(1,length(handles.pO2(1,:)));
end

if isempty(handles.SmallestArteryDistance)
    handles.SmallestArteryDistance = nan(1,length(handles.pO2(1,:)));
end

if isempty(handles.SmallestNotsureDistance)
    handles.SmallestNotsureDistance = nan(1,length(handles.pO2(1,:)));
end

if isempty(handles.SmallestDistance)
    handles.SmallestDistance = nan(1,length(handles.pO2(1,:)));
end

Gradients.SuperMatrix = [handles.pO2(1,:) ; handles.XpO2(1,:) ; handles.YpO2(1,:) ; handles.XpO2(2,:) ; double(handles.UniqueDepths(handles.XpO2(2,:))) ; handles.SmallestVeinDistance ; handles.SmallestArteryDistance ; handles.SmallestNotsureDistance ; handles.SmallestDistance];
% Row 1: pO2 values
% Rows 2 and 3: pO2 x and y values
% Row 4: depth indices
% Row 5: depths
% Row 6: distance to coleset vein
% Row 7: distance to coleset artery
% Row 8: distance to coleset Notsure
% Row 9: distance to coleset vessel (any type)


Gradients.vein_x = handles.vein_x;
Gradients.vein_y = handles.vein_y;
Gradients.artery_x = handles.artery_x;
Gradients.artery_y = handles.artery_y;
Gradients.notsure_x = handles.notsure_x;
Gradients.notsure_y = handles.notsure_y;
Gradients.VesselDiameter = handles.VesselDiameter;

save([handles.Savefolder '/Gradients.mat'],'Gradients')

% Saving the PMT images
for i = 1 : length(handles.UniqueDepths)
    PMTImage = figure;
    imagesc(squeeze(handles.PMT(:,:,i))); colormap 'gray'
    set(gca, 'YTick', []);
    set(gca, 'XTick', []);
    print(PMTImage,'-dtiff', [handles.Savefolder '/PMT' num2str(i) '.tif']);
    close(PMTImage);
end


% Saving the PMT/PO2 image
for k = 1 : length(handles.UniqueDepths)
    PMTPO2Image = figure;
    hPMTPO2Image=imagesc(squeeze(handles.PMT(:,:,k)));  colormap gray; cbFITC = colorbar;
    set(gca, 'YTick', []);
    set(gca, 'XTick', []);
    colormap([gray(64);jet(64)]);
    PMTData = squeeze(handles.PMT(:,:,k));
    maxFTC = max(PMTData(:));
    minFTC = min(PMTData(:));
    ncmp=64;
    c_FITC = min(ncmp,  round(   (ncmp-1).*(squeeze(handles.PMT(:,:,k))-minFTC)./(maxFTC-minFTC)  )+1  );
    
    set(hPMTPO2Image,'CDataMapping','Direct');
    set(hPMTPO2Image,'CData',c_FITC);
    figure(PMTPO2Image);
    caxis([min(c_FITC(:)) max(c_FITC(:))]);
    set(cbFITC,'YLim',[ncmp+1 2*ncmp]);

    PO2Indices = find(handles.pO2(2,:)==k);
    PO2Data = handles.pO2(1,PO2Indices);
    PO2X = handles.XpO2(1,PO2Indices);
    PO2Y = handles.YpO2(1,PO2Indices);

    cmap1=jet(64);
    CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
    minPO2 = min(PO2Data);
    maxPO2 = max(PO2Data);
    cmapPO2idx = ones(length(PO2Data),1); % vector of colormap indexes for pO2
    if maxPO2 == minPO2,
        cmapPO2idx = cmapPO2idx.*round(CmapN/2);
    else
        cmapPO2idx = round((CmapN.*(PO2Data-minPO2)+maxPO2-PO2Data)./(maxPO2-minPO2) );
        cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
        cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
    end;

    for i = 1: length(PO2Data)
         colMap = squeeze(cmap1(cmapPO2idx(i),:));
         rectangle('Position',[(PO2X(i) - 7),(PO2Y(i) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
         daspect ([1,1,1])
    end
    % hold on;
    % plot(handles.reference_x,handles.reference_y,'o','MarkerSize',10,'MarkerFaceColor','k')
    % hold off

    step = 10;         
    newTickLabels = step*ceil(minPO2/step)  : step  : step*floor(maxPO2/step);
    newTicks =( newTickLabels - (minPO2*ncmp-maxPO2)/(ncmp-1) ) .* (ncmp-1) ./ (maxPO2-minPO2);
    newTickLabelsStr=num2str(newTickLabels(1),'%i');
    for idxLab = 2:length(newTickLabels),
        newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
    end;
    pause(0);
    set(cbFITC,'YTick',newTicks+ncmp);
    set(cbFITC,'YTickLabel',newTickLabelsStr,'fontsize',30);

    print(PMTPO2Image,'-dtiff', [handles.Savefolder '/PMTPO2Image'  num2str(k)  '.tif']);
    close(PMTPO2Image);
end


% Saving the PMT/PO2/Vessels image
for k = 1 : length(handles.UniqueDepths)
    PMTPO2Image = figure;
    hPMTPO2Image=imagesc(squeeze(handles.PMT(:,:,k)));  colormap gray; cbFITC = colorbar;
    set(gca, 'YTick', []);
    set(gca, 'XTick', []);
    colormap([gray(64);jet(64)]);
    PMTData = squeeze(handles.PMT(:,:,k));
    maxFTC = max(PMTData(:));
    minFTC = min(PMTData(:));
    ncmp=64;
    c_FITC = min(ncmp,  round(   (ncmp-1).*(squeeze(handles.PMT(:,:,k))-minFTC)./(maxFTC-minFTC)  )+1  );
    
    set(hPMTPO2Image,'CDataMapping','Direct');
    set(hPMTPO2Image,'CData',c_FITC);
    figure(PMTPO2Image);
    caxis([min(c_FITC(:)) max(c_FITC(:))]);
    set(cbFITC,'YLim',[ncmp+1 2*ncmp]);

    PO2Indices = find(handles.pO2(2,:)==k);
    PO2Data = handles.pO2(1,PO2Indices);
    PO2X = handles.XpO2(1,PO2Indices);
    PO2Y = handles.YpO2(1,PO2Indices);

    cmap1=jet(64);
    CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
    minPO2 = min(PO2Data);
    maxPO2 = max(PO2Data);
    cmapPO2idx = ones(length(PO2Data),1); % vector of colormap indexes for pO2
    if maxPO2 == minPO2,
        cmapPO2idx = cmapPO2idx.*round(CmapN/2);
    else
        cmapPO2idx = round((CmapN.*(PO2Data-minPO2)+maxPO2-PO2Data)./(maxPO2-minPO2) );
        cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
        cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
    end;

    for i = 1: length(PO2Data)
         colMap = squeeze(cmap1(cmapPO2idx(i),:));
         rectangle('Position',[(PO2X(i) - 7),(PO2Y(i) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
         daspect ([1,1,1])
    end
    % hold on;
    % plot(handles.reference_x,handles.reference_y,'o','MarkerSize',10,'MarkerFaceColor','k')
    % hold off

    step = 10;         
    newTickLabels = step*ceil(minPO2/step)  : step  : step*floor(maxPO2/step);
    newTicks =( newTickLabels - (minPO2*ncmp-maxPO2)/(ncmp-1) ) .* (ncmp-1) ./ (maxPO2-minPO2);
    newTickLabelsStr=num2str(newTickLabels(1),'%i');
    for idxLab = 2:length(newTickLabels),
        newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
    end;
    pause(0);
    set(cbFITC,'YTick',newTicks+ncmp);
    set(cbFITC,'YTickLabel',newTickLabelsStr,'fontsize',30);

    
    hold on;
    if length(handles.vein_x) ~= 0;
        PointIndices = find(handles.vein_x(:,2)== k);
        plot(handles.vein_x(PointIndices,1),handles.vein_y(PointIndices,1),'ob');
    end
    if length(handles.artery_x) ~= 0;
        PointIndices = find(handles.artery_x(:,2)== k);
        plot(handles.artery_x(PointIndices,1),handles.artery_y(PointIndices,1),'or');
    end
    if length(handles.notsure_x) ~= 0;
        PointIndices = find(handles.notsure_x(:,2)== k);
        plot(handles.notsure_x(PointIndices,1),handles.notsure_y(PointIndices,1),'oy');
    end
    hold off
     
    print(PMTPO2Image,'-dtiff', [handles.Savefolder '/PMTPO2VesselsImage'  num2str(k)  '.tif']);
    close(PMTPO2Image);
end


% Saving the gradient curves

% Gradients.SuperMatrix(1,:): pO2 values
% Gradients.SuperMatrix(6,:): distance to coleset vein
% Gradients.SuperMatrix(7,:): distance to coleset artery
% Gradients.SuperMatrix(8,:): distance to coleset Notsure
% Gradients.SuperMatrix(9,:): distance to coleset vessel (any type)

GradientCurves = figure; hold on;
plot(Gradients.SuperMatrix(6,:),Gradients.SuperMatrix(1,:),'ob', 'MarkerSize',2)
tredline = polyfit(Gradients.SuperMatrix(6,:),Gradients.SuperMatrix(1,:),2);
x1 = linspace(min(Gradients.SuperMatrix(6,:)),max(Gradients.SuperMatrix(6,:)),100);
y1 = polyval(tredline,x1);
plot(x1,y1,'k','LineWidth',2)
legend('Distance to closest vein')
print(GradientCurves,'-dtiff', [handles.Savefolder '/GradientCurvesVein.tif']);
savefig(GradientCurves, [handles.Savefolder '/GradientCurvesVein.fig'])
close(GradientCurves);

GradientCurves = figure; hold on;
plot(Gradients.SuperMatrix(7,:),Gradients.SuperMatrix(1,:),'or', 'MarkerSize',2)
tredline = polyfit(Gradients.SuperMatrix(7,:),Gradients.SuperMatrix(1,:),2);
x1 = linspace(min(Gradients.SuperMatrix(7,:)),max(Gradients.SuperMatrix(7,:)),100);
y1 = polyval(tredline,x1);
plot(x1,y1,'k','LineWidth',2)
legend('Distance to closest artery')
print(GradientCurves,'-dtiff', [handles.Savefolder '/GradientCurvesArtery.tif']);
savefig(GradientCurves, [handles.Savefolder '/GradientCurvesArtery.fig'])
close(GradientCurves);

GradientCurves = figure; hold on;
plot(Gradients.SuperMatrix(8,:),Gradients.SuperMatrix(1,:),'o',  'MarkerEdgeColor', [1 .5 0], 'MarkerSize',2)
tredline = polyfit(Gradients.SuperMatrix(8,:),Gradients.SuperMatrix(1,:),2);
x1 = linspace(min(Gradients.SuperMatrix(8,:)),max(Gradients.SuperMatrix(8,:)),100);
y1 = polyval(tredline,x1);
plot(x1,y1,'k','LineWidth',2)
legend('Distance to closest NotSure')
print(GradientCurves,'-dtiff', [handles.Savefolder '/GradientCurvesNotSure.tif']);
% savefig(GradientCurves, [handles.Savefolder '/GradientCurvesNotSure.fig'])
close(GradientCurves);

GradientCurves = figure; hold on;
plot(Gradients.SuperMatrix(9,:),Gradients.SuperMatrix(1,:),'og', 'MarkerSize',2)
tredline = polyfit(Gradients.SuperMatrix(9,:),Gradients.SuperMatrix(1,:),2);
x1 = linspace(min(Gradients.SuperMatrix(9,:)),max(Gradients.SuperMatrix(9,:)),100);
y1 = polyval(tredline,x1);
plot(x1,y1,'k','LineWidth',2)
legend('Distance to closest vessel')
print(GradientCurves,'-dtiff', [handles.Savefolder '/GradientCurves.tif']);
savefig(GradientCurves, [handles.Savefolder '/GradientCurves.fig'])
close(GradientCurves);



% % % % % % Moving avg curves
% % % % % 
% % % % % MovingAvg = figure; hold on;
% % % % % [SortedDistances,OriginalIndices]=sort(Gradients.SuperMatrix(6,:));
% % % % % SortedPO2 = Gradients.SuperMatrix(1,OriginalIndices);
% % % % % plot(SortedDistances,SortedPO2, '.b')
% % % % % plot(SortedDistances,smooth(SortedPO2,100),'b')
% % % % % xlabel('distance (um)') % x-axis label
% % % % % ylabel('pO2 (mmHg)') % y-axis label
% % % % % legend('','Distance to closest vein')
% % % % % hold off
% % % % % print(MovingAvg,'-dtiff', [handles.Savefolder '/MovingAvgVein.tif']);
% % % % % % savefig(MovingAvg, [handles.Savefolder '/MovingAvgVein.fig'])
% % % % % close(MovingAvg);
% % % % % 
% % % % % MovingAvg = figure; hold on;
% % % % % [SortedDistances,OriginalIndices]=sort(Gradients.SuperMatrix(7,:));
% % % % % SortedPO2 = Gradients.SuperMatrix(1,OriginalIndices);
% % % % % plot(SortedDistances,SortedPO2, '.r')
% % % % % plot(SortedDistances,smooth(SortedPO2,100),'r')
% % % % % xlabel('distance (um)') % x-axis label
% % % % % ylabel('pO2 (mmHg)') % y-axis label
% % % % % legend('','Distance to closest artery')
% % % % % hold off
% % % % % print(MovingAvg,'-dtiff', [handles.Savefolder '/MovingAvgArtery.tif']);
% % % % % % savefig(MovingAvg, [handles.Savefolder '/MovingAvgArtery.fig'])
% % % % % close(MovingAvg);
% % % % % 
% % % % % MovingAvg = figure; hold on;
% % % % % [SortedDistances,OriginalIndices]=sort(Gradients.SuperMatrix(8,:));
% % % % % SortedPO2 = Gradients.SuperMatrix(1,OriginalIndices);
% % % % % plot(SortedDistances,SortedPO2, '.','MarkerEdgeColor', [1 .5 0])
% % % % % plot(SortedDistances,smooth(SortedPO2,100),'Color', [1 .5 0])
% % % % % xlabel('distance (um)') % x-axis label
% % % % % ylabel('pO2 (mmHg)') % y-axis label
% % % % % legend('','Distance to closest NotSure')
% % % % % hold off
% % % % % print(MovingAvg,'-dtiff', [handles.Savefolder '/MovingAvgNotSure.tif']);
% % % % % % savefig(MovingAvg, [handles.Savefolder '/MovingAvgNotSure.fig'])
% % % % % close(MovingAvg);
% % % % % 
% % % % % MovingAvg = figure; hold on;
% % % % % [SortedDistances,OriginalIndices]=sort(Gradients.SuperMatrix(9,:));
% % % % % SortedPO2 = Gradients.SuperMatrix(1,OriginalIndices);
% % % % % plot(SortedDistances,SortedPO2, '.g')
% % % % % plot(SortedDistances,smooth(SortedPO2,100),'g')
% % % % % xlabel('distance (um)') % x-axis label
% % % % % ylabel('pO2 (mmHg)') % y-axis label
% % % % % legend('','Distance to closest vessel')
% % % % % hold off
% % % % % print(MovingAvg,'-dtiff', [handles.Savefolder '/MovingAvg.tif']);
% % % % % % savefig(MovingAvg, [handles.Savefolder '/MovingAvg.fig'])
% % % % % close(MovingAvg);





function PolyDeg_txt_Callback(hObject, eventdata, handles)
% hObject    handle to PolyDeg_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PolyDeg_txt as text
%        str2double(get(hObject,'String')) returns contents of PolyDeg_txt as a double


% --- Executes during object creation, after setting all properties.
function PolyDeg_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PolyDeg_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function XOffset_txt_Callback(hObject, eventdata, handles)
% hObject    handle to XOffset_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of XOffset_txt as text
%        str2double(get(hObject,'String')) returns contents of XOffset_txt as a double
% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function XOffset_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to XOffset_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function YOffset_txt_Callback(hObject, eventdata, handles)
% hObject    handle to YOffset_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of YOffset_txt as text
%        str2double(get(hObject,'String')) returns contents of YOffset_txt as a double
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function YOffset_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YOffset_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Update_button.
function Update_button_Callback(hObject, eventdata, handles)
% hObject    handle to Update_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.slider,'Value',1);
handles.XpO2(1,:) =  handles.OriginalXpO2(1,:) + str2num(get(handles.XOffset_txt,'String')); %
handles.YpO2(1,:) =  handles.OriginalYpO2(1,:) + str2num(get(handles.YOffset_txt,'String')); %

% Update display
cla(handles.LivePMTPO2)
axes(handles.LivePMTPO2)
imagesc(squeeze(handles.PMT(:,:,1))); colormap 'gray'
set(handles.slider,'Value',0)
set(handles.txt_slider,'String', handles.UniqueDepths(int16(get(handles.slider,'Value'))+1));
 % axis off;

% plotting the PO2 values
ColorSegments = 50;
color_value = jet(ColorSegments+1);
PO2Indices = find(handles.pO2(2,:)==1);
PO2Data = handles.pO2(1,PO2Indices);
PO2X = handles.XpO2(1,PO2Indices);
PO2Y = handles.YpO2(1,PO2Indices);
Interval = max(PO2Data)/ColorSegments;
 
hold on
for i = 1: length(PO2Data)
    ColorIndex = ceil(PO2Data(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(PO2X(i),PO2Y(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
end
hold off
set(handles.LivePMTPO2, 'YTick', []);
set(handles.LivePMTPO2, 'XTick', []);

axes(handles.PO2_Colorbar)
ylim([0 max(PO2Data)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions (i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off
set(handles.PO2_Colorbar, 'YTick', linspace(0,max(PO2Data),5));
set(handles.PO2_Colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(PO2Data),5)));  

% Update handles structure
guidata(hObject, handles);


% --- Executes on slider movement.
function slider_Callback(hObject, eventdata, handles)
% hObject    handle to slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider

% Update display
cla(handles.LivePMTPO2)
axes(handles.LivePMTPO2)
imagesc(squeeze(handles.PMT(:,:,(int16(get(handles.slider,'Value'))+1)))); colormap 'gray'
set(handles.txt_slider,'String', handles.UniqueDepths(int16(get(handles.slider,'Value'))+1));
set(handles.txt_slider,'String', handles.UniqueDepths(int16(get(handles.slider,'Value'))+1));
 % axis off;

% plotting the PO2 values
ColorSegments = 50;
color_value = jet(ColorSegments+1);
PO2Indices = find(handles.pO2(2,:)==(int16(get(handles.slider,'Value'))+1));
PO2Data = handles.pO2(1,PO2Indices);
PO2X = handles.XpO2(1,PO2Indices);
PO2Y = handles.YpO2(1,PO2Indices);
Interval = max(PO2Data)/ColorSegments;
 
hold on
for i = 1: length(PO2Data)
    ColorIndex = ceil(PO2Data(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(PO2X(i),PO2Y(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
end
hold off
set(handles.LivePMTPO2, 'YTick', []);
set(handles.LivePMTPO2, 'XTick', []);

axes(handles.PO2_Colorbar)
ylim([0 max(PO2Data)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions (i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off
set(handles.PO2_Colorbar, 'YTick', linspace(0,max(PO2Data),5));
set(handles.PO2_Colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(PO2Data),5)));  

axes(handles.LivePMTPO2); hold on;
if length(handles.vein_x) ~ 0;
    PointIndices = find(handles.vein_x(:,2)==(int16(get(handles.slider,'Value'))+1));
    plot(handles.vein_x(PointIndices,1),handles.vein_y(PointIndices,1),'ob');
end
if length(handles.artery_x) ~ 0;
    PointIndices = find(handles.artery_x(:,2)==(int16(get(handles.slider,'Value'))+1));
    plot(handles.artery_x(PointIndices,1),handles.artery_y(PointIndices,1),'or');
end
if length(handles.notsure_x) ~ 0;
    PointIndices = find(handles.notsure_x(:,2)==(int16(get(handles.slider,'Value'))+1));
    plot(handles.notsure_x(PointIndices,1),handles.notsure_y(PointIndices,1),'oy');
end
hold off

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function slider_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in RefPoints_Arteries.
function RefPoints_Arteries_Callback(hObject, eventdata, handles)
% hObject    handle to RefPoints_Arteries (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[artery_x, artery_y] = getpts(handles.LivePMTPO2);
handles.artery_x = [handles.artery_x; [artery_x repmat(int16(get(handles.slider,'Value'))+1,length(artery_x),1)]];
handles.artery_y = [handles.artery_y; [artery_y repmat(int16(get(handles.slider,'Value'))+1,length(artery_y),1)]];
axes(handles.LivePMTPO2); hold on;
plot(artery_x,artery_y,'or')
hold off

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in RefPoints_NotSure.
function RefPoints_NotSure_Callback(hObject, eventdata, handles)
% hObject    handle to RefPoints_NotSure (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[notsure_x, notsure_y] = getpts(handles.LivePMTPO2);
handles.notsure_x = [handles.notsure_x; [notsure_x repmat(int16(get(handles.slider,'Value'))+1,length(notsure_x),1)]];
handles.notsure_y = [handles.notsure_y; [notsure_y repmat(int16(get(handles.slider,'Value'))+1,length(notsure_y),1)]];
axes(handles.LivePMTPO2); hold on;
plot(notsure_x,notsure_y,'oy')
hold off

% Update handles structure
guidata(hObject, handles);



% --- Executes on button press in ClearRefPoints.
function ClearRefPoints_Callback(hObject, eventdata, handles)
% hObject    handle to ClearRefPoints (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.vein_x = []; handles.vein_y = []; handles.artery_x = []; handles.artery_y = []; handles.notsure_x = []; handles.notsure_y = [];

% Update display
cla(handles.LivePMTPO2)
axes(handles.LivePMTPO2)
imagesc(squeeze(handles.PMT(:,:,(int16(get(handles.slider,'Value'))+1)))); colormap 'gray'
set(handles.txt_slider,'String', handles.UniqueDepths(int16(get(handles.slider,'Value'))+1));
set(handles.txt_slider,'String', handles.UniqueDepths(int16(get(handles.slider,'Value'))+1));
 % axis off;

% plotting the PO2 values
ColorSegments = 50;
color_value = jet(ColorSegments+1);
PO2Indices = find(handles.pO2(2,:)==(int16(get(handles.slider,'Value'))+1));
PO2Data = handles.pO2(1,PO2Indices);
PO2X = handles.XpO2(1,PO2Indices);
PO2Y = handles.YpO2(1,PO2Indices);
Interval = max(PO2Data)/ColorSegments;
 
hold on
for i = 1: length(PO2Data)
    ColorIndex = ceil(PO2Data(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(PO2X(i),PO2Y(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
end
hold off
set(handles.LivePMTPO2, 'YTick', []);
set(handles.LivePMTPO2, 'XTick', []);

axes(handles.PO2_Colorbar)
ylim([0 max(PO2Data)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions (i-1)*(max(PO2Data)/ColorSegments)+(j-1)*(max(PO2Data)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off
set(handles.PO2_Colorbar, 'YTick', linspace(0,max(PO2Data),5));
set(handles.PO2_Colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(PO2Data),5)));  

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in button_zoom.
function button_zoom_Callback(hObject, eventdata, handles)
% hObject    handle to button_zoom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

axes(handles.LivePMTPO2)
zoom

% Update handles structure
guidata(hObject, handles);



% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[handles.line_x, handles.line_y] = getline(handles.LivePMTPO2);
handles.VesselDiameter = sqrt((handles.line_x(2)-handles.line_x(1))^2+(handles.line_y(2)-handles.line_y(1))^2);
% handles.VesselDiameter = handles.VesselDiameter * handles.scale;
set(handles.VesselDiameter_txt, 'String',  handles.VesselDiameter);

% Update handles structure
guidata(hObject, handles);
