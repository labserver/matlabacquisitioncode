function varargout = Gradients(varargin)
% GRADIENTS MATLAB code for Gradients.fig
%      GRADIENTS, by itself, creates a new GRADIENTS or raises the existing
%      singleton*.
%
%      H = GRADIENTS returns the handle to a new GRADIENTS or the handle to
%      the existing singleton*.
%
%      GRADIENTS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GRADIENTS.M with the given input arguments.
%
%      GRADIENTS('Property','Value',...) creates a new GRADIENTS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Gradients_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Gradients_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Gradients

% Last Modified by GUIDE v2.5 14-Jul-2015 18:45:59

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Gradients_OpeningFcn, ...
                   'gui_OutputFcn',  @Gradients_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Gradients is made visible.
function Gradients_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Gradients (see VARARGIN)

% Choose default command line output for Gradients
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Gradients wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Gradients_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in Load_Button.
function Load_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Load_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Read Scan Info for analysis
[fname pname] = uigetfile('*.mat','Select .mat PO2 results file','J:\changed\2Ph PO2\Analyzed data\');
load([pname fname],'-mat'); 
handles.Data= Results;
clear Resluts
[fname pname] = uigetfile('*.mat','Select .mat pmt image data',pname);
load([pname fname],'-mat');
handles.PMTImage = image_ref_pmt;
clear image_ref_pmt

handles.OriginalPointX = handles.Data.PointX;
handles.OriginalPointY = handles.Data.PointY;

handles.Data.PointX = handles.OriginalPointX + str2num(get(handles.XOffset_txt,'String')) %* (str2num(handles.Data.ImageDimention)/ 400);
handles.Data.PointY = handles.OriginalPointY + str2num(get(handles.YOffset_txt,'String')) %* (str2num(handles.Data.ImageDimention)/ 400);

% plotting the PO2 values
ColorSegments = 50;
color_value = jet(ColorSegments+1);
Interval = max(handles.Data.PO2)/ColorSegments;
cla(handles.LivePMTPO2)
axes(handles.LivePMTPO2)
imagesc(handles.PMTImage); colormap 'gray'

hold on
for i = 1: length(handles.Data.PO2)
    ColorIndex = ceil(handles.Data.PO2(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(handles.Data.PointX(i),handles.Data.PointY(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
end
hold off
set(handles.LivePMTPO2, 'YTick', []);
set(handles.LivePMTPO2, 'XTick', []);

axes(handles.PO2_Colorbar)
ylim([0 max(handles.Data.PO2)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(handles.Data.PO2)/ColorSegments)+(j-1)*(max(handles.Data.PO2)/ColorSegments)/BarDevisions (i-1)*(max(handles.Data.PO2)/ColorSegments)+(j-1)*(max(handles.Data.PO2)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off
set(handles.PO2_Colorbar, 'YTick', linspace(0,max(handles.Data.PO2),5));
set(handles.PO2_Colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(handles.Data.PO2),5)));   


% Update handles structure
guidata(hObject, handles);


% --- Executes on selection change in Popup_AnalysisType.
function Popup_AnalysisType_Callback(hObject, eventdata, handles)
% hObject    handle to Popup_AnalysisType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns Popup_AnalysisType contents as cell array
%        contents{get(hObject,'Value')} returns selected item from Popup_AnalysisType

% contents = cellstr(get(handles.Popup_AnalysisType,'String'));
% Selection = contents{get(handles.Popup_AnalysisType,'Value')}


% --- Executes during object creation, after setting all properties.
function Popup_AnalysisType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Popup_AnalysisType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in RefPoints_Button.
function RefPoints_Button_Callback(hObject, eventdata, handles)
% hObject    handle to RefPoints_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% plotting the PO2 values
ColorSegments = 50;
color_value = jet(ColorSegments+1);
Interval = max(handles.Data.PO2)/ColorSegments;
cla(handles.LivePMTPO2)
axes(handles.LivePMTPO2)
imagesc(handles.PMTImage); colormap 'gray'

hold on
for i = 1: length(handles.Data.PO2)
    ColorIndex = ceil(handles.Data.PO2(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(handles.Data.PointX(i),handles.Data.PointY(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
end
hold off
set(handles.LivePMTPO2, 'YTick', []);
set(handles.LivePMTPO2, 'XTick', []);

axes(handles.PO2_Colorbar)
ylim([0 max(handles.Data.PO2)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(handles.Data.PO2)/ColorSegments)+(j-1)*(max(handles.Data.PO2)/ColorSegments)/BarDevisions (i-1)*(max(handles.Data.PO2)/ColorSegments)+(j-1)*(max(handles.Data.PO2)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off
set(handles.PO2_Colorbar, 'YTick', linspace(0,max(handles.Data.PO2),5));
set(handles.PO2_Colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(handles.Data.PO2),5)));   

[handles.reference_x, handles.reference_y] = getpts(handles.LivePMTPO2);
axes(handles.LivePMTPO2); hold on;
plot(handles.reference_x,handles.reference_y,'o')
hold off

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in PlotGradient_Button.
function PlotGradient_Button_Callback(hObject, eventdata, handles)
% hObject    handle to PlotGradient_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

point_distance = []; handles.SmallestDistance=[];
for i = 1: length(handles.Data.PO2)
    for j = 1: length(handles.reference_x)
        point_distance(j) = sqrt((handles.Data.PointX(i) - handles.reference_x(j))^2 + (handles.Data.PointY(i) - handles.reference_y(j))^2);
    end
    handles.SmallestDistance(i) = min(point_distance);
end
handles.SmallestDistance = handles.SmallestDistance * (str2num(handles.Data.ImageDimention)/ 400);


GradientCurve = figure; hold on;
plot(handles.SmallestDistance,handles.Data.PO2,'o','MarkerSize',4)
tredline = polyfit(handles.SmallestDistance,handles.Data.PO2,str2num(get(handles.PolyDeg_txt,'String')));
x1 = linspace(min(handles.SmallestDistance),max(handles.SmallestDistance),100);
y1 = polyval(tredline,x1);
plot(x1,y1,'k','LineWidth',2)
hold off


% Update handles structure
guidata(hObject, handles);



% --- Executes on button press in Save_Button.
function Save_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Save_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

Gradients = [];
handles.Savefolder = uigetdir('J:\changed\2Ph PO2\Analyzed data\');

contents = cellstr(get(handles.Popup_AnalysisType,'String'));
Gradients.VesselType= contents{get(handles.Popup_AnalysisType,'Value')};
Gradients.PO2 = handles.Data.PO2;
Gradients.X = handles.Data.PointX;
Gradients.Y = handles.Data.PointY;
Gradients.Distances = handles.SmallestDistance;
Gradients.RefX = handles.reference_x;
Gradients.RefY = handles.reference_y;
if isfield(handles.Data,'Depth')
    Gradients.Depth = handles.Data.Depth;
end
Gradients.VesselDiameter = handles.VesselDiameter;

save([handles.Savefolder '/Gradients.mat'],'Gradients')

% Saving the PMT image
PMTImage = figure;
imagesc(handles.PMTImage); colormap 'gray'
set(gca, 'YTick', []);
set(gca, 'XTick', []);
print(PMTImage,'-dtiff', [handles.Savefolder '/PMT.tif']);
close(PMTImage);

% Saving the PMT/PO2 image
PMTPO2Image = figure;
hPMTPO2Image=imagesc(handles.PMTImage);  colormap gray; cbFITC = colorbar;
set(gca, 'YTick', []);
set(gca, 'XTick', []);
colormap([gray(64);jet(64)]);
maxFTC = max(handles.PMTImage(:));
minFTC = min(handles.PMTImage(:));
ncmp=64;
c_FITC = min(ncmp,  round(   (ncmp-1).*(handles.PMTImage-minFTC)./(maxFTC-minFTC)  )+1  );

set(hPMTPO2Image,'CDataMapping','Direct');
set(hPMTPO2Image,'CData',c_FITC);
figure(PMTPO2Image);
caxis([min(c_FITC(:)) max(c_FITC(:))]);
set(cbFITC,'YLim',[ncmp+1 2*ncmp]);

cmap1=jet(64);
CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
minPO2 = min(Gradients.PO2);
maxPO2 = max(Gradients.PO2);
cmapPO2idx = ones(length(Gradients.PO2),1); % vector of colormap indexes for pO2
if maxPO2 == minPO2,
    cmapPO2idx = cmapPO2idx.*round(CmapN/2);
else
    cmapPO2idx = round(   (  CmapN.*(Gradients.PO2-minPO2)+maxPO2-Gradients.PO2  )    ./(maxPO2-minPO2) );
    cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
    cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
end;

for i = 1: length(Gradients.PO2)
     colMap = squeeze(cmap1(cmapPO2idx(i),:));
     rectangle('Position',[(Gradients.X(i) - 7),(Gradients.Y(i) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
     daspect ([1,1,1])
end
% hold on;
% plot(handles.reference_x,handles.reference_y,'o','MarkerSize',10,'MarkerFaceColor','k')
% hold off

step = 10;         
newTickLabels = step*ceil(minPO2/step)  : step  : step*floor(maxPO2/step);
newTicks =( newTickLabels - (minPO2*ncmp-maxPO2)/(ncmp-1) ) .* (ncmp-1) ./ (maxPO2-minPO2);
newTickLabelsStr=num2str(newTickLabels(1),'%i');
for idxLab = 2:length(newTickLabels),
    newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
end;
pause(0);
set(cbFITC,'YTick',newTicks+ncmp);
set(cbFITC,'YTickLabel',newTickLabelsStr,'fontsize',30);

print(PMTPO2Image,'-dtiff', [handles.Savefolder '/PMTPO2Image.tif']);
close(PMTPO2Image);

% Saving the gradient curve
GradientCurve = figure; hold on;
plot(Gradients.Distances,Gradients.PO2,'o','MarkerSize',4)
tredline = polyfit(Gradients.Distances,Gradients.PO2,str2num(get(handles.PolyDeg_txt,'String')));
x1 = linspace(min(Gradients.Distances),max(Gradients.Distances),100);
y1 = polyval(tredline,x1);
plot(x1,y1,'k','LineWidth',2)
hold off
print(GradientCurve,'-dtiff', [handles.Savefolder '/GradientCurve.tif']);
savefig(GradientCurve, [handles.Savefolder '/GradientCurve.fig'])
close(GradientCurve);



function PolyDeg_txt_Callback(hObject, eventdata, handles)
% hObject    handle to PolyDeg_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PolyDeg_txt as text
%        str2double(get(hObject,'String')) returns contents of PolyDeg_txt as a double


% --- Executes during object creation, after setting all properties.
function PolyDeg_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PolyDeg_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in VesselDiameter_Button.
function VesselDiameter_Button_Callback(hObject, eventdata, handles)
% hObject    handle to VesselDiameter_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

[handles.line_x, handles.line_y] = getline(handles.LivePMTPO2);
handles.VesselDiameter = sqrt((handles.line_x(2)-handles.line_x(1))^2+(handles.line_y(2)-handles.line_y(1))^2);
handles.VesselDiameter = handles.VesselDiameter * (str2num(handles.Data.ImageDimention)/400);
set(handles.VesselDiameter_txt, 'String',  handles.VesselDiameter);

% Update handles structure
guidata(hObject, handles);


% --- Executes on button press in Zoom_Button.
function Zoom_Button_Callback(hObject, eventdata, handles)
% hObject    handle to Zoom_Button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

axes(handles.LivePMTPO2)
zoom

% Update handles structure
guidata(hObject, handles);



function XOffset_txt_Callback(hObject, eventdata, handles)
% hObject    handle to XOffset_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of XOffset_txt as text
%        str2double(get(hObject,'String')) returns contents of XOffset_txt as a double
% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function XOffset_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to XOffset_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function YOffset_txt_Callback(hObject, eventdata, handles)
% hObject    handle to YOffset_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of YOffset_txt as text
%        str2double(get(hObject,'String')) returns contents of YOffset_txt as a double
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function YOffset_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to YOffset_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in Update_button.
function Update_button_Callback(hObject, eventdata, handles)
% hObject    handle to Update_button (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.Data.PointX = handles.OriginalPointX + str2num(get(handles.XOffset_txt,'String')) %* (str2num(handles.Data.ImageDimention)/ 400);
handles.Data.PointY = handles.OriginalPointY + str2num(get(handles.YOffset_txt,'String')) %* (str2num(handles.Data.ImageDimention)/ 400);

% plotting the PO2 values
ColorSegments = 50;
color_value = jet(ColorSegments+1);
Interval = max(handles.Data.PO2)/ColorSegments;
cla(handles.LivePMTPO2)
axes(handles.LivePMTPO2)
imagesc(handles.PMTImage); colormap 'gray'

hold on
for i = 1: length(handles.Data.PO2)
    ColorIndex = ceil(handles.Data.PO2(i)/Interval);
    if ColorIndex <=0
        ColorIndex=1;
    end
    ColorValue(i,:) = color_value(ColorIndex,:);
    plot(handles.Data.PointX(i),handles.Data.PointY(i),'Marker','s', 'MarkerFaceColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)], 'MarkerEdgeColor',[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)])
end
hold off
set(handles.LivePMTPO2, 'YTick', []);
set(handles.LivePMTPO2, 'XTick', []);

axes(handles.PO2_Colorbar)
ylim([0 max(handles.Data.PO2)]);
xlim([0 1]);
hold on
BarDevisions = 5;
for i = 1: ColorSegments
    for j=1:BarDevisions
        line([0 1],[(i-1)*(max(handles.Data.PO2)/ColorSegments)+(j-1)*(max(handles.Data.PO2)/ColorSegments)/BarDevisions (i-1)*(max(handles.Data.PO2)/ColorSegments)+(j-1)*(max(handles.Data.PO2)/ColorSegments)/BarDevisions],'color',[color_value(i,1),color_value(i,2),color_value(i,3)])
    end
end
hold off
set(handles.PO2_Colorbar, 'YTick', linspace(0,max(handles.Data.PO2),5));
set(handles.PO2_Colorbar,'YTickLabel',sprintf('%0.0f|',linspace(0,max(handles.Data.PO2),5)));   


% Update handles structure
guidata(hObject, handles);

