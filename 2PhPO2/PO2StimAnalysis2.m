function PO2StimAnalysis2(TimeInterval, NeglectTime)

SampFreq = 50000;
StimFreq = 5;
TimeBeforeStim = 4; % sec
handles.ProbeBatch = 2;

%% Load %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

[fname pname] = uigetfile('*.mat','Select the PO2 data','C:\Users\moeini\Mohammad\Data\PO2 data\Awake 2');
load([pname fname],'-mat'); 
handles.ScanInfo = scan;
clear scan
handles.scale = (handles.ScanInfo.scan_height/400);    
handles.ScanInfo.ramp_y = handles.ScanInfo.ramp_y + 20 * handles.scale; % correcting the shift between the pO2 grid coordinate and the pmt coordinate      

load([pname 'image_pmt.mat']); % PMT live image 
handles.PMTImage = image_ref_pmt;
clear image_ref_pmt
handles.PMTImage(1:round(handles.scale * 50),:) = min(handles.PMTImage(:)); % remove the mirror artefact of the PMT image
load([pname '\PreStim\PreStimResults.mat']); % PreStim Results
handles.PreStimResults = PreStimResults;
clear PreStimResults

[fname pname] = uigetfile('*.mat','Select the synchronization data','C:\Users\moeini\Mohammad\Data\PO2 data\Awake 2');
load([pname fname],'-mat'); 
handles.stim= aux(:,1); % stimulation
handles.AOMPulse = aux(:,2); % AOM
clear aux

TotalDecays_txt = handles.ScanInfo.PO2Repitition*handles.ScanInfo.PointRepetition*length(handles.ScanInfo.ramp_x)

handles.time = (1:length(handles.stim))/SampFreq;
handles.stimBlocks = handles.stim;
HighIndices = find(handles.stimBlocks > 100);
handles.stimBlocks(HighIndices + round(SampFreq/(2*StimFreq))) = max(handles.stimBlocks);
LowIndices = find(handles.stimBlocks < 100);
handles.stimBlocks(LowIndices(round(SampFreq/(2*StimFreq))+1:end) - round(SampFreq/(2*StimFreq))) = min(handles.stimBlocks);

%% Detect AOM pulses, seperate points %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 

dd=[];

handles.ImagingRange = find(diff(handles.AOMPulse)<-500);
bb = handles.AOMPulse(handles.ImagingRange(1):handles.ImagingRange(end));
HighIndices = find(bb > 500);
bb(HighIndices) = max(handles.AOMPulse);
LowIndices = find(bb < 500);
bb(LowIndices) = 0;

RisingEdgeIndices = find(diff(bb)>500) + handles.ImagingRange(1);
RisingEdgeIndices = [handles.ImagingRange(1); RisingEdgeIndices];

TotalDecaysDetected_txt = length(RisingEdgeIndices)

aa = handles.time(RisingEdgeIndices);
aa= reshape(aa,handles.ScanInfo.PO2Repitition * length(handles.ScanInfo.ramp_x) , handles.ScanInfo.PointRepetition);
for i = 1 : length(handles.ScanInfo.ramp_x)
    cc = aa(1+(i-1)*handles.ScanInfo.PO2Repitition : i*handles.ScanInfo.PO2Repitition , :);
    dd(:,i) = reshape(cc , handles.ScanInfo.PO2Repitition * handles.ScanInfo.PointRepetition , 1);
end
handles.aomTimes = dd; % sec

%% sort decays based on time from the stimulation blocks %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

StimStartIndices = [find(diff(handles.stimBlocks)>1000); handles.ImagingRange(end)];
NoStimBlocks_txt = length(StimStartIndices)-1
% BetweenIndices = StimStartIndices(2:end) - StimStartIndices(1:end-1);

DecaySum = handles.ScanInfo.PO2data;
DistancesToArteries = [];
for i = 1: size(DecaySum,3)
    DistancesToArteries(i,:) = repmat(handles.PreStimResults.Distances(i),1,size(DecaySum,2));
end
IntervalTimes =nan(size(handles.aomTimes,1),size(handles.aomTimes,2),2);

for j = 1: size(handles.aomTimes,2)  
    StimBlockNo = 1;
    for i=1:size(handles.aomTimes,1)
        IntervalTimes(i,j,1) = handles.aomTimes(i,j)- handles.time(StimStartIndices(StimBlockNo));
        IntervalTimes(i,j,2) = StimBlockNo;
        if StimBlockNo ~= length(StimStartIndices-1)
            if (handles.aomTimes(i,j)- handles.time(StimStartIndices(StimBlockNo+1))) >= -1*TimeBeforeStim
                StimBlockNo = StimBlockNo +1;
            end
        end
    end
end

for j = 1: size(handles.aomTimes,2)
    [~, Ind] = sort(IntervalTimes(:,j,1));
    IntervalTimes(:,j,:) = IntervalTimes(Ind,j,:);
    DecaySum(:,:,j) = DecaySum(:,Ind,j);
    DistancesToArteries(j,:) = DistancesToArteries(j,Ind);
end

%% Remove points with weak signal (first 500 points of the stimulation decays) or weak BL PO2 signal

% Baseline pO2 from the data before the first stimulation block
BLDecaySum = nan(size(DecaySum,1), size(DecaySum,3));
BLPO2 = nan(1,size(DecaySum,3));
BL_RSquare = nan(1,size(DecaySum,3));
R_square5000 = nan(1,size(DecaySum,3));

for j = 1: size(handles.aomTimes,2)
    BLDecaySum(:,j) = mean(DecaySum(:,find(IntervalTimes(:,j,1) < -1*TimeBeforeStim),j),2);
    Data5000 = mean(DecaySum(:,1:5000,j),2);
    if handles.ProbeBatch == 1
        [~ , ~, ~, ~, ~, ~, R_square5000(j)] = CalcPO2_2(Data5000, handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, NeglectTime);
    elseif handles.ProbeBatch == 2
        [~ , ~, ~, ~, ~, ~, R_square5000(j)] = CalcPO2_2015(Data5000, handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, NeglectTime);
    end        
    if handles.ProbeBatch == 1
        [~ , ~, BLPO2(j), ~, ~, ~, BL_RSquare(j)] = CalcPO2_2(BLDecaySum(:,j), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, NeglectTime);
    elseif handles.ProbeBatch == 2
        [~ , ~, BLPO2(j), ~, ~, ~, BL_RSquare(j)] = CalcPO2_2015(BLDecaySum(:,j), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, NeglectTime);
    end            
end


% average pO2 during stimulation
StimDecaySum = nan(size(DecaySum,1), size(DecaySum,3));
StimPO2 = nan(1,size(DecaySum,3));
Stim_RSquare = nan(1,size(DecaySum,3));

for j = 1: size(handles.aomTimes,2)
    StimDecaySum(:,j) = mean(DecaySum(:,find( (IntervalTimes(:,j,1) > 0) & (IntervalTimes(:,j,1) < 5) ),j),2);
    if handles.ProbeBatch == 1
        [~ , ~, StimPO2(j), ~, ~, ~, Stim_RSquare(j)] = CalcPO2_2(StimDecaySum(:,j), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, NeglectTime);
    elseif handles.ProbeBatch == 2
        [~ , ~, StimPO2(j), ~, ~, ~, Stim_RSquare(j)] = CalcPO2_2015(StimDecaySum(:,j), handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, NeglectTime);
    end
end


WeakSignal = find(R_square5000<0.95 | BL_RSquare<0.95);
RemovedPoints = length(WeakSignal)
DecaySum(:,:,WeakSignal)=[];
DistancesToArteries(WeakSignal,:)=[];
IntervalTimes(:,WeakSignal,:)=[];
BLDecaySum(:,WeakSignal)=[];
handles.ScanInfo.ramp_x(WeakSignal) = [];
handles.ScanInfo.ramp_y(WeakSignal) = [];
handles.PreStimResults.Distances(WeakSignal) = [];
BLPO2(WeakSignal)=[];
BL_RSquare(WeakSignal)=[];
StimDecaySum(:,WeakSignal)=[];
StimPO2(WeakSignal)=[];
Stim_RSquare(WeakSignal)=[];

%% Reshaping the data matrices 

SuperDecaySum=nan(size(DecaySum,1)+7,size(DecaySum,2)*size(DecaySum,3));
SuperDecaySum(6:size(DecaySum,1)+5,:) = reshape(DecaySum, [size(DecaySum,1), size(DecaySum,2)*size(DecaySum,3)]);
SuperDecaySum(1,:) = reshape( repmat([1:size(DecaySum,3)],size(DecaySum,2),1)  , [1,size(DecaySum,2)*size(DecaySum,3)] );
SuperDecaySum(2,:) = repmat(handles.PreStimResults.Depth , 1, size(DecaySum,2)*size(DecaySum,3));
SuperDecaySum(3,:) = reshape( repmat(BLPO2 ,size(DecaySum,2),1)  , [1,size(DecaySum,2)*size(DecaySum,3)] );
SuperDecaySum(4,:) = reshape(IntervalTimes(:,:,1), [size(IntervalTimes,1)*size(IntervalTimes,2) , 1])';
SuperDecaySum(5,:) = reshape(IntervalTimes(:,:,2), [size(IntervalTimes,1)*size(IntervalTimes,2) , 1])';
SuperDecaySum(81,:) = reshape(DistancesToArteries, [1, size(DecaySum,2)*size(DecaySum,3)]);
SuperDecaySum(82,:) = repmat(handles.PreStimResults.VesselDiameter , 1, size(DecaySum,2)*size(DecaySum,3));

% If hist length is less than 75, we make it 75 by interpolation 
if size(DecaySum,1) ~= 75
    for i=1:size(SuperDecaySum,2)
        pp = interp1([1:handles.ScanInfo.PO2PulsePeriod/size(DecaySum,1):handles.ScanInfo.PO2PulsePeriod],SuperDecaySum(6:size(DecaySum,1)+5,i),[1:4:300]);
        pp(end)=pp(end-1);
        SuperDecaySum(6:80,i) = pp;
    end
end

%1: point number
%2: depth
%3: BLpO2
%4: interval time
%5: stim block
%6:80: decays
%81: distance to artery
%82: artery diameter

[~ , Ind] = sort(SuperDecaySum(4,:));
SuperDecaySum(:,:) = SuperDecaySum(:,Ind);
SuperDecaySum(:,find(SuperDecaySum(4,:) < -1*TimeBeforeStim)) = [];

%% stimulation response

% intervals
GroupAvgPO2 = []; n = []; GroupSDPO2 = []; GroupAvgTime = []; GroupSDTime = []; Group_Rsquare = [];

for i=1:ceil((SuperDecaySum(4,end) - SuperDecaySum(4,1))/TimeInterval)
    if i == ceil((SuperDecaySum(4,end) - SuperDecaySum(4,1))/TimeInterval)
         GroupIndices = find( (SuperDecaySum(4,1)+ (i-1)*TimeInterval ) <= SuperDecaySum(4,:) & SuperDecaySum(4,:) <= (SuperDecaySum(4,1)+ i*TimeInterval ) );
    else
         GroupIndices = find( ( SuperDecaySum(4,1) + (i-1)*TimeInterval ) <= SuperDecaySum(4,:) & SuperDecaySum(4,:) <  (SuperDecaySum(4,1)+ i*TimeInterval) );
    end
    GroupSDTime(i)=nanstd(SuperDecaySum(4,GroupIndices));
    GroupAvgTime(i)=nanmean(SuperDecaySum(4,GroupIndices));
    Data=nanmean(SuperDecaySum(6:80,GroupIndices),2);   
    if handles.ProbeBatch == 1
        [~ , ci, GroupAvgPO2(i), ~, ~, ~, Group_Rsquare(i)] = CalcPO2_2(Data, handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, NeglectTime);  
        GroupSDPO2(i) = 0.5 * abs(OldPO2Dye(ci(2,2)*1.0e-6, NeglectTime) - OldPO2Dye(ci(1,2)*1.0e-6, NeglectTime)); 
    elseif handles.ProbeBatch == 2
        
        Ind = find(isfinite(Data));
        Data = Data(Ind);
        
        if length(Data) > 0
            [~ , ci, GroupAvgPO2(i), ~, ~, ~, Group_Rsquare(i)] = CalcPO2_2015(Data, handles.ScanInfo.PO2PulsePeriod, handles.ScanInfo.PO2PulseHighTime, NeglectTime);  
            GroupSDPO2(i) = 0.5 * abs(NewPO2Dye(ci(2,2)*1.0e-6, NeglectTime) - NewPO2Dye(ci(1,2)*1.0e-6, NeglectTime));  
        else
            GroupAvgPO2(i)=NaN;
            Group_Rsquare(i)=NaN;
            GroupSDPO2(i)=NaN;
        end
    end             
    n(i)= length(GroupIndices);
end               

%% Save %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% If hist length is less than 75, we make it 75 by interpolation 
if size(BLDecaySum,1) ~= 75
    vv=[];
    for i=1:size(BLDecaySum,2)
        vv(:,i) = interp1([1:handles.ScanInfo.PO2PulsePeriod/size(BLDecaySum,1):handles.ScanInfo.PO2PulsePeriod],BLDecaySum(:,i),[1:4:300]);
    end
    vv(end,:)=vv(end-1,:);
    BLDecaySum = vv;
end

if size(StimDecaySum,1) ~= 75
    vv=[];
    for i=1:size(StimDecaySum,2)
        vv(:,i) = interp1([1:handles.ScanInfo.PO2PulsePeriod/size(StimDecaySum,1):handles.ScanInfo.PO2PulsePeriod],StimDecaySum(:,i),[1:4:300]);
    end
    vv(end,:)=vv(end-1,:);
    StimDecaySum = vv;
end


% Results file
Results.SuperDecaySum =SuperDecaySum;
Results.TimeInterval = TimeInterval;
Results.BLPO2 = BLPO2;
Results.BLDecaySum = BLDecaySum;
Results.BL_RSquare = BL_RSquare;
Results.StimDecaySum=StimDecaySum;
Results.StimPO2=StimPO2;
Results.Stim_RSquare=Stim_RSquare;
Results.IntervalPO2 = [GroupAvgTime; n; GroupSDTime; GroupAvgPO2; GroupSDPO2; Group_Rsquare];
Results.Depths = repmat(handles.PreStimResults.Depth,1,length(BLPO2));
Results.VesselDiameter = repmat(handles.PreStimResults.VesselDiameter,1,length(BLPO2));
Results.Distances = handles.PreStimResults.Distances;

handles.Savefolder = uigetdir('C:\Users\moeini\Desktop\Awake 2\Stim');
save([handles.Savefolder '/Results.mat'],'Results')            

% Response curve
TimeIntervalFig = figure; hold on;
plot(GroupAvgTime,GroupAvgPO2)
xlabel('Time (s)') % x-axis label
ylabel('pO2 (mmHg)') % y-axis label
hold off
print(TimeIntervalFig,'-dtiff', [handles.Savefolder '/' num2str(TimeInterval) 'Interval.tif']);
savefig(TimeIntervalFig, [handles.Savefolder '/' num2str(TimeInterval) 'Interval.fig'])

% Saving the PMT/PO2 image (BL PO2)
BLPO2Image = figure;
hBLPO2Image=imagesc(handles.PMTImage);  colormap gray; cbFITC = colorbar;
set(gca, 'YTick', []);
set(gca, 'XTick', []);
colormap([gray(64);jet(64)]);
maxFTC = max(handles.PMTImage(:));
minFTC = min(handles.PMTImage(:));
ncmp=64;
c_FITC = min(ncmp,  round(   (ncmp-1).*(handles.PMTImage-minFTC)./(maxFTC-minFTC)  )+1  );

set(hBLPO2Image,'CDataMapping','Direct');
set(hBLPO2Image,'CData',c_FITC);
figure(BLPO2Image);
caxis([min(c_FITC(:)) max(c_FITC(:))]);
set(cbFITC,'YLim',[ncmp+1 2*ncmp]);

cmap1=jet(64);
CmapN = size(cmap1,1); % length of colormap[N,3] matrix is N
minPO2 = min(BLPO2);
maxPO2 = max(BLPO2);

cmapPO2idx = ones(length(BLPO2),1); % vector of colormap indexes for pO2
if maxPO2 == minPO2,
    cmapPO2idx = cmapPO2idx.*round(CmapN/2);
else
    cmapPO2idx = round(   (  CmapN.*(BLPO2-minPO2)+maxPO2-BLPO2  )    ./(maxPO2-minPO2) );
    cmapPO2idx(find(cmapPO2idx <= 0)) = 1;
    cmapPO2idx(find(cmapPO2idx > CmapN)) = CmapN;
end;

for i = 1: length(BLPO2)
     colMap = squeeze(cmap1(cmapPO2idx(i),:));
     rectangle('Position',[(handles.ScanInfo.ramp_x(i) - 7),(handles.ScanInfo.ramp_y(i) - 7), 14, 14],'Curvature',[0,0],'FaceColor',colMap,'LineStyle','none');
     daspect ([1,1,1])
end

step = round((maxPO2-minPO2)/5);         
newTickLabels = step*ceil(minPO2/step)  : step  : step*floor(maxPO2/step);
newTicks =( newTickLabels - (minPO2*ncmp-maxPO2)/(ncmp-1) ) .* (ncmp-1) ./ (maxPO2-minPO2);
newTickLabelsStr=num2str(newTickLabels(1),'%i');
for idxLab = 2:length(newTickLabels),
    newTickLabelsStr = char(newTickLabelsStr,num2str(newTickLabels(idxLab),'%i'));
end;
pause(0);
set(cbFITC,'YTick',newTicks+ncmp);
set(cbFITC,'YTickLabel',newTickLabelsStr, 'fontsize',30);

print(BLPO2Image,'-dtiff', [handles.Savefolder '/BLPO2Image.tif']);
close(BLPO2Image);



end
