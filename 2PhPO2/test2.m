function varargout = test2(varargin)
% TEST2 MATLAB code for test2.fig
%      TEST2, by itself, creates a new TEST2 or raises the existing
%      singleton*.
%
%      H = TEST2 returns the handle to a new TEST2 or the handle to
%      the existing singleton*.
%
%      TEST2('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TEST2.M with the given input arguments.
%
%      TEST2('Property','Value',...) creates a new TEST2 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before test2_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to test2_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help test2

% Last Modified by GUIDE v2.5 15-Sep-2014 16:26:16

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @test2_OpeningFcn, ...
                   'gui_OutputFcn',  @test2_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before test2 is made visible.
function test2_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to test2 (see VARARGIN)

% Choose default command line output for test2
handles.output = hObject;

handles.scan.segment_handles=[];
handles.scan.number_points_per_segment = [];
handles.line_PO2_handles =[];
handles.point_PO2_handles =[];
handles.current =[];
handles.TwoDGrid_PO2_handles=[];

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes test2 wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = test2_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

axes(handles.axes1);
% current_point_handle = impoint();
% set(current_point_handle, 'UserData',0);
% % Here we only keep the handles to the line, we will get the positions at the end so that the user can move the line
% handles.scan.segment_handles = [handles.scan.segment_handles, {current_point_handle}];
% handles.point_PO2_handles = [handles.point_PO2_handles, current_point_handle];
% handles.scan.number_points_per_segment = [handles.scan.number_points_per_segment, 1];

% set(handles.current,'String','Drag or resize the 2D Grid');
% axes(handles.pmt_image);
current_2DGrid_handle = imrect(gca,[0 0 1 1]);
setFixedAspectRatioMode(current_2DGrid_handle,'True')
set(current_2DGrid_handle, 'UserData',2);
handles.scan.segment_handles = [handles.scan.segment_handles, {current_2DGrid_handle}];
handles.TwoDGrid_PO2_handles = [handles.TwoDGrid_PO2_handles, current_2DGrid_handle];
prompt = 'Number of points in x and y directions?';
set(handles.current,'String','Enter the number of points in x and y directions');
handles.scan.number_points_per_segment = [handles.scan.number_points_per_segment, str2double((cell2mat(inputdlg(prompt))))];%bug:user input should be limited to int only
% set(handles.current,'String','Click "2D Grid" to draw another grid or click "Start" when ready');


guidata(hObject, handles);





% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

positions=[];

n_segments = length(handles.scan.segment_handles); % Count segments (either a point or a line or grid)

for i=1:n_segments
    if (get(handles.scan.segment_handles{i},'UserData') == 0)
        % This is a point
        point_positions = getPosition(handles.scan.segment_handles{i}); % it gives x and y of the point
        positions = [positions;point_positions];% each row represents x and y of one point
    elseif (get(handles.scan.segment_handles{i},'UserData') == 1)
        % This is a line
        line_positions = getPosition(handles.scan.segment_handles{i});
        line_positionsX = linspace(line_positions(1,1),line_positions(2,1), handles.scan.number_points_per_segment(i))';
        line_positionsY = linspace(line_positions(1,2),line_positions(2,2), handles.scan.number_points_per_segment(i))';
        line_positions = [line_positionsX  line_positionsY]; % each row represents x and y of one point on the line
        positions = [positions;line_positions]; % each row represents x and y of one point 
    else
        % This is a 2D grid
        grid_positions = getPosition(handles.scan.segment_handles{i}); %gives the position of the grid: [xmin ymin width height]
        X1= grid_positions(1);
        X2= X1 + grid_positions(3);
        Y1= grid_positions(2);
        Y2= Y1 + grid_positions(4);
        [grid_positionsX,grid_positionsY] = meshgrid(linspace(X1,X2,handles.scan.number_points_per_segment(i)),linspace(Y1,Y2,handles.scan.number_points_per_segment(i)));
        grid_positionsX=reshape(grid_positionsX,size(grid_positionsX,1)*size(grid_positionsX,2),1);
        grid_positionsY=reshape(grid_positionsY,size(grid_positionsY,1)*size(grid_positionsY,2),1);
        grid_positions = [grid_positionsX  grid_positionsY]; % each row represents x and y of one point on the line
        positions = [positions;grid_positions]; % each row represents x and y of one point 
    end
end

handles.pos_x = positions(:,1)';
handles.pos_y = positions(:,2)';

for i = 1: length(handles.pos_x)
    PO2(i) = 10*i;
end

% plotting the PO2 values
axes(handles.axes1)
color_value = jet(length(PO2));
for i = 1: length(PO2)
    ColorIndex = round(length(PO2)*PO2(i)/max(PO2));
    ColorValue(i,:) = color_value(ColorIndex,:);
    h = impoint(gca,handles.pos_x(i),handles.pos_y(i));
    setColor(h,[ColorValue(i,1),ColorValue(i,2),ColorValue(i,3)]);
end
colorbar
