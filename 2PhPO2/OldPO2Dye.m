function PO2 = OldPO2Dye(tau, NeglectTime)
% tau in s

%Calibration curve coefficients
if NeglectTime <4
    y0 = -6.8610; A1 = 306.6738; t1 = 1.2974e-05; A2 = 437.4606; t2 = 3.6406e-06; % parameters for 0 point ignored
elseif NeglectTime<8
    y0 = -4.9459; A1 = 380.9448; t1 = 1.1787e-05; A2 = 6.0676e+03; t2 = 2.3411e-06; % parameters for 1 point ignored
elseif NeglectTime<12
    y0 = -6.2014; A1 = 332.9207; t1 = 1.2988e-05; A2 = 2.8406e+03; t2 = 3.3869e-06; % parameters for 2 points ignored
elseif NeglectTime<16
    y0 = -6.7328; A1 = 329.0197; t1 = 1.3425e-05; A2 = 4.7137e+03; t2 = 3.3883e-06; % parameters for 3 points ignored
elseif NeglectTime<20
    y0 = -7.1437; A1 = 317.0903; t1 = 1.4335e-05; A2 = 9.6985e+03; t2 = 3.1854e-06;  % parameters for 4 points ignored
else
    y0=-6.94916; A1=378.94838; t1=1.31291E-5;  A2=10187.27521; t2=3.31711E-6; %Sergei's values (5 points ignored)
end

PO2 = A1*exp(-tau/t1) + A2*exp(-tau/t2) + y0;  % PO2 value in mmHg

end