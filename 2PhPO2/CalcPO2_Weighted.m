function [p, ci, PO2, FitResults3, x, y, R2] = CalcPO2_2(HistData, PulseDuration, PulseHighTime, NeglectTime)
% HistData: Histogram data 
% PulseDuration: laser pulse duration for one lifetime measurement (us)
% PulseHighTime: laser high pulse duration (us)

NeglectPoints = ceil(NeglectTime / (PulseDuration/length(HistData)));
temps = linspace(0,PulseDuration, length(HistData)); % in us
DecayStartPoint = ceil(length(HistData)*PulseHighTime/PulseDuration); % the index of temps in which the decay starts
% We do the fit from the point that the decay starts
x=temps(DecayStartPoint+NeglectPoints:end-1)';
x=x-temps(DecayStartPoint);
y=HistData(DecayStartPoint+NeglectPoints:end-1);
AvgSteadyY = mean(HistData(end-4:end));
y = y - AvgSteadyY;
y=y/max(y);

ffun=fittype(@(a,b,c,x)(a*(exp(-x/b)))+c,'coefficients',{'a','b','c'},'independent','x');
options = fitoptions(ffun);
options.Weights = [0.1*ones(1,4), 1*ones(1,30), 0.05*ones(1,length(y)-34)];
% options.Weights = [1*ones(1,23), 0.1*ones(1,23), 0.5*ones(1,length(y)-46)];
options.Lower = [0 0 -0.1];
options.Upper = [100 100 0.1];
options.StartPoint = [0.04 50 0];
options.Normalize = 'off';
[FitResults,gof,fitinfo]=fit(x,y,ffun,options);
% p1=coeffvalues(FitResults); %coefficients from the fitting
residuals = fitinfo.residuals;
I = abs( residuals) > 1.5* std( residuals );
outliers = excludedata(x,y,'indices',I);
% options.Exclude = outliers;
% options.Robust = 'on';
[FitResults3,gof3] = fit(x,y,ffun,options);
p=coeffvalues(FitResults3); %coefficients from the fitting
% Predicted_y3 = p(1)*(exp(-x/p(2)))+p(3);   



ci = confint(FitResults3,0.95); % confidence intervals for fit coefficients

tau=p(2)*1.0e-6; % lifetime (in s)
R2 = gof3.adjrsquare;

%Calibration curve coefficients
if NeglectTime <4
    y0 = -6.8610; A1 = 306.6738; t1 = 1.2974e-05; A2 = 437.4606; t2 = 3.6406e-06; % parameters for 0 point ignored
elseif NeglectTime<8
    y0 = -4.9459; A1 = 380.9448; t1 = 1.1787e-05; A2 = 6.0676e+03; t2 = 2.3411e-06; % parameters for 1 point ignored
elseif NeglectTime<12
    y0 = -6.2014; A1 = 332.9207; t1 = 1.2988e-05; A2 = 2.8406e+03; t2 = 3.3869e-06; % parameters for 2 points ignored
elseif NeglectTime<16
    y0 = -6.7328; A1 = 329.0197; t1 = 1.3425e-05; A2 = 4.7137e+03; t2 = 3.3883e-06; % parameters for 3 points ignored
elseif NeglectTime<20
    y0 = -7.1437; A1 = 317.0903; t1 = 1.4335e-05; A2 = 9.6985e+03; t2 = 3.1854e-06;  % parameters for 4 points ignored
else
    y0=-6.94916; A1=378.94838; t1=1.31291E-5;  A2=10187.27521; t2=3.31711E-6; %Sergei's values (5 points ignored)
end

PO2 = A1*exp(-tau/t1) + A2*exp(-tau/t2) + y0;  % PO2 value in mmHg

% figure;plot(x,y);hold on;plot(x,(p(1).*(exp(-x./p(3)))+p(2).*(exp(-x./p(3))))+p(4),'r');
end



