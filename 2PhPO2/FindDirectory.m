function  [filename,live_num,line_num,vol_num, PO2_num] = FindDirectory()

button = questdlg('Did you abort Matlab just now?','Find Directory','Yes, find my previous saving directory','No, make a new saving directory','Yes, find my previous saving directory');

switch (button)
    
    case 'No, make a new saving directory'
        
        myDir = uigetdir('C:\OLD_MICE');
        num1=0;
        num2=0;
        num3=0;
        num4=0;
        
    case 'Yes, find my previous saving directory'
        
        myDir = uigetdir('C:\OLD_MICE');
        live_dir =dir(fullfile(myDir,'Live_*'));
        line_dir =dir(fullfile(myDir,'Line_*'));
        vol_dir =dir(fullfile(myDir,'vol_*'));
        PO2_dir =dir(fullfile(myDir,'PO2_*')); 
        
        if(size(live_dir,1)>= 1)            
            myString = live_dir(end).name;
            num1 = str2num(myString(end)) ;
        else
            num1 = 0;
        end
        
        if (size(line_dir,1) >= 1)
            
            myString = line_dir(end).name;
            num2 = str2num(myString(end));
         else
            num2 = 0;
        end
        
        if(size(vol_dir,1) >=1 )
            
            myString = vol_dir(end).name;
            num3 = str2num(myString(end));
        else
            num3 = 0;
        end
        
        if(size(PO2_dir,1) >=1 )
            
            myString = PO2_dir(end).name;
            num4 = str2num(myString(end));
        else
            num4 = 0;
        end

end
filename = myDir;
live_num = num1;
line_num = num2;
vol_num = num3;
PO2_num = num4;

