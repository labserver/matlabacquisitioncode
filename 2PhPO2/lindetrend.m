function data = lindetrend(data)

x=[1:size(data,1)]';

Starts=data(1,:);
Ends=data(end,:);

Slopes=(Ends-Starts)/size(data,1);

y=Slopes'*x';
data=data-y';

data=data-ones(size(data,1),1)*data(1,:);

end