function [p, ci, PO2, FitResults, x, y, R2] = CalcPO2_Sava(HistData, PulseDuration, PulseHighTime, NeglectTime)
% HistData: Histogram data 
% PulseDuration: laser pulse duration for one lifetime measurement (us)
% PulseHighTime: laser high pulse duration (us)

NeglectPoints = ceil(NeglectTime / (PulseDuration/length(HistData)));
temps = linspace(0,PulseDuration, length(HistData)); % in us
DecayStartPoint = ceil(length(HistData)*PulseHighTime/PulseDuration); % the index of temps in which the decay starts
% We do the fit from the point that the decay starts
x=temps(DecayStartPoint+NeglectPoints:end-1)';
x=x-temps(DecayStartPoint);
y=HistData(DecayStartPoint+NeglectPoints:end-1);
AvgSteadyY = mean(HistData(end-4:end));
y = y - AvgSteadyY;
y=y/max(y);

c0 = [1 40 0];
decay_profile = @(c,xdata) (c(1)*exp(-xdata/c(2))+c(3));
decay_profile_error = @(c,xdata,ydata) ((ydata - (c(1)*exp(-xdata/c(2))+c(3)))  );
MyOptions = optimset('Display','off','Algorithm','levenberg-marquardt');
upper_coeffs = [1e8 230 100];
lower_coeffs = [0.0 0.0 0.0];

[p,resnorm,resid,exitflag,output,lambda,jacobian_c] = lsqnonlin(decay_profile_error, c0, [],[],MyOptions,x,y);
            %lsqnonlin(decay_profile_error, c0, lower_coeffs,upper_coeffs,MyOptions,fittedDecayTimePoints_us',(normalized_lifetime_data(j,fittedDecayTimePoints_indices))');
 
tau=p(2)*1.0e-6; % lifetime (in s)
ci=0;
FitResults=0;

Mean_y = mean(y);
SS_tot = 0; SS_res = 0;
for i = 1 : length(y)
    SS_tot = SS_tot + (y(i) - Mean_y)^2;
    SS_res = SS_res + (y(i) - (p(1)*exp(-x(i)/p(2))+p(3)))^2;
end
R2 = 1 - (SS_res / SS_tot);

%Calibration curve coefficients
y0=-6.94916; A1=378.94838; t1=1.31291E-5;  A2=10187.27521; t2=3.31711E-6; %Sergei's values
% y0=-10.0951; A1=271.1947;  t1=1.5902E-5; A2=9588.5;     t2=3.5263E-6; %my parameters

PO2 = A1*exp(-tau/t1) + A2*exp(-tau/t2) + y0;  % PO2 value in mmHg

end