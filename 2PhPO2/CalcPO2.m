function [p, ci, PO2, FitResults, x, y] = CalcPO2(HistData, PulseDuration, PulseHighTime)
% HistData: Histogram data 
% PulseDuration: laser pulse duration for one lifetime measurement (us)
% PulseHighTime: laser high pulse duration (us)

temps = linspace(0,PulseDuration, length(HistData)); % in us
DecayStartPoint = ceil(length(HistData)*PulseHighTime/PulseDuration); % the index of temps in which the decay starts
% We do the fit from the point that the decay starts
x=temps(DecayStartPoint:end-1)';
x=x-temps(DecayStartPoint);
y=HistData(DecayStartPoint:end-1);
y=y*10000000;

ffun=fittype(@(a,b,c,x)(a*(exp(-x/b)))+c,'coefficients',{'a','b','c'},'independent','x');
startpoint=[1 25 0]; % the initial guess of of the coefficients to start the iretation
[FitResults gof]=fit(x,y,ffun,'Startpoint',startpoint,'Lower',[0 0 -100],'Upper',[1e7 500 500]);
p=coeffvalues(FitResults); %coefficients from the fitting
ci = confint(FitResults,0.95); % confidence intervals for fit coefficients

tau=p(2)*1.0e-6; % lifetime (in s)

%Calibration curve coefficients
y0=-6.94916; A1=378.94838; t1=1.31291E-5;  A2=10187.27521; t2=3.31711E-6; %Sergei's values
% y0=-10.0951; A1=271.1947;  t1=1.5902E-5; A2=9588.5;     t2=3.5263E-6; %my parameters

PO2 = A1*exp(-tau/t1) + A2*exp(-tau/t2) + y0;  % PO2 value in mmHg

end