function [CMRO2, x, y, xx, FittedCurve] = FitCMRO2(VesselDiameter, Distances, PO2, DistanceLimit)

alpha = 1.3e-3;   % umolO2/ml/mmHg
D = 2000;       % um2/sec
Rart = VesselDiameter / 2.0; % um
xx = 0:0.1:DistanceLimit;
xx = xx + Rart;

Indices = find(Distances <=DistanceLimit);
x = Distances(Indices) + Rart;
y = PO2(Indices);

ffun=fittype(@(a,b,c,x)(c + (a/(4 * alpha * D))*((x.^2 - Rart^2) - 2*(b.^2)*log(x/Rart))) , 'coefficients',{'a','b','c'},'independent','x');

% startpoint=[100 0.02 100]; % the initial guess of of the coefficients to start the iretation
% [FitResults gof]=fit(x',y',ffun,'Startpoint',startpoint,'Lower',[0 0 0],'Upper',[1000 300 1000]);

options = fitoptions(ffun);
AdjacentPart = length(find(x<=30))
options.Weights = [1*ones(1,AdjacentPart), 0.05*ones(1,length(y)-2*AdjacentPart), 1*ones(1,AdjacentPart)];
% options.Weights = [1*ones(1,AdjacentPart), 1*ones(1,length(y)-2*AdjacentPart), 1*ones(1,AdjacentPart)];
options.Lower = [0 0 0];
options.Upper = [1000 300 1000];
options.StartPoint = [100 0.02 100];
options.Normalize = 'off';
[FitResults,gof,fitinfo]=fit(x',y',ffun,options);

p=coeffvalues(FitResults); %coefficients from the fitting
ci = confint(FitResults,0.95); % confidence intervals for fit coefficients

CMRO2.CMRO2 = p(1)*60;   % umolO2/ml/min
CMRO2.Rt = p(2); % um
CMRO2.PO20 = p(3);
CMRO2.CMRO2ci = ci(:,1)*60;
CMRO2.Rtci = ci(:,2);
CMRO2.PO20ci = ci(:,3);

FittedCurve = (p(3) + (p(1)/(4 * alpha * D))*((xx.^2 - Rart^2) - 2*(p(2).^2)*log(xx/Rart)));

end
