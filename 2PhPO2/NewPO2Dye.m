function PO2 = NewPO2Dye(tau, NeglectTime)
% tau in s

%Calibration curve coefficients
if NeglectTime <4
    y0 = -7.9696; A1 = 286.7229; t1 = 1.4103e-05; A2 = 9.0867e+06; t2 = 8.9046e-07; % parameters for 0 point ignored
elseif NeglectTime<8
    y0 = -8.2877; A1 = 317.8207; t1 = 1.3988e-05; A2 = 7.7918e+06; t2 = 1.0261e-06; % parameters for 1 point ignored
elseif NeglectTime<12
    y0 = -6.2143; A1 = 376.7995; t1 = 1.2702e-05; A2 = 9.2900e+04; t2 = 1.6623e-06; % parameters for 2 points ignored
elseif NeglectTime<16
    y0 = -7.2201; A1 = 393.7629; t1 = 1.3041e-05; A2 = 8.4393e+05; t2 = 1.4602e-06; % parameters for 3 points ignored
elseif NeglectTime<20
    y0 = -8.6347; A1 = 370.6810; t1 = 1.3899e-05; A2 = 6.5758e+04; t2 = 2.1262e-06;  % parameters for 4 points ignored
else
    y0 = -10.07748; A1 = 4163.10268; t1 = 3.95074e-6; A2 = 264.02718; t2 = 1.61237e-5; %Sergei's values (5 points ignored)
end

PO2 = A1*exp(-tau/t1) + A2*exp(-tau/t2) + y0;  % PO2 value in mmHg

end